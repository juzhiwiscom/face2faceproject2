package com.wiscomwis.facetoface.customload;

import android.content.Context;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.model.QaAnswer;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;

/**
 * QA问题展示
 * Created by WangYong on 2017/9/19.
 */
public class QaQuestionAdapter extends CommonRecyclerViewAdapter<QaAnswer> {
    public QaQuestionAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(QaAnswer answer, int position, RecyclerViewHolder holder) {
        if(answer!=null){
           holder.setText(R.id.qa_question_item_tv_answer,answer.getContent());
        }
    }

//    tv_answer= (TextView) itemView.findViewById(R.id.qa_question_item_tv_answer);
}
