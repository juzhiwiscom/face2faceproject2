package com.wiscomwis.facetoface.base;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.danikula.videocache.HttpProxyCacheServer;
import com.mob.MobApplication;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.service.InitializeService;

/**
 * Created by zhangdroid on 2017/5/11.
 */
public class BaseApplication extends MobApplication {
    private static Context sApplicationContext;

    private HttpProxyCacheServer proxy;

    public static HttpProxyCacheServer getProxy(Context context) {
        BaseApplication app = (BaseApplication) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(this).maxCacheSize(1024 * 1024 * 1024)       // 1 Gb for cache
                .build();
    }

    public static Context getGlobalContext() {
        return sApplicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplicationContext = getApplicationContext();
        // 初始化环信
        HyphenateHelper.getInstance().init(this);
        InitializeService.start(this);
        DbModle.getInstance().init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

}
