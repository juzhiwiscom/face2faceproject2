package com.wiscomwis.facetoface.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 跳转支付页面需要传递的参数
 * Created by WangYong on 2017/6/10.
 */
public class PayInfoParcelable implements Parcelable {
    public String serviceId;
    public String serviceName;
    public int type;// 1钻石 2 vip 3 钥匙
    public String price;
    public int paySource;
    public PayInfoParcelable(String serviceId, String serviceName,int type,String price,int paySource) {
        this.serviceId = serviceId;
        this.serviceName=serviceName;
        this.type=type;
        this.paySource=paySource;
        this.price=price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.serviceId);
        dest.writeString(this.serviceName);
        dest.writeString(this.price);
        dest.writeInt(this.type);
        dest.writeInt(this.paySource);
    }
    protected PayInfoParcelable(Parcel in) {
        this.serviceId = in.readString();
        this.serviceName = in.readString();
        this.price = in.readString();
        this.type = in.readInt();
        this.paySource = in.readInt();
    }

    public static final Creator<PayInfoParcelable> CREATOR = new Creator<PayInfoParcelable>() {

        @Override
        public PayInfoParcelable createFromParcel(Parcel source) {
            return new PayInfoParcelable(source);
        }

        @Override
        public PayInfoParcelable[] newArray(int size) {
            return new PayInfoParcelable[size];
        }
    };
}
