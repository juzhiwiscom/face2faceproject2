package com.wiscomwis.facetoface.event;

/**
 * Created by WangYong on 2018/4/26.
 */

public class MakeViewIsSee {
    private int isSee;

    public MakeViewIsSee(int isSee) {
        this.isSee = isSee;
    }

    public MakeViewIsSee() {
    }

    public int getIsSee() {
        return isSee;
    }

    public void setIsSee(int isSee) {
        this.isSee = isSee;
    }
}
