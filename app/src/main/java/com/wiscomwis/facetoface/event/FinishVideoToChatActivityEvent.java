package com.wiscomwis.facetoface.event;

/**
 * Created by WangYong on 2018/4/16.
 */

public class FinishVideoToChatActivityEvent {
    private long guid;// 用户id
    private String account;// 用户account
    private String nickname;// 用户昵称
    private String imageUrl;// 用户头像url
    private int flag;

    public FinishVideoToChatActivityEvent() {
    }

    public FinishVideoToChatActivityEvent(long guid, String account, String nickname, String imageUrl, int flag) {
        this.guid = guid;
        this.account = account;
        this.nickname = nickname;
        this.imageUrl = imageUrl;
        this.flag = flag;
    }
    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "FinishVideoToChatActivityEvent{" +
                "guid=" + guid +
                ", account='" + account + '\'' +
                ", nickname='" + nickname + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", flag=" + flag +
                '}';
    }
}
