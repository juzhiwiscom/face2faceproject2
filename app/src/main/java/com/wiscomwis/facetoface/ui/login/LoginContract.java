package com.wiscomwis.facetoface.ui.login;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/6/1.
 */
public interface LoginContract {
    interface IView extends BaseView {
        void setAccount(String account);

        void setPassword(String password);

        String getAccount();

        String getPassword();

        void showLoading();

        void dismissLoading();

        void showNetworkError();

        void showFindPwdDialog(String account, String password);
    }

    interface IPresenter extends BasePresenter {
        void login();

        void findPassword();
    }

}
