package com.wiscomwis.facetoface.ui.personalcenter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShareRuleActivity extends AppCompatActivity {

    @BindView(R.id.iv_back_share_rule)
    ImageView mIvBackShareRule;
    @BindView(R.id.ll_bar_share_rule)
    RelativeLayout mLlBarShareRule;
    @BindView(R.id.tv_award_1_count)
    TextView mTvAward1Count;
    @BindView(R.id.tv_award_2_count)
    TextView mTvAward2Count;
    @BindView(R.id.tv_award_3_count)
    TextView mTvAward3Count;
    @BindView(R.id.tv_award_desc_2)
    TextView mTvAwardDesc2;
    @BindView(R.id.tv_award_desc_3)
    TextView mTvAwardDesc3;
    @BindView(R.id.tv_cycle_desc_1)
    TextView mTvCycleDesc1;
    @BindView(R.id.tv_cycle_desc_2)
    TextView mTvCycleDesc2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_rule);
        ButterKnife.bind(this);

        String string1 = getResources().getString(R.string.award_1, "6元");
        mTvAward1Count.setText(Html.fromHtml(string1));

        String string2 = getResources().getString(R.string.award_2, "10%");
        mTvAward2Count.setText(Html.fromHtml(string2));

        mTvAwardDesc2.setText(getResources().getString(R.string.award_2_desc, "10%", "10%"));

        String string3 = getResources().getString(R.string.award_3, "10%");
        mTvAward3Count.setText(Html.fromHtml(string3));
        mTvAwardDesc3.setText(getResources().getString(R.string.award_3_desc, "10%", "10%"));

        mTvCycleDesc1.setText(Html.fromHtml(getResources().getString(R.string.cycle_desc_1)));
        mTvCycleDesc2.setText(Html.fromHtml(getResources().getString(R.string.cycle_desc_2)));
    }

    @OnClick(R.id.iv_back_share_rule)
    public void onViewClicked() {
        finish();
    }

    @OnClick({R.id.tv_award_desc_2, R.id.tv_award_desc_3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_award_desc_2:
                break;
            case R.id.tv_award_desc_3:
                break;
        }
    }
}
