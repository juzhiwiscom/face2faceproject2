package com.wiscomwis.facetoface.ui.pay.fragment;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseFragment;
import com.wiscomwis.facetoface.common.MarqueeView;
import com.wiscomwis.facetoface.common.TimeUtils;
import com.wiscomwis.facetoface.customload.TextViewSplash;
import com.wiscomwis.facetoface.parcelable.PayInfoParcelable;
import com.wiscomwis.facetoface.ui.pay.activity.PayActivity;
import com.wiscomwis.facetoface.ui.pay.adapter.DredgeVipAdapter;
import com.wiscomwis.facetoface.ui.pay.contract.DredgeVipContract;
import com.wiscomwis.facetoface.ui.pay.presenter.DredgeVipPresenter;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.SnackBarUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/8/29.
 */

public class DredgeVipFragment extends BaseFragment implements View.OnClickListener, DredgeVipContract.IView {
    @BindView(R.id.fragment_dregevip_ll_root)
    LinearLayout mLlRoot;
    @BindView(R.id.fragment_dregevip_recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.fragment_dregevip_tv_gundong)
    MarqueeView tv_gundong;
    @BindView(R.id.fragment_dredge_vip_tv_service_name3)
    TextView tv_service_name3;
    @BindView(R.id.fragment_dredge_vip_tv_service_name2)
    TextView tv_service_name2;
    @BindView(R.id.fragment_dredge_vip_tv_service_name1)
    TextView tv_service_name1;
    @BindView(R.id.fragment_dredge_vip_btn_price3)
    Button btn_price3;
    @BindView(R.id.fragment_dredge_vip_btn_price2)
    Button btn_price2;
    @BindView(R.id.fragment_dredge_vip_btn_price1)
    Button btn_price1;
    @BindView(R.id.fragment_dredge_vip_tv_des3)
    TextView tv_des3;
    @BindView(R.id.fragment_dredge_vip_tv_des2)
    TextView tv_des2;
    @BindView(R.id.fragment_dredge_vip_tv_des1)
    TextView tv_des1;
    @BindView(R.id.fragment_dredge_vip_tv_service_id3)
    TextView tv_service_id3;
    @BindView(R.id.fragment_dredge_vip_tv_service_id2)
    TextView tv_service_id2;
    @BindView(R.id.fragment_dredge_vip_tv_service_id1)
    TextView tv_service_id1;
    @BindView(R.id.fragment_dredge_vip_forver)
    RelativeLayout rl_forver;
    @BindView(R.id.fragment_dredge_vip2)
    RelativeLayout rl_vip2;
    @BindView(R.id.fragment_dredge_vip1)
    RelativeLayout rl_vip1;
    @BindView(R.id.fragment_dredge_vip_tv_time)
    TextViewSplash tv_time;
    @BindView(R.id.fragment_dredge_vip_tv_cutdown)
    TextView tv_cutdown;
    @BindView(R.id.fragment_dredge_vip_iv_changxiao)
    ImageView iv_changxiao;
    private DredgeVipPresenter mDredgeVipPresenter;
    private LinearLayoutManager linearLayoutManager;
    List<View> views1 = new ArrayList<>();
    private int clo = 0;
    private int location=6;//显示支付来源
    private int time;
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            time--;
            if (time>0) {
                tv_cutdown.setText(TimeUtils.fromSecondToTime(time, 1));
                handler.sendEmptyMessageDelayed(1,1000);
            }else{
                tv_cutdown.setVisibility(View.GONE);
                iv_changxiao.setVisibility(View.VISIBLE);
            }
        }
    };
    public DredgeVipFragment(int location) {
        this.location = location;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_dregevip_layout;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mDredgeVipPresenter = new DredgeVipPresenter(this);
        mDredgeVipPresenter.getPayWay("1");
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    @Override
    protected void setListeners() {
        rl_forver.setOnClickListener(this);
        rl_vip1.setOnClickListener(this);
        rl_vip2.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_dredge_vip_forver:
                String serviceId3 = tv_service_id3.getText().toString();
                String serviceName3 = tv_service_name3.getText().toString();
                String price3 = btn_price3.getText().toString();
                if (!TextUtils.isEmpty(serviceId3) && !TextUtils.isEmpty(serviceName3) && !TextUtils.isEmpty(price3)) {
                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayInfoParcelable(serviceId3, serviceName3, 2, price3,location));
                }
                break;
            case R.id.fragment_dredge_vip2:
                String serviceId2 = tv_service_id2.getText().toString();
                String serviceName2 = tv_service_name2.getText().toString();
                String price2 = btn_price2.getText().toString();
                if (!TextUtils.isEmpty(serviceId2) && !TextUtils.isEmpty(serviceName2) && !TextUtils.isEmpty(price2)) {
                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayInfoParcelable(serviceId2, serviceName2, 2, price2,location));
                }
                break;
            case R.id.fragment_dredge_vip1:
                String serviceId1 = tv_service_id1.getText().toString();
                String serviceName1 = tv_service_name1.getText().toString();
                String price1 = btn_price1.getText().toString();
                if (!TextUtils.isEmpty(serviceId1) && !TextUtils.isEmpty(serviceName1) && !TextUtils.isEmpty(price1)) {
                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayInfoParcelable(serviceId1, serviceName1, 2, price1,location));
                }
                break;
        }
    }


    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void setInfoString1(String serviceName1, String price1, String serviceId1, String des1) {
        tv_service_name1.setText(serviceName1 + mContext.getString(R.string.day)+"VIP");
        btn_price1.setText("￥" + price1);
        tv_service_id1.setText(serviceId1);
        tv_des1.setText("￥" + price1+" | "+des1);
    }

    @Override
    public void setInfoString2(String serviceName2, String price2, String serviceId2, String des2) {
        tv_service_name2.setText(serviceName2 + mContext.getString(R.string.day)+"VIP");
        btn_price2.setText("￥" + price2);
        tv_service_id2.setText(serviceId2);
        tv_des2.setText("￥" + price2+" | "+des2);
    }

    @Override
    public void setInfoString3(String serviceName3, String price3, String serviceId3, String des3) {
        tv_service_name3.setText(serviceName3+"VIP");
        btn_price3.setText("￥" + price3);
        tv_service_id3.setText(serviceId3);
        tv_des3.setText(des3);
    }

    @Override
    public void setAdapter(DredgeVipAdapter adapter) {
//        mRecyclerView.setAdapter(adapter);
    }


    @Override
    public void getGunDongText(List<String> title, List<String> content) {
        setViewTwoLines(title, content);
        tv_gundong.setViews(views1);
    }

    @Override
    public void isPast24Hours() {
        tv_time.setVisibility(View.GONE);
        tv_cutdown.setVisibility(View.GONE);
        iv_changxiao.setVisibility(View.VISIBLE);
    }

    @Override
    public void getCutdownTime(int s) {
        time=s;
        handler.sendEmptyMessage(1);
        tv_cutdown.setVisibility(View.VISIBLE);
        iv_changxiao.setVisibility(View.GONE);
    }

    private void setViewTwoLines(List<String> title, List<String> content) {
        views1.clear();//记得加这句话，不然可能会产生重影现象
        for (int i = 0; i < content.size(); i = i + 2) {
            final int position = i;
            //设置滚动的单个布局
            LinearLayout moreView = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.item_view, null);
            //初始化布局的控件
            TextView tv1 = (TextView) moreView.findViewById(R.id.tv1);
            TextView tv2 = (TextView) moreView.findViewById(R.id.tv2);
            TextView tv3 = (TextView) moreView.findViewById(R.id.tv3);
            TextView tv_title1 = (TextView) moreView.findViewById(R.id.title_tv1);
            TextView tv_title2 = (TextView) moreView.findViewById(R.id.title_tv2);
            TextView tv_title3 = (TextView) moreView.findViewById(R.id.title_tv3);

            //进行对控件赋值
            tv1.setText(content.get(i).toString());
            tv_title1.setText("[" + title.get(i) + "]");
            if (content.size() > i + 2) {//奇数条
                tv2.setText(content.get(i + 1).toString());
                tv3.setText(content.get(i + 2).toString());
                tv_title2.setText("[" + title.get(i + 1) + "]");
                tv_title3.setText("[" + title.get(i + 2) + "]");
            } else {//偶数条
                //因为淘宝那儿是两条数据，但是当数据是奇数时就不需要赋值第二个，所以加了一个判断，还应该把第二个布局给隐藏掉
                //moreView.findViewById(R.id.rl2).setVisibility(View.GONE);
                //修改了最后一个没有 将第一个拼接到最后显示
                tv2.setText(content.get(0).toString());
                tv3.setText(content.get(1).toString());
                tv_title2.setText("[" + title.get(0) + "]");
                tv_title3.setText("[" + title.get(1) + "]");
            }

            //添加到循环滚动数组里面去
            views1.add(moreView);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (tv_gundong != null) {
                tv_gundong.startFlipping();
            }
        } else {
            if (tv_gundong != null) {
                tv_gundong.stopFlipping();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        tv_gundong.startFlipping();
    }

    @Override
    public void onStop() {
        super.onStop();
        tv_gundong.stopFlipping();
    }
}
