package com.wiscomwis.facetoface.ui.personalcenter;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseAppCompatActivity;
import com.wiscomwis.facetoface.onekeyshare.OnekeyShare;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;

public class ShareActivity extends BaseAppCompatActivity {


    @BindView(R.id.iv_back_share)
    ImageView mIvBackShare;
    @BindView(R.id.tv_rule_share)
    TextView mTvRuleShare;
    @BindView(R.id.tv_award_1)
    TextView mTvAward1;
    @BindView(R.id.tv_award_2)
    TextView mTvAward2;
    @BindView(R.id.tv_award_3)
    TextView mTvAward3;
    @BindView(R.id.btn_share)
    Button mBtnShare;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_share;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @OnClick({R.id.iv_back_share, R.id.tv_rule_share, R.id.btn_share})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back_share:
                finish();
                break;
            case R.id.tv_rule_share:
                LaunchHelper.getInstance().launch(mContext, ShareRuleActivity.class);

                break;
            case R.id.btn_share:
//                showShare();
                showShare();
                break;
        }
    }

    private void showShare() {
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();

        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间等使用
        oks.setTitle("泡泡交友");
        // titleUrl是标题的网络链接，QQ和QQ空间等使用
        oks.setTitleUrl("www.baidu.com");
        // text是分享文本，所有平台都需要这个字段
        oks.setText("泡泡交友分享赚钱");
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
//        oks.setImagePath("/sdcard/test.png");//确保SDcard下面存在此张图片
        oks.setImageUrl("https://apollo-video1.oss-cn-beijing.aliyuncs.com/photo_test47.png");
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setUrl("www.github.com");
        // 启动分享GUI
        oks.show(this);
    }
}
