package com.wiscomwis.facetoface.ui.register.contract;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/5/26.
 */
public interface ChangePasswordContract {

    interface IView extends BaseView {
        /**
         * 新密码
         */
        String getNewPassword();
        /**
         * 确认密码
         */
        String confirmPassrod();
        /**
         * 关闭activity
         */
        void finishActivity();
        /**
         * 获取Id
         */
        void getId(String id);
        /**
         * 获取原密码
         */
        void getOriginalPassword(String password);
    }

    interface IPresenter extends BasePresenter {
        /**
         * 修改密码
         */
        void changePassword();
        /**
         * 确认修改
         */
        void sureConfirm();
    }

}
