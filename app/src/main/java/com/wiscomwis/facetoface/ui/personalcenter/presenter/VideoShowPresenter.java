package com.wiscomwis.facetoface.ui.personalcenter.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.TUserVideoShow;
import com.wiscomwis.facetoface.data.model.UpLoadMyInfo;
import com.wiscomwis.facetoface.data.model.UploadInfoParams;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.event.UserInfoChangedEvent;
import com.wiscomwis.facetoface.ui.personalcenter.contract.VideoShowContract;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

/**
 * Created by Administrator on 2017/6/16.
 */

public class VideoShowPresenter implements VideoShowContract.IPresenter {
    private VideoShowContract.IView mVideoShowIview;
    private Context mContext;
    private String mVideoFile;
    private String mAvatarFile;
    private String mVideoNumber;
    private String mShowDuration;
    private String mVideoShowFile;
    private String mBitmapFile;
    private String mVoiceDuration;
    private String mVoiceFile;
    private String mOwnWord;


    public VideoShowPresenter(VideoShowContract.IView mVideoShowIview) {
        this.mVideoShowIview = mVideoShowIview;
        this.mContext = mVideoShowIview.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void sendParams(String videoNumber, String videoFile, String avatarFile, String showDuration, String videoShowFile, String bitmapFile, String voiceDuration, String voiceFile, String ownWord) {
        this.mVideoNumber = videoNumber;
        this.mVideoFile = videoFile;
        this.mAvatarFile = avatarFile;
        this.mShowDuration = showDuration;
        this.mVideoShowFile = videoShowFile;
        this.mBitmapFile = bitmapFile;
        this.mVoiceDuration = voiceDuration;
        this.mVoiceFile = voiceFile;
        this.mOwnWord = ownWord;
        submitAuth(mVideoNumber, mVideoFile, mAvatarFile);
    }


    @Override
    public void submitAuth(final String videoNumber, String videoFilePath, String avatarFilePath) {
        if (!TextUtils.isEmpty(videoFilePath) && !TextUtils.isEmpty(avatarFilePath)) {
            File videoFile = new File(videoFilePath);
            File avatarFile = new File(avatarFilePath);
            if (!TextUtils.isEmpty(videoNumber) && videoFile != null && avatarFile != null) {
                mVideoShowIview.showLoading();
                ApiManager.submitCheck(videoNumber, videoFile, avatarFile, new IGetDataListener<String>() {
                    @Override
                    public void onResult(String s, boolean isEmpty) {
                        mVideoShowIview.dismissLoading();
                        submitShow(mOwnWord, mVideoShowFile, mBitmapFile, mShowDuration, mVoiceFile, mVoiceDuration);
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.upload_fail));
                        mVideoShowIview.dismissLoading();
                    }
                });
            } else {
                mVideoShowIview.showTip(mContext.getString(R.string.upload_video_first));
            }
        }
    }


    @Override
    public void submitShow(String mOwnWord, String videoFilePath, String bitmapFilePath, String videoDuration, String voiceFilePath, String voiceDuration) {
        File videoShowFile = new File(videoFilePath);
        File bitmapFile = new File(bitmapFilePath);
        File voiceFile = new File(voiceFilePath);
        String content = Util.encodeHeadInfo(mOwnWord);
        if (!TextUtils.isEmpty(videoDuration) && videoShowFile != null && bitmapFile != null) {
            mVideoShowIview.showLoading();
            ApiManager.uploadAllShow(content, videoShowFile, bitmapFile, videoDuration, voiceFile, voiceDuration, new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel s, boolean isEmpty) {
                    mVideoShowIview.dismissLoading();
                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.upload_success));
                    mVideoShowIview.finishActivity();
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.upload_fail));
                    mVideoShowIview.dismissLoading();
                    mVideoShowIview.isShowFail();
                }
            });
        } else {
            mVideoShowIview.isShowFail();
            mVideoShowIview.dismissLoading();
            mVideoShowIview.showTip(mContext.getString(R.string.video_show_first));
        }
    }
}

//    @Override
//    public void submitVoice(String voiceDuration, String voiceFilePath) {
//        if (!TextUtils.isEmpty(voiceFilePath) && !TextUtils.isEmpty(voiceDuration)) {
//            File voiceFile = new File(voiceFilePath);
//            if (voiceFile.exists()) {
//                String mVoiceDuration = Integer.parseInt(voiceDuration) / 1000 + "";
//                ApiManager.uploadVoiceShow("0", mVoiceDuration, voiceFile, new IGetDataListener<BaseModel>() {
//                    @Override
//                    public void onResult(BaseModel s, boolean isEmpty) {
//                        submitOwnWord(mOwnWord);
//                    }
//
//                    @Override
//                    public void onError(String msg, boolean isNetworkError) {
//                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.upload_fail));
//                        mVideoShowIview.dismissLoading();
//                    }
//                });
//            } else {
//                mVideoShowIview.dismissLoading();
//                mVideoShowIview.showTip(mContext.getString(R.string.voice_show_first));
//            }
//        } else {
//            mVideoShowIview.dismissLoading();
//            mVideoShowIview.showTip(mContext.getString(R.string.voice_show_first));
//        }
//    }

//    @Override
//    public void submitOwnWord(String ownWord) {
//        UploadInfoParams uploadInfoParams = new UploadInfoParams();
//        uploadInfoParams.setOwnWords(ownWord);
//        ApiManager.upLoadMyInfo(uploadInfoParams, new IGetDataListener<UpLoadMyInfo>() {
//            @Override
//            public void onResult(UpLoadMyInfo upLoadMyInfo, boolean isEmpty) {
//                mVideoShowIview.dismissLoading();
//                ToastUtil.showShortToast(mContext, mContext.getString(R.string.upload_success));
//                mVideoShowIview.finishActivity();
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//                ToastUtil.showShortToast(mContext, mContext.getString(R.string.upload_fail));
//                mVideoShowIview.dismissLoading();
//            }
//        });
//    }
