package com.wiscomwis.facetoface.ui.message.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.wiscomwis.facetoface.C;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.common.VideoHelper;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.BaseModel;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.VideoRecord;
import com.wiscomwis.facetoface.data.preference.SwitchPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
import com.wiscomwis.facetoface.parcelable.VideoInviteParcelable;
import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.DateTimeUtil;
import com.wiscomwis.library.util.LaunchHelper;

import java.util.List;

/**
 * 视频记录适配器
 * Created by zhangdroid on 2017/7/6.
 */
public class VideoHistoryAdapter extends CommonRecyclerViewAdapter<VideoRecord> {
    private FragmentManager fragmentManager;
    private Context context;
    public VideoHistoryAdapter(Context context, int layoutResId,FragmentManager fragmentManager) {
        super(context, layoutResId);
        this.fragmentManager=fragmentManager;
        this.context=context;
    }

    public VideoHistoryAdapter(Context context, int layoutResId, List<VideoRecord> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(final VideoRecord videoRecord, int position, RecyclerViewHolder holder) {
        if (null != videoRecord) {
            final UserBase userBase = videoRecord.getUserBase();
            if (null != userBase) {
                switch (Integer.parseInt(userBase.getStatus())) {
                    case C.homepage.STATE_FREE:// 空闲
                        holder.setText(R.id.item_video_history_state, mContext.getString(R.string.message_state_free));
                        holder.setBackgroudResource(R.id.item_video_history_state, R.drawable.shape_state_green);
                        break;
                    case C.homepage.STATE_BUSY:// 忙线中
                        holder.setText(R.id.item_video_history_state, mContext.getString(R.string.message_state_busy));
                        holder.setBackgroudResource(R.id.item_video_history_state, R.drawable.shape_state_red);
                        break;
                    case C.homepage.STATE_NO_DISTRUB:// 勿扰
                        holder.setText(R.id.item_video_history_state, mContext.getString(R.string.message_state_nodistrub));
                        holder.setBackgroudResource(R.id.item_video_history_state, R.drawable.shape_state_gray);
                        break;
                }
                CardView cd_avatar_video = (CardView) holder.getView(R.id.item_video_cardview_avatar);
                ImageView iv_avatar_video = (ImageView) holder.getView(R.id.item_video_history_avatar);
                ImageView iv_video_history = (ImageView) holder.getView(R.id.item_video_history_iv);
                cd_avatar_video.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                                new UserDetailParcelable(String.valueOf(userBase.getGuid())));
                    }
                });
                iv_video_history.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(SwitchPreference.getAlipay()==1){
                            if(UserPreference.isAnchor()){
                                Toast.makeText(mContext,mContext.getString(R.string.send_invite_success), Toast.LENGTH_SHORT).show();
                                ApiManager.videoInvite(String.valueOf(userBase.getGuid()), new IGetDataListener<BaseModel>() {
                                    @Override
                                    public void onResult(BaseModel baseModel, boolean isEmpty) {
                                    }
                                    @Override
                                    public void onError(String msg, boolean isNetworkError) {
                                    }
                                });
                            }else{
                                VideoHelper.startVideoInvite(new VideoInviteParcelable(false,userBase.getGuid(), userBase.getAccount()
                                        , userBase.getNickName(), userBase.getIconUrlMininum(),0,0),context,"1");
                            }
                        }else{
                                VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                                        , userBase.getNickName(), userBase.getIconUrlMininum(),0,0), context,"1");
                        }
                    }
                });
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage())
                        .url(userBase.getIconUrlMininum()).imageView(iv_avatar_video).build());
                holder.setText(R.id.item_video_history_nickname, userBase.getNickName());
            }
            // 通话时间
            holder.setText(R.id.item_video_history_tv_time, DateTimeUtil.convertTimeMillis2String(videoRecord.getRecordTime()));
        }
    }

}
