package com.wiscomwis.facetoface.ui.chat;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.Util;
import com.wiscomwis.facetoface.data.model.PicInfo;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.adapter.provider.ItemViewProvider;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.DateTimeUtil;

import java.util.List;

/**
 * 聊天文字消息（发送方）
 * Created by zhangdroid on 2017/6/29.
 */
public class TextMessageRightProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;
    private List<EMMessage> messageList;

    public TextMessageRightProvider(Context context, List<EMMessage> messageList) {
        this.mContext = context;
        this.messageList = messageList;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_text_right;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.TXT && item.direct() == EMMessage.Direct.SEND);
    }

    @Override
    public void convert(EMMessage emMessage, int position, RecyclerViewHolder holder) {
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                .error(Util.getDefaultImageCircle()).url(UserPreference.getSmallImage()).imageView((ImageView) holder.getView(R.id.item_chat_text_avatar_right)).build());
        if (null != emMessage) {
            if (position > 0) {
                long msgTime = messageList.get(position - 1).getMsgTime();
                if ((emMessage.getMsgTime()-msgTime) /(1000 * 60) > 5) {
                    holder.getView(R.id.item_chat_text_time_right).setVisibility(View.VISIBLE);
                    holder.setText(R.id.item_chat_text_time_right, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
                } else {
                    holder.getView(R.id.item_chat_text_time_right).setVisibility(View.GONE);
                }
            }else{
                holder.getView(R.id.item_chat_text_time_right).setVisibility(View.VISIBLE);
                holder.setText(R.id.item_chat_text_time_right, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
            }

            EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
            if (null != emTextMessageBody) {
                TextView tv_right = (TextView) holder.getView(R.id.item_chat_tv_right);
                LinearLayout ll_send_gifts = (LinearLayout) holder.getView(R.id.item_chat_ll_right_send_gifts);
                ImageView iv_gifts = (ImageView) holder.getView(R.id.item_chat_iv_right_gifts);
                TextView tv_male = (TextView) holder.getView(R.id.item_chat_tv_right_meinv_or_shuaige);
                TextView tv_gift_num = (TextView) holder.getView(R.id.item_chat_tv_right_gifts_num);
                if (emTextMessageBody.getMessage().contains("{") && emTextMessageBody.getMessage().contains("}")) {
                    tv_right.setVisibility(View.GONE);
                    ll_send_gifts.setVisibility(View.VISIBLE);
                    PicInfo picInfo = new Gson().fromJson(emTextMessageBody.getMessage(), PicInfo.class);
                    if (picInfo != null) {
                        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(picInfo.getPicIcon())
                                .placeHolder(0).error(0).imageView(iv_gifts).build());
                        if (UserPreference.isMale()) {
                            tv_male.setText(mContext.getString(R.string.hi_girl));
                        } else {
                            tv_male.setText(mContext.getString(R.string.hi_boy));
                        }
                        tv_gift_num.setText(picInfo.getPicNum() + "个" + picInfo.getPicName());
                    }
                } else {
                    tv_right.setVisibility(View.VISIBLE);
                    ll_send_gifts.setVisibility(View.GONE);
                    tv_right.setText(emTextMessageBody.getMessage());
                }
            }
        }
    }

}
