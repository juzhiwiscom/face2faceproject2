package com.wiscomwis.facetoface.ui.pay.presenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.WithdrawRecord;
import com.wiscomwis.facetoface.data.model.WithdrawRecordList;
import com.wiscomwis.facetoface.ui.pay.adapter.WithdrawDetailAdapter;
import com.wiscomwis.facetoface.ui.pay.contract.IncomeContract;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.util.Utils;

import java.util.List;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public class WithrawalDetailPresenter implements IncomeContract.IPresenter {
    private IncomeContract.IView mView;
    private Context mContext;
    private int pageNum = 1;
    private String pageSize = "20";
    private WithdrawDetailAdapter mWithdrawDetailAdapter;

    public WithrawalDetailPresenter(IncomeContract.IView view) {
        this.mView = view;
        mContext = view.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void loadData(final boolean isRefresh) {
        ApiManager.getWithdrawRecord(pageNum, pageSize, new IGetDataListener<WithdrawRecordList>() {

            @Override
            public void onResult(WithdrawRecordList recordList, boolean isEmpty) {
                pageNum++;
                if (isEmpty) {
                    mWithdrawDetailAdapter.setEmptyView(R.layout.common_empty);
                } else {
                    if (null != recordList) {
                        List<WithdrawRecord> list = recordList.getListRecord();
                        if (!Utils.isListEmpty(list)) {
                            if (isRefresh) {
                                mWithdrawDetailAdapter.setNewData(list);
                            } else {
                                if (list.size() > 0) {
                                    mWithdrawDetailAdapter.addData(list);
                                }
                            }
                            if (list.size() < Integer.parseInt(pageSize)) {
                                mWithdrawDetailAdapter.loadMoreEnd(isRefresh);
                            } else {
                                mWithdrawDetailAdapter.loadMoreComplete();
                            }
                        }
                    }
                }
                mView.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mView.hideRefresh(1);
                if (isNetworkError) {
                    mView.toggleShowError(false, TextUtils.isEmpty(msg) ? mContext.getString(R.string.tip_error) : msg);
                } else {
                    if (isRefresh) {
                        mWithdrawDetailAdapter.setEnableLoadMore(true);
                    } else {
                        mWithdrawDetailAdapter.loadMoreFail();
                    }
                }
            }
        });
    }

    @Override
    public void refresh() {
        pageNum = 1;
        loadData(true);
    }

    @Override
    public void start(RecyclerView recyclerView) {
        mWithdrawDetailAdapter = new WithdrawDetailAdapter(R.layout.item_withdraw_record);
        mView.setAdapter(mWithdrawDetailAdapter);
        mWithdrawDetailAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                loadData(false);
            }
        }, recyclerView);
    }
}
