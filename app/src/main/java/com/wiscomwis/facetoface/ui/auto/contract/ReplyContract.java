package com.wiscomwis.facetoface.ui.auto.contract;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.wiscomwis.facetoface.mvp.BasePresenter;
import com.wiscomwis.facetoface.mvp.BaseView;
import com.wiscomwis.library.adapter.wrapper.OnLoadMoreListener;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public interface ReplyContract {

    interface IView extends BaseView {

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        void toggleShowError(boolean toggle, String msg);

        void toggleShowEmpty(boolean toggle, String msg);

        /**
         * 设置adapter
         *
         * @param adapter        adapter
         */
        void setAdapter(RecyclerView.Adapter adapter);

        /**
         * 删除item
         */
        void deleteItem(int position);

        /**
         * 隐藏添加自动回复
         */
        void hindEmpty(boolean b);
    }

    interface IPresenter extends BasePresenter {

        /**
         * 第一次加载数据
         */
        void loadReplyList();
        /**
         * 下拉刷新
         */
        void refresh();

    }

}
