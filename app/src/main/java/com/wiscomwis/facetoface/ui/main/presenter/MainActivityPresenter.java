package com.wiscomwis.facetoface.ui.main.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.wiscomwis.facetoface.common.CustomDialogAboutOther;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.CallVideo;
import com.wiscomwis.facetoface.data.model.MyInfo;
import com.wiscomwis.facetoface.data.model.SearchUser;
import com.wiscomwis.facetoface.data.model.SearchUserList;
import com.wiscomwis.facetoface.data.model.UserBase;
import com.wiscomwis.facetoface.data.model.UserDetail;
import com.wiscomwis.facetoface.data.preference.PayPreference;
import com.wiscomwis.facetoface.data.preference.UserPreference;
import com.wiscomwis.facetoface.ui.main.contract.MainActivityContract;

import java.util.List;

/**
 * Created by WangYong on 2017/12/12.
 */

public class MainActivityPresenter implements MainActivityContract.IPresenter {
    private Context context;
    private MainActivityContract.IView mMainActiviyIview;

    public MainActivityPresenter(MainActivityContract.IView mMainActiviyIview) {
        this.mMainActiviyIview = mMainActiviyIview;
        context = mMainActiviyIview.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void loadData() {
        ApiManager.sayMoneyHello(new IGetDataListener<SearchUserList>() {
            @Override
            public void onResult(SearchUserList searchUserList, boolean isEmpty) {
                if (searchUserList != null) {
                    List<SearchUser> searchUserList1 = searchUserList.getSearchUserList();
                    mMainActiviyIview.sayHellAll(searchUserList1);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });

        ApiManager.getDiamondPayWay("", new IGetDataListener<String>() {
            @Override
            public void onResult(String s, boolean isEmpty) {
                PayPreference.saveDiamondInfo(s);
            }

            @Override
            public void onError(String msg, boolean isNtworkError) {
            }
        });
        ApiManager.getVipPayWay("", new IGetDataListener<String>() {
            @Override
            public void onResult(String s, boolean isEmpty) {
                PayPreference.saveVipInfo(s);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
        ApiManager.getGiftDic(new IGetDataListener<String>() {
            @Override
            public void onResult(String str, boolean isEmpty) {
                PayPreference.saveSendGifts(str);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        String vipDays = userDetail.getVipDays();
                        if (!TextUtils.isEmpty(vipDays) && vipDays.length() > 0) {
                            int i = Integer.parseInt(vipDays);
                            if (i > 0) {
                                mMainActiviyIview.isVipUser();
                            }
                            UserPreference.setIsVip(i);
                        }
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            UserPreference.saveUserInfo(userBase);
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    public void makeFreeVideo() {
        ApiManager.makeFreeCall(new IGetDataListener<CallVideo>() {
            @Override
            public void onResult(CallVideo callVideo, boolean isEmpty) {
                if (callVideo != null) {
                    CustomDialogAboutOther.makeFreeCallShow(context, callVideo);
                    UserPreference.registerModification();
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
}
