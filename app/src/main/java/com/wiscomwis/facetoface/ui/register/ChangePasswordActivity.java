package com.wiscomwis.facetoface.ui.register;

import android.content.Context;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.base.BaseAppCompatActivity;
import com.wiscomwis.facetoface.ui.register.contract.ChangePasswordContract;
import com.wiscomwis.facetoface.ui.register.presenter.ChangePasswordPresenter;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.SnackBarUtil;

import butterknife.BindView;

/**
 * Created by WangYong on 2018/2/5.
 */

public class ChangePasswordActivity extends BaseAppCompatActivity implements ChangePasswordContract.IView, View.OnClickListener {
    @BindView(R.id.change_password_activity_ll_root)
    LinearLayout ll_root;
    @BindView(R.id.change_password_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.change_password_activity_tv_id)
    TextView tv_id;
    @BindView(R.id.change_password_activity_et_new_password)
    EditText et_new_password;
    @BindView(R.id.change_password_activity_et_sure_password)
    EditText et_sure_password;
    @BindView(R.id.change_password_activity_btn_sure)
    Button btn_sure;
    @BindView(R.id.change_password_activity_tv_original_password)
    TextView tv_original_password;

    private ChangePasswordPresenter mChangePasswordPresenter;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_change_password;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return ll_root;
    }

    @Override
    protected void initViews() {
        mChangePasswordPresenter=new ChangePasswordPresenter(this);
        mChangePasswordPresenter.changePassword();
    }

    @Override
    protected void setListeners() {
        btn_sure.setOnClickListener(this);
        rl_back.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(ll_root,msg);
    }

    @Override
    public String getNewPassword() {
        return et_new_password.getText().toString().trim();
    }

    @Override
    public String confirmPassrod() {
        return et_sure_password.getText().toString();
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void getId(String id) {
        tv_id.setText(id);
    }

    @Override
    public void getOriginalPassword(String password) {
        tv_original_password.setText(password);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.change_password_activity_btn_sure:
                mChangePasswordPresenter.sureConfirm();
                break;
            case R.id.change_password_activity_rl_back:
                finish();
                break;
        }
    }
}
