package com.wiscomwis.facetoface.ui.message.presenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.wiscomwis.facetoface.R;
import com.wiscomwis.facetoface.common.HyphenateHelper;
import com.wiscomwis.facetoface.data.api.ApiManager;
import com.wiscomwis.facetoface.data.api.IGetDataListener;
import com.wiscomwis.facetoface.data.model.HuanXinUser;
import com.wiscomwis.facetoface.data.model.SearchCriteria;
import com.wiscomwis.facetoface.data.model.SearchUser;
import com.wiscomwis.facetoface.data.model.SearchUserList;
import com.wiscomwis.facetoface.data.preference.SearchPreference;
import com.wiscomwis.facetoface.db.DbModle;
import com.wiscomwis.facetoface.event.UnreadMsgChangedEvent;
import com.wiscomwis.facetoface.parcelable.ChatParcelable;
import com.wiscomwis.facetoface.parcelable.UserDetailParcelable;
import com.wiscomwis.facetoface.ui.chat.ChatActivity;
import com.wiscomwis.facetoface.ui.detail.UserDetailActivity;
import com.wiscomwis.facetoface.ui.homepage.adapter.HomeListAdapter;
import com.wiscomwis.facetoface.ui.message.adapter.ChatHistoryAdapter;
import com.wiscomwis.facetoface.ui.message.adapter.HelloPeopleAdapter;
import com.wiscomwis.facetoface.ui.message.contract.HelloPeopleContract;
import com.wiscomwis.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.util.LaunchHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xuzhaole on 2017/12/2.
 */

public class HelloPeoplePresenter implements HelloPeopleContract.IPresenter {
    private HelloPeopleContract.IView mHelloView;
    private Context mContext;
    private HelloPeopleAdapter mHelloPeopleAdapter;
    List<HuanXinUser> allAcount = null;
    List<HuanXinUser> helloList = null;

    public HelloPeoplePresenter(HelloPeopleContract.IView view) {
        this.mHelloView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void start(RecyclerView recyclerView) {
        helloList = new ArrayList<>();
        mHelloPeopleAdapter = new HelloPeopleAdapter(R.layout.item_chat_history);
        mHelloPeopleAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (helloList != null && helloList.size() > 0) {
                    HuanXinUser huanXinUser = helloList.get(position);
                    if (huanXinUser != null) {
                        EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                        DbModle.getInstance().getUserAccountDao().setState(huanXinUser, true);//把标记设置为已经读取
                        String hxId = huanXinUser.getHxId();
                        if (!TextUtils.isEmpty(hxId)) {
                            long guid = Long.parseLong(hxId);
                            LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(guid,
                                    huanXinUser.getAccount(), huanXinUser.getHxName(), huanXinUser.getHxIcon(), 0));
                        }
                    }
                }
            }
        });
        mHelloView.setAdapter(mHelloPeopleAdapter);

        mHelloPeopleAdapter.bindToRecyclerView(recyclerView);
//        mHelloPeopleAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
//            @Override
//            public void onLoadMoreRequested() {
//                mHelloPeopleAdapter.loadMoreComplete();
//            }
//        }, recyclerView);
    }

    @Override
    public void loadData() {
        helloList = new ArrayList<>();
        // 查询未读消息数
        allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
        if (allAcount != null && allAcount.size() > 0) {
            for (HuanXinUser user : allAcount) {
                if (user.getExtendType() == 8) {
                    helloList.add(user);
                }
            }
            if (helloList != null && helloList.size() > 0) {
                mHelloPeopleAdapter.setNewData(helloList);
                mHelloPeopleAdapter.loadMoreComplete();
            } else {
                mHelloPeopleAdapter.setNewData(null);
                mHelloPeopleAdapter.setEmptyView(R.layout.common_empty);
            }
        } else {
            mHelloPeopleAdapter.setEmptyView(R.layout.common_empty);
        }
        mHelloView.hideRefresh(1);
    }

    @Override
    public void refresh() {
        loadData();
    }
}
