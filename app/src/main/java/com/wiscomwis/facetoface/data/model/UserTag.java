package com.wiscomwis.facetoface.data.model;

/**
 * Created by WangYong on 2018/4/24.
 */

public class UserTag {
    private String guid;
    private String userId;
    private String tagId;
    private String tagName;
    private String tagType;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagType() {
        return tagType;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    @Override
    public String toString() {
        return "UserTag{" +
                "guid='" + guid + '\'' +
                ", userId='" + userId + '\'' +
                ", tagId='" + tagId + '\'' +
                ", tagName='" + tagName + '\'' +
                ", tagType='" + tagType + '\'' +
                '}';
    }
}
