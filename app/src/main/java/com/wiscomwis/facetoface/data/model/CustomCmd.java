package com.wiscomwis.facetoface.data.model;

/**
 * Created by WangYong on 2018/4/18.
 */

public class CustomCmd {
    private String content;//发送的内容
    private String sendUserAccount;//发送方account
    private String reservedField;//预留字段
    private String sendUserImageUrl;//发送方的头像
    private String sendUserId;//发送方的ID
    private int type;//1为正在输入
    public CustomCmd() {
    }

    public CustomCmd(String content, String sendUserAccount, String reservedField, String sendUserImageUrl, String sendUserId, int type) {
        this.content = content;
        this.sendUserAccount = sendUserAccount;
        this.reservedField = reservedField;
        this.sendUserImageUrl = sendUserImageUrl;
        this.sendUserId = sendUserId;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSendUserAccount() {
        return sendUserAccount;
    }

    public void setSendUserAccount(String sendUserAccount) {
        this.sendUserAccount = sendUserAccount;
    }

    public String getReservedField() {
        return reservedField;
    }

    public void setReservedField(String reservedField) {
        this.reservedField = reservedField;
    }

    public String getSendUserImageUrl() {
        return sendUserImageUrl;
    }

    public void setSendUserImageUrl(String sendUserImageUrl) {
        this.sendUserImageUrl = sendUserImageUrl;
    }

    public String getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(String sendUserId) {
        this.sendUserId = sendUserId;
    }

    @Override
    public String toString() {
        return "CustomCmd{" +
                "content='" + content + '\'' +
                ", sendUserAccount='" + sendUserAccount + '\'' +
                ", reservedField='" + reservedField + '\'' +
                ", sendUserImageUrl='" + sendUserImageUrl + '\'' +
                ", sendUserId='" + sendUserId + '\'' +
                '}';
    }
}
