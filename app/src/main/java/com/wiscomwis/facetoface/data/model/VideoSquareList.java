package com.wiscomwis.facetoface.data.model;

import java.util.List;

/**
 * 声网频道channel key
 * Created by zhangdroid on 2017/6/23.
 */
public class VideoSquareList extends BaseModel {
    private List<VideoSquare> videoSquareList;

    public List<VideoSquare> getVideoSquareList() {
        return videoSquareList;
    }

    public void setVideoSquareList(List<VideoSquare> videoSquareList) {
        this.videoSquareList = videoSquareList;
    }

    @Override
    public String toString() {
        return "VideoSquareList{" +
                "videoSquareList=" + videoSquareList +
                '}';
    }
}
