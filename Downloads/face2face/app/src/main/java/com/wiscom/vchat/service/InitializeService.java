package com.wiscom.vchat.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.wiscom.vchat.C;
import com.wiscom.library.util.CrashHelper;
import com.wiscom.okhttp.OkHttpHelper;
import com.wiscom.okhttp.log.LoggerInterceptor;

import okhttp3.OkHttpClient;

/**
 * Application initialize intent service(just in non-ui thread)
 */
public class InitializeService extends IntentService {

    public InitializeService() {
        super("InitializeService");
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, InitializeService.class);
        intent.setAction(C.service.ACTION_APP_CREATE);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (!TextUtils.isEmpty(action) && C.service.ACTION_APP_CREATE.equals(action)) {
                // 初始化异常收集
                CrashHelper.getInstance().init(this);
                // 初始化Stetho
                Stetho.initializeWithDefaults(this);
                // 初始化OkHttpClient
                initOkhttpClient();
            }
        }
    }

    /**
     * 初始化OkHttp
     */
    private void initOkhttpClient() {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                // Okhttp log
                .addInterceptor(new LoggerInterceptor(null))
                // Stetho网络调试
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
        OkHttpHelper.init(okHttpClient);
    }

}
