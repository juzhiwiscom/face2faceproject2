package com.wiscom.vchat.ui.dialog;

import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.wiscom.vchat.R;
import com.wiscom.vchat.common.Util;
import com.wiscom.library.dialog.BaseDialogFragment;
import com.wiscom.library.widget.WheelView;

import java.util.ArrayList;
import java.util.List;

/**
 * 年龄选择框
 * Created by zhangdroid on 2017/5/31.
 */
public class AgeSelectDialog extends BaseDialogFragment {
    private static boolean isAge=true;
    private List<String> mAgeList = new ArrayList<>();
    private String mSelectedText;
    private OnAgeSelectListener mOnAgeSelectListener;

    private static AgeSelectDialog newInstance(String selectedText, OnAgeSelectListener listener) {
        AgeSelectDialog ageSelectDialog = new AgeSelectDialog();
        ageSelectDialog.mSelectedText = selectedText;
        ageSelectDialog.mOnAgeSelectListener = listener;
        return ageSelectDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_select_age;
    }

    @Override
    protected void setDialogContentView(View view) {
        final WheelView wheelView = (WheelView) view.findViewById(R.id.dialog_age_wheelview);
        if(isAge){
            for (int i = 18; i < 66; i++) {
                mAgeList.add(String.valueOf(i));
            }
        }else{
            for (int i = 0; i < Util.getCountryList().size(); i++) {
                mAgeList.add(Util.getCountryList().get(i));
            }
        }

        wheelView.setData(mAgeList);
        if (!TextUtils.isEmpty(mSelectedText)) {
            wheelView.setDefaultIndex(mAgeList.indexOf(mSelectedText));
        }
        Button btnSure = (Button) view.findViewById(R.id.dialog_age_sure);
        btnSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mOnAgeSelectListener) {
                    mOnAgeSelectListener.onSelected(wheelView.getSelectedText());
                }
                dismiss();
            }
        });
    }

    public static void show(FragmentManager fragmentManager, String selectedText, OnAgeSelectListener onSexSelectedListener) {
        try {
            Integer.parseInt(selectedText);
            newInstance(selectedText, onSexSelectedListener).show(fragmentManager, "age_select");
            isAge=true;
        } catch (NumberFormatException e) {
            newInstance(selectedText, onSexSelectedListener).show(fragmentManager, "country_select");
            isAge=false;
        }
    }

    public interface OnAgeSelectListener {
        void onSelected(String age);
    }

}
