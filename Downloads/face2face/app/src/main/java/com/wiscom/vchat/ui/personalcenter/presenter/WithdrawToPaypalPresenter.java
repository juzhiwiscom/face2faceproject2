package com.wiscom.vchat.ui.personalcenter.presenter;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.CommonWebActivity;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.parcelable.WebParcelable;
import com.wiscom.vchat.ui.dialog.CountrySelectDialog;
import com.wiscom.vchat.ui.personalcenter.contract.WithdrawToPaypalContract;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Administrator on 2017/7/7.
 */

public class WithdrawToPaypalPresenter implements WithdrawToPaypalContract.IPresenter {



    private PopupWindow mPopWindow;
    private WithdrawToPaypalContract.IView mWithdrawToPaypalView;
    private Context mContext;

    public WithdrawToPaypalPresenter(WithdrawToPaypalContract.IView mWithdrawToPaypalView) {
        this.mWithdrawToPaypalView = mWithdrawToPaypalView;
        this.mContext = mWithdrawToPaypalView.obtainContext();
    }


    @Override
    public void start() {

    }

    @Override
    public void finish() {
        mWithdrawToPaypalView = null;

    }

    public void showWithdrawExplain(String title, String url) {
        LaunchHelper.getInstance().launch(mContext, CommonWebActivity.class,
                new WebParcelable(title, url, false));
    }

    public void selectCountry() {
        CountrySelectDialog.show(mWithdrawToPaypalView.obtainFragmentManager(), TextUtils.isEmpty(mWithdrawToPaypalView.getCountry()) ? null : mWithdrawToPaypalView.getCountry(),
                new CountrySelectDialog.OnCountrySelectListener() {
                    @Override
                    public void onSelected(String country) {
                        mWithdrawToPaypalView.setCountry(country);
                    }
                });
    }

    @Override
    public void withdrawSure(String getMoney,String getEmail, String getName, String getSur, String getNation) {
        showPopupWindow(getMoney,getEmail,getName,getSur,getNation);
    }

    private void showPopupWindow(final String getMoney, final String getEmail, final String getName, final String getSur, final String getNation) {
        //设置contentView
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.popu_withdraw, null);
        mPopWindow = new PopupWindow(contentView,
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT, true);
        mPopWindow.setContentView(contentView);
        //设置各个控件的点击响应
        TextView tvEmail = (TextView) contentView.findViewById(R.id.tv_email);
        TextView tvName = (TextView) contentView.findViewById(R.id.tv_name);
        TextView tvSur = (TextView) contentView.findViewById(R.id.tv_sur);
        TextView tvNation = (TextView) contentView.findViewById(R.id.tv_nation);
        Button btnCancel = (Button) contentView.findViewById(R.id.btn_cancel);
        Button btnSure = (Button) contentView.findViewById(R.id.btn_sure);
        tvEmail.setText(getEmail);
        tvName.setText(getName);
        tvNation.setText(getNation);
        tvSur.setText(getSur);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPopWindow.dismiss();
            }
        });
        btnSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPopWindow.dismiss();
                /**将数据传给后台**/
                sureWithdraw(getMoney,getEmail,getName,getSur,getNation);
                /**finish掉当前页面**/
                mWithdrawToPaypalView.finishActivity();
            }
        });
        //显示PopupWindow
        View rootview = LayoutInflater.from(mContext).inflate(R.layout.activity_withdraw_to_paypal, null);
        mPopWindow.showAtLocation(rootview, Gravity.BOTTOM, 0, 0);
    }

    private void sureWithdraw(final String getMoney, String getEmail, String getName, String getSur, String getNation) {
        ApiManager.withdraw(getMoney, getEmail, getName, getSur, getNation,new IGetDataListener<BaseModel>(){

            @Override
            public void onResult(BaseModel callBack, boolean isEmpty) {
              if(callBack!=null){
                  /**将更改后的价格传给个人中心**/
                  //发送消息更新价格
                  Message message = new Message();
                  message.what=1111;
                  message.obj=getMoney;
                  EventBus.getDefault().post(message);
                  ToastUtil.showShortToast(mContext,"提现成功，预计3-15日到账");
              }else{
                  ToastUtil.showShortToast(mContext,"每天最高提现$600,您已超出最大提现金额");
              }
            }


            @Override
            public void onError(String msg, boolean isNetworkError) {
                if(isNetworkError){
                    ToastUtil.showShortToast(mContext,"网络错误");
                }else{
                    ToastUtil.showShortToast(mContext,"每天最高提现$600,您已超出最大提现金额...");            }

            }
        });

    }
}
