package com.wiscom.vchat.ui.message;

import android.content.Context;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragment;
import com.wiscom.vchat.ui.message.contract.MessageContract;
import com.wiscom.vchat.ui.message.presenter.MessagePresenter;

import butterknife.BindView;

/**
 * 消息
 * Created by zhangdroid on 2017/5/23.
 */
public class MessageFragment extends BaseFragment implements MessageContract.IView {
    @BindView(R.id.message_tab)
    TabLayout mTabLayout;
    @BindView(R.id.message_viewpager)
    ViewPager mViewPager;

    private MessagePresenter mMessagePresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_message;
    }

//    @Override
//    protected String getDefaultTitle() {
//        return getString(R.string.title_message);
//    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mMessagePresenter = new MessagePresenter(this);
        mMessagePresenter.addTabs();
    }

    @Override
    protected void setListeners() {
    }

    @Override
    protected void loadData() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMessagePresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }

    @Override
    public FragmentManager getManager() {
        return getChildFragmentManager();
    }

    @Override
    public void setAdapter(PagerAdapter pagerAdapter) {
        mViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

}
