package com.wiscom.vchat.data.model;

/**
 *  * 用户上传的视频对象
 * Created by tianzhentao on 2018/4/3.
 */

public class UserVideoShow {
    private String videoUrl;//视频地址
    private int videoSeconds;//视频时长
    private String thumbnailUrl;       //缩略图
    private String nickName;
    private String age;
    private int praiseCount;//点赞数
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPraiseCount() {
        return praiseCount;
    }

    public void setPraiseCount(int praiseCount) {
        this.praiseCount = praiseCount;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public int getVideoSeconds() {
        return videoSeconds;
    }

    public void setVideoSeconds(int videoSeconds) {
        this.videoSeconds = videoSeconds;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
