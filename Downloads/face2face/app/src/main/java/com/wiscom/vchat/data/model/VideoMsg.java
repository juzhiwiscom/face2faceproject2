package com.wiscom.vchat.data.model;

/**
 * 视频时消息对象
 * Created by zhangdroid on 2017/6/22.
 */
public class VideoMsg {
    private String nickname;// 用户昵称
    private String message;// 消息内容

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
