package com.wiscom.vchat.ui.videoshow;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tianzhentao on 2018/5/4.
 */

class VideoShowAdapter extends FragmentPagerAdapter{
    private List<Fragment> mFragmentList;
    private List<String> mTabList;

    public VideoShowAdapter(FragmentManager fm) {
        super(fm);
        mFragmentList = new ArrayList<>();
        mTabList = new ArrayList<>();
    }

    public void setData(List<Fragment> fragments, List<String> tabs) {
        if (!isListEmpty(fragments)) {
            this.mFragmentList = fragments;
        }
        if (!isListEmpty(tabs)) {
            this.mTabList = tabs;
        }
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return isListEmpty(mFragmentList) ? null : mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return isListEmpty(mFragmentList) ? 0 : mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return isListEmpty(mTabList) ? null : mTabList.get(position);
    }
    public static boolean isListEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }
}
