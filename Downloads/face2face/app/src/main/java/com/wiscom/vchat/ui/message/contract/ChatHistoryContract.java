package com.wiscom.vchat.ui.message.contract;

import android.support.v7.widget.RecyclerView;

import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;
import com.wiscom.vchat.ui.message.adapter.ChatHistoryAdapter;

/**
 * Created by zhangdroid on 2017/7/6.
 */
public interface ChatHistoryContract {

    interface IView extends BaseView {

        void toggleShowEmpty(boolean toggle, String msg);

        void toggleShowError(boolean toggle, String msg);

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        /**
         * 显示加载更多
         */
        void showLoadMore();

        /**
         * 隐藏加载更多
         */
        void hideLoadMore();

        /**
         * 显示没有更多（滑动到最底部）
         */
        void showNoMore();

        ChatHistoryAdapter getChatHistoryAdapter();
        /**
         * 设置Adapter
         *
         */
        void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId);
    }

    interface IPresenter extends BasePresenter {

        void loadConversationList();

        void refresh();

        void loadMore();

    }

}
