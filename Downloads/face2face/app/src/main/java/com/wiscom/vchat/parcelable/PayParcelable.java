package com.wiscom.vchat.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 跳转支付页面需要传递的参数
 * Created by zhangdroid on 2017/6/10.
 */
public class PayParcelable implements Parcelable {
    // 服务类型，1钻石 2 vip 3 钥匙
    public String fromTag;

    public PayParcelable(String fromTag) {
        this.fromTag = fromTag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fromTag);
    }

    protected PayParcelable(Parcel in) {
        this.fromTag = in.readString();
    }

    public static final Parcelable.Creator<PayParcelable> CREATOR = new Parcelable.Creator<PayParcelable>() {
        @Override
        public PayParcelable createFromParcel(Parcel source) {
            return new PayParcelable(source);
        }

        @Override
        public PayParcelable[] newArray(int size) {
            return new PayParcelable[size];
        }
    };
}
