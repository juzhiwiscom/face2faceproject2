package com.wiscom.vchat.ui.video;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.PowerManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wiscom.library.adapter.wrapper.HeaderAndFooterWrapper;
import com.wiscom.library.dialog.AlertDialog;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.LogUtil;
import com.wiscom.library.util.SharedPreferenceUtil;
import com.wiscom.library.util.SnackBarUtil;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.base.BaseFragmentActivity;
import com.wiscom.vchat.common.AgoraHelper;
import com.wiscom.vchat.common.ParamsUtils;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.AgoraParams;
import com.wiscom.vchat.data.model.ApplyAddFriendMode;
import com.wiscom.vchat.data.model.ApplyPayModel;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.model.MyInfo;
import com.wiscom.vchat.data.model.RequestVideoCall;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserBean;
import com.wiscom.vchat.data.model.UserDetail;
import com.wiscom.vchat.data.model.VedioScreenInfo;
import com.wiscom.vchat.data.model.VideoMsg;
import com.wiscom.vchat.data.preference.BeanPreference;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.AgoraEvent;
import com.wiscom.vchat.event.AgoraMediaEvent;
import com.wiscom.vchat.event.ExitVideoEvent;
import com.wiscom.vchat.event.FinishVideoEvent;
import com.wiscom.vchat.event.InterruptedVIPEvent;
import com.wiscom.vchat.event.InviteSuccessEvent;
import com.wiscom.vchat.parcelable.PayParcelable;
import com.wiscom.vchat.parcelable.VideoParcelable;
import com.wiscom.vchat.ui.pay.PayActivity;
import com.wiscom.vchat.ui.video.contract.VideoContract;
import com.wiscom.vchat.ui.video.presenter.VideoPresenter;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 视频页面
 * Created by zhangdroid on 2017/5/27.
 */
public class VideoActivity extends BaseFragmentActivity implements View.OnClickListener, VideoContract.IView {
    private static final int RENDER_NONE = 0;
    private static final int RENDER_TEXTURE_VIEW = 1;
    private static final int RENDER_SURFACE_VIEW = 2;
    @BindView(R.id.video_root)
    RelativeLayout mRlRoot;
    @BindView(R.id.video_bootom_exit)
    RelativeLayout mBootomExit;

    @BindView(R.id.video_local)
    FrameLayout mFlLocal;// 本地视频
    @BindView(R.id.video_remote)
    FrameLayout mFlRemote;// 远端视频
    // 控制区
    @BindView(R.id.video_follow)
    TextView mIvFollow;// 加好友
    @BindView(R.id.video_set_report)
    ImageView mIvReport;// 举报
    @BindView(R.id.video_switch)
    ImageView mIvSwitch;// 切换摄像头
    @BindView(R.id.video_exit)
    ImageView mIvExit;// 关闭视频
    @BindView(R.id.video_present_gift)
    ImageView mIvPresentGift;// 关闭视频
    @BindView(R.id.video_time)
    AnnulusView mTvTime;// 视频计时
    @BindView(R.id.video_balance)
    TextView mTvBalance;// 余额
    // 消息列表
    @BindView(R.id.video_chat_list)
    RecyclerView mRecyclerView;
    // 消息输入框和发送
    @BindView(R.id.video_input)
    EditText mEtInput;
    @BindView(R.id.video_send)
    Button mBtnSend;
    @BindView(R.id.video_left_user_name)
    TextView videoLeftUserName;
    @BindView(R.id.video_left_user_national_flag)
    ImageView videoLeftUserNationalFlag;
    @BindView(R.id.video_left_user_country)
    TextView videoLeftUserCountry;
    @BindView(R.id.video_left_user_sex)
    TextView videoLeftUserSex;
    @BindView(R.id.video_left_user_info)
    LinearLayout videoLeftUserInfo;
    //    @BindView(R.id.video_follow)
//    TextView videoFollow;
//    对方头像(中间位置）
    @BindView(R.id.video_anim_icon)
    ImageView videoAnimIcon;
    //    寻找新朋友
    @BindView(R.id.video_tv_search_user)
    TextView videoTvSearchUser;
    @BindView(R.id.video_user_national_flag)
    ImageView videoUserNationalFlag;
    @BindView(R.id.video_user_country)
    TextView videoUserCountry;
    @BindView(R.id.video_user_sex_icon)
    ImageView videoUserSexIcon;
    @BindView(R.id.video_user_sex)
    TextView videoUserSex;
    @BindView(R.id.video_user_name)
    TextView videoUserName;
    @BindView(R.id.video_user_info)
    LinearLayout videoUserInfo;
    //    对方用户信息
    @BindView(R.id.video_search_user)
    LinearLayout videoSearchUser;
    //    @BindView(R.id.video_chat_list)
//    RecyclerView videoChatList;
//    @BindView(R.id.video_input)
//    EditText videoInput;
    @BindView(R.id.video_bottom)
    RelativeLayout videoBottom;
    //    右下角3秒的进度条
    @BindView(R.id.video_progrsss)
    AnnulusView videoProgrsss;
    @BindView(R.id.video_user_progrsss)
    AnnulusView videoUserProgrsss;
    @BindView(R.id.video_user_info_left)
    LinearLayout videoUserInfoLeft;
    @BindView(R.id.video_left_user_icon)
    ImageView videoLeftUserIcon;
    @BindView(R.id.video_search)
    LinearLayout videoSearch;
    @BindView(R.id.video_user_icon)
    ImageView videoUserIcon;
    @BindView(R.id.video_left_user_progrsss)
    AnnulusView videoLeftUserProgrsss;
    LockableScrollView mScrollView;
    @BindView(R.id.video_slide_naim)
    FrameLayout videoSlideNaim;
    @BindView(R.id.video_palayview)
    FullScreenVideoView playView;
    @BindView(R.id.video_fl_playview)
    FrameLayout videoFlPlayview;
    @BindView(R.id.video_ll_time)
    LinearLayout videoLlTime;

    private VideoParcelable mVideoParcelable;
    private VideoPresenter mVideoPresenter;
    private VideoMsgAdapter mVideoMsgAdapter;

    private static final int MSG_TYPE_TIMER = 0;
    private String mChannelId;
    private Handler mHandler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
//            requestVideo(isVideoSwitch);
        }
    };
    private boolean removeLeftInfoSuccess = false;
    private String matchedCountry;
    private String matchedNickName;
    private String matchedIconUrl;
    private int matchedGender;
    private boolean finishVideo = false;
    private int i = 0;
    private boolean fromRandomVideo = true;
    private String fromView;
    private int height;
    private int moveY;
    private int changeNum = 0;
    private int n = 0;
    private int mVideoTime = 60;
    private Handler mHandlerTime = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_TYPE_TIMER:// 计时
                    mVideoTime--;
//                    mTvTime.setText(mVideoPresenter.convertSecondsToString(mVideoTime));
//                    mTvTime.setText(mVideoTime+"");
                    // 每过一分钟计费
                    if (mVideoTime % 60 == 0) {
//                        if (UserPreference.isAnchor()) {
//                            // 播主累计收入
//                            AnchorPreference.setIncome(Float.parseFloat(AnchorPreference.getIncome()) + Float.parseFloat(mVideoParcelable.hostPrice));
//                            mTvBalance.setText(AnchorPreference.getIncome());
//                        } else {
//                            // 普通用户扣除余额
//                            BeanPreference.setBeanCount(BeanPreference.getBeanCount() - Integer.parseInt(mVideoParcelable.hostPrice));
//                            mTvBalance.setText(String.valueOf(BeanPreference.getBeanCount()));
//                        }
                    }
                    if (mVideoTime > 0) {
                            mHandlerTime.sendEmptyMessageDelayed(MSG_TYPE_TIMER, 1000);
                    }
                    break;
            }
        }
    };
    private String mAccount;
    private boolean canScrollDown = true;
    private long duration = -1;
    private boolean gildeStart = true;
    private long guid;
    private boolean firstRequest = true;
    private VedioScreenInfo vedioScreenInfo;
    private int matchTimes = 1;
    private int wait_time = 5000;
    private boolean canRequest = true;
    private boolean inviteSuccess = false;
    private int system = 0;
    private Dialog dialog;
    private boolean cancelExitVideo=false;
    private boolean getSystem=false;
    private View inflate;
    private boolean playViewPrepare;
    private int payIntercept=0;
    private PowerManager.WakeLock wakeLock;

    @Override
    protected int getLayoutResId() {
         inflate = View.inflate(VideoActivity.this, R.layout.activity_video, null);
        return R.layout.activity_video;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mVideoParcelable = (VideoParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
//        屏幕持续点亮
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "TAG");

//屏幕会持续点亮
        wakeLock.acquire();

//        设置变化的背景颜色和随机图标
        setBackgroundColor();
        setBackgroundIcon();
        WindowManager wm = this.getWindowManager();
        height = wm.getDefaultDisplay().getHeight();//屏幕高度
        // 初始化消息列表
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mVideoMsgAdapter = new VideoMsgAdapter(this, R.layout.item_video_msg_list);
        // 添加HeaderView
        HeaderAndFooterWrapper headerAndFooterWrapper = new HeaderAndFooterWrapper(mVideoMsgAdapter);
        TextView headerView = new TextView(this);
        headerView.setTextSize(14);
        headerView.setTextColor(getResources().getColor(R.color.purple));
        String warn = getString(R.string.video_warn);
        SpannableString spannableString = new SpannableString(warn);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.main_color)), 0, warn.indexOf("：") + 1,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        headerView.setText(spannableString);
        headerAndFooterWrapper.addHeaderView(headerView);
        mRecyclerView.setAdapter(headerAndFooterWrapper);

        // 根据用户类型，设置收入或余额
//        if (UserPreference.isAnchor()) {
//            mTvBalance.setText(AnchorPreference.getIncome());
//        } else {
//            mTvBalance.setText(String.valueOf(BeanPreference.getBeanCount()));
//        }


        if (mVideoParcelable != null) {
            if (mVideoParcelable.fromView.equals(C.Video.GO_CALL)) {
//                来自主动呼叫的界面
                fromView = C.Video.GO_CALL;
                mChannelId = mVideoParcelable.channelId;
                mVideoPresenter = new VideoPresenter(this, mChannelId, mVideoParcelable.nickname, String.valueOf(mVideoParcelable.guid), fromView);
            } else if (mVideoParcelable.fromView.equals(C.Video.ACCEPT_INVITE)) {
//                来自受邀请的界面
                fromView = C.Video.ACCEPT_INVITE;
                mChannelId = mVideoParcelable.channelId;
                mVideoPresenter = new VideoPresenter(this, mVideoParcelable.channelId, mVideoParcelable.nickname, String.valueOf(mVideoParcelable.guid), fromView);
            }
//            来自视频邀请或者被邀请界面
            fromRandomVideo = false;
            matchedCountry = mVideoParcelable.country;
            matchedCountry = Util.getLacalCountry(matchedCountry);
            matchedNickName = mVideoParcelable.nickname;
            matchedIconUrl = mVideoParcelable.imageurl;
            mAccount = mVideoParcelable.account;
            if (mVideoParcelable.gender != null) {
                matchedGender = Integer.parseInt(mVideoParcelable.gender);
            }
            AgoraHelper.getInstance().joinChannel(mChannelId);
        } else {
//            来自随机视频界面
            fromView = C.Video.RANDOM_VIDEO;
            fromRandomVideo = true;
            mVideoPresenter = new VideoPresenter(this);
//        没有频道id，发起视频接口的请求
            requestVideo(false);
        }
//        设置本地视频预览
        AgoraHelper.getInstance().setupLocalVideo(true, mContext, getLocalVideoView());

    }


    private void requestVideo(final boolean isVideoSwitch) {
        if (!finishVideo && canRequest && !getSystem) {
            if (dialog != null) {
                dialog.dismiss();
            }
            vedioScreenInfo = new VedioScreenInfo();
            vedioScreenInfo.setGender(BeanPreference.getScreenGender());
            ApiManager.requestVideoCall(isVideoSwitch, vedioScreenInfo, matchTimes, new IGetDataListener<RequestVideoCall>() {
                @Override
                public void onResult(RequestVideoCall videoCall, boolean isEmpty) {
                    String isSucceed = videoCall.getIsSucceed();
                    if (isSucceed.equals("1")) {
                        matchTimes = videoCall.getMatchTimes();
                        wait_time = videoCall.getWait_time() * 1000;
                        String beanStatus = videoCall.getBeanStatus();
                        String matchResultType = videoCall.getMatchResultType();
                        UserBase remoteUser = videoCall.getRemoteMatchUser();
                        if (matchResultType.equals("1")) {//真实匹配成功
                            LogUtil.v("AgoraHelper=VideoActivity==", "调用随机视频,接口请求成功remoteUser==" + remoteUser);
                            if (remoteUser != null) {
                                canRequest = false;
                                getRealUser(remoteUser);
                            }
                        } else if (matchResultType.equals("2")) {//虚拟视频返回
                            LogUtil.v("AgoraHelper==VideoActivity==", "调用随机视频,虚拟视频返回");

//                                    请求系统用户接口
//                            if (requestSystemUser)
                            getSystem = true;
                            if(remoteUser!=null){
                                getSystemUser(videoCall.getMatchVideo(), remoteUser);
                            }else{
                                //                            根据后台返回的时间，下次调用接口
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        requestVideo(isVideoSwitch);
                                    }
                                }, wait_time);
                            }
                        } else if (matchResultType.equals("-1")) {//结果为空 等待
                            LogUtil.v("AgoraHelper==VideoActivity==", "调用随机视频,结果为空 等待");
//                            根据后台返回的时间，下次调用接口
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    requestVideo(isVideoSwitch);
                                }
                            }, wait_time);
                        }

                    }

                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
//                网络错误，5秒后重新请求
//                    mHandler.postDelayed(runnable, 5000);
                }
            });
        }

    }

    private void getRealUser(UserBase remoteUser) {
        mAccount = remoteUser.getAccount();
        long guid = remoteUser.getGuid();
        matchedCountry = ParamsUtils.getCountryValues(remoteUser.getCountry());
        matchedNickName = remoteUser.getNickName();
        matchedIconUrl = remoteUser.getIconUrlMininum();
        matchedGender = remoteUser.getGender();
//                    拼接频道，自己的guid+获取到的guid
        mChannelId = UserPreference.getId() + guid;
//                请求成功，从后台获得频道，加入频道，发起邀请

//                  1.发起视频邀请
        AgoraHelper.getInstance().startInvite(mChannelId, mAccount, new Gson().toJson(
                new AgoraParams(Integer.parseInt(UserPreference.getId()), UserPreference.getNickname(), UserPreference.getSmallImage(), UserPreference.getCountry_num(), UserPreference.getGender(), C.Video.RANDOM_VIDEO)));
//                  2.加入频道
        AgoraHelper.getInstance().joinChannel(mChannelId);
        mVideoParcelable = new VideoParcelable(guid, mAccount, matchedNickName, mChannelId, "1", matchedCountry, matchedGender + "", matchedIconUrl, C.Video.RANDOM_VIDEO);
    }

    private void getSystemUser(String matchVideo, UserBase remoteUser) {
        matchedIconUrl = remoteUser.getIconUrlMininum();
        matchedNickName = remoteUser.getNickName();
        matchedCountry = remoteUser.getCountry();
        matchedGender = remoteUser.getGender();
        mAccount = remoteUser.getAccount();
        guid = remoteUser.getGuid();
        mVideoParcelable = new VideoParcelable(remoteUser.getGuid(), mAccount, matchedNickName, null, "1", matchedCountry, matchedGender + "", matchedIconUrl, C.Video.SYSYTEM_VIDEO);
        mVideoPresenter = new VideoPresenter(VideoActivity.this, null, matchedNickName, String.valueOf(mVideoParcelable.guid), C.Video.SYSYTEM_VIDEO);
        if (!TextUtils.isEmpty(matchVideo)) {
//            playView=inflate.findViewById(R.id.video_palayview);
            playView.stopPlayback();
            intoPlayView(matchVideo,matchedIconUrl, matchedNickName, matchedCountry, matchedGender);
        } else {
            system = 4;//有系统用户头像但是没有视频的情况
            setInviteSuccessView(matchedIconUrl, matchedNickName, matchedCountry, matchedGender, null);
        }
    }

    private void intoPlayView(String matchVideo,final String iconUrlMininum, final String nickName, final String country, final int gender) {
         playViewPrepare = false;
        playView.setVideoURI(Uri.parse(matchVideo));
        if (playView.getCurrentPosition() == playView.getDuration()) {
            playView.seekTo(0);
        }
        playView.start();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if(!playViewPrepare && !inviteSuccess){
//                    playViewPrepare=false;
//                    setInviteSuccessView(iconUrlMininum, nickName, country, gender, null);
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            videoFlPlayview.setVisibility(View.VISIBLE);
//                            mFlRemote.setVisibility(View.GONE);
//                            playView.setVisibility(View.VISIBLE);
//                            mVideoPresenter.systemVideoSuccess(guid);
//                        }
//                    }, 3000);
//                }
//            }
//        },10000);
        playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playView.seekTo(0);
                videoFlPlayview.setVisibility(View.INVISIBLE);
                mFlRemote.setVisibility(View.VISIBLE);
                playView.stopPlayback();
                videoUserInfoLeft.setVisibility(View.GONE);
                requestData();
                LogUtil.v("tiantian","搜索展示--视频播放结束");

            }
        });
        playView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return false;
            }
        });
        playView.setBackgroundColor(Color.YELLOW);
        videoFlPlayview.setVisibility(View.VISIBLE);
        mFlRemote.setVisibility(View.GONE);
        playView.setVisibility(View.VISIBLE);
        setPlayViewBackgroundColor();
        setInviteSuccessView(iconUrlMininum, nickName, country, gender, null);
        playView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                duration = mp.getDuration();
                mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START && !inviteSuccess){
                            LeftuserInfo(iconUrlMininum, nickName, country, gender, null);
                            playViewPrepare = true;
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    playView.setBackgroundColor(Color.TRANSPARENT);
//                                    videoFlPlayview.setVisibility(View.VISIBLE);
//                                    mFlRemote.setVisibility(View.GONE);
//                                    playView.setVisibility(View.VISIBLE);
                                    mVideoPresenter.systemVideoSuccess(guid);
//                                }
//                            }, 3000);
                        }
//
                        return true;
                    }
                });
            }
        });

//        videoFlPlayview.setVisibility(View.VISIBLE);
//        playView.setVisibility(View.VISIBLE);
//        playView.setBackgroundColor(Color.YELLOW);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (!inviteSuccess) {
////                    mFlRemote.setVisibility(View.GONE);
//                    mVideoPresenter.systemVideoSuccess(guid);
//                }
//
//            }
//        }, 3000);
    }

    //          移除左上角用户信息,并将中间的寻找新朋友信息显示
    private void requestData() {
        removeLeftInfo();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getSystem=false;
                canRequest = true;
                requestVideo(false);
            }
        }, wait_time);
    }


    @Override
    protected void setListeners() {
        mIvFollow.setOnClickListener(this);
        mIvReport.setOnClickListener(this);
        mIvSwitch.setOnClickListener(this);
        mIvExit.setOnClickListener(this);
        mBtnSend.setOnClickListener(this);
        mIvPresentGift.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.video_follow:// 加好友
                addFriend();
                break;

            case R.id.video_set_report:// 举报
                if (mVideoParcelable != null) {
                    mVideoPresenter.report(String.valueOf(mVideoParcelable.guid));
                }

                break;

            case R.id.video_switch:// 切换摄像头
                mVideoPresenter.switchCamera();
                break;

            case R.id.video_exit:// 关闭
                mVideoPresenter.closeVideo();
//                防止再次请求网络
                finishVideo = true;
//                    将声网中的接受邀请的方法onInviteReceived打开
                SharedPreferenceUtil.setBooleanValue(BaseApplication.getGlobalContext(), "VideoSuccess", "closeReceiveInviter", false);
                finish();
                break;

            case R.id.video_present_gift://赠送礼物
//                givePresent();
                break;
            case R.id.video_send:// 发送文字消息
                if (mVideoParcelable != null) {
                    mVideoPresenter.sendMsg(mVideoParcelable.account);
                }
//                else {
////                    系统用户发送消息拦截
//                    AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.person_open_vip), mContext.getString(R.string.vip_intercept),
//                            mContext.getString(R.string.person_open_vip), mContext.getString(R.string.cancel), new OnDialogClickListener() {
//                                @Override
//                                public void onNegativeClick(View view) {
//                                }
//
//                                @Override
//                                public void onPositiveClick(View view) {
////                                                跳转开通vip的界面
//                                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.OPEN_VIP));
//                                }
//                            });
//                }
                break;
        }
    }

    @Override
    protected void loadData() {
//        获取礼物列表
//        ApiManager.getGiftList(new IGetDataListener<String>() {
//            @Override
//            public void onResult(String str, boolean isEmpty) {
//                PayPreference.saveSendGifts(str);
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//
//            }
//        });
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
        // 网络断开连接
//        mVideoPresenter.closeVideo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finishVideo = true;
        //释放锁，屏幕熄灭。
        wakeLock.release();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {//onDestroy有可能不走，但是isFinishing会走
            if (!(fromView.equals(C.Video.GO_CALL) || fromView.equals(C.Video.ACCEPT_INVITE))) {
                mHandler.removeCallbacks(runnable);
                mHandler.removeCallbacksAndMessages(null);
                //            调用退出匹配的接口
                exitMatch();
            }
            mVideoPresenter.finish();

        }
    }

    private void exitMatch() {
        ApiManager.exitMatch(new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });

    }

    @Override
    public void onBackPressed() {
        // do nothing 屏蔽返回键
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mRlRoot, msg);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void postDelayed(Runnable runnable, int delayedSecs) {
        mHandler.postDelayed(runnable, delayedSecs);
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public FrameLayout getLocalVideoView() {
        return mFlLocal;
    }

    @Override
    public FrameLayout getRemoteVideoView() {
        return mFlRemote;
    }

    @Override
    public void videoTimeing() {
//        firstRecord=true;
//        mVideoTime=60;
//        mHandlerTime.sendEmptyMessageDelayed(MSG_TYPE_TIMER, 1000);
    }

    @Override
    public void setFollowVisibility(boolean isVisible) {
//        mIvFollow.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        mIvFollow.setBackgroundResource(R.drawable.shape_adding_friend);
        mIvFollow.setText(getString(R.string.friend_applying));
        mIvFollow.setClickable(false);
        Drawable leftDrawable = getResources().getDrawable(R.mipmap.friend_delete);
        leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
        mIvFollow.setCompoundDrawables(leftDrawable, null, null, null);
    }

    @Override
    public String getInputMessage() {
        return mEtInput.getText().toString();
    }

    @Override
    public void clearInput() {
        mEtInput.setText("");
    }

    @Override
    public void updateMsgList(VideoMsg videoMsg) {
        // 更新
        List<VideoMsg> list = new ArrayList<>();
        list.add(videoMsg);
        mVideoMsgAdapter.appendToList(list);
        // 更新后自动滚动到列表底部
        mRecyclerView.scrollToPosition(mRecyclerView.getChildCount());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AgoraMediaEvent event) {
        if (event != null) {
            if ((event.eventCode == AgoraHelper.EVENT_CODE_VIDEO_ERROR)) {
                // 视频发生错误
                mVideoPresenter.handleVideoEvent(event);
                LogUtil.v("AgoraHelper=VideoActivity==", "视频发生错误：eventCode=" + event.eventCode);
                if (!removeLeftInfoSuccess) {
                    // 登出声网信令系统并销毁资源
//                    AgoraHelper.getInstance().logout();
//                    AgoraHelper.getInstance().destoryRtcEngine();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // 重新登陆声网信令系统
//                            AgoraHelper.getInstance().login();
                            removeInfoAndrequest();
                            LogUtil.v("tiantian","搜索展示-- 重新登陆声网信令系统");

                        }
                    }, 3000);
                }
            } else if (event.eventCode == AgoraHelper.EVENT_CODE_USER_OFFLINE) {
// 用户离线或掉线
                removeInfoAndrequest();
                LogUtil.v("tiantian","搜索展示-- 用户离线或掉线");

                mVideoPresenter.handleVideoEvent(event);
            } else if (event.eventCode == AgoraHelper.EVENT_CODE_SETUP_REMOTE_VIDEO) {
//                创建远端视频
                LogUtil.v("AgoraHelper=VideoActivity==", "创建远端视频：eventCode=" + event.eventCode);

//                    获取到用户后改变布局
                setInviteSuccessView(matchedIconUrl, matchedNickName, matchedCountry, matchedGender, event);
                mChannelId = mVideoParcelable.channelId;
                mVideoPresenter = new VideoPresenter(this, mChannelId, mVideoParcelable.nickname, String.valueOf(mVideoParcelable.guid), fromView);

            } else {
                mVideoPresenter.handleVideoEvent(event);
            }

        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(InterruptedVIPEvent event) {
        if(!getSystem){
            //        判断是否有钻石，如果没有，拦截
            ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
                @Override
                public void onResult(MyInfo myInfo, boolean isEmpty) {
                    // 更新用户信息
                    if (null != myInfo) {
                        UserBean userBase = myInfo.getUserDetail().getUserBean();
                        if (null != userBase) {
                            BeanPreference.setBeanCount(userBase.getCounts());
                            int counts = userBase.getCounts();
                            if (counts < 10) {
//                            钻石拦截
                                diamonInterrupt(false);
                                cancelExitVideo=false;
                            } else {
                                //                            根据钻石，计算聊天时间
                                diamonHintDialog();
                                cancelExitVideo=true;
                            }
                        }
                    }
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                    diamonInterrupt(false);
                }

            });
        }

    }

    //       钻石消费提示框
    private void diamonHintDialog() {
        AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.alert), mContext.getString(R.string.diamon_alert),
                mContext.getString(R.string.go_on), mContext.getString(R.string.exit_video), new OnDialogClickListener() {
                    @Override
                    public void onNegativeClick(View view) {
                        firstRequest = true;
                        removeInfoAndrequest();
                        LogUtil.v("tiantian","搜索展示-- 钻石消费提示框");


//                    将声网中的接受邀请的方法onInviteReceived打开
                        SharedPreferenceUtil.setBooleanValue(BaseApplication.getGlobalContext(), "VideoSuccess", "closeReceiveInviter", false);
                        if (mChannelId != null)
                            AgoraHelper.getInstance().leaveChannel(mChannelId);
                    }

                    @Override
                    public void onPositiveClick(View view) {
//                        心跳接口开始扣费

                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ExitVideoEvent event) {
        if(!cancelExitVideo && !getSystem){
            if (dialog != null) {
                dialog.dismiss();
            }
            firstRequest = true;
            removeInfoAndrequest();
            LogUtil.v("tiantian","搜索展示-- 0-ExitVideoEvent");


//                    将声网中的接受邀请的方法onInviteReceived打开
            SharedPreferenceUtil.setBooleanValue(BaseApplication.getGlobalContext(), "VideoSuccess", "closeReceiveInviter", false);
            if (mChannelId != null)
                AgoraHelper.getInstance().leaveChannel(mChannelId);
        }
    }

    @Override
    public void diamonInterrupt(boolean changeMessage) {
        if(dialog==null || !dialog.isShowing()){
            dialog = new Dialog(this);
            LayoutInflater li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View v = li.inflate(R.layout.dialog_interrupted_diamon, null);
            dialog.setContentView(v);
            Button exitVideo = (Button) v.findViewById(R.id.alert_dialog_negative);
            Button buyDiamon = (Button) v.findViewById(R.id.alert_dialog_positive);
            TextView message = (TextView) v.findViewById(R.id.alert_dialog_message);
            if(changeMessage){
                message.setText(getString(R.string.lack_diamon));
            }else{
                message.setText(getString(R.string.vip_intercept));
            }

            exitVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    firstRequest = true;
                    removeInfoAndrequest();
//                    将声网中的接受邀请的方法onInviteReceived打开
                    SharedPreferenceUtil.setBooleanValue(BaseApplication.getGlobalContext(), "VideoSuccess", "closeReceiveInviter", false);
                    if (mChannelId != null)
                        AgoraHelper.getInstance().leaveChannel(mChannelId);//                mVideoPresenter.closeVideo();

                }
            });
            buyDiamon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mVideoPresenter.recordIntoPayActivity("3",guid);
                    LaunchHelper.getInstance().launch(VideoActivity.this, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
                    mVideoPresenter.closeVideo();
//                防止再次请求网络
                    finishVideo = true;
                    dialog.dismiss();
//                    将声网中的接受邀请的方法onInviteReceived打开
                    SharedPreferenceUtil.setBooleanValue(BaseApplication.getGlobalContext(), "VideoSuccess", "closeReceiveInviter", false);
                    finish();
                }
            });
            Window dialogWindow = dialog.getWindow();
            WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
            dialogWindow.setGravity(Gravity.LEFT | Gravity.BOTTOM);
        /*
         * 将对话框的大小按屏幕大小的百分比设置
         */
            WindowManager m = getWindowManager();
            Display d = m.getDefaultDisplay(); // 获取屏幕宽、高用
            p.height = (int) (d.getHeight() * 0.3); // 高度设置为屏幕的0.6
            p.width = (int) (d.getWidth() * 0.9); // 宽度设置为屏幕的0.65
            p.x = (int) (d.getWidth() * 0.05); // 新位置X坐标
            p.y = (int) (d.getHeight() * 0.3); // 新位置Y坐标
            p.alpha = 1.0f; // 透明度
            p.dimAmount = 0.7f;//黑暗度
            dialogWindow.setAttributes(p);
            dialog.setCancelable(false);
            dialog.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(dialog!=null&&dialog.isShowing()){
                        dialog.dismiss();
                        firstRequest = true;
                        removeInfoAndrequest();
                        LogUtil.v("tiantian","搜索展示-- diamonInterrupt");

//                    将声网中的接受邀请的方法onInviteReceived打开
                        SharedPreferenceUtil.setBooleanValue(BaseApplication.getGlobalContext(), "VideoSuccess", "closeReceiveInviter", false);
                        if (mChannelId != null)
                            AgoraHelper.getInstance().leaveChannel(mChannelId);//                mVideoPresenter.closeVideo();
                    }

                }
            },10000);
        }

    }

    private void closeSystemVideo() {
        playView.seekTo(0);
        playView.stopPlayback();

        playView.setVisibility(View.INVISIBLE);
        videoFlPlayview.setVisibility(View.INVISIBLE);
        mFlRemote.setVisibility(View.VISIBLE);
//          移除左上角用户信息,并将中间的寻找新朋友信息显示
        removeLeftInfo();
        LogUtil.v("tiantian","搜索展示--邀请成功");
    }

    //  清除上一个用户信息，重新向后台发起请求
    private void removeInfoAndrequest() {
        if (firstRequest) {
            firstRequest = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    firstRequest = true;
                }
            }, 3000);
//        重新向后台发起请求
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    canRequest = true;
                    requestVideo(true);
                }
            }, wait_time);
//        清空远端视频
            mFlRemote.removeAllViews();
//          移除左上角用户信息,并将中间的寻找新朋友信息显示
            removeLeftInfo();
            removeLeftInfoSuccess = true;
        }
    }

    private void removeLeftInfo() {
        videoUserInfoLeft.setVisibility(View.GONE);
        videoSearch.setVisibility(View.VISIBLE);
        videoSearchUser.setVisibility(View.GONE);
//                隐藏输入框
        videoBottom.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);

    }

    float y1 = 0;
    float x1 = 0;


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //继承了Activity的onTouchEvent方法，直接监听点击事件
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            //当手指按下的时候
            x1 = event.getX();
            y1 = event.getY();
        }
        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            //当手指移动时
            float x2 = event.getX();
            float y2 = event.getY();
//                向下滑启动，开始伴随动画
            if (fromRandomVideo && canScrollDown) {
                moveY = (int) (y2 - y1);
                int moveX = (int) (x2 - x1);
                if (moveY > moveX) {
                    if (moveY > (height / 3)) {
                        moveY = moveY / 3 + (2 * height) / 9;
                    }
                    videoSlideNaim.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, moveY));
                }
            }

        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            //当手指离开的时候
            if (fromRandomVideo && canScrollDown) {
                if (moveY > (height / 5)) {
                    int startMoveY = (height - moveY) / 20;
                    setChange(moveY, startMoveY);
//                  关闭系统视频
                    playView.seekTo(0);
                    playView.stopPlayback();
                    playView.setVisibility(View.INVISIBLE);
                    videoFlPlayview.setVisibility(View.INVISIBLE);
                    mFlRemote.setVisibility(View.VISIBLE);
                    firstRequest = true;
                    getSystem = false;
                    removeInfoAndrequest();

//                    将声网中的接受邀请的方法onInviteReceived打开
                    SharedPreferenceUtil.setBooleanValue(BaseApplication.getGlobalContext(), "VideoSuccess", "closeReceiveInviter", false);
                    if (mChannelId != null)
                        AgoraHelper.getInstance().leaveChannel(mChannelId);
                    changeNum = 0;
                } else {
                    videoSlideNaim.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 0));
                }
            }

        }
        return super.onTouchEvent(event);
    }

    private void setChange(final int startY, final int startMoveY) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                changeNum++;
                if (changeNum < 20) {
                    int Y = startY + (changeNum * startMoveY);
                    setChange(Y, startMoveY);
                    videoSlideNaim.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, startY));
                } else {
                    videoSlideNaim.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 0));

                }

            }
        }, 100);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AgoraEvent event) {
        mVideoPresenter.handleAgoraEvent(event);
        if (event != null) {
            if (event.eventCode == AgoraHelper.EVENT_CODE_JOIN_CHANNEL_FAILED || event.eventCode == AgoraHelper.EVENT_CODE_INVITE_FAILED || event.eventCode == AgoraHelper.EVENT_CODE_LEAVE_CHANNEL_SELF || event.eventCode == AgoraHelper.EVENT_CODE_INVITE_REFUSE) {
//                加入频道失败，呼叫失败，对方已离开频道，对方不在随机视频频道
                LogUtil.v("AgoraHelper=VideoActivity==", "信令：加入频道失败，呼叫失败，对方已离开频道：eventCode=" + event.eventCode);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        removeInfoAndrequest();
                        LogUtil.v("tiantian","搜索展示-- 加入频道失败");
                    }
                }, 3000);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(InviteSuccessEvent event) {
        mVideoParcelable = event.getVideoParcelable();
        LogUtil.v("AgoraHelper=VideoActivity==", "受邀请成功mVideoParcelable=" + mVideoParcelable);
        if (mVideoParcelable != null) {
//                视频接通，关闭系统视频
            closeSystemVideo();
            inviteSuccess = true;
            mHandler.removeCallbacks(runnable);
            mHandler.removeCallbacksAndMessages(null);
//            mVideoPresenter = new VideoPresenter(this, mVideoParcelable.channelId, mVideoParcelable.nickname, String.valueOf(mVideoParcelable.guid));
//            受邀成功，改变界面
//            屏幕中间显示用户信息
            matchedIconUrl = mVideoParcelable.imageurl;
            matchedNickName = mVideoParcelable.nickname;
//            根据语言获取国家
            matchedCountry = ParamsUtils.getCountryValues(mVideoParcelable.country);
            mChannelId = mVideoParcelable.channelId;
            mAccount = mVideoParcelable.account;
            matchedGender = Integer.parseInt(mVideoParcelable.gender);
            fromView = C.Video.RANDOM_VIDEO;
            canRequest = false;
        }
    }

    @Subscribe
    public void onEvent(FinishVideoEvent event) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1000);
    }

    //    改变背景颜色
    private void setBackgroundColor() {
        i++;
        final int n = this.i % 5;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (n == 0) {
                    mFlRemote.setBackgroundColor(Color.parseColor("#ADD7D3"));
                } else if (n == 1) {
                    mFlRemote.setBackgroundColor(Color.parseColor("#BBD864"));
                } else if (n == 2) {
                    mFlRemote.setBackgroundColor(Color.parseColor("#C3FF90"));

                } else if (n == 3) {
                    mFlRemote.setBackgroundColor(Color.parseColor("#FFC124"));
                } else if (n == 4) {
                    mFlRemote.setBackgroundColor(Color.parseColor("#FFB6EB"));
                } else if (n == 5) {
                    mFlRemote.setBackgroundColor(Color.parseColor("#77B4FF"));
                } else {
                    mFlRemote.setBackgroundColor(Color.parseColor("#A1FFAD"));
                }
                setBackgroundColor();
            }
        }, 800);
    }
    //    改变背景颜色
    private void setPlayViewBackgroundColor() {
        i++;
        final int n = this.i % 5;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (n == 0) {
                    playView.setBackgroundColor(Color.parseColor("#ADD7D3"));
                } else if (n == 1) {
                    playView.setBackgroundColor(Color.parseColor("#BBD864"));
                } else if (n == 2) {
                    playView.setBackgroundColor(Color.parseColor("#C3FF90"));

                } else if (n == 3) {
                    playView.setBackgroundColor(Color.parseColor("#FFC124"));
                } else if (n == 4) {
                    playView.setBackgroundColor(Color.parseColor("#FFB6EB"));
                } else if (n == 5) {
                    playView.setBackgroundColor(Color.parseColor("#77B4FF"));
                } else {
                    playView.setBackgroundColor(Color.parseColor("#A1FFAD"));
                }
                if(!playViewPrepare){
                    setPlayViewBackgroundColor();
                }else{
                    playView.setBackgroundColor(Color.TRANSPARENT);
                }
            }
        }, 800);
    }

    //    改变随机改变的图标
    private void setBackgroundIcon() {
        n++;
        n = n % 5;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (n == 0) {
                    videoAnimIcon.setImageResource(R.drawable.default_bear_circle);
                } else if (n == 1) {
                    videoAnimIcon.setImageResource(R.drawable.default_dog_circle);
                } else if (n == 2) {
                    videoAnimIcon.setImageResource(R.drawable.default_pig_circle);
                } else if (n == 3) {
                    videoAnimIcon.setImageResource(R.drawable.default_owl_circle);
                } else if (n == 4) {
                    videoAnimIcon.setImageResource(R.drawable.default_penguin_circle);
                } else if (n == 5) {
                    videoAnimIcon.setImageResource(R.drawable.default_bear_circle);
                }
                setBackgroundIcon();
            }
        }, 500);
    }

    //    受邀成功（包括随机视频和主动呼叫）后改变布局
    private void setInviteSuccessView(final String iconUrlMininum, final String nickName, final String country, final int gender, final AgoraMediaEvent event) {
//        此时不能进行下滑操作
        canScrollDown = false;
        if (event == null) {
//            虚拟视频
            if (system != 4) {
                system = 1;//当系统视频处于3秒加载时，真实视频过来了。移除系统视频。
            }else{
                //        只有头像，3秒后移除头像，重新请求
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                            system = 2;
                            requestData();
                            LogUtil.v("tiantian","搜索展示--system=4");
                    }
                }, 3000);
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (system == 2) {
                        system = 3;
                    }
//                    右下角进度圈隐藏，叉号显示
                    videoProgrsss.setVisibility(View.GONE);
                    mIvExit.setVisibility(View.VISIBLE);
                }
            }, 3000);
            //        开始虚拟视频的n秒的进度条
            videoProgrsss.setVisibility(View.VISIBLE);
            videoProgrsss.setmShowProgress(100);
            videoUserProgrsss.setVisibility(View.VISIBLE);
            videoUserProgrsss.setHideFont();
            videoUserProgrsss.setChangeProgress(100);
        } else {
//            真实视频
            system = 2;
            //        开始3秒的进度条
            videoProgrsss.setVisibility(View.VISIBLE);
            videoProgrsss.setmShowProgress(100);
            videoUserProgrsss.setVisibility(View.VISIBLE);
            videoUserProgrsss.setHideFont();
            videoUserProgrsss.setmShowProgress(100);
            //        3秒后将用户信息移到左上角
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    LeftuserInfo(iconUrlMininum, nickName, country, gender, event);
//                    if (system == 1 || system == 2) {
//                        LeftuserInfo(iconUrlMininum, nickName, country, gender, event);
//                    } else if (system == 4) {//移除头像，并请求接口
//                        system = 2;
//                        requestData();
//                        LogUtil.v("tiantian","搜索展示--system=4");
//                        videoProgrsss.setVisibility(View.GONE);
//                        mIvExit.setVisibility(View.VISIBLE);
//                    } else {
//                        system = 2;
//                    }

                }
            }, 3000);
        }
        LogUtil.v("tiantian","搜索消失--邀请成功");
        videoSearch.setVisibility(View.GONE);
        videoSearchUser.setVisibility(View.VISIBLE);
//        if (com.bumptech.glide.util.Util.isOnMainThread() && !VideoActivity.this.isFinishing()){
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(iconUrlMininum).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(videoUserIcon).build());
        videoUserName.setText(nickName);
        videoUserCountry.setText(country);
        videoUserNationalFlag.setImageResource(Util.getBanner(country));
        if (gender == 0) {
            videoUserSex.setText(R.string.register_male);
            videoUserSexIcon.setImageResource(R.drawable.ic_male);
            videoProgrsss.setRingColor("#85AEF6");
            videoUserProgrsss.setRingColor("#85AEF6");
        } else {
            videoUserSex.setText(R.string.register_female);
            videoUserSexIcon.setImageResource(R.drawable.ic_female);
            videoProgrsss.setRingColor("#FFC1EB");
            videoUserProgrsss.setRingColor("#FFC1EB");
        }
        mIvExit.setVisibility(View.GONE);



//        }

    }

    private void LeftuserInfo(String iconUrlMininum, String nickName, String country, int gender, AgoraMediaEvent event) {

//        判断是否是好友
        isFriend();
        if (event != null)
            mVideoPresenter.handleVideoEvent(event);
        LogUtil.v("tiantian","搜索消失--LeftuserInfo");
        videoSearch.setVisibility(View.GONE);
        videoSearchUser.setVisibility(View.GONE);
        videoProgrsss.setVisibility(View.GONE);
        mIvExit.setVisibility(View.VISIBLE);
        videoUserInfoLeft.setVisibility(View.VISIBLE);
        if (com.bumptech.glide.util.Util.isOnMainThread() && !VideoActivity.this.isFinishing())
            ImageLoaderUtil.getInstance().loadImage(VideoActivity.this, new ImageLoader.Builder().url(iconUrlMininum).transform(new CropCircleTransformation(mContext))
                    .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(videoLeftUserIcon).build());
        videoLeftUserName.setText(nickName);
        videoLeftUserCountry.setText(country);
        videoLeftUserNationalFlag.setImageResource(Util.getBanner(country));
        if (gender == 0) {
            videoLeftUserSex.setText(R.string.register_male);
//                    设置圆环无限旋转
//            if(fromRandomVideo){
            if (UserPreference.isMale()) {
                mTvTime.setCirculation("#FFC1EB", 100, true);
                videoLeftUserProgrsss.setVisibility(View.VISIBLE);
                videoLeftUserProgrsss.setHideFont();
                videoLeftUserProgrsss.setCirculation("#85AEF6", 100, false);
            } else {
                videoLlTime.setVisibility(View.GONE);
                videoLeftUserProgrsss.setVisibility(View.GONE);
//                videoLeftUserProgrsss.setDefaulRingColor("#85AEF6");
            }
        } else {
            videoLeftUserSex.setText(R.string.register_female);
//                    设置圆环无限旋转
            if (UserPreference.isMale()) {
                mTvTime.setCirculation("#FFC1EB", 100, true);
                videoLeftUserProgrsss.setVisibility(View.VISIBLE);
                videoLeftUserProgrsss.setHideFont();
                videoLeftUserProgrsss.setCirculation("#FFC1EB", 100, false);
            } else {
                videoLlTime.setVisibility(View.GONE);
                videoLeftUserProgrsss.setVisibility(View.GONE);
//                videoLeftUserProgrsss.setDefaulRingColor("#85AEF6");
            }
        }
//                显示输入框
        videoBottom.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
        removeLeftInfoSuccess = false;
//        此时能进行下滑操作
        canScrollDown = true;
        inviteSuccess = false;
    }

    private void isFriend() {
        ApiManager.getFriendFromId(mAccount, new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            int isFriend = userDetail.getIsFriend();
                            if (isFriend == 1) {
                                mIvFollow.setVisibility(View.GONE);
                                mIvFollow.setClickable(false);
                            } else {
                                mIvFollow.setBackgroundResource(R.drawable.shape_add_friend);
                                mIvFollow.setVisibility(View.VISIBLE);
                                mIvFollow.setText(R.string.add_friend);
                                mIvFollow.setClickable(true);
                                Drawable leftDrawable = getResources().getDrawable(R.drawable.add_icon);
                                leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
                                mIvFollow.setCompoundDrawables(leftDrawable, null, null, null);
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    //    添加好友
    public void addFriend() {
//                判断是否拦截
        ApiManager.applyAddFriend(String.valueOf(mVideoParcelable.guid), new IGetDataListener<ApplyAddFriendMode>() {
            @Override
            public void onResult(ApplyAddFriendMode baseModel, boolean isEmpty) {
                String isSucceed = baseModel.getIsSucceed();
                if (!TextUtils.isEmpty(isSucceed) && (isSucceed.equals("1")||isSucceed.equals("2"))) {
                     payIntercept = baseModel.getPayIntercept();
                    if (payIntercept == 0) {
                        //不拦截
                        setFollowVisibility(false);
                    } else {
                        //弹出拦截对话框
                        AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.alert), mContext.getString(R.string.intercept),
                                 mContext.getString(R.string.sure), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                                    @Override
                                    public void onNegativeClick(View view) {
                                    }

                                    @Override
                                    public void onPositiveClick(View view) {
//                                        判断是否有钱
                                        if(BeanPreference.getBeanCount()>10){
//                                            调扣费接口
                                            payRequest();
                                        }else{
                                            //     跳转开通钻石的界面
                                            LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
                                        }

                                    }
                                });
                    }
                }

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });

    }

    private void payRequest() {
        ApiManager.applypay(new IGetDataListener<ApplyPayModel>() {
            @Override
            public void onResult(ApplyPayModel baseModel, boolean isEmpty) {
                String isSucceed = baseModel.getIsSucceed();
                String beanCounts = baseModel.getBeanCounts();
                String beanStatus = baseModel.getBeanStatus();
                setFollowVisibility(false);

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //获取当前屏幕内容的高度
//        screenHeight = getWindow().getDecorView().getHeight();
        mRlRoot.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        Rect r = new Rect();
                        mRlRoot.getWindowVisibleDisplayFrame(r);
                        RelativeLayout.LayoutParams layout = (RelativeLayout.LayoutParams) mBootomExit.getLayoutParams();
                        layout.setMargins(0, 0, 0, height - r.bottom);
//设置button的新位置属性,left，top，right，bottom
                        mBootomExit.setLayoutParams(layout);
//将新的位置加入button控件中
//                       LogUtil.v("hdh","可见范围的高度："+r.bottom);
                    }
                });
    }

    //    赠送礼物的逻辑
    public void givePresent() {
        View view = getLayoutInflater().inflate(R.layout.dialog_present_gift,
                null);
        RecyclerView recycle = view.findViewById(R.id.present_gift_recyclerview);
        recycle.setLayoutManager(new GridLayoutManager(this, 3, OrientationHelper.VERTICAL, false));
        recycle.setAdapter(new PresentGiftAdapter(mContext));
        final Dialog dialog = new Dialog(this, R.style.transparentFrameWindowStyle);
        dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        Window window = dialog.getWindow();


        // 设置显示动画
        window.setWindowAnimations(R.style.main_menu_animstyle);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.x = 0;
        wl.y = getWindowManager().getDefaultDisplay().getHeight();
        // 以下这两句是为了保证按钮可以水平满屏
        wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
        wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        // 设置显示位置
        dialog.onWindowAttributesChanged(wl);
        // 设置点击外围解散
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }


    //点击编辑框以外的任意位置，关闭编辑框
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);

            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
//            if (event.getX() > left && event.getX() < right
//                    && event.getY() > top && event.getY() < bottom) {
            if (event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

}
