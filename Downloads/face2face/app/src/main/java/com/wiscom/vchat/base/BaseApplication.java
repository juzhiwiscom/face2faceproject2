package com.wiscom.vchat.base;

import android.app.Application;
import android.content.Context;

//import com.facebook.appevents.AppEventsLogger;
import com.wiscom.vchat.common.HyphenateHelper;
import com.wiscom.vchat.db.DbModle;
import com.wiscom.vchat.service.InitializeService;

/**
 * Created by zhangdroid on 2017/5/11.
 */
public class BaseApplication extends Application {
    private static Context sApplicationContext;

    public static Context getGlobalContext() {
        return sApplicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplicationContext = getApplicationContext();
        // 初始化环信
        HyphenateHelper.getInstance().init(this);
//        facebook手动记录应用事件
//        AppEventsLogger.activateApp(this);
//            com.facebook



        InitializeService.start(this);
        DbModle.getInstance().init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
