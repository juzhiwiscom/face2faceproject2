package com.wiscom.vchat.ui.message.contract;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;

import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/6/10.
 */
public interface MessageContract {

    interface IView extends BaseView {

        FragmentManager getManager();

        void setAdapter(PagerAdapter pagerAdapter);

    }

    interface IPresenter extends BasePresenter {
        void addTabs();
    }

}
