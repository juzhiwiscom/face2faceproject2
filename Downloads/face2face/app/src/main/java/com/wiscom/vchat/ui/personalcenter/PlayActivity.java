package com.wiscom.vchat.ui.personalcenter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiscom.library.dialog.AlertDialog;
import com.wiscom.library.dialog.LoadingDialog;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.widget.PlayView;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragmentActivity;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.ApplyAddFriendMode;
import com.wiscom.vchat.data.model.ApplyPayModel;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.model.MyInfo;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserDetail;
import com.wiscom.vchat.data.preference.BeanPreference;
import com.wiscom.vchat.event.UpdateRecommendVideoEvent;
import com.wiscom.vchat.parcelable.PayParcelable;
import com.wiscom.vchat.parcelable.PlayViewParcelable;
import com.wiscom.vchat.ui.pay.PayActivity;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2017/6/20.
 * 播放视频的Activity，时间有限暂时不做抽取
 */

public class PlayActivity extends BaseFragmentActivity {
    @BindView(R.id.play_activity_palayview)
    PlayView playView;
    @BindView(R.id.video_left_user_icon)
    ImageView videoLeftUserIcon;
    @BindView(R.id.video_left_user_name)
    TextView videoLeftUserName;
    @BindView(R.id.video_left_user_national_flag)
    ImageView videoLeftUserNationalFlag;
    @BindView(R.id.video_left_user_country)
    TextView videoLeftUserCountry;
    @BindView(R.id.video_left_user_sex)
    TextView videoLeftUserSex;
    @BindView(R.id.video_left_user_info)
    LinearLayout videoLeftUserInfo;
    @BindView(R.id.video_set_report)
    ImageView ivReport;
    @BindView(R.id.video_follow)
    TextView tvAddFriend;
    @BindView(R.id.video_user_info_left)
    LinearLayout videoUserInfoLeft;
    @BindView(R.id.activity_play)
    FrameLayout activityPlay;
    @BindView(R.id.iv_preview)
    ImageView ivPreview;
    @BindView(R.id.iv_heart)
    ImageView ivHeart;
    @BindView(R.id.play_heart)
    TextView playHeart;
    @BindView(R.id.play_chat)
    TextView playChat;
    @BindView(R.id.flowLikeView)
    FlowLikeView flowLikeView;
    private String videoUrl;
    private long playPostion = -1;
    private long duration = -1;
    private PlayViewParcelable mPlayViewParcelable;
    private String thumbnailUrl;
    private String guid;
    private String nickName;
    private int payIntercept;
    private String resonCode;
    private String account;
    private int count = 0;
    private long firClick = 0;
    private long secClick = 0;
    private String praiseCount;
    private String videoId;
    private boolean isFirstClick=true;
    private PowerManager.WakeLock wakeLock;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_play;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mPlayViewParcelable = (PlayViewParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        //        屏幕持续点亮
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "TAG");
//屏幕会持续点亮
        wakeLock.acquire();

//        stringExtra=getIntent().getStringExtra("key");
        videoUrl = mPlayViewParcelable.videoUrl;
        thumbnailUrl = mPlayViewParcelable.thumbnailUrl;
        guid = mPlayViewParcelable.guid;
        nickName = mPlayViewParcelable.nickName;
        String icon = mPlayViewParcelable.icon;
        account = mPlayViewParcelable.account;
         praiseCount = mPlayViewParcelable.praiseCount;
         videoId = mPlayViewParcelable.videoId;
        String isPraise = mPlayViewParcelable.isPraise;
        if(isPraise.equals("1")){
//            点赞
            Drawable topDrawable = getResources().getDrawable(R.drawable.play_heart);
            playHeart.setCompoundDrawablesWithIntrinsicBounds(null, topDrawable , null, null);
            isFirstClick=false;
        }
        playHeart.setText(praiseCount);
        videoLeftUserName.setText(nickName);
        ImageLoaderUtil.getInstance().loadImage(PlayActivity.this, new ImageLoader.Builder().url(icon).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(videoLeftUserIcon).build());
        ImageLoaderUtil.getInstance().loadImage(PlayActivity.this, new ImageLoader.Builder().url(thumbnailUrl)
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(ivPreview).build());
        LoadingDialog.show(getSupportFragmentManager());
        isFriend();
        initPlayView();
        play();
    }

    @Override
    protected void setListeners() {
        ivReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                report();
            }
        });
        tvAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                判断是否拦截
                ApiManager.applyAddFriend(guid, new IGetDataListener<ApplyAddFriendMode>() {
                    @Override
                    public void onResult(ApplyAddFriendMode baseModel, boolean isEmpty) {
                        String isSucceed = baseModel.getIsSucceed();
                        if (!TextUtils.isEmpty(isSucceed) && isSucceed.equals("1")) {
                            payIntercept = baseModel.getPayIntercept();
                            if (payIntercept == 0) {
                                //不拦截
                                setFollowVisibility(false);
                            } else {
                                //弹出拦截对话框
                                AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.alert), mContext.getString(R.string.intercept),
                                        mContext.getString(R.string.sure), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                                            @Override
                                            public void onNegativeClick(View view) {
                                            }

                                            @Override
                                            public void onPositiveClick(View view) {
//                                        判断是否有钱
                                                if (BeanPreference.getBeanCount() > 10) {
//                                            调扣费接口
                                                    payRequest();
                                                } else {
                                                    //     跳转开通钻石的界面
                                                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
                                                }

                                            }
                                        });
                            }
                        }

                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {

                    }
                });
            }
        });

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    private void initPlayView() {
        if (!TextUtils.isEmpty(videoUrl)) {
            playView.setVideoURI(Uri.parse(videoUrl));
            playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    playView.seekTo(1);
//                    if (playView.getCurrentPosition() == playView.getDuration()) {
//                        playView.seekTo(0);
//                    }
                    startVideo();
                }
            });
            playView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
                            if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                                LoadingDialog.hide();
                                ivPreview.setVisibility(View.GONE);

                            }
                            return true;
                        }
                    });
                    //获取视频资源的宽度
                    int videoWidth = mp.getVideoWidth();
                    //获取视频资源的高度
                    int videoHeight = mp.getVideoHeight();
                    playView.setSizeH(videoHeight);
                    playView.setSizeW(videoWidth);
                    playView.requestLayout();
                    duration = mp.getDuration();
                }
            });
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            boolean isScreenOn = pm.isScreenOn();//如果为true，则表示屏幕“亮”了，否则屏幕“暗”了。
            if (!isScreenOn) {
                pauseVideo();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (playPostion > 0) {
            pauseVideo();
        }
        playView.seekTo((int) ((playPostion > 0 && playPostion < duration) ? playPostion : 1));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playView.stopPlayback();
        //释放锁，屏幕熄灭。
        wakeLock.release();
    }

    @Override
    protected void onPause() {
        super.onPause();
        playView.pause();
        playPostion = playView.getCurrentPosition();
        pauseVideo();

    }

    private void pauseVideo() {
        playView.pause();
    }

    private void startVideo() {
        playView.start();

    }

    /**
     * 播放
     */
    private void play() {
        if (playView.isPlaying()) {
            pauseVideo();
        } else {
            if (playView.getCurrentPosition() == playView.getDuration()) {
                playView.seekTo(0);
            }
            startVideo();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    public void setFollowVisibility(boolean isVisible) {
//        mIvFollow.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        tvAddFriend.setBackgroundResource(R.drawable.shape_adding_friend);
        tvAddFriend.setText(getString(R.string.friend_applying));
        tvAddFriend.setClickable(false);
        Drawable leftDrawable = getResources().getDrawable(R.mipmap.friend_delete);
        leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
        tvAddFriend.setCompoundDrawables(leftDrawable, null, null, null);
    }

    private void payRequest() {
        ApiManager.applypay(new IGetDataListener<ApplyPayModel>() {
            @Override
            public void onResult(ApplyPayModel baseModel, boolean isEmpty) {
                String isSucceed = baseModel.getIsSucceed();
                String beanCounts = baseModel.getBeanCounts();
                String beanStatus = baseModel.getBeanStatus();
                setFollowVisibility(false);

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                count++;
//                if (1 == count) {
//                    firClick = System.currentTimeMillis();
//                } else if (count>1) {
//                    secClick = System.currentTimeMillis();
//                    if (secClick - firClick < interval) {
//                        if (mCallback != null) {
//                            mCallback.onDoubleClick();
//                        } else {
////                        Log.e(TAG, "请在构造方法中传入一个双击回调");
//                        }
//                        count = 0;
//                        firClick = 0;
//                    } else {
//                        firClick = secClick;
//                        count = 1;
//                    }
//                    secClick = 0;
//                }
//                break;
//            case MotionEvent.ACTION_MOVE:
//                break;
//            case MotionEvent.ACTION_UP:
//                break;
//        }
    }

    private void isFriend() {
        ApiManager.getFriendFromId(account, new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            int isFriend = userDetail.getIsFriend();
                            if (isFriend == 1) {
                                tvAddFriend.setVisibility(View.GONE);
                                tvAddFriend.setClickable(false);
                            } else {
                                tvAddFriend.setBackgroundResource(R.drawable.shape_add_friend);
                                tvAddFriend.setVisibility(View.VISIBLE);
                                tvAddFriend.setText(R.string.add_friend);
                                tvAddFriend.setClickable(true);
                                Drawable leftDrawable = getResources().getDrawable(R.drawable.add_icon);
                                leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
                                tvAddFriend.setCompoundDrawables(leftDrawable, null, null, null);
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    public void report() {
        resonCode = "";
        final Dialog bottomDialog = new Dialog(mContext, R.style.BottomDialog);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_report, null);
        bottomDialog.setContentView(contentView);
        final TextView tvViolent = (TextView) contentView.findViewById(R.id.report_violent);
        final TextView tvAdvert = (TextView) contentView.findViewById(R.id.report_advert);
        final TextView tvgender = (TextView) contentView.findViewById(R.id.report_fake_gender);
        final TextView tvBawdry = (TextView) contentView.findViewById(R.id.report_bawdry);
        final TextView tvClown = (TextView) contentView.findViewById(R.id.report_clown);
        final TextView tvPoli = (TextView) contentView.findViewById(R.id.report_politice);
        final TextView tvOther = (TextView) contentView.findViewById(R.id.report_other);
        final TextView tvCancel = (TextView) contentView.findViewById(R.id.report_cancel);
        final TextView tvReport = (TextView) contentView.findViewById(R.id.report_report);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = Util.dp2px(mContext, 220);
        params.height = Util.dp2px(mContext, 260);
        params.topMargin = Util.dp2px(mContext, 150);
        contentView.setLayoutParams(params);
        bottomDialog.getWindow().setGravity(Gravity.TOP);
        bottomDialog.show();
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.dismiss();
            }
        });
        tvReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startReport(guid, resonCode);
                bottomDialog.dismiss();
            }
        });
        tvViolent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode = "2";
            }
        });
        tvAdvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode = "2";
            }
        });
        tvgender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvgender.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode = "1";
            }
        });
        tvBawdry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode = "1";
            }
        });
        tvClown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvClown.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode = "1";
            }
        });
        tvPoli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode = "4";
            }
        });
        tvOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvOther.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode = "3";
            }
        });

    }

    //    举报
    public void startReport(String userId, String resonCode) {
        ApiManager.report(userId, resonCode, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel aSwitch, boolean isEmpty) {

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    @OnClick({R.id.play_heart, R.id.play_chat, R.id.iv_heart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.play_heart:
                int random = (int) (3 + Math.random() * 2);
                for (int i = 0; i < random; i++) {
                    flowLikeView.addLikeView();
                }

                if(isFirstClick){
                    isFirstClick=false;
                    Drawable topDrawable = getResources().getDrawable(R.drawable.play_heart);
                    playHeart.setCompoundDrawablesWithIntrinsicBounds(null, topDrawable , null, null);
                    praiseCount = Integer.parseInt(praiseCount) + 1+"";
                    playHeart.setText(praiseCount);
                    clickPraise();
                    EventBus.getDefault().post(new UpdateRecommendVideoEvent());
                }


//                topDrawable.setBounds(topDrawable.getMinimumWidth(), 0, 0,topDrawable.getMinimumHeight());
//                playHeart.setCompoundDrawables(null, null, topDrawable, null);
//                playHeart.setDraw
                break;
            case R.id.play_chat:
                break;
            case R.id.iv_heart:
                AnimationSet animationSet = (AnimationSet) AnimationUtils.loadAnimation(PlayActivity.this, R.anim.anim_show_heart);
                ivHeart.startAnimation(animationSet);
//                startAnim();
                break;

        }
    }

    private void clickPraise() {
        ApiManager.clickPraise(videoId,new IGetDataListener<String>(){

            @Override
            public void onResult(String s, boolean isEmpty) {

            }


            @Override
            public void onError(String msg, boolean isNetworkError) {

            }

        });
    }

    private void startAnim() {
        //创建动画，参数表示他的子动画是否共用一个插值器
        AnimationSet animationSet = new AnimationSet(true);
        //添加动画
        animationSet.addAnimation(new AlphaAnimation(1.0f, 0.0f));
        //设置插值器
        animationSet.setInterpolator(new LinearInterpolator());
        //设置动画持续时长
        animationSet.setDuration(3000);
        //设置动画结束之后是否保持动画的目标状态
        animationSet.setFillAfter(true);
        //设置动画结束之后是否保持动画开始时的状态
        animationSet.setFillBefore(false);
        //设置重复模式
        animationSet.setRepeatMode(AnimationSet.REVERSE);
        //设置重复次数
        animationSet.setRepeatCount(AnimationSet.INFINITE);
        //设置动画延时时间
        animationSet.setStartOffset(2000);
        //取消动画
        animationSet.cancel();
        //释放资源
        animationSet.reset();
        //开始动画
        ivHeart.startAnimation(animationSet);
    }
}
