package com.wiscom.vchat.data.model;

public class UploadInfoParams {
    private String nickName;//昵称
    private String age;//年龄
    private String country;//国家
    private String spokenLanguage;//语言
    private String state;//地区州
    private String ownWords;//个人介绍

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSpokenLanguage() {
        return spokenLanguage;
    }

    public void setSpokenLanguage(String spokenLanguage) {
        this.spokenLanguage = spokenLanguage;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getOwnWords() {
        return ownWords;
    }

    public void setOwnWords(String ownWords) {
        this.ownWords = ownWords;
    }

    @Override
    public String toString() {
        return "UploadInfoParams{" +
                "nickName='" + nickName + '\'' +
                ", age='" + age + '\'' +
                ", country='" + country + '\'' +
                ", spokenLanguage='" + spokenLanguage + '\'' +
                ", state='" + state + '\'' +
                ", ownWords='" + ownWords + '\'' +
                '}';
    }
}
