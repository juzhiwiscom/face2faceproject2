package com.wiscom.vchat.ui.chat;

import android.content.Context;

import com.wiscom.library.adapter.MultiTypeRecyclerViewAdapter;

/**
 * 聊天列表适配器
 * Created by zhangdroid on 2017/6/28.
 */
public class ChatAdapter extends MultiTypeRecyclerViewAdapter {

    public ChatAdapter(Context context, String url) {
        super(context);
        // 文本消息
        addItemViewProvider(new TextMessageLeftProvider(context, url));
        addItemViewProvider(new TextMessageRightProvider(context));
        // 语音消息
        addItemViewProvider(new VoiceMessageLeftProvider(context, url));
        addItemViewProvider(new VoiceMessageRightProvider(context));
        // 图片消息
        addItemViewProvider(new ImageMessageLeftProvider(context, url));
        addItemViewProvider(new ImageMessageRightProvider(context));
    }

}
