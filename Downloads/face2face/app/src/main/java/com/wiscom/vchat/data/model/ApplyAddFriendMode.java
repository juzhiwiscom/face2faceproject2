package com.wiscom.vchat.data.model;

/**
 * Created by tianzhentao on 2018/1/13.
 */

public class ApplyAddFriendMode extends BaseModel{
    private  int payIntercept;

    public int getPayIntercept() {
        return payIntercept;
    }

    public void setPayIntercept(int payIntercept) {
        this.payIntercept = payIntercept;
    }
}
