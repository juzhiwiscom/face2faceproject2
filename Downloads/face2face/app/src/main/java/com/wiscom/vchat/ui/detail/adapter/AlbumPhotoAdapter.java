package com.wiscom.vchat.ui.detail.adapter;

import android.content.Context;
import android.os.Handler;
import android.widget.ImageView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.model.UserPhoto;
import com.wiscom.library.adapter.CommonRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;

public class AlbumPhotoAdapter extends CommonRecyclerViewAdapter<UserPhoto> {

    public AlbumPhotoAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(final UserPhoto userPhoto, int position, final RecyclerViewHolder holder) {
        if (userPhoto != null) {
            new Handler().postDelayed(new Runnable(){
                public void run() {
                    ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userPhoto.getFileUrlMinimum()).imageView((ImageView) holder.getView(R.id.item_photo_iv))
                            .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).build());
                }
            }, 3000);

        }
    }
}
