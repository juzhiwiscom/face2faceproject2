package com.wiscom.vchat.ui.personalcenter;

import android.os.Parcelable;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.library.net.NetUtil;

/**
 * Created by tianzhentao on 2018/1/6.
 */

public class OpenVipActivity extends BaseTopBarActivity {
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_open_vip;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_open_vip);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }
}
