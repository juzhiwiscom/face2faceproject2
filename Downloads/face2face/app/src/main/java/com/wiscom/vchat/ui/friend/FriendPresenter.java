package com.wiscom.vchat.ui.friend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.common.HyphenateHelper;
import com.wiscom.vchat.common.ParamsUtils;
import com.wiscom.vchat.common.RefreshRecyclerView;
import com.wiscom.vchat.common.recyclerview.CommonRecyclerViewAdapter;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.FriendListModel;
import com.wiscom.vchat.data.model.HuanXinUser;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.db.DbModle;
import com.wiscom.vchat.event.AllUnReadMsgEvent;
import com.wiscom.vchat.event.UnreadMsgChangedEvent;
import com.wiscom.vchat.parcelable.ChatParcelable;
import com.wiscom.vchat.ui.chat.ChatActivity;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.SharedPreferenceUtil;
import com.wiscom.library.util.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Administrator on 2017/6/14.
 */

public class FriendPresenter implements FriendContract.IPresenter {
    private FriendContract.IView mFollowIview;
    private Context mContext;
    private FriendAdapter mFollowAdater;
//    List<UserBase> aboutNameList;
    private int pageNum = 1;
    List<HuanXinUser> allAcount=new ArrayList<>();
    List<UserBase> friendApplyList=new ArrayList<>();

    private List<UserBase> list;
    private int i=0;
    private TextView tvSUnreadMsgNum;
    private boolean initData=false;
    private TextView tvFriendMsg;
    private boolean haveValue=false;

    public FriendPresenter(FriendContract.IView mFollowIview) {
        this.mFollowIview = mFollowIview;
        this.mContext = mFollowIview.obtainContext();
    }


    @Override
    public void start() {

    }

    @Override
    public void finish() {
        mFollowIview = null;
        mContext = null;
        mFollowAdater = null;
    }

    @Override
    public void loadFollowUserList() {
        mFollowIview.setOnLoadMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                loadMore();
            }
        });
        initData();
        load();
    }

    public void initData() {
        mFollowAdater = new FriendAdapter(mContext, R.layout.item_friend);
//        mFollowIview.setAdapter(mFollowAdater, R.layout.common_load_more);

        // 添加HeaderView
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View headView = li.inflate(R.layout.item_friend_header, null);
        RelativeLayout friendRlApplay = (RelativeLayout) headView.findViewById(R.id.friend_rl_applay);
        LinearLayout friendLlSecretary = (LinearLayout) headView.findViewById(R.id.friend_ll_secretary);
        tvSUnreadMsgNum = (TextView) headView.findViewById(R.id.friend_s_unread_msg_count);
        tvFriendMsg = (TextView) headView.findViewById(R.id.friend_msg_count);
////        设置小秘书的未读消息
//        int readMsgNum = SharedPreferenceUtil.getIntValue(mContext, "Read_Msg_Num", "10000", 0);
//        if(readMsgNum==0){
//            tvUnreadMsgNum.setVisibility(View.INVISIBLE);
//        }else{
//            tvUnreadMsgNum.setText(readMsgNum+"");
//            tvUnreadMsgNum.setVisibility(View.VISIBLE);
//        }

        mFollowAdater.addHeaderView(headView);
        mFollowIview.setAdapter(mFollowAdater, R.layout.common_load_more);
        friendRlApplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvFriendMsg.setVisibility(View.GONE);
                LaunchHelper.getInstance().launch(mContext, FriendApplyActivity.class);
            }
        });
        final List<HuanXinUser> allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
        friendLlSecretary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                小秘书点击事件
//                    条目点击，跳转到聊天界面
//                将小秘书消息清空
                int read_msg_num = SharedPreferenceUtil.getIntValue(mContext, "Read_Msg_Num", "10000", 0);
                if(read_msg_num>0){
//                        取出总的未读消息
                    int allReadMsgNum = SharedPreferenceUtil.getIntValue(mContext, "Read_Msg_Num", "All_Read_Msg_Num", 0);
//                 刷新总的未读消息
                    EventBus.getDefault().post(new AllUnReadMsgEvent(allReadMsgNum-read_msg_num));
//            总的未读消息-本条目消息
                    SharedPreferenceUtil.setIntValue(mContext,"Read_Msg_Num","All_Read_Msg_Num",allReadMsgNum-read_msg_num);
//                    将本条目未读消息清空
                    SharedPreferenceUtil.setIntValue(mContext,"Read_Msg_Num","10000",0);
                }

                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(10000,
                        "100010000", mContext.getString(R.string.friend_secretary), "KeFuIconUrl","country","1"));
                tvSUnreadMsgNum.setVisibility(View.INVISIBLE);
                for (int i = 0; i < allAcount.size(); i++) {
                    if(allAcount.get(i).getHxId().equals("10000")){
                        DbModle.getInstance().getUserAccountDao().setState(allAcount.get(i), true);//把标记设置为已经读取
                    }
                }

            }
        });
//
//                for (int i = 0; i < allAcount.size(); i++) {
//                    if(allAcount.get(i).getHxId().equals("10000")){
//                        if(!allAcount.get(i).getIsRead().equals("0")){
//                            tvUnreadMsgNum.setVisibility(View.VISIBLE);
//                            tvUnreadMsgNum.setText(allAcount.get(i).getIsRead());
//                        }else{
//                            tvUnreadMsgNum.setVisibility(View.GONE);
//                        }
//                    }
////                }
////            }
//        }


        mFollowAdater.setFragmentManager(mFollowIview.obtainFragmentManager());
        mFollowAdater.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, com.wiscom.vchat.common.recyclerview.RecyclerViewHolder viewHolder) {
                UserBase userBase = mFollowAdater.getItemByPosition(position);
                if (null != userBase) {
//                    刷新未读消息
                    EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
                    final List<HuanXinUser> allAcount = DbModle.getInstance().getUserAccountDao().getAllAcount();
                    if(allAcount!=null&&allAcount.size()>0) {
                        for (int i = 0; i < allAcount.size(); i++) {
                            if(allAcount.get(i).getHxId().equals(userBase.getGuid()+"")){
                                DbModle.getInstance().getUserAccountDao().setState(allAcount.get(i), true);//把标记设置为已经读取
                            }
                        }
                    }
                    int read_msg_num = SharedPreferenceUtil.getIntValue(mContext, "Read_Msg_Num", userBase.getGuid() + "", 0);
                    if(read_msg_num>0){
//                        取出总的未读消息
                        int allReadMsgNum = SharedPreferenceUtil.getIntValue(mContext, "Read_Msg_Num", "All_Read_Msg_Num", 0);
//                        刷新总的未读消息
                        EventBus.getDefault().post(new AllUnReadMsgEvent(allReadMsgNum-read_msg_num));
//            总的未读消息-本条目消息
                        SharedPreferenceUtil.setIntValue(mContext,"Read_Msg_Num","All_Read_Msg_Num",allReadMsgNum-read_msg_num);//                    将本条目未读消息清空
                        SharedPreferenceUtil.setIntValue(mContext,"Read_Msg_Num",userBase.getGuid()+"",0);
                    }



//                    条目点击，跳转到聊天界面
                    String gender= String.valueOf(userBase.getGender());
                    String country = ParamsUtils.getCountryValues(userBase.getCountry());
                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(userBase.getGuid(),
                            userBase.getAccount(), userBase.getNickName(), userBase.getIconUrlMininum(),country,gender));
                }
            }
        });
        initData=true;
    }

    @Override
    public void refresh() {
        if(!initData){
            initData();
        }

//        判断是否有新的好友申请消息
        haveFriendApply();
//                for (int i = 0; i < allAcount.size(); i++) {
//                    if(allAcount.get(i).getHxId().equals("10000")){
//                        if(!allAcount.get(i).getIsRead().equals("0")){
//                            tvUnreadMsgNum.setVisibility(View.VISIBLE);
//                            tvUnreadMsgNum.setText(allAcount.get(i).getIsRead());
//                        }else{
//                            tvUnreadMsgNum.setVisibility(View.GONE);
//                        }
//                    }
////                }
////            }
//        }

        pageNum = 1;
        mFollowIview.toggleShowError(false, null);
        load();
    }

    private void haveFriendApply() {
        ApiManager.getFriendApply(pageNum, new IGetDataListener<FriendListModel>() {
            @Override
            public void onResult(FriendListModel friendListModel, boolean isEmpty) {
                if (isEmpty) {
                    //        设置小秘书的未读消息
                    int readMsgNum = SharedPreferenceUtil.getIntValue(mContext, "Read_Msg_Num", "10000", 0);
                    if(readMsgNum==0){
                        tvSUnreadMsgNum.setVisibility(View.INVISIBLE);
                    }else{
                        tvSUnreadMsgNum.setVisibility(View.VISIBLE);
                        tvSUnreadMsgNum.setText(readMsgNum+"");
                    }
                } else {
                    //        设置小秘书的未读消息
                    int readMsgNum = SharedPreferenceUtil.getIntValue(mContext, "Read_Msg_Num", "10000", 0);
                    if(readMsgNum==0){
                        tvSUnreadMsgNum.setVisibility(View.INVISIBLE);
                    }else{
                        tvSUnreadMsgNum.setVisibility(View.VISIBLE);
                        tvSUnreadMsgNum.setText(readMsgNum+"");
                    }
                    if (null != friendListModel) {
                        List<UserBase> list = friendListModel.getListUser();
                        if (!Utils.isListEmpty(list)) {
                            if(friendApplyList.size()==0){
                                tvFriendMsg.setVisibility(View.VISIBLE);
                                friendApplyList=friendListModel.getListUser();
                            }else {
                                for (int i = 0; i < list.size(); i++) {
                                    long guid = list.get(i).getGuid();
                                    for (int j = 0; j < friendApplyList.size(); j++) {
                                        if(friendApplyList.size()>i){
                                            if(guid==friendApplyList.get(i).getGuid()){
                                                haveValue=true;
                                            }
                                        }

                                    }
                                    if(!haveValue){
                                        haveValue=false;
                                        tvFriendMsg.setVisibility(View.VISIBLE);
                                        friendApplyList.add(list.get(i));
                                    }
                                }
                            }


                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                //        设置小秘书的未读消息
                int readMsgNum = SharedPreferenceUtil.getIntValue(mContext, "Read_Msg_Num", "10000", 0);
                if(readMsgNum==0){
                    tvSUnreadMsgNum.setVisibility(View.INVISIBLE);
                }else{
                    tvSUnreadMsgNum.setVisibility(View.VISIBLE);
                    tvSUnreadMsgNum.setText(readMsgNum+"");
                }
            }
        });
    }


    private void load() {

        // 查询未读消息数
        allAcount=DbModle.getInstance().getUserAccountDao().getAllAcount();
        ApiManager.getFriendList(pageNum, new IGetDataListener<FriendListModel>() {
            @Override
            public void onResult(FriendListModel friendListModel, boolean isEmpty) {
                if (isEmpty) {
                    if (pageNum == 1) {
//                        mFollowIview.toggleShowEmpty(true, null);
                    } else if (pageNum > 1) {
                        mFollowIview.showNoMore();
                        mFollowIview.hideLoadMore();
                    }
                } else {
                    if (null != friendListModel) {
                         list = friendListModel.getListUser();
                        if (!Utils.isListEmpty(list)) {
                            if(pageNum==1){
                                if(mFollowAdater!=null)
                                mFollowAdater.replaceAll(list);
                            }else if(pageNum>1){
                                if(mFollowAdater!=null)
                                    mFollowAdater.appendToList(list);
                            }
                        }
                    }
                }
                mFollowIview.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mFollowIview.hideRefresh(1);
                if (!isNetworkError) {
                    mFollowIview.toggleShowError(true, msg);
                }

            }
        });


    }

//    private List<UserBase> getAboutNameList(List<UserBase> list) {
//        if (list != null && list.size() > 0) {
//            List<UserBase> followNameList = new ArrayList<>();
//            for (int i = 0; i < list.size(); i++) {
//                followNameList.add(new FollowName(list.get(i).getNickName(),
//                        String.valueOf(list.get(i).getGuid()), list.get(i).getIcon()));
//            }
//            Collections.sort(followNameList);
//            return followNameList;
//        } else {
//            return null;
//        }
//    }


    /**
     * 上拉加载更多
     */
    private void loadMore() {
        mFollowIview.showLoadMore();
        mFollowIview.toggleShowError(false, null);
        pageNum++;
        load();
    }
}
