package com.wiscom.vchat.ui.personalcenter.contract;

import android.support.v4.app.FragmentManager;

import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;

/**
 * Created by Administrator on 2017/7/7.
 */

public interface WithdrawToPaypalContract {



    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        void finishActivity();

        FragmentManager obtainFragmentManager();

        String getCountry();

        void setCountry(String country);
    }
    interface IPresenter extends BasePresenter {
        void selectCountry();

        void withdrawSure(String getMoney,String getEmail, String getName, String getSur, String getNation);
    }
}
