package com.wiscom.vchat.ui.message.presenter;

import android.content.Context;

import com.wiscom.vchat.common.RefreshRecyclerView;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.CallRecord;
import com.wiscom.vchat.data.model.VideoRecord;
import com.wiscom.vchat.ui.message.contract.VideoHistoryContract;
import com.wiscom.library.util.Utils;

import java.util.List;

/**
 * Created by zhangdroid on 2017/7/6.
 */
public class VideoHistoryPresenter implements VideoHistoryContract.IPresenter {
    private VideoHistoryContract.IView mVideoHistoryView;
    private Context mContext;
    private int pageNum = 1;
    private static final String pageSize = "20";

    public VideoHistoryPresenter(VideoHistoryContract.IView view) {
        this. mVideoHistoryView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void finish() {
        mVideoHistoryView = null;
        mContext = null;
    }

    @Override
    public void loadHistoryList() {
        mVideoHistoryView.setOnLoadMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                loadMore();
            }
        });
        load();
    }

    @Override
    public void refresh() {
        pageNum = 1;
        load();
    }

    @Override
    public void loadMore() {
        mVideoHistoryView.showLoadMore();
        pageNum++;
        load();
    }

    private void load() {
        ApiManager.getVideoCallRecord(pageNum, pageSize, new IGetDataListener<CallRecord>() {

            @Override
            public void onResult(CallRecord callRecord, boolean isEmpty) {
                if (isEmpty) {
                    if (pageNum == 1) {
                        mVideoHistoryView.toggleShowEmpty(true, null);
                    } else if (pageNum > 1) {
                        mVideoHistoryView.showNoMore();
                    }
                } else {
                    if (null != callRecord) {
                        List<VideoRecord> list = callRecord.getListRecord();
                        if (!Utils.isListEmpty(list)) {
                            if (pageNum == 1) {
                                mVideoHistoryView.getVideoHistoryAdapter().replaceAll(list);
                            } else if (pageNum > 1) {
                                mVideoHistoryView.getVideoHistoryAdapter().appendToList(list);
                            }
                            mVideoHistoryView.hideLoadMore();
                        }
                    }
                }
                mVideoHistoryView.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mVideoHistoryView.hideRefresh(1);
                if (!isNetworkError) {
                    mVideoHistoryView.toggleShowError(true, msg);
                }
            }
        });
    }

}
