package com.wiscom.vchat.ui.follow.presenter;

import android.content.Context;
import android.view.View;

import com.wiscom.vchat.R;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.FollowName;
import com.wiscom.vchat.data.model.FollowUser;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.parcelable.UserDetailParcelable;
import com.wiscom.vchat.ui.detail.UserDetailActivity;
import com.wiscom.vchat.ui.follow.FindUserActivity;
import com.wiscom.vchat.ui.follow.adapter.FollowAdapter;
import com.wiscom.vchat.ui.follow.contract.FollowContract;
import com.wiscom.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.adapter.wrapper.OnLoadMoreListener;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 */

public class FollowPresenter implements FollowContract.IPresenter {
    private FollowContract.IView mFollowIview;
    private Context mContext;
    private FollowAdapter mFollowAdater;
    List<FollowName> aboutNameList;
    private int pageNum = 1;

    public FollowPresenter(FollowContract.IView mFollowIview) {
        this.mFollowIview = mFollowIview;
        this.mContext = mFollowIview.obtainContext();
    }


    @Override
    public void start() {

    }

    @Override
    public void finish() {
        mFollowIview = null;
        mContext = null;
        mFollowAdater = null;
    }

    @Override
    public void loadFollowUserList() {
        mFollowIview.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadMore();
            }
        });
        load();
    }

    public void initData() {
        mFollowAdater = new FollowAdapter(mContext, R.layout.follow_list_item);
        mFollowAdater.setFragmentManager(mFollowIview.obtainFragmentManager());
        mFollowAdater.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                FollowName itemByPosition = mFollowAdater.getItemByPosition(position);
                if (null != itemByPosition) {
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(String.valueOf(itemByPosition.getRemoteUid())));
                }
            }
        });
        // 设置加载更多
        mFollowIview.setAdapter(mFollowAdater, R.layout.common_load_more);
    }

    @Override
    public void refresh() {
        if (!Utils.isListEmpty(aboutNameList)) {
            aboutNameList.clear();
        }
        pageNum = 1;
        mFollowIview.toggleShowError(false, null);
        load();
    }

    @Override
    public void getSlidingWord(String word) {
        for (int i = 0; i < aboutNameList.size(); i++) {
            String listWord = aboutNameList.get(i).getPinyin().substring(0, 1);//获取拼音的第一个字母
            if (word.equals(listWord.toUpperCase())) {
                //i是recyclerView中的位置
                mFollowIview.recyclerViewMoveTo(i);//定位到recyclerView中的某个位置
            }
        }
    }

    @Override
    public void goToSearchPage() {
        LaunchHelper.getInstance().launch(mContext, FindUserActivity.class);
    }

    private void load() {
        ApiManager.getFollowList(pageNum, new IGetDataListener<FollowUser>() {
            @Override
            public void onResult(FollowUser followUser, boolean isEmpty) {
                if (isEmpty) {
                    if (pageNum == 1) {
                        mFollowIview.toggleShowEmpty(true, null);
                    } else if (pageNum > 1) {
                        mFollowIview.showNoMore();
                        mFollowIview.hideLoadMore();
                    }

                } else {
                    if (null != followUser) {
                        List<UserBase> list = followUser.getUserBaseList();
                        if (!Utils.isListEmpty(list)) {
                            aboutNameList = getAboutNameList(list);
                            initData();
                            mFollowAdater.bind(aboutNameList);
                            mFollowIview.showNoMore();
                            mFollowIview.hideLoadMore();
                        }
                    }
                }
                mFollowIview.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mFollowIview.hideRefresh(1);
                if (!isNetworkError) {
                    mFollowIview.toggleShowError(true, msg);
                }
            }
        });
    }

    private List<FollowName> getAboutNameList(List<UserBase> list) {
        if (list != null && list.size() > 0) {
            List<FollowName> followNameList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                followNameList.add(new FollowName(list.get(i).getNickName(),
                        String.valueOf(list.get(i).getGuid()), list.get(i).getIcon()));
            }
            Collections.sort(followNameList);
            return followNameList;
        } else {
            return null;
        }
    }


    /**
     * 上拉加载更多
     */
    private void loadMore() {
        mFollowIview.showLoadMore();
        mFollowIview.toggleShowError(false, null);
        pageNum++;
        load();
    }
}
