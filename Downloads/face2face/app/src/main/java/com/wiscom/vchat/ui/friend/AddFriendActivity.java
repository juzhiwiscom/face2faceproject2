package com.wiscom.vchat.ui.friend;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.ApplyAddFriendMode;
import com.wiscom.vchat.data.model.MyInfo;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserDetail;
import com.wiscom.vchat.event.UpdateFriendListEvent;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.net.NetUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tianzhentao on 2018/1/5.
 */

public class AddFriendActivity extends BaseTopBarActivity {
    @BindView(R.id.et_add_friend)
    EditText etAddFriend;
    @BindView(R.id.add_friend_delete)
    ImageView addFriendDelete;
    @BindView(R.id.add_friend_icon)
    ImageView addFriendIcon;
    @BindView(R.id.add_friend_name)
    TextView addFriendName;
    @BindView(R.id.add_friend_btn)
    Button addFriendBtn;
    @BindView(R.id.add_friend_rl)
    LinearLayout addFriendRl;
    @BindView(R.id.add_friend_search)
    ImageView addFriendSearch;
    private UserBase userBase;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_add_friend;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.add_friend);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mIvLeft.setVisibility(View.VISIBLE);

    }

    @Override
    protected void setListeners() {
        mIvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        etAddFriend.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // 输入的内容变化的监听
                Log.e("输入过程中执行该方法", "文字变化");
                if (s.length() > 0) {
                    addFriendDelete.setVisibility(View.VISIBLE);
                }
//                if (s.length() == 9) {
////                    请求接口
//                    showLoading();
//                    request(etAddFriend.getText().toString());
//                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // 输入前的监听
                Log.e("输入前确认执行该方法", "开始输入");

            }

            @Override
            public void afterTextChanged(Editable s) {
                // 输入后的监听
                Log.e("输入结束执行该方法", "输入结束");

            }
        });
        addFriendSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    请求接口
                showLoading();
                request(etAddFriend.getText().toString());

            }
        });
        addFriendDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    etAddFriend.getText().delete(0,etAddFriend.length());
                etAddFriend.setText("");
            }
        });

        addFriendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                申请添加好友
                ApiManager.applyAddFriend(String.valueOf(userBase.getGuid()), new IGetDataListener<ApplyAddFriendMode>() {
                    @Override
                    public void onResult(ApplyAddFriendMode baseModel, boolean isEmpty) {
                        String isSucceed = baseModel.getIsSucceed();
                        if (!TextUtils.isEmpty(isSucceed) && isSucceed.equals("1")) {
                            addFriendBtn.setBackgroundResource(R.mipmap.friend_is_friend);
                            addFriendBtn.setText(getString(R.string.friend_applying));
                            addFriendBtn.setClickable(false);
                        }

                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {

                    }
                });
            }
        });
    }
    private void request(String mAccount) {
        addFriendRl.setVisibility(View.GONE);
        ApiManager.getFriendFromId(mAccount, new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                dismissLoading();
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            addFriendRl.setVisibility(View.VISIBLE);
                            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userBase.getIconUrl())
                                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(addFriendIcon).build());
                            addFriendName.setText(userBase.getNickName());
                            int isFriend = userDetail.getIsFriend();
                            if (isFriend == 1) {
                                addFriendBtn.setBackgroundResource(R.mipmap.friend_is_friend);
                                addFriendBtn.setText(R.string.friend_is_friend);
                                addFriendBtn.setClickable(false);
                            } else {
                                addFriendBtn.setBackgroundResource(R.mipmap.friend_add_friend);
                                addFriendBtn.setText(R.string.add_friend);
                                addFriendBtn.setClickable(true);
                            }
                        }
                    }
                } else {
//                    toggleShowEmpty(false,getString(R.string.friend_noting),null);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                dismissLoading();
                if (isNetworkError) {
                    showNetworkError();
                }
            }
        });
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    public void showLoading() {
//        toggleShowLoading(true, null);
    }

    public void dismissLoading() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                toggleShowLoading(false, null);
            }
        }, 1_000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//            刷新好友列表
        EventBus.getDefault().post(new UpdateFriendListEvent());
    }
}
