package com.wiscom.vchat.ui.video;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.wiscom.vchat.R;
import com.wiscom.vchat.event.ExitVideoEvent;
import com.wiscom.vchat.event.InterruptedVIPEvent;

import org.greenrobot.eventbus.EventBus;

public class AnnulusView extends View {

    // 画实心圆的画笔
    private Paint mCirclePaint;
    // 画圆环的画笔
    private Paint mRingDefaultPaint;
    // 已用环的画笔
    private Paint mUsePaint;
    // 画圆环的画笔
    private Paint mRingPaint;
    // 画白线的画笔
    private Paint mLinePaint;
    // 画字体的画笔
    private Paint mTextPaint;
    // 圆形颜色
    private int mCircleColor;
    // 圆环颜色
    private int mRingColor;
    // 半径
    private float mRadius;
    // 圆环半径
    private float mRingRadius;
    // 圆环宽度
    private float mStrokeWidth;
    // 圆心x坐标
    private int mXCenter;
    // 圆心y坐标
    private int mYCenter;
    // 字的长度
    private float mTxtWidth;
    // 字的高度
    private float mTxtHeight;
    // 总进度
    private int mTotalProgress = 100;
    // 当前进度
    private float mProgress;
    // 实际展示总进度
    private int mShowProgress;
    // 已用流量
//    private String usedFlow;

    private Context mContext;
    private String rotateColor;
    private float iii=0;
    private int nnnn=0;

    private Handler circleHandler = new Handler(){

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == 1){
                float temp = (float) msg.obj;
                setProgress(temp);
            }
        }
    };
    private Handler handler = new Handler(){

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == 1){
                float temp = (float) msg.obj;
                setProgress(temp);
                handler.postDelayed(r,100);//将线程重新加入到消息队列中，产生一种循环
            }
        }
    };
    Runnable r=new Runnable(){
        @Override
        public void run() {
            Message msg = handler.obtainMessage(); //获得message对象
            msg.what=1;
            if(iii < (mShowProgress-17)){
                iii = (float) (iii+0.22);
            }else if(iii <mShowProgress){
                iii = (float) (iii+0.22);
                nnnn++;
                if(nnnn%4==0){
                    mRingPaint.setColor(Color.parseColor("#FF333F"));
                    mRingDefaultPaint.setColor(getResources().getColor(R.color.divider_color));
                }else{
                    mRingPaint.setColor(getResources().getColor(R.color.divider_color));
                    mRingDefaultPaint.setColor(getResources().getColor(R.color.divider_color));
                }
            }else{
                return;
            }
            msg.obj = iii;
            handler.sendMessage(msg); //发送消息搜索
        }
    };

    private Message msgRotate;
    private boolean time=false;
    private boolean first10=true;
    private boolean first0=true;

    public AnnulusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        // 获取自定义的属性
        initAttrs(context, attrs);
        initVariable();
    }

    private void initAttrs(Context context, AttributeSet attrs) {
        TypedArray typeArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.AnnulusView, 0, 0);
        mRadius = typeArray.getDimension(R.styleable.AnnulusView_radius, 30);
        mStrokeWidth = typeArray.getDimension(R.styleable.AnnulusView_strokeWidth, 2);
        mCircleColor = typeArray.getColor(R.styleable.AnnulusView_circleColor, 000000000);
        mRingColor = typeArray.getColor(R.styleable.AnnulusView_ringColor, 0xFFFFFFFF);
        mRingRadius = mRadius + mStrokeWidth / 2;
    }

    private void initVariable() {
        //画圆画笔设置
        mCirclePaint = new Paint();
        mCirclePaint.setAntiAlias(true);//防锯齿
        mCirclePaint.setColor(mCircleColor);
        mCirclePaint.setStyle(Paint.Style.FILL);

        //“使用”字画笔设置
        mUsePaint = new Paint();
        mUsePaint.setAntiAlias(true);
        mUsePaint.setStyle(Paint.Style.FILL);
        mUsePaint.setColor(getResources().getColor(R.color.base_color));
        mUsePaint.setTextSize(20);

        //圆环画笔设置
        mRingDefaultPaint = new Paint();
        mRingDefaultPaint.setAntiAlias(true);
        mRingDefaultPaint.setColor(getResources().getColor(R.color.divider_color));
        mRingDefaultPaint.setStyle(Paint.Style.STROKE);
        mRingDefaultPaint.setStrokeWidth(mStrokeWidth);

        //已使用多少圆环画笔设置
        mRingPaint = new Paint();
        mRingPaint.setAntiAlias(true);
        mRingPaint.setColor(mRingColor);
        mRingPaint.setStyle(Paint.Style.STROKE);
        mRingPaint.setStrokeWidth(mStrokeWidth);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTextSize(30);

        mLinePaint = new Paint();
        mLinePaint.setColor(Color.WHITE);


        //获取字体高度
        FontMetrics fm = mTextPaint.getFontMetrics();
        mTxtHeight = (int) Math.ceil(fm.descent - fm.ascent);

    }

    @Override
    protected void onDraw(Canvas canvas) {

        mXCenter = getWidth() / 2;
        mYCenter = getHeight() / 2;

        //画圆
        canvas.drawCircle(mXCenter, mYCenter, mRadius, mCirclePaint);

        RectF oval = new RectF();
        oval.left = (mXCenter - mRingRadius);
        oval.top = (mYCenter - mRingRadius);
        oval.right = mRingRadius * 2 + (mXCenter - mRingRadius);
        oval.bottom = mRingRadius * 2 + (mYCenter - mRingRadius);
        //画整圆弧
        canvas.drawArc(oval, -90, 360, false, mRingDefaultPaint);
        //已使用多少圆弧
        canvas.drawArc(oval, -90, (mProgress / mTotalProgress) * 360, false, mRingPaint);
        //文字绘制
        String txt ="";
        if(time){
             txt=60-(int) (mProgress*3/5)+"";
            if(txt.equals("10") && first10){
                first10=false;
                EventBus.getDefault().post(new InterruptedVIPEvent());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        first10=true;
                    }
                },8000);
            }
            if(txt.equals("0") && first0){
                first0=false;
                EventBus.getDefault().post(new ExitVideoEvent());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        first0=true;
                    }
                },8000);
            }

        }else{
            if(mProgress<33){
                txt="3";
            }else if(mProgress<80 && mProgress>32){
                txt="2";
            }else{
                txt="1";
            }
        }


        //文字的长度
        mTxtWidth = mTextPaint.measureText(txt, 0, txt.length());
        canvas.drawText(txt, mXCenter - mTxtWidth/2 , mYCenter + mTxtHeight/4, mTextPaint);

//        Rect _pb = new Rect();
//        String sup = "已用";
//        mUsePaint.getTextBounds(sup, 0, sup.length(), _pb);
//        int perX = mXCenter - _pb.width() / 2;
//        canvas.drawText(sup, perX, mYCenter / 2, mUsePaint);

//        if (!TextUtils.isEmpty(usedFlow)) {
//            mUsePaint.getTextBounds(usedFlow, 0, usedFlow.length(), _pb);
//            perX = mXCenter - _pb.width() / 2;
//            canvas.drawText(usedFlow, perX, (float) (mYCenter + mYCenter / 1.7), mUsePaint);
//        }

        //画横线图片
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.circle_bottom_bg);
//        perX = mXCenter - bitmap.getWidth() / 2;
//        canvas.drawBitmap(bitmap, perX, (float) (mYCenter + mYCenter / 5), mLinePaint);
    }

    /**
     * 设置当前进度
     * @param progress
     */
    public void setProgress(float progress) {
        Log.v("progressprogress==", String.valueOf(progress));
        mProgress = progress;
        postInvalidate();
    }
    /**
     * 无限循环旋转圆环
     * @param color 旋转颜色
     */
    public void setCirculation(String color,int progress,boolean fromTime) {
//        if(msgRotate!=null){
//            circleHandler.removeMessages(msgRotate.what);
//        }
        time=fromTime;
        iii=0;
        nnnn=0;
        handler.removeCallbacks(r);
        mRingPaint.setColor(Color.parseColor(color));
        mShowProgress = progress;
        rotateColor = color;
        handler.post(r); //将线程加入到消息队列中
//        new Thread(new CircleRotateThread()).start();
    }
    /**
     * 实际展示总进度
     * @param progress
     */
    public void setmShowProgress(int progress) {
        mShowProgress = progress;
        new Thread(new CircleThread()).start();
    }
    /**
     * 当加载时间不定时，设置先快后慢的进度条
     * @param progress
     */
    public void setChangeProgress(int progress) {
        mShowProgress = progress;
        new Thread(new CircleTimeThread()).start();
    }

    /**
     * 设置圆环颜色
     * @param color
     */
    public void setRingColor(String color) {
        mRingPaint.setColor(Color.parseColor(color));
    }
    /**
     * 设置字体隐藏
     * @param
     */
    public void setHideFont() {
        mTextPaint.setAlpha(0);
    }


    public void setUsedFlow(String usedFlow) {
//        this.usedFlow = usedFlow;
    }

    public void setDefaulRingColor(String color) {
        mRingDefaultPaint.setColor(Color.parseColor(color));

    }


    private class CircleThread implements Runnable{
        int m=0;
        int i=0;
        @Override
        public void run() {
            // TODO Auto-generated method stub
            while(!Thread.currentThread().isInterrupted()){
                try {
                    Thread.sleep(150);
                    m++;
                    Message msg = new Message();
                    msg.what = 1;
                    if(i < mShowProgress){
                        i += m;
                    }else{
                        i = mShowProgress;
                        return;
                    }
                    msg.obj = (float)i;
                    circleHandler.sendMessage(msg);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

    }
    private class CircleTimeThread implements Runnable{
        int m=0;
        int i=0;
        int n=250;
        @Override
        public void run() {
            // TODO Auto-generated method stub
            while(!Thread.currentThread().isInterrupted()){
                try {

                    if(i>50 && i<=70)
                        n=5000;
                    if(i>70 && i<=90)
                        n=8000;
                    if(i>90)
                        n=1000000;
                    Thread.sleep(n);
                    m++;
                    Message msg = new Message();
                    msg.what = 1;
                    if(i < mShowProgress){
                        i += m;
                    }else{
                        i = mShowProgress;
                        return;
                    }
                    msg.obj = (float)i;
                    circleHandler.sendMessage(msg);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

    }
    private class CircleRotateThread implements Runnable{
        int m=0;
        float i=0;
        int n=0;
        @Override
        public void run() {
            // TODO Auto-generated method stub
            while(!Thread.currentThread().isInterrupted()){
                try {
                    Thread.sleep(100);
                     msgRotate = new Message();
                    msgRotate.what = 1;
                    if(i < (mShowProgress-20)){
                        i = (float) (i+0.2);
                    }else if(i <mShowProgress){
                        i = (float) (i+0.2);
                        n++;
                        if(n%4==0){
                            mRingPaint.setColor(Color.parseColor("#FF333F"));
                            mRingDefaultPaint.setColor(getResources().getColor(R.color.divider_color));
                        }else{
                            mRingPaint.setColor(getResources().getColor(R.color.divider_color));
                            mRingDefaultPaint.setColor(getResources().getColor(R.color.divider_color));
                        }
                    }else{
                        return;
                    }
                    msgRotate.obj = i;
                    circleHandler.sendMessage(msgRotate);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

    }
}

