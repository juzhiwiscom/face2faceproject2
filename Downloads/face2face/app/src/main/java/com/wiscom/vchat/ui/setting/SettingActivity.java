package com.wiscom.vchat.ui.setting;

import android.content.Context;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.library.dialog.AlertDialog;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.DeviceUtil;
import com.wiscom.library.util.SnackBarUtil;

import butterknife.BindView;

/**
 * 设置页面
 * Created by zhangdroid on 2017/5/12.
 */
public class SettingActivity extends BaseTopBarActivity implements View.OnClickListener, SettingContract.IView {
    @BindView(R.id.setting_root)
    LinearLayout mLlRoot;
    @BindView(R.id.setting_switch)
    Switch mSwitch;
    @BindView(R.id.setting_clear_cache)
    RelativeLayout mRlClearCache;
    @BindView(R.id.setting_cache_size)
    TextView mTvCacheSize;
    @BindView(R.id.setting_grade)
    RelativeLayout mRlGrade;
    @BindView(R.id.setting_logout)
    Button mBtnLogout;
    @BindView(R.id.setting_version)
    TextView mTvVersion;
    @BindView(R.id.setting_change_password)
    RelativeLayout mRlChangPassword;

    private SettingPresenter mSettingPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_setting;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.title_setting);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected void initViews() {
        mSettingPresenter = new SettingPresenter(this);
    }

    @Override
    protected void setListeners() {
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mSettingPresenter.toggleNoDistrub(isChecked);
            }
        });
        mRlClearCache.setOnClickListener(this);
        mRlGrade.setOnClickListener(this);
        mBtnLogout.setOnClickListener(this);
        mRlChangPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setting_clear_cache:// 清除缓存
                mSettingPresenter.clearCache();
                break;

            case R.id.setting_grade:// 去应用商店评分
                DeviceUtil.showGooglePlayDetail(mContext);
                break;

            case R.id.setting_logout:// 登出
                mSettingPresenter.logout();
                break;
            case R.id.setting_change_password://更改密码
                mSettingPresenter.changePassword();
                break;
        }
    }

    @Override
    protected void loadData() {
        mSettingPresenter.getNoDistrubState();
        mSettingPresenter.getCacheSize();
        mSettingPresenter.getVersionName();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSettingPresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void toggleNoDistrub(boolean toggle) {
        mSwitch.setChecked(toggle);
    }

    @Override
    public void setCacheSize(String cacheSize) {
        mTvCacheSize.setText(getString(R.string.setting_clear_cache, cacheSize));
    }

    @Override
    public void setVersion(String versionName) {
        mTvVersion.setText(getString(R.string.setting_version, versionName));
    }

    @Override
    public void showAlertDialog(String message, OnDialogClickListener listener) {
        AlertDialog.show(getSupportFragmentManager(), getString(R.string.alert), message,
                getString(R.string.positive), getString(R.string.negative), listener);
    }

}
