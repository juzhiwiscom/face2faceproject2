package com.wiscom.vchat.data.model;

import java.util.List;

/**
 * Created by tianzhentao on 2018/4/8.
 *     * 推荐秀：最新视频
 */

public class RecommendNewJoinList extends BaseModel{
    private List<VideoSquare> videoSquareList;//用户视频秀集合

    public List<VideoSquare> getVideoSquareList() {
        return videoSquareList;
    }

    public void setVideoSquareList(List<VideoSquare> videoSquareList) {
        this.videoSquareList = videoSquareList;
    }
}
