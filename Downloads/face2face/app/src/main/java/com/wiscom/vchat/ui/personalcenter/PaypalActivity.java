package com.wiscom.vchat.ui.personalcenter;

import android.os.Parcelable;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.library.net.NetUtil;

/**
 * Paypal提现页面
 * Created by zhangdroid on 2017/5/27.
 */
public class PaypalActivity extends BaseTopBarActivity {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_paypal;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_withdraw);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        setStatusBar(getResources().getColor(R.color.main_color));
    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }
}
