package com.wiscom.vchat.ui.video;

import android.content.Context;

import com.wiscom.vchat.R;
import com.wiscom.vchat.data.model.VideoMsg;
import com.wiscom.library.adapter.CommonRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;

/**
 * 视频时消息列表适配器
 * Created by zhangdroid on 2017/6/22.
 */
public class VideoMsgAdapter extends CommonRecyclerViewAdapter<VideoMsg> {

    public VideoMsgAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(VideoMsg videoMsg, int position, RecyclerViewHolder holder) {
        if (null != videoMsg) {
            holder.setText(R.id.item_video_nickname, videoMsg.getNickname());
            holder.setText(R.id.item_video_msg, videoMsg.getMessage());
        }
    }

}
