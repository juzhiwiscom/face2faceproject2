package com.wiscom.vchat.ui.login;

import android.content.Context;
import android.text.TextUtils;

import com.wiscom.vchat.R;
import com.wiscom.vchat.common.HyphenateHelper;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.FindPassword;
import com.wiscom.vchat.data.model.HostInfo;
import com.wiscom.vchat.data.model.Login;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserBean;
import com.wiscom.vchat.data.model.UserDetail;
import com.wiscom.vchat.data.preference.AnchorPreference;
import com.wiscom.vchat.data.preference.BeanPreference;
import com.wiscom.vchat.data.preference.PlatformPreference;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.FinishEvent;
import com.wiscom.vchat.ui.main.MainActivity;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by zhangdroid on 2017/6/1.
 */
public class LoginPresenter implements LoginContract.IPresenter {
    private LoginContract.IView mLoginView;
    private Context mContext;
    private boolean isFrist=true;

    public LoginPresenter(LoginContract.IView view) {
        this.mLoginView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
        // 获取本地保存的账号密码并设置
        String account = UserPreference.getAccount();
        String password = UserPreference.getPassword();
        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
            mLoginView.setAccount(account);
            mLoginView.setPassword(password);
        }
    }

    @Override
    public void finish() {
        mLoginView = null;
        mContext = null;
    }

    @Override
    public void login() {
        mLoginView.showLoading();
        ApiManager.login(mLoginView.getAccount(), mLoginView.getPassword(), new IGetDataListener<Login>() {
            @Override
            public void onResult(Login login, boolean isEmpty) {
                String token = login.getToken();
                PlatformPreference.setToken(token);
                UserDetail userDetail = login.getUserDetail();
                if (null != userDetail) {
                    // 保存用户相关信息
                    UserBean userBean = userDetail.getUserBean();
                    if (null != userBean) {
                        BeanPreference.saveUserBean(userBean);
                    }
                    HostInfo hostInfo = userDetail.getHostInfo();
                    if (null != hostInfo) {
                        AnchorPreference.saveHostInfo(hostInfo);
                    }
                    UserBase userBase = userDetail.getUserBase();
                    if (null != userBase) {
                        // 保存用户信息到本地
                        UserPreference.saveUserInfo(userBase);
//                        huanXin(userBase);
                        handleHyphenateLoginResult();

                    } else {
//                        if(isFrist)
                            handleHyphenateLoginResult();
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if(mLoginView!=null){
                    mLoginView.dismissLoading();
                    if (isNetworkError) {
                        mLoginView.showNetworkError();
                    } else {
                        mLoginView.showTip(msg);
                    }
                }

            }
        });
    }

    private void huanXin(final UserBase userBase) {
        // 登录环信
        HyphenateHelper.getInstance().login(userBase.getAccount(), userBase.getPassword(), new HyphenateHelper.OnLoginCallback() {
            @Override
            public void onSuccess() {
                if(isFrist)
                    handleHyphenateLoginResult();
            }

            @Override
            public void onFailed() {
                huanXin(userBase);
                if(isFrist)
                handleHyphenateLoginResult();
            }
        });
    }

    @Override
    public void findPassword() {
        mLoginView.showLoading();
        ApiManager.findPassword(new IGetDataListener<FindPassword>() {
            @Override
            public void onResult(FindPassword findPassword, boolean isEmpty) {
                mLoginView.dismissLoading();
                if(findPassword.getAccount()==null || findPassword.getPassword()==null){
                    ToastUtil.showShortToast(mContext, String.valueOf(R.string.input_account));
                }else{
                    mLoginView.showFindPwdDialog(findPassword.getAccount(), findPassword.getPassword());
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if(mLoginView!=null){
                    mLoginView.dismissLoading();
                }
            }

        });
    }

    private void handleHyphenateLoginResult() {
//        isFrist=false;
        if(mLoginView!=null){
            mLoginView.dismissLoading();
        }//        ToastUtil.showShortToast(mContext, mContext.getString(R.string.login_success));
        // 关闭之前打开的页面
        EventBus.getDefault().post(new FinishEvent());
        // 跳转主页面
        if(mContext!=null)
        LaunchHelper.getInstance().launchFinish(mContext, MainActivity.class);
    }

}
