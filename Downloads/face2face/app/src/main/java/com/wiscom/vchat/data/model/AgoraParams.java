package com.wiscom.vchat.data.model;

/**
 * 声网信令系统呼叫时需要传递的参数对象
 * Created by zhangdroid on 2017/6/15.
 */
public class AgoraParams {
    /**
     * 被邀请人id
     */
    private long uId;
    /**
     * 邀请人昵称
     */
    private String nickname;
    /**
     * 被邀请人头像
     */
    private String imgageUrl;
    /**
     * 被邀请人国家
     */
    private String country;
    /**
     * 被邀请人性别
     */
    private String genter;
    /**
     * 进入视频通话的来源界面：1受邀请界面，2邀请别人加入视频的界面，3随机视频的界面
     */
    public String fromView;


    public AgoraParams(long uId, String nickname, String imgageUrl) {
        this.uId = uId;
        this.nickname = nickname;
        this.imgageUrl = imgageUrl;
    }

    public String getFromView() {
        return fromView;
    }

    public void setFromView(String fromView) {
        this.fromView = fromView;
    }

    public AgoraParams(long uId, String nickname, String imgageUrl, String country, String genter, String fromView) {
        this.uId = uId;
        this.nickname = nickname;
        this.imgageUrl = imgageUrl;
        this.country=country;
        this.genter=genter;
        this.fromView=fromView;

    }


    public long getuId() {
        return uId;
    }

    public void setuId(long uId) {
        this.uId = uId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getImgageUrl() {
        return imgageUrl;
    }

    public void setImgageUrl(String imgageUrl) {
        this.imgageUrl = imgageUrl;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGenter() {
        return genter;
    }

    public void setGenter(String genter) {
        this.genter = genter;
    }
}
