package com.wiscom.vchat.ui.chat;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.SnackBarUtil;
import com.wiscom.library.widget.AutoSwipeRefreshLayout;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.common.HyphenateHelper;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.common.VideoHelper;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.event.MessageArrive;
import com.wiscom.vchat.event.SeeDetailEvent;
import com.wiscom.vchat.event.UnreadMsgChangedEvent;
import com.wiscom.vchat.event.UpdateFriendListEvent;
import com.wiscom.vchat.parcelable.ChatParcelable;
import com.wiscom.vchat.parcelable.UserDetailParcelable;
import com.wiscom.vchat.parcelable.VideoInviteParcelable;
import com.wiscom.vchat.ui.detail.UserDetailActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 聊天页面
 * Created by zhangdroid on 2017/5/27.
 */
public class ChatActivity extends BaseTopBarActivity implements ChatContract.IView, View.OnClickListener {
    @BindView(R.id.chat_root)
    RelativeLayout mRlRoot;
    @BindView(R.id.chat_refresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.chat_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.chat_switch)
    ImageView mIvSwitch;
    @BindView(R.id.chat_text_input)
    EditText mEtInput;
    @BindView(R.id.chat_voice)
    Button mBtnVoiceSend;
    @BindView(R.id.chat_send)
    Button mBtnSend;
    @BindView(R.id.chat_more)
    ImageView mIvMore;
    @BindView(R.id.chat_more_container)
    LinearLayout mLlMore;
    @BindView(R.id.chat_album)
    TextView mTvAlbum;
    @BindView(R.id.chat_camera)
    TextView mTvCamera;
    @BindView(R.id.chat_vedio)
    TextView mTvVedio;

    // 录音框
    private PopupWindow mRecordPopupWindow;
    private int mRecordDuration;
    // 标记是否初次加载
    private boolean mIsFirstLoad;
    private LinearLayoutManager mLinearLayoutManager;
    private ChatAdapter mChatAdapter;
    private ChatParcelable mChatParcelable;
    private ChatPresenter mChatPresenter;
    // 异步任务
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == C.message.MSG_TYPE_VOICE_UI_TIME) {
                mRecordDuration++;
                if (null != mRecordPopupWindow) {
                    TextView tvTime = (TextView) mRecordPopupWindow.getContentView().findViewById(R.id.popup_record_duration);
                    tvTime.setText(Util.convertSecondsToString(mRecordDuration));
                }
                mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1_000);
            } else {
                mChatPresenter.handleAsyncTask(msg);
            }
        }
    };
    private String resonCode;
    private boolean haveRecordPermission=false;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_chat;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mChatParcelable = (ChatParcelable) parcelable;
        EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));
    }

    @Override
    protected void initViews() {


        // 设置Notice替换View
        setNoticeView(mSwipeRefreshLayout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mChatAdapter = new ChatAdapter(this, mChatParcelable.imageUrl);
        mRecyclerView.setAdapter(mChatAdapter);
        mChatPresenter = new ChatPresenter(this, mChatParcelable.account, mChatParcelable.guid);
        mChatPresenter.start();

        setTitle(mChatParcelable.nickname);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        mIvSwitch.setOnClickListener(this);
        mBtnSend.setOnClickListener(this);
        mIvMore.setOnClickListener(this);
        mTvAlbum.setOnClickListener(this);
        mTvCamera.setOnClickListener(this);
        mRlRoot.setOnClickListener(this);
        mTvVedio.setOnClickListener(this);

        if(mChatParcelable.guid!=10000){//小秘书
            mIvRight.setVisibility(View.VISIBLE);
//            mIvRight.setBackgroundResource(R.drawable.set_report);
            mIvRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                           report();
                }
            });
        }else{
            mTvVedio.setVisibility(View.GONE);//小秘书隐藏视频通话
        }
        // 下拉刷新，加载之前的聊天记录
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                    hideRefresh();
                } else {
                    mChatPresenter.refresh();
                }
            }
        });
        mEtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mEtInput.getText().length() > 0) {
                    mBtnSend.setEnabled(true);
                    mBtnSend.setTextColor(getResources().getColor(R.color.main_color));
                } else {
                    mBtnSend.setEnabled(false);
                    mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mBtnVoiceSend.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:// 按下时开始录音
//                        录音权限
                        getRecordPermission();
                        if(haveRecordPermission){
                            mBtnVoiceSend.setText(getString(R.string.chat_loose_to_end));
                            mChatPresenter.handleTouchEventDown();
                            // 开始计时
                            mRecordDuration = 0;
                            mHandler.sendEmptyMessageDelayed(C.message.MSG_TYPE_VOICE_UI_TIME, 1000);
                        }

                        break;

                    case MotionEvent.ACTION_MOVE:// 判断是否上滑取消
                        if(haveRecordPermission)
                        mChatPresenter.handleTouchEventMove(event);
                        break;

                    case MotionEvent.ACTION_UP:// 松开时停止录音并发送
                        if(haveRecordPermission){
                            mBtnVoiceSend.setText(getString(R.string.chat_press_to_speak));
                            mChatPresenter.handleTouchEventUp();
                            // 结束计时
                            mHandler.removeMessages(C.message.MSG_TYPE_VOICE_UI_TIME);
                        }

                        break;
                }
                return true;
            }
        });
    }

    private void getRecordPermission() {
        int checkCallPhonePermission = ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.RECORD_AUDIO);
        if(checkCallPhonePermission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(ChatActivity.this,new String[]{Manifest.permission.RECORD_AUDIO},222);
            return ;
        } else {
            //有权限，
            haveRecordPermission=true;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 222) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //申请成功，可以拍照／相册
                haveRecordPermission=true;
            } else {
                Toast.makeText(this, "RECORD AUDIO PERMISSION DENIED", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chat_switch:// 切换文字/语音
                mIvSwitch.setSelected(!mIvSwitch.isSelected());
                if (mIvSwitch.isSelected()) { // 显示发送语音
                    mBtnVoiceSend.setText(getString(R.string.chat_press_to_speak));
                    mBtnVoiceSend.setVisibility(View.VISIBLE);
                    mEtInput.setVisibility(View.GONE);
                    mBtnSend.setEnabled(false);
                    mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                } else { // 显示输入框
                    mBtnVoiceSend.setVisibility(View.GONE);
                    mEtInput.setVisibility(View.VISIBLE);
                    if (mEtInput.getText().length() > 0) {// 已经输入文字
                        mBtnSend.setEnabled(true);
                        mBtnSend.setTextColor(getResources().getColor(R.color.main_color));
                    } else {
                        mBtnSend.setEnabled(false);
                        mBtnSend.setTextColor(getResources().getColor(R.color.drak_gray));
                    }
                }
                break;

            case R.id.chat_send:// 发送
                mChatPresenter.sendTextMessage();
                break;

            case R.id.chat_more:// 显示更多
                if (mLlMore.isShown()) {
                    mLlMore.setVisibility(View.GONE);
                    mEtInput.requestFocus();
                } else {
                    mEtInput.clearFocus();
                    mLlMore.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.chat_album:// 相册
                mLlMore.setVisibility(View.GONE);
                mChatPresenter.sendImageMessage(false);
                break;

            case R.id.chat_camera:// 拍照
                mLlMore.setVisibility(View.GONE);
                mChatPresenter.sendImageMessage(true);
                break;
            case R.id.chat_root:
                mEtInput.clearFocus();
                break;
            case R.id.chat_vedio:
//                AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.pay_diamon), mContext.getString(R.string.vedio_interupt),
//                        mContext.getString(R.string.sure), mContext.getString(R.string.cancel), new OnDialogClickListener() {
//                            @Override
//                            public void onNegativeClick(View view) {
//                            }
//
//                            @Override
//                            public void onPositiveClick(View view) {
////                                判断钻石是否足够，不够的话跳转支付界面
//                                int beanCount = BeanPreference.getBeanCount();
//                                if(beanCount!=0 && beanCount>20){
////                                    钻石足够
                                    if (null != mChatParcelable) {
                                        VideoHelper.startVideoInvite(new VideoInviteParcelable(false, mChatParcelable.guid, mChatParcelable.account,
                                                mChatParcelable.nickname, mChatParcelable.imageUrl,mChatParcelable.country,mChatParcelable.gender), getSupportFragmentManager());
                                    }
//                                }else{
////                                    弹出钻石不足需要充值的按钮
//                                    AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.lack_diamon), mContext.getString(R.string.interupt_null),
//                                            mContext.getString(R.string.go_buy), mContext.getString(R.string.cancel), new OnDialogClickListener() {
//                                                @Override
//                                                public void onNegativeClick(View view) {
//                                                }
//
//                                                @Override
//                                                public void onPositiveClick(View view) {
////                                                    跳转钻石购买界面
//                                                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
//                                                }
//                                            });
//                                }
//
//                            }
//                        });
                break;
        }
    }

    @Override
    protected void loadData() {
        mChatPresenter.initChatConversation();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        发送消息，更新好友列表
        EventBus.getDefault().post(new UpdateFriendListEvent());
        EventBus.getDefault().post(new UnreadMsgChangedEvent(HyphenateHelper.getInstance().getUnreadMsgCount()));

        mChatPresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mRlRoot, msg);
    }

    @Override
    public void showRecordPopupWindow(int state) {
        if (null == mRecordPopupWindow) {
            View contentView = LayoutInflater.from(mContext).inflate(R.layout.popup_chat_record, null);
            mRecordPopupWindow = new PopupWindow(contentView, WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT, true);
            // 设置背景透明
            mRecordPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        View view = mRecordPopupWindow.getContentView();
        if (null != view) {
            // 录音中
            LinearLayout llRecording = (LinearLayout) view.findViewById(R.id.popup_recording_container);
            // 上滑取消
            TextView tvCancel = (TextView) view.findViewById(R.id.popup_cancel_record);
            switch (state) {
                case C.message.STATE_RECORDING: // 正在录音
                    llRecording.setVisibility(View.VISIBLE);
                    tvCancel.setVisibility(View.GONE);
                    break;

                case C.message.STATE_CANCELED: // 取消录音
                    llRecording.setVisibility(View.GONE);
                    tvCancel.setVisibility(View.VISIBLE);
                    break;

                case C.message.STATE_IDLE:// 录音结束
                    llRecording.setVisibility(View.GONE);
                    tvCancel.setVisibility(View.GONE);
                    break;
            }
        }
        // 居中显示
        mRecordPopupWindow.showAtLocation(mRlRoot, Gravity.CENTER, 0, 0);
    }

    @Override
    public void dismissPopupWindow() {
        if (null != mRecordPopupWindow && mRecordPopupWindow.isShowing()) {
            mRecordPopupWindow.dismiss();
        }
    }

    @Override
    public String getInputText() {
        return mEtInput.getText().toString();
    }

    @Override
    public void clearInput() {
        mEtInput.setText("");
    }

    @Override
    public void hideRefresh() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        msg="setBackgroundColor";
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChatPresenter.refresh();
            }
        });
    }

    @Override
    public void scroollToBottom() {
        mLinearLayoutManager.scrollToPosition(mChatAdapter.getItemCount() - 1);
    }

    @Override
    public ChatAdapter getChatAdapter() {
        return mChatAdapter;
    }

    @Override
    public void sendMessage(long delayedMills, int msgType) {
        mHandler.sendEmptyMessageDelayed(msgType, delayedMills);
    }

    @Override
    public void sendMessage(Message message) {
        mHandler.sendMessage(message);
    }

    @Override
    public void showInterruptDialog(String tip) {
//        AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.alert), tip,
//                mContext.getString(R.string.message_recharge), mContext.getString(R.string.message_wait), new OnDialogClickListener() {
//                    @Override
//                    public void onNegativeClick(View view) {
//                    }
//
//                    @Override
//                    public void onPositiveClick(View view) {
//                        // 充值
//                        LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.FROM_TAG_CHAT));
//                    }
//                });
    }

    @Subscribe
    public void onEvent(SeeDetailEvent event) {
        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                new UserDetailParcelable(String.valueOf(mChatParcelable.guid)));
    }
    @Subscribe
    public void onEvent(MessageArrive event) {
       mChatPresenter.initChatConversation();
    }

    @Override
    protected void onResume() {//当打开聊天界面的时候消息提醒以通知的形式展示
        super.onResume();
        HyphenateHelper.getInstance().isChatActivity();
    }
    @Override
    protected void onPause() {//当离开聊天界面打开通知的形式展示
        super.onPause();
        HyphenateHelper.getInstance().isNotChatActivity();
    }

    public void report() {
         resonCode ="";
        final Dialog bottomDialog = new Dialog(mContext, R.style.BottomDialog);
        View contentView = LayoutInflater.from(mContext).inflate(R.layout.dialog_report, null);
        bottomDialog.setContentView(contentView);
        final TextView tvViolent = (TextView) contentView.findViewById(R.id.report_violent);
        final TextView tvAdvert = (TextView) contentView.findViewById(R.id.report_advert);
        final TextView tvgender = (TextView) contentView.findViewById(R.id.report_fake_gender);
        final TextView tvBawdry = (TextView) contentView.findViewById(R.id.report_bawdry);
        final TextView tvClown = (TextView) contentView.findViewById(R.id.report_clown);
        final TextView tvPoli = (TextView) contentView.findViewById(R.id.report_politice);
        final TextView tvOther = (TextView) contentView.findViewById(R.id.report_other);
        final TextView tvCancel = (TextView) contentView.findViewById(R.id.report_cancel);
        final TextView tvReport = (TextView) contentView.findViewById(R.id.report_report);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = Util.dp2px(mContext,220);
        params.height =Util.dp2px(mContext,260);
        params.topMargin=Util.dp2px(mContext,150);
        contentView.setLayoutParams(params);
//        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(Gravity.TOP);
//        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialog.dismiss();
            }
        });
        tvReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startReport(String.valueOf(mChatParcelable.guid), resonCode);
                bottomDialog.dismiss();
            }
        });
        tvViolent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="2";
            }
        });
        tvAdvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="2";
            }
        });
        tvgender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvgender.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="1";
            }
        });
        tvBawdry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="1";
            }
        });
        tvClown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvClown.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="1";
            }
        });
        tvPoli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvOther.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="4";
            }
        });
        tvOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvOther.setTextColor(mContext.getResources().getColor(R.color.main_color));
                tvAdvert.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvgender.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvBawdry.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvClown.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvPoli.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                tvViolent.setTextColor(mContext.getResources().getColor(R.color.drak_gray));
                resonCode ="3";
            }
        });

    }
    //    举报
    public void startReport(String userId,String resonCode) {
        ApiManager.report(userId, resonCode,new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel aSwitch, boolean isEmpty) {

            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

//点击编辑框以外的任意位置，关闭编辑框
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public  boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = { 0, 0 };
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);

            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
//            if (event.getX() > left && event.getX() < right
//                    && event.getY() > top && event.getY() < bottom) {
            if (event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

}
