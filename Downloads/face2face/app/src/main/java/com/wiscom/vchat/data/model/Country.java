package com.wiscom.vchat.data.model;

/**
 * Created by Administrator on 2017/7/6.
 */

public class Country {
     private String guid;
     private String name;
     private String language;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
