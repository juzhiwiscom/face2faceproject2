package com.wiscom.vchat.ui.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hyphenate.chat.EMClient;
import com.wiscom.library.dialog.AlertDialog;
import com.wiscom.library.dialog.DialogUtil;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.dialog.OnDoubleDialogClickListener;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LogUtil;
import com.wiscom.library.util.SharedPreferenceUtil;
import com.wiscom.library.widget.NoScrollViewPager;
import com.wiscom.library.widget.tab.ItemBadge;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragmentActivity;
import com.wiscom.vchat.common.AgoraHelper;
import com.wiscom.vchat.common.HyphenateHelper;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.model.GoogleGPS;
import com.wiscom.vchat.data.model.LocationInfo;
import com.wiscom.vchat.data.model.MyInfo;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.AllUnReadMsgEvent;
import com.wiscom.vchat.event.CloseLocaleVedioEvent;
import com.wiscom.vchat.event.FinishEvent;
import com.wiscom.vchat.event.IntoPersonFragmentEvent;
import com.wiscom.vchat.event.UnreadMsgChangedEvent;
import com.wiscom.vchat.event.UpadateLocationEvent;
import com.wiscom.vchat.event.UpdateFriendListEvent;
import com.wiscom.vchat.event.UpdateVedioListEvent;
import com.wiscom.vchat.ui.friend.FriendFragment;
import com.wiscom.vchat.ui.homepage.presenter.HomeFragment;
import com.wiscom.vchat.ui.message.VideoHistoryFragment;
import com.wiscom.vchat.ui.personalcenter.PersonFragment;
import com.wiscom.vchat.ui.videoshow.VideoShowFragment;
import com.wiscom.vchat.view.beauty.filter.MagicCameraDisplay;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 主Activity，包含所有tab
 * Created by zhangdroid on 2017/5/12.
 */
public class MainActivity extends BaseFragmentActivity {
    @BindView(R.id.main_viewpager)
    NoScrollViewPager mViewPager;
    //    @BindView(R.id.main_tab)
//    TabLayout mTabLayout;
    @BindView(R.id.rl_main)
    RelativeLayout rlMain;
    // 消息tab角标
    private ItemBadge mTabMessageBadge;
    List<Fragment> fragmentList = new ArrayList<>();
    private LocationManager mLocationManager;
    private static final int PERMISSION_CODE_ACCESS_FINE_LOCATION = 0;
    private String jingdu;
    private String weidu;
    private boolean getCountry = false;
    private boolean isFirstPosition = true;
    private boolean isinto = true;
    private MainAdapter mainAdapter;
    private MagicCameraDisplay mMagicCameraDisplay;

    // tab页
    @BindView(R.id.main_tab_find)
    ImageView mTvTabFind;
    @BindView(R.id.main_tab_record)
    ImageView mTvTabRecord;
    @BindView(R.id.main_tab_video)
    ImageView mTvTabVideo;
    @BindView(R.id.main_tab_friend)
    ImageView mTvTabFriend;
    @BindView(R.id.unread_msg_count)
    TextView mTvUnreadMsgCount;// 未读消息数
    @BindView(R.id.main_tab_me)
    ImageView mTvTabMe;


    private static final int TAB_INDEX_FATE = 0;
    private static final int TAB_INDEX_RECORD = 1;
    private static final int TAB_INDEX_FRIEND = 3;
    private static final int TAB_INDEX_NEARBY = 4;
    private static final int TAB_INDEX_ME = 4;
    private static final int TAB_INDEX_VIDEO = 2;
    private int i = 0;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

//    @Override
//    protected String getDefaultTitle() {
//        return getString(R.string.tab_homepage);
//    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

//    @Override
//    protected View getNoticeView() {
//        return mViewPager;
//    }


    @Override
    protected void initViews() {


//        上传位置信息
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        requestLocation();
//        // 登陆声网信令系统
        AgoraHelper.getInstance().login();
//        // 设置消息tab角标
//        mTabMessageBadge = new ItemBadge(Color.WHITE, 12, Gravity.TOP | Gravity.RIGHT).hide();
////         设置tab
//        mTabLayout
//                .addTabView(new TabView(getString(R.string.tab_homepage), R.drawable.tab_homepage_normal, R.drawable.tab_homepage_selected))
//                .addTabView(new TabView(getString(R.string.tab_message), R.drawable.tab_message_normal, R.drawable.tab_message_selected))
//                .addTabView(new TabView(getString(R.string.tab_follow), R.drawable.tab_follow_normal, R.drawable.tab_follow_selected).setBadge(mTabMessageBadge))
//                .addTabView(new TabView(getString(R.string.tab_person), R.drawable.tab_person_normal, R.drawable.tab_person_selected))
//                .setFirstSelectedPosition(0)
//                .initialize();
//        // 设置viewpager
//        fragmentList.add(new HomeFragment());
//        fragmentList.add(new VideoHistoryFragment());
//        fragmentList.add(new FriendFragment());
//        fragmentList.add(new PersonFragment());
//        MainAdapter mainAdapter = new MainAdapter(getSupportFragmentManager(), fragmentList);
//        mViewPager.setAdapter(mainAdapter);
//        mViewPager.setCurrentItem(0);
//        mViewPager.setOffscreenPageLimit(fragmentList.size() - 1);
//        mTabMessageBadge.hide();


        mainAdapter = new MainAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mainAdapter);
        mViewPager.setOffscreenPageLimit(5);
        mViewPager.setCurrentItem(TAB_INDEX_FATE);
        mTvTabFind.setSelected(true);

        setStatusBar(Color.TRANSPARENT);
        // 登录过环信，加载
        if (HyphenateHelper.getInstance().isLoggedIn()) {
            // 登录成功后加载聊天会话
            new Thread(new Runnable() {
                @Override
                public void run() {
                    EMClient.getInstance().chatManager().loadAllConversations();
                }
            }).start();
        } else {
            HuanXinLogin();
        }

    }

    private void HuanXinLogin() {
        HyphenateHelper.getInstance().login(UserPreference.getAccount(), UserPreference.getPassword(), new HyphenateHelper.OnLoginCallback() {
            @Override
            public void onSuccess() {
                LogUtil.v("HMP==", "环信登陆成功");

            }

            @Override
            public void onFailed() {
                i++;
                if (i < 6)
                    HuanXinLogin();
                LogUtil.v("HMP==", "环信登陆失败");
            }
        });
    }

    @Override
    protected void setListeners() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case TAB_INDEX_FATE:
                        mTvTabFind.setSelected(true);
                        mTvTabRecord.setSelected(false);
                        mTvTabFriend.setSelected(false);
                        mTvTabMe.setSelected(false);
                        mTvTabVideo.setSelected(false);
//                      打开本地视频
                        isFirstPosition = true;
                        EventBus.getDefault().post(new CloseLocaleVedioEvent(false));
                        break;

                    case TAB_INDEX_RECORD:
                        mTvTabFind.setSelected(false);
                        mTvTabRecord.setSelected(true);
                        mTvTabFriend.setSelected(false);
                        mTvTabMe.setSelected(false);
                        mTvTabVideo.setSelected(false);
//                    刷新视频聊天记录列表
                        EventBus.getDefault().post(new UpdateVedioListEvent());
//                     关闭本地视频
                        isFirstPosition = false;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!isFirstPosition) {
                                    EventBus.getDefault().post(new CloseLocaleVedioEvent(true));
                                }
                            }
                        }, 500);
                        break;
                    case TAB_INDEX_VIDEO:
                        mTvTabFind.setSelected(false);
                        mTvTabRecord.setSelected(false);
                        mTvTabVideo.setSelected(true);
                        mTvTabFriend.setSelected(false);
                        mTvTabMe.setSelected(false);
//                     关闭本地视频
                        isFirstPosition = false;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!isFirstPosition) {
                                    EventBus.getDefault().post(new CloseLocaleVedioEvent(true));
                                }
                            }
                        }, 500);
                        break;
                    case TAB_INDEX_FRIEND:
                        mTvTabFind.setSelected(false);
                        mTvTabRecord.setSelected(false);
                        mTvTabFriend.setSelected(true);
                        mTvTabMe.setSelected(false);
                        mTvTabVideo.setSelected(false);
//                        更新未读消息
                        int unreadMsgCount = SharedPreferenceUtil.getIntValue(MainActivity.this, "Read_Msg_Num", "All_Read_Msg_Num", 0);
                        if (unreadMsgCount > 0) {
                            mTvUnreadMsgCount.setVisibility(View.VISIBLE);
                            mTvUnreadMsgCount.setText(String.valueOf(unreadMsgCount));
                        } else {
                            mTvUnreadMsgCount.setVisibility(View.GONE);
                        }
//                    刷新好友列表
                        EventBus.getDefault().post(new UpdateFriendListEvent());
//                     关闭本地视频
                        isFirstPosition = false;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!isFirstPosition) {
                                    EventBus.getDefault().post(new CloseLocaleVedioEvent(true));
                                }
                            }
                        }, 500);
                        break;
//                    case TAB_INDEX_NEARBY:
//                        mTvTabFind.setSelected(false);
//                        mTvTabRecord.setSelected(false);
//                        mTvTabFriend.setSelected(false);
//                        mTvTabMe.setSelected(true);
//                        mTvTabVideo.setSelected(false);
////                     关闭本地视频
//                        isFirstPosition=false;
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                if(!isFirstPosition){
//                                    EventBus.getDefault().post(new CloseLocaleVedioEvent(true));
//                                }
//                            }
//                        },500);
//                        break;

                    case TAB_INDEX_ME:
                        mTvTabFind.setSelected(false);
                        mTvTabRecord.setSelected(false);
                        mTvTabFriend.setSelected(false);
                        mTvTabMe.setSelected(true);
                        mTvTabVideo.setSelected(false);
//                     关闭本地视频
                        isFirstPosition = false;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!isFirstPosition) {
                                    EventBus.getDefault().post(new CloseLocaleVedioEvent(true));
                                }
                            }
                        }, 500);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.v("position", "positionOffset==" + positionOffset + "positionOffsetPixels" + positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {

            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//        mTabLayout.setOnTabSelectedListener(new OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(int position) {
//                if(isinto){
//                    mViewPager.setCurrentItem(position, false);
//                }
//                mViewPager.setCurrentItem(position, false);
//                if(position==0){
////                    打开本地视频
//                    isFirstPosition=true;
//                    EventBus.getDefault().post(new CloseLocaleVedioEvent(false));
//                }else{
////                    关闭本地视频
//                    isFirstPosition=false;
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            if(!isFirstPosition){
//                                EventBus.getDefault().post(new CloseLocaleVedioEvent(true));
//                            }
//                        }
//                    },500);
//                }
//                if(position==2){
////                    刷新好友列表
//                    EventBus.getDefault().post(new UpdateFriendListEvent());
//                }
//                if(position==1){
////                    刷新视频聊天记录列表
//                    EventBus.getDefault().post(new UpdateVedioListEvent());
////                    EventBus.getDefault().post(new UpdateVedioListEvent());
//                }
//                int unreadMsgCount = HyphenateHelper.getInstance().getUnreadMsgCount();
//                if(unreadMsgCount>0){
//                    mTabMessageBadge.setText(String.valueOf(unreadMsgCount)).show();
//                }else{
//                    mTabMessageBadge.hide();
//                }
////                if(position==0){
////                    setStatusBar(Color.TRANSPARENT);
////                }
//            }
//        });
    }

    private static class MainAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments = new ArrayList<>();

        public MainAdapter(FragmentManager fm) {
            super(fm);
            fragments.add(new HomeFragment());
            fragments.add(new VideoHistoryFragment());
            fragments.add(new VideoShowFragment());
            fragments.add(new FriendFragment());
            fragments.add(new PersonFragment());
//            fragments.add(new RecentVistorsActivity());
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
    @OnClick({R.id.main_tab_find, R.id.main_tab_record, R.id.main_tab_video, R.id.main_tab_friend, R.id.main_tab_me})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_tab_find:
                mViewPager.setCurrentItem(TAB_INDEX_FATE);
                break;

            case R.id.main_tab_record:
                mViewPager.setCurrentItem(TAB_INDEX_RECORD);
                break;
            case R.id.main_tab_video:
                mViewPager.setCurrentItem(TAB_INDEX_VIDEO);
                break;

            case R.id.main_tab_friend:
                mViewPager.setCurrentItem(TAB_INDEX_FRIEND);
                break;

            case R.id.main_tab_me:
                mViewPager.setCurrentItem(TAB_INDEX_ME);
                break;
        }
    }


    /**
     * 设置状态栏背景色
     *
     * @param statusBarColorRes 5.0及以上系统可以设置状态栏背景色
     */
    protected void setStatusBar(int statusBarColorRes) {

        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
//            getWindow().setStatusBarColor(100);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 登出声网信令系统并销毁资源
        AgoraHelper.getInstance().logout();
        AgoraHelper.getInstance().destoryRtcEngine();
        // 登出环信
        HyphenateHelper.getInstance().logout();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.show(getSupportFragmentManager(), getString(R.string.alert), getString(R.string.exit_tip),
                getString(R.string.positive), getString(R.string.negative), new OnDialogClickListener() {
                    @Override
                    public void onNegativeClick(View view) {
                    }

                    @Override
                    public void onPositiveClick(View view) {
//                        关闭本地视频预览
                        AgoraHelper.getInstance().stopPreview();
                        finish();
                    }
                });
    }

    @Subscribe
    public void onEvent(UnreadMsgChangedEvent event) {
//        int unreadMsgCnt = event.unreadMsg;
//        if(unreadMsgCnt>0){
//            mTabMessageBadge.setText(String.valueOf(unreadMsgCnt)).show();
//        }else{
//            mTabMessageBadge.hide();
//        }
    }

    @Subscribe
    public void onEvent(AllUnReadMsgEvent event) {
        int unreadMsgCount = event.allMsg;
        if (unreadMsgCount > 0) {
            mTvUnreadMsgCount.setVisibility(View.VISIBLE);
            mTvUnreadMsgCount.setText(String.valueOf(unreadMsgCount));
        } else {
            mTvUnreadMsgCount.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

    @Subscribe
    public void onEvent(IntoPersonFragmentEvent event) {
        mViewPager.setCurrentItem(4, false);
//        mTabLayout.setFirstSelectedPosition(3).initialize();
//        int unreadMsgCount = HyphenateHelper.getInstance().getUnreadMsgCount();
//        if(unreadMsgCount>0){
//            mTabMessageBadge.setText(String.valueOf(unreadMsgCount)).show();
//        }else{
//            mTabMessageBadge.hide();
//        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
//                        更新未读消息
        int unreadMsgCount = SharedPreferenceUtil.getIntValue(MainActivity.this, "Read_Msg_Num", "All_Read_Msg_Num", 0);
        if (unreadMsgCount > 0) {
            mTvUnreadMsgCount.setVisibility(View.VISIBLE);
            mTvUnreadMsgCount.setText(String.valueOf(unreadMsgCount));
        } else {
            mTvUnreadMsgCount.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    /**
     * 请求定位信息
     */
    private void requestLocation() {
        if (checkGPS()) {// GPS打开
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CODE_ACCESS_FINE_LOCATION);
                } else {
                    if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, mLocationListener);
                    } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                        getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                    }
                }
            } else {
                if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, mLocationListener);
                }
                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                    getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                }
            }
//            }
        } else {// GPS关闭
            // 跳转到设置页，打开定位开关
            DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), null, getString(R.string.dialog_location_tip), getString(R.string.dialog_set), null, true, new OnDoubleDialogClickListener() {
                @Override
                public void onPositiveClick(View view) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 0);
                }

                @Override
                public void onNegativeClick(View view) {
                }
            });
        }
    }

    /**
     * 检查GPS定位开关
     *
     * @return
     */
    private boolean checkGPS() {
        if (mLocationManager != null) {
            return (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
        }
        return false;
    }

    private boolean noDestroy = true;
    private LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {

            if (noDestroy) {
                getCountryByLoaction(location);
            }

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    /**
     * 根据位置信息（经纬度）获得所在国家
     *
     * @param location 定位信息
     */
    private void getCountryByLoaction(Location location) {
        if (location != null && !getCountry) {
            LocationInfo locationInfo = new LocationInfo();
//美国
//            locationInfo.setLatitude("31.543114");
//            locationInfo.setLongitude("-97.154185");
//            jingdu=String.valueOf("31.543114");
//            weidu=String.valueOf("-97.154185");
//英国
//            locationInfo.setLatitude("52.2729944");
//            locationInfo.setLongitude("-0.8755515");
//            jingdu=String.valueOf("52.2729944");
//            weidu=String.valueOf("-0.8755515");

//印度
//            locationInfo.setLatitude("28.61345942");
//            locationInfo.setLongitude("77.20092773");
//            jingdu=String.valueOf("28.61345942");
//            weidu=String.valueOf("77.20092773");


            locationInfo.setLatitude(String.valueOf(location.getLatitude()));
            locationInfo.setLongitude(String.valueOf(location.getLongitude()));
            jingdu = String.valueOf(location.getLatitude());
            weidu = String.valueOf(location.getLongitude());
            getAddress(jingdu, weidu, "English");
        }
    }

    private void getAddress(String getLatitude, String getLongitude, final String language) {
        //		本地
        String url = "http://ditu.google.cn/maps/api/geocode/json?latlng=" + getLatitude + "," + getLongitude + "&&sensor=false&&language=";
//		线上
//		String url = "http://ditu.google.com/maps/api/geocode/json?latlng="+ lat + "," + lng + "&&sensor=false&&language=";
//        美国地址
//        String url = "http://ditu.google.cn/maps/api/geocode/json?latlng=31.543114,-97.154185&&sensor=false&&language=en_US";

        //获取国际化参数
        String localInfo = "en_US";
        url += localInfo;    //拼接url地址

        String charset = "UTF-8";

        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = okHttpClient.newCall(request);


        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (request != null) {
                    String str = response.body().string();
                    Gson gson = new Gson();
                    GoogleGPS googleGPS = gson.fromJson(str, GoogleGPS.class);
                    if (googleGPS.getResults() != null) {
                        String province = null;
                        String city = null;
                        String country = null;
                        for (int i = 0; i < googleGPS.getResults().size(); i++) {

                            if (googleGPS.getResults().size() > 0 && googleGPS.getResults().get(i).getTypes().get(0).equals("country")) {

                                country = googleGPS.getResults().get(i).getAddress_components().get(0).getLong_name();
                            }

                            if (googleGPS.getResults().size() > 0 && googleGPS.getResults().get(i).getTypes().get(0).equals("administrative_area_level_1")) {
                                province = googleGPS.getResults().get(i).getAddress_components().get(0).getLong_name();
                            }

                            if (googleGPS.getResults().size() > 0 && googleGPS.getResults().get(i).getTypes().get(0).equals("administrative_area_level_2")) {
                                city = googleGPS.getResults().get(i).getAddress_components().get(0).getLong_name();
                            }

                        }
                        if (TextUtils.isEmpty(province)) {
                            province = city;     //没有一级城市，使用二级城市
                        }
                        if (country != null) {
                            getCountry = true;
                            for (int i = 0; i < Util.getCountryList().size(); i++) {
                                if (country.equals(Util.getCountryList().get(i))) {
                                    if (UserPreference.getState() == null || !UserPreference.getState().equals(province)) {
//                                        UserPreference.setState(province);
                                        getLocation(jingdu, weidu, country, province);
                                    }
                                }

                            }
                        }
                    }

                }
            }
        });
    }

    private void getLocation(String latitude, String longitude, String country, String province) {
        LocationInfo locationInfo = new LocationInfo();
        locationInfo.setAddrStr("");
        locationInfo.setProvince(province);
        locationInfo.setCity("");
        locationInfo.setCityCode("");
        locationInfo.setDistrict("");
        locationInfo.setStreet("");
        locationInfo.setStreetNumber("");
        locationInfo.setLatitude(latitude);
        locationInfo.setLongitude(longitude);
        locationInfo.setLanguage(Util.getLacalLanguage(country));
        locationInfo.setCountry(country);
        ApiManager.setLocation(locationInfo, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                String isSucceed = baseModel.getIsSucceed();
                if (!TextUtils.isEmpty(isSucceed) && isSucceed.equals("1")) {
                    LogUtil.v("上传位置信息成功", "上传位置信息成功");
//                    更新用户资料
                    ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
                        @Override
                        public void onResult(MyInfo myInfo, boolean isEmpty) {
                            // 更新用户信息
                            if (null != myInfo) {
                                UserBase userBase = myInfo.getUserDetail().getUserBase();
                                if (null != userBase) {
                                    UserPreference.saveUserInfo(userBase);
//                                    发送给个人信息，更新消息
                                    EventBus.getDefault().post(new UpadateLocationEvent());
                                }
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                        }

                    });
                }

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

}
