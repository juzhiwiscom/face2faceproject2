package com.wiscom.vchat.ui.follow.adapter;


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.data.model.FollowName;
import com.wiscom.library.adapter.CommonRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;

import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 * 试了好多遍，亮哥封装的Adapter与我的自定义View冲突，没办法，技术有限，我只能改变传递数据的方式
 */

public class FollowAdapter extends CommonRecyclerViewAdapter<FollowName> {
    private FragmentManager mFragmentManager;
    private List<FollowName> dataList;
    public void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    public FollowAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
        this.dataList =  getAdapterDataList();
        Collections.sort( this.dataList);
    }
    @Override
    public void convert(FollowName userName, int position, RecyclerViewHolder holder) {
        TextView tv_word = (TextView) holder.getView(R.id.follow_list_item_tv_word);
        TextView tv_name = (TextView) holder.getView(R.id.follow_list_item_tv_name);
        ImageView iv_icon = (ImageView) holder.getView(R.id.follow_list_item_iv_icon);
        FrameLayout cardView_word = (FrameLayout) holder.getView(R.id.follow_list_item_cradview_word);
        //根据position获取首字母作为目录catalog
        String catalog = dataList.get(position).getFirstLetter();

        //如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
        if(position == getPositionForSection(catalog)){
            cardView_word.setVisibility(View.VISIBLE);
            tv_word.setText(dataList.get(position).getFirstLetter().toUpperCase());
        }else{
            cardView_word.setVisibility(View.GONE);
        }
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                .url(dataList.get(position).getIcon()).imageView(iv_icon).build());
        tv_name.setText(dataList.get(position).getName());
    }
    /**
     * 获取catalog首次出现位置
     */
    public int getPositionForSection(String catalog) {
        for (int i = 0; i < getCount(); i++) {
            String sortStr = dataList.get(i).getFirstLetter();
            if (catalog.equalsIgnoreCase(sortStr)) {
                return i;
            }
        }
        return -1;
    }
    public int getCount() {
        return this.dataList.size();
    }


}
