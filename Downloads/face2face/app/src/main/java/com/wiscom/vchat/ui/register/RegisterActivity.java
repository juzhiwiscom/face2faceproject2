package com.wiscom.vchat.ui.register;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.base.BaseFragmentActivity;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.model.PlatformInfo;
import com.wiscom.vchat.data.preference.PlatformPreference;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.FinishEvent;
import com.wiscom.vchat.ui.dialog.AgeSelectDialog;
import com.wiscom.vchat.ui.dialog.SexSelectDialog;
import com.wiscom.vchat.ui.photo.GetPhotoActivity;
import com.wiscom.library.dialog.LoadingDialog;
import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.DeviceUtil;
import com.wiscom.library.util.FileUtil;
import com.wiscom.library.util.SnackBarUtil;
import com.wiscom.library.util.ToastUtil;
import com.wiscom.okhttp.OkHttpHelper;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 注册页面
 * Created by zhangdroid on 2017/5/12.
 */
public class RegisterActivity extends BaseFragmentActivity implements View.OnClickListener, RegisterContract.IView {
    private static final int PERMISSION_CODE_READ_EXTERNAL_STORAGE = 1;
    private static final int REQUEST_TAKE_PHOTO_PERMISSION = 2;
    @BindView(R.id.register_root)
    LinearLayout mLlRoot;
    @BindView(R.id.register_name)
    TextInputEditText mEtName;
    @BindView(R.id.register_sex)
    TextView mTvSex;
    @BindView(R.id.register_age)
    TextView mTvAge;
    @BindView(R.id.register)
    Button mBtnRegister;
    @BindView(R.id.register_login)
    TextView mTvLogin;
    @BindView(R.id.register_agreement)
    TextView mTvAgreement;
    @BindView(R.id.iv_register_icon)
    ImageView ivRegisterIcon;
    @BindView(R.id.register_email)
    TextInputEditText registerEmail;
    @BindView(R.id.register_country)
    TextView mCountry;
    @BindView(R.id.register_ll_country)
    LinearLayout registerLlCountry;
    @BindView(R.id.register_avatar)
    FrameLayout registerAvatar;
    @BindView(R.id.tv_register_avater)
    TextView tvRegisterAvater;

    private RegisterPresenter mRegisterPresenter;
    private File file;
    private int avatarPath=0;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        mRegisterPresenter = new RegisterPresenter(this);
        String text = mTvAgreement.getText().toString();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                mRegisterPresenter.showAgreement(getString(R.string.user_agreement), getString(R.string.user_agreement_connet));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.drak_blue));
            }

//        }, text.indexOf("《"), text.indexOf("》") + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
    }, text.indexOf("《"), text.indexOf("》") + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                mRegisterPresenter.showAgreement(getString(R.string.privacy_agreement), getString(R.string.privacy_agreement_connet));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.drak_blue));
            }
        }, text.lastIndexOf("《"), text.lastIndexOf("》") + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mTvAgreement.setMovementMethod(new LinkMovementMethod());
        mTvAgreement.setText(spannableStringBuilder);
    }

    @Override
    protected void setListeners() {

        mEtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mRegisterPresenter.checkValid();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mTvSex.setOnClickListener(this);
        mTvAge.setOnClickListener(this);
        mBtnRegister.setOnClickListener(this);
        mTvLogin.setOnClickListener(this);
        registerAvatar.setOnClickListener(this);
        if (TextUtils.isEmpty(UserPreference.getRegisterCountry())||UserPreference.getRegisterCountry().equals("111")) {
            mCountry.setOnClickListener(this);
        } else {
            registerLlCountry.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_sex:// 设置性别
                SexSelectDialog.show(getSupportFragmentManager(), getString(R.string.register_female).equals(mTvSex.getText().toString()) ? false : true,
                        new SexSelectDialog.OnSexSelectedListener() {
                            @Override
                            public void onSelected(boolean isMale) {
                                mTvSex.setText(isMale ? getString(R.string.register_male) : getString(R.string.register_female));
                                mRegisterPresenter.checkValid();
                            }
                        });
                break;

            case R.id.register_age:// 设置年龄
                AgeSelectDialog.show(getSupportFragmentManager(), mBtnRegister.getText().toString(), new AgeSelectDialog.OnAgeSelectListener() {
                    @Override
                    public void onSelected(String age) {
                        mTvAge.setText(age);
//                        mRegisterPresenter.checkValid();

                    }
                });

                break;

            case R.id.register:// 注册
                String email = String.valueOf(registerEmail.getText());
                if (!TextUtils.isEmpty(email) && email.contains("@") && email.contains(".")) {
                    if(file!=null){
                        String country = (String) mCountry.getText();
                        if (registerLlCountry.getVisibility()!=View.VISIBLE||!country.equals(getString(R.string.register_select_country))) {
                            if((getString(R.string.register_female).equals(mTvSex.getText().toString())
                                    || getString(R.string.register_male).equals(mTvSex.getText().toString()))){
                                if(!country.equals(getString(R.string.register_select_country))){
                                    UserPreference.setCountry(Util.setEnglishCountry((String) mCountry.getText()));
                                    UserPreference.setRegisterCountry(Util.setEnglishCountry((String) mCountry.getText()));
                                }

              //                设置平台信息
                                initPlatformInfo();
//                先判断是否激活
//                                activation();
                                mRegisterPresenter.register();
                            }else{
                                ToastUtil.showShortToast(mContext, getString(R.string.register_uploading_gender));
                            }

                        }else{
                            ToastUtil.showShortToast(mContext, getString(R.string.register_uploading_country));
                        }
                    }else{
                        ToastUtil.showShortToast(mContext, getString(R.string.register_uploading_avater));
                    }

                } else {
                    ToastUtil.showShortToast(mContext, getString(R.string.email_error));
                }
                break;

            case R.id.register_login:// 跳转登录页面
                mRegisterPresenter.goLoginPage();
                break;
            case R.id.register_country://选择国家
                AgeSelectDialog.show(getSupportFragmentManager(), getString(R.string.united_states), new AgeSelectDialog.OnAgeSelectListener() {
                    @Override
                    public void onSelected(String country) {
                        mCountry.setText(country);
                        Util.setCountryAndFid(country);
                        mRegisterPresenter.checkValid();
//                        UserPreference.setCountry_id();
                    }
                });

                break;
            case R.id.register_avatar:
//                添加相机权限
                getPhotoPermission();

                break;
        }
    }

    private void getPhoto() {
        GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
            @Override
            public void getSelectedPhoto(File getFile) {
                file=getFile;
                String s = getFile.toString();
                String AVATAR_PATH = FileUtil.getExternalFilesDir(BaseApplication.getGlobalContext(), Environment.DIRECTORY_PICTURES)
                        + File.separator + avatarPath+"photo.jpg";
                ImageLoaderUtil.getInstance().loadImage(RegisterActivity.this, new ImageLoader.Builder().url(s).transform(new CropCircleTransformation(mContext))
                            .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(ivRegisterIcon).build());
                tvRegisterAvater.setVisibility(View.GONE);
                mRegisterPresenter.checkValid();

//                        upLoadAvator(file, true);
            }
        });
    }

    private void getPhotoPermission() {
        // 7.0及以上系统需要请求android.permission.READ_EXTERNAL_STORAGE权限，用于转换拍照后路径
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                // 请求android.permission.READ_EXTERNAL_STORAGE权限
                ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CODE_READ_EXTERNAL_STORAGE);
                return;
            }
        }

        int checkCallPhonePermission = ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.CAMERA);
        if(checkCallPhonePermission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(RegisterActivity.this,new String[]{Manifest.permission.CAMERA},REQUEST_TAKE_PHOTO_PERMISSION);
            return;
        } else {
            //有权限，直接拍照／相册
            getPhoto();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CODE_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                int checkCallPhonePermission = ContextCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.CAMERA);
                if(checkCallPhonePermission != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(RegisterActivity.this,new String[]{Manifest.permission.CAMERA},REQUEST_TAKE_PHOTO_PERMISSION);
                    return;
                } else {
                    //有权限，直接拍照／相册
                    getPhoto();
                }
            } else {
                Toast.makeText(this, "CAMERA PERMISSION DENIED", Toast.LENGTH_SHORT).show();

            }
        }else if (requestCode == REQUEST_TAKE_PHOTO_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //申请成功，可以拍照／相册
                getPhoto();
            } else {
                Toast.makeText(this, "CAMERA PERMISSION DENIED", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


    }

    @Override
    protected void loadData() {
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRegisterPresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public String getNickname() {
        String headInfo = mEtName.getText().toString();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0, length = headInfo.length(); i < length; i++) {
            char c = headInfo.charAt(i);
            if (c <= '\u001f' || c >= '\u007f') {
                stringBuffer.append( String.format ("\\u%04x", (int)c) );
            } else {
                stringBuffer.append(c);
            }
        }
        return stringBuffer.toString();
    }

    @Override
    public File getAvatarFile() {
        return file;
    }

    @Override
    public boolean getSex() {
        return getString(R.string.register_male).equals(mTvSex.getText().toString());
    }

    @Override
    public String getAge() {
        return mTvAge.getText().toString();
    }

    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void setRegisterEnable() {
//        mBtnRegister.setEnabled(true);
        if(!TextUtils.isEmpty(mEtName.getText().toString()) && (getString(R.string.register_female).equals(mTvSex.getText().toString())
                || getString(R.string.register_male).equals(mTvSex.getText().toString()))&& file!=null){
            String country = (String) mCountry.getText();
            if (registerLlCountry.getVisibility()!=View.VISIBLE||!country.equals(getString(R.string.register_select_country))) {
                mBtnRegister.setBackgroundColor(Color.parseColor("#FFD366"));
            }
        }
    }

    @Subscribe
    public void onEvent(FinishEvent event) {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    private void activation() {
        if (!UserPreference.isActived()) {
            ApiManager.activation(new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                    // 记录已经激活过
                    UserPreference.activation();
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }

            });
        }
    }
    private void initPlatformInfo() {
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setW(String.valueOf(DeviceUtil.getScreenWidth(this)));
        platformInfo.setH(String.valueOf(DeviceUtil.getScreenHeight(this)));
        platformInfo.setVersion(DeviceUtil.getVersionName(this));
        platformInfo.setPhonetype(Build.MODEL);
        platformInfo.setSystemVersion(Build.VERSION.RELEASE);
        platformInfo.setPlatform("2");
        platformInfo.setProduct(C.PRODUCT_ID);
        platformInfo.setPid(getAndroidId());
        platformInfo.setImsi(getAndroidId());
        platformInfo.setCountry(Util.setEnglishCountry(UserPreference.getCountry()));
        if (UserPreference.getFid() != null) {
            platformInfo.setFid(UserPreference.getFid());
        } else {
//            默认美国
            platformInfo.setFid(Util.F_IDAM);

        }
        platformInfo.setLanguage(Util.getLacalLanguage());
        platformInfo.setNetType(getNetType());
        platformInfo.setMobileIP(getMobileIP());
        platformInfo.setRelease(String.valueOf(DeviceUtil.getVersionCode(this)));
        // 保存platform信息
        PlatformPreference.setPlatfromInfo(platformInfo);
        // 设置API公用参数
        OkHttpHelper.getInstance().addCommonParam("platformInfo", PlatformPreference.getPlatformJsonString());
    }

    private String getNetType() {
        String type = "0";
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            type = "0";
        } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            type = "2";
        } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            String extraInfo = info.getExtraInfo();
            if (extraInfo.equals("cmwap")) {
                type = "3";
            } else if (extraInfo.equals("cmnet")) {
                type = "4";
            } else if (extraInfo.equals("ctnet")) {
                type = "5";
            } else if (extraInfo.equals("ctwap")) {
                type = "6";
            } else if (extraInfo.equals("3gwap")) {
                type = "7";
            } else if (extraInfo.equals("3gnet")) {
                type = "8";
            } else if (extraInfo.equals("uniwap")) {
                type = "9";
            } else if (extraInfo.equals("uninet")) {
                type = "10";
            }
        }
        return type;
    }

    private String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private String getAndroidId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }
    //点击编辑框以外的任意位置，关闭编辑框
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        if (getWindow().superDispatchTouchEvent(ev)) {
            return true;
        }
        return onTouchEvent(ev);
    }

    public  boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = { 0, 0 };
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);

            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
//            if (event.getX() > left && event.getX() < right
//                    && event.getY() > top && event.getY() < bottom) {
            if (event.getY() > top && event.getY() < bottom) {
                // 点击的是输入框区域，保留点击EditText的事件
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

}
