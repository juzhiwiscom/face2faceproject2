package com.wiscom.vchat.ui.personalcenter.presenter;

import com.wiscom.vchat.data.preference.AnchorPreference;
import com.wiscom.vchat.data.preference.BeanPreference;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.ui.personalcenter.contract.PersonContract;

/**
 * Created by zhangdroid on 2017/5/27.
 */
public class PersonPresenter implements PersonContract.IPresenter {


    private PersonContract.IView mPersonView;

    public PersonPresenter(PersonContract.IView view) {
        this.mPersonView = view;
    }

    @Override
    public void start() {
    }

    @Override
    public void finish() {
        mPersonView = null;
    }

    @Override
    public void getUserInfo() {
        //  获取用户信息
        mPersonView.setUserAvatar(UserPreference.getSmallImage());
        if(UserPreference.getRegisterName()!=null){
            mPersonView.setUserName(UserPreference.getRegisterName());
        }else{
            mPersonView.setUserName(UserPreference.getNickname());
        }
        mPersonView.setUserId(UserPreference.getAccount());
        mPersonView.setUserPrecent("100", "0");
        mPersonView.setBalance(String.valueOf(BeanPreference.getBeanCount()));
//        mPersonView.setIsAnchor(UserPreference.isAnchor());
        mPersonView.setAccumulatedIncome(AnchorPreference.getIncome());
        mPersonView.setAnchorCurrentIncome(AnchorPreference.getCurrentBalance());
        mPersonView.setPrice(AnchorPreference.getPrice());
    }

    @Override
    public void updateBalance() {
        // 更新当前余额（充值/消费操作后）
        mPersonView.setBalance(String.valueOf(BeanPreference.getBeanCount()));
    }

    @Override
    public void updatePrice(String newPrice) {
        // 更新当前主播价格
        mPersonView.setPrice(newPrice);
    }


}
