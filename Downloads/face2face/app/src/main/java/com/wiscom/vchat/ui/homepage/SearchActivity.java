package com.wiscom.vchat.ui.homepage;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.ui.homepage.contract.SearchContract;
import com.wiscom.vchat.ui.homepage.presenter.SearchPresenter;
import com.wiscom.library.net.NetUtil;

import butterknife.BindView;

/**
 * 搜索页面
 * Created by zhangdroid on 2017/5/27.
 */
public class SearchActivity extends BaseTopBarActivity implements View.OnClickListener, SearchContract.IView {
    @BindView(R.id.search_country)
    LinearLayout mLlCountry;
    @BindView(R.id.search_country_selected)
    TextView mTvCountrySelected;
    @BindView(R.id.search_language)
    LinearLayout mLlLanguage;
    @BindView(R.id.search_language_selected)
    TextView mTvLanguageSelected;

    private SearchPresenter mSearchPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_search;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.homepage_search);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected void initViews() {
        mSearchPresenter = new SearchPresenter(this);
    }

    @Override
    protected void setListeners() {
        mLlCountry.setOnClickListener(this);
        mLlLanguage.setOnClickListener(this);
        setBackListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                mSearchPresenter.startSearch();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_country:// 设置国家
                mSearchPresenter.selectCountry();
                break;

            case R.id.search_language:// 设置语言
                mSearchPresenter.selectLanguage();
                break;
        }
    }

    @Override
    protected void loadData() {
        mSearchPresenter.start();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSearchPresenter.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mSearchPresenter.startSearch();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }

    @Override
    public void setCountry(String country) {
        if (!TextUtils.isEmpty(country)) {
            mTvCountrySelected.setText(country);
        }
    }

    @Override
    public void setLanguage(String language) {
        if (!TextUtils.isEmpty(language)) {
            mTvLanguageSelected.setText(language);
        }
    }

    @Override
    public String getCountry() {
        return mTvCountrySelected.getText().toString();
    }

    @Override
    public String getLanguage() {
        return mTvLanguageSelected.getText().toString();
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

}
