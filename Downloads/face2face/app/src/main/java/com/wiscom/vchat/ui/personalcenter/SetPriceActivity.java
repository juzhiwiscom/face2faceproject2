package com.wiscom.vchat.ui.personalcenter;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarActivity;
import com.wiscom.vchat.parcelable.TvPriceParcelable;
import com.wiscom.vchat.ui.personalcenter.contract.SetPriceConteact;
import com.wiscom.vchat.ui.personalcenter.presenter.SetPricePresenter;
import com.wiscom.library.dialog.LoadingDialog;
import com.wiscom.library.net.NetUtil;

import butterknife.BindView;

/**
 * 价格设置页面
 * Created by zhangdroid on 2017/5/27.
 */
public class SetPriceActivity extends BaseTopBarActivity implements SetPriceConteact.IView {




    @BindView(R.id.Tv_price)
    TextView TvPrice;
    @BindView(R.id.sPrice)
    TextView sPrice;

    @BindView(R.id.setting_range_seekBar)
    SeekBar settingRangeSeekBar;
    @BindView(R.id.button2)
    Button button2;
    private SetPricePresenter mSetPricePresenter;
    private TvPriceParcelable priceParcelable;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_set_price;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.person_price_set);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
         priceParcelable = (TvPriceParcelable) parcelable;
    }

    @Override
    protected void initViews() {
//        Intent intent=getIntent();
//        Bundle bundle=intent.getExtras();
//        String price=bundle.getString("price");
        settingRangeSeekBar.setProgress((int)Double.parseDouble(priceParcelable.price));
        TvPrice.setText(priceParcelable.price);
        mSetPricePresenter = new SetPricePresenter(this);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSetPricePresenter.savePrice();

            }
        });
    }

    @Override
    protected void setListeners() {

        settingRangeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TvPrice.setText((10+progress/10)+ "");
//                 newPrice = TvPrice.getText();
//                settingRangeSeekBar.setProgress(Integer.parseInt(TvPrice.getText()+""));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation

    }


    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }


    @Override
    public void showLoading() {
        LoadingDialog.show(getSupportFragmentManager());
    }

    @Override
    public void dismissLoading() {
        LoadingDialog.hide();
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }


    @Override
    public String getTestring() {
        return TvPrice.getText().toString();
    }

    @Override
    public void setTextString(String string) {

    }

    @Override
    public void finishActivity() {
        finish();

    }
}
