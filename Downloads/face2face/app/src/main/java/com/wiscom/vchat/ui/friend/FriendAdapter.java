package com.wiscom.vchat.ui.friend;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.common.ParamsUtils;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.common.recyclerview.CommonRecyclerViewAdapter;
import com.wiscom.vchat.common.recyclerview.RecyclerViewHolder;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.event.RefreshFriendApplyListEven;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.util.LogUtil;
import com.wiscom.library.util.SharedPreferenceUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by tianzhentao on 2018/1/3.
 */

class FriendAdapter extends CommonRecyclerViewAdapter<UserBase> {
    private FragmentManager mFragmentManager;
    private List<UserBase> dataList;
    private boolean fromFriendApply=false;

    public FriendAdapter(Context mContext, int item_friend) {
        super(mContext, item_friend);
    }


    public FriendAdapter(Context mContext, int item_friend,boolean fromFriendApply) {
        super(mContext, item_friend);
        this.fromFriendApply=fromFriendApply;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;

    }
    @Override
    protected void convert(int position, RecyclerViewHolder holder, UserBase userBase) {

        LogUtil.v("绑定数据源=", "adapter成功-position="+position);
        holder.setText(R.id.friend_tv_nickname,userBase.getNickName());
        String country = ParamsUtils.getCountryValues(userBase.getCountry());
        TextView tvCountry = (TextView) holder.getView(R.id.friend_tv_country);
        TextView tvCity = (TextView) holder.getView(R.id.friend_tv_city);
        final Button btnReceive = (Button) holder.getView(R.id.btn_receive);
        final Button btnRefuse = (Button) holder.getView(R.id.btn_refuse);


        if(fromFriendApply){
            tvCountry.setVisibility(View.GONE);
            tvCity.setVisibility(View.GONE);
            btnReceive.setVisibility(View.VISIBLE);
            btnRefuse.setVisibility(View.VISIBLE);
        }else{
            tvCountry.setVisibility(View.VISIBLE);
            tvCity.setVisibility(View.VISIBLE);
            btnReceive.setVisibility(View.GONE);
            btnRefuse.setVisibility(View.GONE);
            TextView msgNum = (TextView)holder.getView(R.id.friend_unread_msg_count);
            int readMsgNum = SharedPreferenceUtil.getIntValue(BaseApplication.getGlobalContext(), "Read_Msg_Num", userBase.getGuid()+"", 0);
            if(readMsgNum==0){
              msgNum.setVisibility(View.GONE);
            }else{
                msgNum.setVisibility(View.VISIBLE);
                msgNum.setText(readMsgNum+"");
            }
        }
        holder.setText(R.id.friend_tv_country,country);
        holder.setText(R.id.friend_tv_city,userBase.getCity());
        ImageView ivBanner = (ImageView) holder.getView(R.id.friend_iv_banner);
        ivBanner.setImageResource(Util.getBanner(country));//根据国家获取国旗
        ImageView ivGender = (ImageView) holder.getView(R.id.friend_iv_gender);
        if(userBase.getGender()==1){
            ivGender.setImageResource(R.drawable.ic_female);
        }else{
            ivGender.setImageResource(R.drawable.ic_male);
        }
        ImageView imageView = (ImageView) holder.getView(R.id.friend_iv_icon);
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userBase.getIcon())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build());
//        接收和拒绝添加好友的点击事件
        final String guid = String.valueOf(userBase.getGuid());
        btnReceive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnReceive.setEnabled(false);
                ApiManager.agreeAddFriend(guid, new IGetDataListener<BaseModel>() {
                    @Override
                    public void onResult(BaseModel baseModel, boolean isEmpty) {
                        String isSucceed = baseModel.getIsSucceed();
                        if(!TextUtils.isEmpty(isSucceed) && isSucceed.equals("1")){
//                            发送消息，刷新列表，删除此用户
                            EventBus.getDefault().post(new RefreshFriendApplyListEven());
                        }

                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        btnReceive.setClickable(true);
                        btnReceive.setEnabled(true);
                    }
                });

            }
        });
        btnRefuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiManager.refuseAddFriend(guid, new IGetDataListener<BaseModel>() {
                    @Override
                    public void onResult(BaseModel baseModel, boolean isEmpty) {
//                        btnRefuse.setClickable(false);
                        btnRefuse.setEnabled(false);
                        String isSucceed = baseModel.getIsSucceed();
                        if(!TextUtils.isEmpty(isSucceed) && isSucceed.equals("1")){
//                            发送消息，刷新列表，删除此用户
                            EventBus.getDefault().post(new RefreshFriendApplyListEven());
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    btnRefuse.setClickable(true);
                                    btnRefuse.setEnabled(true);
                                }
                            },2000);
                        }

                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        btnRefuse.setClickable(true);
                        btnRefuse.setEnabled(true);

                    }
                });

            }
        });

    }


}
