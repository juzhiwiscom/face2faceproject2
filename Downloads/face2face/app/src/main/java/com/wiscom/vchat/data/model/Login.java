package com.wiscom.vchat.data.model;

/**
 * 登录对象
 * Created by zhangdroid on 2017/5/24.
 */
public class Login extends BaseModel {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private UserDetail userDetail;

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }
}
