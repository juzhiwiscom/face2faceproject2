package com.wiscom.vchat.data.model;

/**
 * 视频心跳结果对象
 * Created by zhangdroid on 2017/6/23.
 */
public class VideoHeartBeat extends BaseModel {
    private String connectStatus;// 连接信息状态：-1 未建立 -2 已关闭 挂断声网接口 1连接正常
    private String totalMinute;// 视频聊天总时长
    private String avalibleSeconds;//":{剩余可通话时长，描述},1分钟内剩余免费时长   1分钟后剩余可付费时长
    private String beanStatus;//":{钻石状态} 1 充足 -1 不足 弹出拦截去支付

    public String getConnectStatus() {
        return connectStatus;
    }

    public void setConnectStatus(String connectStatus) {
        this.connectStatus = connectStatus;
    }

    public String getBeanStatus() {
        return beanStatus;
    }

    public void setBeanStatus(String beanStatus) {
        this.beanStatus = beanStatus;
    }

    public String getTotalMinute() {
        return totalMinute;
    }

    public void setTotalMinute(String totalMinute) {
        this.totalMinute = totalMinute;
    }

}
