package com.wiscom.vchat.ui.personalcenter;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscom.library.dialog.AlertDialog;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragment;
import com.wiscom.vchat.base.CommonWebActivity;
import com.wiscom.vchat.common.AgoraHelper;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiConstant;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.MyInfo;
import com.wiscom.vchat.data.model.UpLoadMyInfo;
import com.wiscom.vchat.data.model.UpLoadMyPhoto;
import com.wiscom.vchat.data.model.UploadInfoParams;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserPhoto;
import com.wiscom.vchat.data.model.UserVideoShow;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.PaySuccessEvent;
import com.wiscom.vchat.event.UpadateLocationEvent;
import com.wiscom.vchat.event.UpdateAvatorEvent;
import com.wiscom.vchat.event.UpdateMyInfo;
import com.wiscom.vchat.event.UserInfoChangedEvent;
import com.wiscom.vchat.parcelable.PayParcelable;
import com.wiscom.vchat.parcelable.TvBalanceParcelable;
import com.wiscom.vchat.parcelable.WebParcelable;
import com.wiscom.vchat.ui.dialog.EditNicknameDialog;
import com.wiscom.vchat.ui.dialog.OnEditDoubleDialogClickListener;
import com.wiscom.vchat.ui.pay.PayActivity;
import com.wiscom.vchat.ui.personalcenter.contract.PersonContract;
import com.wiscom.vchat.ui.personalcenter.presenter.PersonPresenter;
import com.wiscom.vchat.ui.photo.GetPhotoActivity;
import com.wiscom.vchat.ui.setting.SettingActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.wiscom.vchat.C.Video.REQUEST_CODE;

/**
 * 个人中心
 * Created by zhangdroid on 2017/5/23.
 */
public class PersonFragment extends BaseFragment implements View.OnClickListener, PersonContract.IView {


    private static final int PERMISSION_CODE_READ_EXTERNAL_STORAGE = 1;
    private static final int REQUEST_TAKE_PHOTO_PERMISSION = 2;
    // 用户信息
    @BindView(R.id.person_user_layout)
    RelativeLayout mRlUser;
    @BindView(R.id.person_user_avatar)
    ImageView mIvUserAvatar;
    @BindView(R.id.person_user_name)
    TextView mTvUserName;
    @BindView(R.id.person_user_id)
    TextView mTvUserId;
    @BindView(R.id.person_user_percent)
    TextView mTvUserPercent;// 接听率/投诉率
    // 账号信息
    @BindView(R.id.person_account_balance)
    TextView mTvBalance;
    @BindView(R.id.person_income_layout)
    LinearLayout mLlIncome;
    @BindView(R.id.person_accumulated_income)
    TextView mTvAccumulatedIncome;
    @BindView(R.id.person_recharge)
    TextView mTvRecharge;// 充值
    @BindView(R.id.person_withdraw)
    TextView mTvWithdraw;// 提现
    // 主播认证
    @BindView(R.id.person_anchor_authentication)
    RelativeLayout mRlAnchorAuthentication;
    // 价格设置（主播）
    @BindView(R.id.person_price_layout)
    RelativeLayout mRlPrice;
    @BindView(R.id.person_price)
    TextView mTvPrice;
    // 设置
    @BindView(R.id.person_setting_layout)
    RelativeLayout mRlSetting;
    //    国旗
    @BindView(R.id.person_banner)
    ImageView personBanner;
    @BindView(R.id.person_country)
    TextView personCountry;
    @BindView(R.id.person_city)
    TextView personCity;
    //    性别
    @BindView(R.id.person_gender)
    ImageView personGender;

    //    视频秀
    @BindView(R.id.person_vedio_layout)
    RelativeLayout personVedioLayout;
    //    购买钻石
    @BindView(R.id.person_buy_diamon_layout)
    RelativeLayout personBuyDiamonLayout;
    //    我的收益
    @BindView(R.id.person_my_income_layout)
    RelativeLayout personMyIncomeLayout;
    Unbinder unbinder;
    //    编辑个人昵称
    @BindView(R.id.person_edit_name)
    LinearLayout personEditName;
//    开通VIP
    @BindView(R.id.person_open_vip_layout)
    RelativeLayout personOpenVipLayout;

    private PersonPresenter mPersonPresenter;
    private File videoFile;
    public final static String DATA = "URL";
    public final static String VIDEO_URL = "VIDEO_URL";
    public final static String VIDEO_IMAGE_URL = "VIDEO_IMAGE_URL";
    private boolean isUideoUrl=false;
    private String videoUrl;
    private String videoImageUrl;


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_person;
    }


//    @Override
//    protected String getDefaultTitle() {
//        return getString(R.string.title_person);
//    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
        mPersonPresenter = new PersonPresenter(this);
        if ("0".equals(UserPreference.getGender())) {
            personGender.setImageResource(R.drawable.ic_male);
        } else {
            personGender.setImageResource(R.drawable.ic_female);
        }
        String country = UserPreference.getCountry();
        personCountry.setText(country);
        personBanner.setImageResource(Util.getBanner(country));//根据国家获取国旗
        personCity.setText(UserPreference.getState());
        getMyInfo();

    }

    @Override
    protected void setListeners() {
        mIvUserAvatar.setOnClickListener(this);
//        mTvRecharge.setOnClickListener(this);
//        mTvWithdraw.setOnClickListener(this);
//        mRlAnchorAuthentication.setOnClickListener(this);
//        mRlPrice.setOnClickListener(this);
        mRlSetting.setOnClickListener(this);
        personEditName.setOnClickListener(this);
        personOpenVipLayout.setOnClickListener(this);
        personBuyDiamonLayout.setOnClickListener(this);
        personOpenVipLayout.setOnClickListener(this);
        personVedioLayout.setOnClickListener(this);
//        personMyIncomeLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.person_user_avatar://设置头像
                GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        upLoadAvator(file, true);
                    }
                });
                break;
            case R.id.person_edit_name://编辑昵称
                EditNicknameDialog.newInstance(getString(R.string.nickname), getString(R.string.nick_name_hint),
                        getString(R.string.positive), getString(R.string.negative), true, new OnEditDoubleDialogClickListener() {
                            @Override
                            public void onPositiveClick(View view, String content) {
                                if (!TextUtils.isEmpty(content)) {
                                    mTvUserName.setText(content);
                                    UploadInfoParams uploadInfoParams = new UploadInfoParams();
                                    uploadInfoParams.setNickName(content);
                                    updateMyInfo(uploadInfoParams);
                                }
                            }

                            @Override
                            public void onNegativeClick(View view) {

                            }
                        }).show(getFragmentManager(), "doubleBtnDialog");
                break;
            case R.id.person_vedio_layout://视频秀
                //                添加录音权限
                getRecordPermission();


                break;
            case R.id.person_open_vip_layout://开通VIP
                LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.OPEN_VIP));


                break;
            case R.id.person_buy_diamon_layout://购买钻石
                LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
                break;
//            case R.id.person_my_income_layout://我的收益
//                break;
            case R.id.person_setting_layout:// 设置
                LaunchHelper.getInstance().launch(mContext, SettingActivity.class);
                break;


//            case R.id.person_user_layout:// 查看个人页
//                LaunchHelper.getInstance().launch(mContext, MyDetailActivity.class);
//                break;
            case R.id.person_recharge:// 充值
                LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON));
                break;

            case R.id.person_withdraw:// 提现
//                LaunchHelper.getInstance().launch(mContext, WithdrawActivity.class);
                LaunchHelper.getInstance().launch(mContext, WithdrawActivity.class,
                        new TvBalanceParcelable((String) mTvBalance.getText()));
                break;

            case R.id.person_anchor_authentication:// 主播认证
                AlertDialog.show(getFragmentManager(), getString(R.string.alert), getString(R.string.person_anchor_tip),
                        getString(R.string.positive), getString(R.string.negative), new OnDialogClickListener() {
                            @Override
                            public void onNegativeClick(View view) {
                            }

                            @Override
                            public void onPositiveClick(View view) {
                                LaunchHelper.getInstance().launch(mContext, CommonWebActivity.class,
                                        new WebParcelable(getString(R.string.anchor_agreement), ApiConstant.URL_AGREEMENT_ANCHOR, true));
                            }
                        });
                break;

            case R.id.person_price_layout:// 主播价格设置
//                TvPriceParcelable tvPriceParcelable = new TvPriceParcelable((String) mTvPrice.getText());
//                LaunchHelper.getInstance().launch(mContext, SetPriceActivity.class,
//                        tvPriceParcelable);
                break;
        }
    }

    private void intoRecord() {
        //                        关闭本地视频预览
        AgoraHelper.getInstance().stopPreview();
        Intent intent = new Intent(mContext, RecordActivity.class);
        if(videoUrl!=null){
            intent.putExtra(VIDEO_URL, videoUrl);
            if(videoImageUrl!=null)
                intent.putExtra(VIDEO_IMAGE_URL, videoImageUrl);
        }
        videoDir();
        intent.putExtra(DATA, videoFile.getAbsolutePath());
        startActivityForResult(intent,REQUEST_CODE);
    }


    private void getRecordPermission() {
        int checkCallWritePermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int checkCallReadPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO);
        if(checkCallWritePermission != PackageManager.PERMISSION_GRANTED){
            PersonFragment.this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_TAKE_PHOTO_PERMISSION);
            return;
        } else if(checkCallReadPermission != PackageManager.PERMISSION_GRANTED) {
            PersonFragment.this.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSION_CODE_READ_EXTERNAL_STORAGE);
            return;
        }else{
            //有权限，进入
            intoRecord();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
         if (requestCode == REQUEST_TAKE_PHOTO_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //有权限，进入
                int checkCallReadPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO);
                if(checkCallReadPermission != PackageManager.PERMISSION_GRANTED){
                    PersonFragment.this.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},PERMISSION_CODE_READ_EXTERNAL_STORAGE);
                    return;
                } else{
                    intoRecord();
                }

            } else {
                Toast.makeText(getActivity(), "RECORD PERMISSION DENIED", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        if (requestCode == PERMISSION_CODE_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //有权限，进入
                    intoRecord();
            } else {
                Toast.makeText(getActivity(), "RECORD PERMISSION DENIED", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


    }


    private void updateMyInfo(UploadInfoParams uploadInfoParams) {
        ApiManager.upLoadMyInfo(uploadInfoParams, new IGetDataListener<UpLoadMyInfo>() {
            @Override
            public void onResult(UpLoadMyInfo upLoadMyInfo, boolean isEmpty) {
                // 发送用户资料改变事件
//                EventBus.getDefault().post(new UserInfoChangedEvent());
                if (upLoadMyInfo != null) {
                    UserBase userBase = upLoadMyInfo.getUserBase();
                    if (userBase != null) {
                        // 更新本地用户信息
                        UserPreference.saveUserInfo(userBase);
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    @Override
    protected void loadData() {
        mPersonPresenter.getUserInfo();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPersonPresenter.finish();
    }

    @Override
    public void setUserAvatar(String url) {
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(url).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(mIvUserAvatar).build());

//        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
//                .url(url).placeHolder(Util.getDefaultImageCircle()).transform(new CropCircleTransformation(mContext)).imageView(mIvUserAvatar).build(), new ImageLoaderUtil.OnImageLoadListener() {
//            @Override
//            public void onCompleted() {
//                Log.v("Tian==","显示成功");
//            }
//
//            @Override
//            public void onFailed() {
//                Log.v("Tian==","显示失败");
//            }
//        });
    }

    @Override
    public void setUserName(String name) {
        if (!TextUtils.isEmpty(name)) {
            mTvUserName.setText(name);
        }
    }

    @Override
    public void setUserId(String id) {
        if (!TextUtils.isEmpty(id)) {
            mTvUserId.setText(getString(R.string.person_id, id));
        }
    }

    @Override
    public void setUserPrecent(String receivePrecent, String complainPrecent) {
        if (!TextUtils.isEmpty(receivePrecent) && !TextUtils.isEmpty(complainPrecent)) {
            mTvUserPercent.setText(TextUtils.concat(getString(R.string.person_receive_precent, receivePrecent), "% | ",
                    getString(R.string.person_complain_precent, complainPrecent), "%"));
        } else if (!TextUtils.isEmpty(complainPrecent)) {
            mTvUserPercent.setText(getString(R.string.person_complain_precent, complainPrecent));
        }
    }

    @Override
    public void setBalance(String balance) {
        if (!TextUtils.isEmpty(balance)) {
            mTvBalance.setText(balance);
        }
    }

    @Override
    public void setIsAnchor(boolean isAnchor) {
//        mLlIncome.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
//        mTvWithdraw.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
//        mTvRecharge.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
////        mRlAnchorAuthentication.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
//        mRlPrice.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setAccumulatedIncome(String income) {
        if (!TextUtils.isEmpty(income)) {
            mTvAccumulatedIncome.setText(income);
        }
    }

    @Override
    public void setAnchorCurrentIncome(String currentIncome) {

    }

    @Override
    public void setPrice(String price) {
        if (!TextUtils.isEmpty(price)) {
            mTvPrice.setText(getString(R.string.person_price, price));
        }
    }
    public void videoDir() {
        videoFile = new File(C.Video.SD_PATH);
        // 创建文件
        try {
            if (!videoFile.exists()) {
                videoFile.createNewFile();
                videoFile.setWritable(Boolean.TRUE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void upLoadAvator(File file, boolean photoType) {
        ApiManager.upLoadMyPhotoOrAvator(file, photoType, new IGetDataListener<UpLoadMyPhoto>() {
            @Override
            public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                if (upLoadMyPhoto != null) {
                    UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                    if (userPhoto != null) {
                        final String stringFile = userPhoto.getFileUrlMiddle();
                        String fileUrl = userPhoto.getFileUrl();
                        String fileUrlMinimum = userPhoto.getFileUrlMinimum();
                        if (!TextUtils.isEmpty(stringFile)) {
                            ImageLoaderUtil.getInstance().loadImage(getActivity(), new ImageLoader.Builder().url(stringFile).transform(new CropCircleTransformation(mContext))
                                    .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(mIvUserAvatar).build());
                            UserPreference.setOriginalImage(fileUrl);
                            UserPreference.setMiddleImage(stringFile);
                            UserPreference.setSmallImage(fileUrlMinimum);
//                            发送消息，更改头像
                            EventBus.getDefault().post(new UpdateAvatorEvent());
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    public void getMyInfo() {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                // 更新用户信息
                if (null != myInfo) {
                    List<UserVideoShow> userVideoShows = myInfo.getUserDetail().getUserVideoShows();
                    if (null != userVideoShows && userVideoShows.size()>0) {
                        UserVideoShow userVideoShow = userVideoShows.get(userVideoShows.size()-1);
                        if(userVideoShow!=null){
                             videoUrl = userVideoShow.getVideoUrl();
                             videoImageUrl = userVideoShow.getThumbnailUrl();
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }

        });
    }


    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }

    @Subscribe
    public void onEvent(UserInfoChangedEvent event) {
        mPersonPresenter.getUserInfo();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Message message) {

        if (message.what == 1010) {
            String newPrice = (String) message.obj;
            mPersonPresenter.updatePrice(newPrice);
        }
        if (message.what == 1111) {
            String newMoney = (String) message.obj;
            double withdrawMoney = Double.parseDouble(newMoney) * 200;
            double curentMoney = Double.parseDouble((String) mTvBalance.getText()) - withdrawMoney;
            mTvBalance.setText((int) curentMoney + "");
//            mTvBalance.setText("6000");
        }
    }


    @Subscribe
    public void onEvent(PaySuccessEvent event) {
        mPersonPresenter.updateBalance();
    }
    @Subscribe
    public void onEvent(UpadateLocationEvent event) {
        personCity.setText(UserPreference.getState());
    }
    @Subscribe
    public void onEvent(UpdateMyInfo event) {
        getMyInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
