package com.wiscom.vchat.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.Gson;
import com.wiscom.library.dialog.DialogUtil;
import com.wiscom.library.dialog.OnDoubleDialogClickListener;
import com.wiscom.library.util.DeviceUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.LogUtil;
import com.wiscom.library.util.SharedPreferenceUtil;
import com.wiscom.library.util.ToastUtil;
import com.wiscom.okhttp.OkHttpHelper;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.HyphenateHelper;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.model.GoogleGPS;
import com.wiscom.vchat.data.model.HostInfo;
import com.wiscom.vchat.data.model.LocationInfo;
import com.wiscom.vchat.data.model.Login;
import com.wiscom.vchat.data.model.PlatformInfo;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserBean;
import com.wiscom.vchat.data.model.UserDetail;
import com.wiscom.vchat.data.preference.AnchorPreference;
import com.wiscom.vchat.data.preference.BeanPreference;
import com.wiscom.vchat.data.preference.PlatformPreference;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.ui.login.LoginActivity;
import com.wiscom.vchat.ui.main.MainActivity;
import com.wiscom.vchat.ui.register.RegisterActivity;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 启动页面
 * Created by zhangdroid on 2017/5/12.
 */
public class SplashActivity extends FragmentActivity {
    private LocationManager mLocationManager;
    private static final int PERMISSION_CODE_ACCESS_FINE_LOCATION = 0;
    private String jingdu;
    private String weidu;
    private boolean isFirst=true;
    private boolean firstLogin=true;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        requestLocation();

        // 请求所有高危权限
//        PermissionManager.getInstance(this)
//                .addPermission(new PermissonItem(C.permission.PERMISSION_PHONE, getString(R.string.permission_phone), R.drawable.permission_phone))
//                .addPermission(new PermissonItem(C.permission.PERMISSION_WRITE_EXTERNAL_STORAGE, getString(R.string.permission_write_file), R.drawable.permission_write_file))
//                .addPermission(new PermissonItem(C.permission.PERMISSION_READ_EXTERNAL_STORAGE, getString(R.string.permission_read_file), R.drawable.permission_read_file))
//                .addPermission(new PermissonItem(C.permission.PERMISSION_CAMERA, getString(R.string.permission_camera), R.drawable.permission_camera))
//                .addPermission(new PermissonItem(C.permission.PERMISSION_RECORD_AUDIO, getString(R.string.permission_record), R.drawable.permission_record))
//                .addPermission(new PermissonItem(C.permission.ACCESS_FINE_LOCATION, getString(R.string.permission_location), R.mipmap.permission_location))
//                .checkMutiPermission(new PermissionCallback() {
//                    @Override
//                    public void onGuaranteed(String permisson, int position) {
//                    }
//
//                    @Override
//                    public void onDenied(String permisson, int position) {
//                    }
//
//                    @Override
//                    public void onFinished() {
////                      请求定位
//                        String account = UserPreference.getAccount();
//                        String password = UserPreference.getPassword();
//                        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
//                            login();
//                        }else{
//                            requestLocation();
//                            intoLogin();
//                        }
//
//                    }
//
//                    @Override
//                    public void onClosed() {
////                      请求定位
//                        String account = UserPreference.getAccount();
//                        String password = UserPreference.getPassword();
//                        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
//                            login();
//                        }else{
//                            requestLocation();
//                            intoLogin();
//                        }
//                        login();
//                    }
//                });
    }

    private void intoLogin() {
//        if(firstLogin){
//            firstLogin=false;
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    login();
//                }
//            },5000);
//        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(firstLogin){
                    firstLogin=false;
                    login();
                }
            }
        },5000);
    }

    private void login() {
        // 初始化平台信息
        initPlatformInfo();
        //        激活
        activation();
        String account = UserPreference.getAccount();
        String password = UserPreference.getPassword();
        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
            ApiManager.login(account, password, new IGetDataListener<Login>() {
                @Override
                public void onResult(Login login, boolean isEmpty) {
                    String token = login.getToken();
                    PlatformPreference.setToken(token);
                    UserDetail userDetail = login.getUserDetail();
                    if (null != userDetail) {
                        // 更新用户相关信息
                        UserBase userBase = userDetail.getUserBase();
                        if (null != userBase) {
                            UserPreference.saveUserInfo(userBase);
                        }
                        UserBean userBean = userDetail.getUserBean();
                        if (null != userBean) {
                            BeanPreference.saveUserBean(userBean);
                        }
                        HostInfo hostInfo = userDetail.getHostInfo();
                        if (null != hostInfo) {
                            AnchorPreference.saveHostInfo(hostInfo);
                        }
//                        // 登录过环信，加载
//                        if (HyphenateHelper.getInstance().isLoggedIn()) {
//                            // 登录成功后加载聊天会话
//                            new Thread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    EMClient.getInstance().chatManager().loadAllConversations();
//                                }
//                            }).start();
                            launchMainActivity();
//                        } else {
//                            HuanXinLogin(userBase);
//                        }
                    }else{
                        LaunchHelper.getInstance().launchFinish(SplashActivity.this, LoginActivity.class);
                    }
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                    LaunchHelper.getInstance().launchFinish(SplashActivity.this, LoginActivity.class);
//                   涛哥临时添加，
//                    launchMainActivity();
                }

            });
        } else {
            mLocationManager.removeUpdates(mLocationListener);
            LaunchHelper.getInstance().launchFinish(SplashActivity.this, RegisterActivity.class);
        }
    }

    private void HuanXinLogin(final UserBase userBase) {
        HyphenateHelper.getInstance().login(userBase.getAccount(), userBase.getPassword(), new HyphenateHelper.OnLoginCallback() {
            @Override
            public void onSuccess() {
                if(isFirst)
                launchMainActivity();
                LogUtil.v("HMP==","环信登陆成功");

            }

            @Override
            public void onFailed() {
                HuanXinLogin(userBase);
                if(isFirst)
                launchMainActivity();
                LogUtil.v("HMP==","环信登陆失败");
            }
        });
    }

    private void launchMainActivity() {
        isFirst=false;
        mLocationManager.removeUpdates(mLocationListener);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToastUtil.showShortToast(SplashActivity.this, getString(R.string.login_success));
                LaunchHelper.getInstance().launchFinish(SplashActivity.this, MainActivity.class);
            }
        });
    }

    private void activation() {
        if (!UserPreference.isActived()) {
            ApiManager.activation(new IGetDataListener<BaseModel>() {
                @Override
                public void onResult(BaseModel baseModel, boolean isEmpty) {
                    // 记录已经激活过
                    UserPreference.activation();
                }

                @Override
                public void onError(String msg, boolean isNetworkError) {
                }

            });
        }
    }



    private String getAndroidId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }
    private void initPlatformInfo() {
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setW(String.valueOf(DeviceUtil.getScreenWidth(this)));
        platformInfo.setH(String.valueOf(DeviceUtil.getScreenHeight(this)));
        platformInfo.setVersion(DeviceUtil.getVersionName(this));
        platformInfo.setPhonetype(Build.MODEL);
        platformInfo.setSystemVersion(Build.VERSION.RELEASE);
        platformInfo.setPlatform("2");
        platformInfo.setProduct(C.PRODUCT_ID);
        platformInfo.setPid(getAndroidId());
        platformInfo.setImsi(getAndroidId());
        if(UserPreference.getCountry()!=null){
            platformInfo.setCountry(Util.setEnglishCountry(UserPreference.getCountry()));
        }else{
            platformInfo.setCountry(Util.getLacalCountry());
        }
        if(UserPreference.getFid()!=null){
            platformInfo.setFid(UserPreference.getFid());
        }else{
//            默认美国
            platformInfo.setFid(Util.F_IDAM);

        }
        platformInfo.setLanguage(Util.getLacalLanguage());
        platformInfo.setNetType(getNetType());
        platformInfo.setMobileIP(getMobileIP());
        platformInfo.setRelease(String.valueOf(DeviceUtil.getVersionCode(this)));
        // 保存platform信息
        PlatformPreference.setPlatfromInfo(platformInfo);
        // 设置API公用参数
        OkHttpHelper.getInstance().addCommonParam("platformInfo", PlatformPreference.getPlatformJsonString());
    }
    private String getNetType() {
        String type = "0";
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            type = "0";
        } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            type = "2";
        } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            String extraInfo = info.getExtraInfo();
            if (extraInfo.equals("cmwap")) {
                type = "3";
            } else if (extraInfo.equals("cmnet")) {
                type = "4";
            } else if (extraInfo.equals("ctnet")) {
                type = "5";
            } else if (extraInfo.equals("ctwap")) {
                type = "6";
            } else if (extraInfo.equals("3gwap")) {
                type = "7";
            } else if (extraInfo.equals("3gnet")) {
                type = "8";
            } else if (extraInfo.equals("uniwap")) {
                type = "9";
            } else if (extraInfo.equals("uninet")) {
                type = "10";
            }
        }
        return type;
    }

    private String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 请求定位信息
     */
    private void requestLocation() {
        if (checkGPS()) {// GPS打开
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CODE_ACCESS_FINE_LOCATION);
                } else {
                    intoLogin();
                    if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, mLocationListener);
                    }else
                    if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                        getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                    }
                }
            } else {
                intoLogin();
                if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 5, mLocationListener);
                }
                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                    getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                }
            }
//            }
        } else {// GPS关闭
            // 跳转到设置页，打开定位开关
            DialogUtil.showDoubleBtnDialog(getSupportFragmentManager(), null, getString(R.string.dialog_location_tip), getString(R.string.dialog_set), null, true, new OnDoubleDialogClickListener() {
                @Override
                public void onPositiveClick(View view) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, 0);
                }

                @Override
                public void onNegativeClick(View view) {
                }
            });
        }
    }

    /**
     * 检查GPS定位开关
     *
     * @return
     */
    private boolean checkGPS() {
        if (mLocationManager != null) {
            return (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
        }
        return false;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {// 申请权限成功
                if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {// Network定位
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 5, mLocationListener);
                    getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
                } else if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {// GPS定位
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    getCountryByLoaction(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                }
                intoLogin();

            } else {
                if(firstLogin){
                    firstLogin=false;
                    login();
                }

            }
        }
    }



    private boolean noDestroy=true;
    private LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {

            if(noDestroy){
                getCountryByLoaction(location);
            }

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };
    /**
     * 根据位置信息（经纬度）获得所在国家
     *
     * @param location 定位信息
     */
    private void getCountryByLoaction(Location location) {
        if (location != null) {
            LocationInfo locationInfo = new LocationInfo();
//美国
//            locationInfo.setLatitude("31.543114");
//            locationInfo.setLongitude("-97.154185");
//            jingdu=String.valueOf("31.543114");
//            weidu=String.valueOf("-97.154185");
//英国
//            locationInfo.setLatitude("52.2729944");
//            locationInfo.setLongitude("-0.8755515");
//            jingdu=String.valueOf("52.2729944");
//            weidu=String.valueOf("-0.8755515");

//印度
//            locationInfo.setLatitude("28.61345942");
//            locationInfo.setLongitude("77.20092773");
//            jingdu=String.valueOf("28.61345942");
//            weidu=String.valueOf("77.20092773");


            locationInfo.setLatitude(String.valueOf(location.getLatitude()));
            locationInfo.setLongitude(String.valueOf(location.getLongitude()));
            jingdu=String.valueOf(location.getLatitude());
            weidu=String.valueOf(location.getLongitude());
//            Log.v("设置成功","jingdu=="+jingdu+"==weidu="+weidu);

//            if(!TextUtils.isEmpty(jingdu) && !TextUtils.isEmpty(weidu) && isFirst){
//                mLocationManager.removeUpdates(mLocationListener);
                getAddress(jingdu,weidu,"English");
//            }
            SharedPreferenceUtil.setStringValue(SplashActivity.this,"LOCATION","getLatitude",jingdu);
            SharedPreferenceUtil.setStringValue(SplashActivity.this,"LOCATION","getLongitude",weidu);
        }
    }

    private void getAddress(String getLatitude, String getLongitude, final String language) {
        //		本地
        String url = "http://ditu.google.cn/maps/api/geocode/json?latlng="+ getLatitude + "," + getLongitude + "&&sensor=false&&language=";
//		线上
//		String url = "http://ditu.google.com/maps/api/geocode/json?latlng="+ lat + "," + lng + "&&sensor=false&&language=";
//        美国地址
//        String url = "http://ditu.google.cn/maps/api/geocode/json?latlng=31.543114,-97.154185&&sensor=false&&language=en_US";

        //获取国际化参数
        String localInfo = "en_US";
        url += localInfo;	//拼接url地址

        String charset = "UTF-8";

        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = okHttpClient.newCall(request);


        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                if(firstLogin){
                    firstLogin=false;
                    login();
                }
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(request!=null){
                    String str = response.body().string();
                    Gson gson =new Gson();
                    GoogleGPS googleGPS = gson.fromJson(str, GoogleGPS.class);
                    if(googleGPS.getResults()!=null){
                        String province = null;
                        String city = null;
                        String country = null;
                        for (int i = 0; i < googleGPS.getResults().size(); i++) {

                            if (googleGPS.getResults().size() > 0 && googleGPS.getResults().get(i).getTypes().get(0).equals("country")) {

                                 country = googleGPS.getResults().get(i).getAddress_components().get(0).getLong_name();
                            }

                            if (googleGPS.getResults().size() > 0 && googleGPS.getResults().get(i).getTypes().get(0).equals("administrative_area_level_1")) {
                                 province = googleGPS.getResults().get(i).getAddress_components().get(0).getLong_name();
                            }

                            if (googleGPS.getResults().size() > 0 && googleGPS.getResults().get(i).getTypes().get(0).equals("administrative_area_level_2")) {
                                 city = googleGPS.getResults().get(i).getAddress_components().get(0).getLong_name();
                            }

                        }
                        if(TextUtils.isEmpty(province)){
                            province=city;     //没有一级城市，使用二级城市
                        }
                        if(country!=null){
                            for (int i = 0; i <Util.getEnglishCountryList().size() ; i++) {
                                if(country.equals(Util.getEnglishCountryList().get(i))){
                                    Util.setCountryAndFid(country);
                                    UserPreference.setCountry(country);
                                    UserPreference.setRegisterCountry(country);
                                    if(firstLogin){
                                        firstLogin=false;
                                        login();

                                    }
                                    if(UserPreference.getProvince()==null || !UserPreference.getProvince().equals(province))
                                        UserPreference.setProvince(province);
                                }

                            }
                        }
                    }

                }
                if(firstLogin){
                    firstLogin=false;
                    login();
                }
            }
        });




//        OkHttpHelper.get()
//                .url(url)
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//
//                    }
//
//                    @Override
//                    public void onSuccess(String response, int id) {
//
//                        if (null != response) {
//
//
//
//
//
//
//
////                            JSONObject json = JSON.parseObject(response);// 转换为json数据
////                            JSONArray jsonArray = json.getJSONArray("results");
////
////                            Map<String, String> map = null;
////                            if (null != jsonArray && jsonArray.size() > 0) {// 增加验证
////                                JSONObject objResult = (JSONObject) jsonArray.get(0);
////                                JSONArray addressComponents = objResult.getJSONArray("address_components");
////                                //解析结果
////                                getByGoogleMap(addressComponents, objResult, language);
////
////                            }
//                        }
//
//                    }
//                });






//
//        OkHttpHelper.get()
//                .url(url)
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        Log.v("Exception",e.toString());
//                    }
//                    @Override
//                    public void onResponse(String response, int id) {
//                        if (null != response) {
//                            JSONObject json = JSON.parseObject(response);// 转换为json数据
//                            JSONArray jsonArray = json.getJSONArray("results");
//
//                            Map<String, String> map = null;
//                            if (null != jsonArray && jsonArray.size() > 0) {// 增加验证
//                                JSONObject objResult = (JSONObject) jsonArray.get(0);
//                                JSONArray addressComponents = objResult.getJSONArray("address_components");
//                                //解析结果
//                                getByGoogleMap(addressComponents, objResult, language);
//
//                            }
//                        }
//
//                    }
//                });
    }
//    private void getByGoogleMap(JSONArray addressComponents, JSONObject objResult, String language) {
//        String countryResult="";
//        String province="";
//        String city="";
//        if(addressComponents!=null&&addressComponents.size()>0){
//            for(int i=0;i<addressComponents.size();i++){
//                JSONObject obj = (JSONObject) addressComponents
//                        .get(i);
//                JSONArray types = obj.getJSONArray("types");
//                String type = (String)types.get(0);
//
//                //获取目标解析单位的key
//                String proviceKey ="administrative_area_level_1";
//                if (type.equals("country")){ //国家
//                    countryResult = obj.getString("long_name");
//                }
//
//                //获取目标解析单位的key
//                proviceKey = getGoogleProviceL1Key(countryResult);
//
//                if(type.equals(proviceKey)){
//                    province = obj.getString("long_name");// 州
//                }else if(type.equals("administrative_area_level_2")){
//                    city = obj.getString("long_name");// 城市
//                }else if (type.equals("country")){ //国家
//                    countryResult = obj.getString("long_name");
//                }
//            }
//
////			if(StringUtils.isBlank(province)){ //不是一级城市 取二级城市
//            if(TextUtils.isEmpty(province)){ //不是一级城市 取二级城市
//                province = city;
//            }
//        }
////        Log.v("province===",province);
//        String formatted_address = objResult.getString("formatted_address");
////
////        if (!TextUtils.isEmpty(province) && !province.equals(UserInfoXml.getProvinceName())) {
////            UserInfoXml.setProvinceName(province);
////        }
//        Log.v("setProvinceName",province+SettingXml.getProvinceName()+SettingXml.getUpdateArea());
//        if (!TextUtils.isEmpty(province) && !province.equals(SettingXml.getProvinceName())) {
//            if(!TextUtils.isEmpty(SettingXml.getUpdateArea())&&SettingXml.getUpdateArea()!=null){
//                SettingXml.setProvinceName(SettingXml.getUpdateArea());
//                UserInfoXml.setProvinceName(SettingXml.getUpdateArea());
//
//            }else{
//                SettingXml.setProvinceName(province);
//                UserInfoXml.setProvinceName(province);
//            }
//
//        }
//        if(!TextUtils.isEmpty(countryResult)){
//            String locationCountry;
//            locationCountry = countryResult;
//            // 香港需要转换
//            if ("Hong Kong-China".equals(locationCountry)) {
//                locationCountry = "HongKong";
//            }
//            if("United States".equals(locationCountry)){
//                locationCountry="America";
//            }
//            if (locationCountry.contains(" ")) {
//                locationCountry = locationCountry.replaceAll(" ", "");
//            }
//            // 支持的国家列表
//            List<String> countryList = new ArrayList<String>();
//            // 添加13个英语国家
//            countryList.add("America");
//            countryList.add("Australia");
//            countryList.add("India");
//            countryList.add("Indonesia");
//            countryList.add("UnitedKingdom");
//            countryList.add("Canada");
//            countryList.add("NewZealand");
//            countryList.add("Ireland");
//            countryList.add("SouthAfrica");
//            countryList.add("Singapore");
//            countryList.add("Pakistan");
//            countryList.add("Philippines");
//            countryList.add("HongKong");
//            boolean getCountry=false;
//            for (int i = 0; i < countryList.size(); i++) {
//                if (countryList.get(i).equals(locationCountry)) {
//                    getCountry = true;
//                    setCountry=locationCountry;
//                    PlatformInfoXml.setCountryFromSplishActivity(locationCountry);
////                      定位成功
//                    if(TextUtils.isEmpty(PlatformInfoXml.getCountry())|| PlatformInfoXml.getCountry()==null
//                            || TextUtils.isEmpty(PlatformInfoXml.getKeyFid()) || PlatformInfoXml.getKeyFid()==null){
//                        setCountryAndFid(locationCountry);
//                    }
//                    UserInfoXml.setWelComeLocation(true);
//                    if(!UserInfoXml.getIsClient()){
////                            Log.v("设置成功","Splash开始激活");
//                        activation();
//                    }
//                }
//            }
//            if(!getCountry){
//                init();
//            }
//        }else{
////            没有获取到国家信息
//            init();
//        }
//        completeLocation=true;
//    }
    public static String getGoogleProviceL1Key(String country) {
        String proviceL1Key = "administrative_area_level_1";
        if("UnitedKingdom".equals(country) || "United Kingdom".equals(country)){
            proviceL1Key = "administrative_area_level_2";
        }
        return proviceL1Key;
    }
}
