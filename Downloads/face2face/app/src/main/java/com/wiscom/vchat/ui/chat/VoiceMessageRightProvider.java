package com.wiscom.vchat.ui.chat;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMVoiceMessageBody;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.HyphenateHelper;
import com.wiscom.vchat.common.RecordUtil;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.adapter.provider.ItemViewProvider;
import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.util.DateTimeUtil;
import com.wiscom.library.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 聊天语音消息（发送方）
 * Created by zhangdroid on 2017/6/29.
 */
public class VoiceMessageRightProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;

    public VoiceMessageRightProvider(Context context) {
        this.mContext = context;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_voice_right;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.VOICE && item.direct() == EMMessage.Direct.SEND);
    }

    @Override
    public void convert(EMMessage emMessage, int position, final RecyclerViewHolder holder) {
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                .error(Util.getDefaultImageCircle()).url(UserPreference.getSmallImage()).imageView((ImageView) holder.getView(R.id.item_chat_voice_avatar_right)).build());
        if (null != emMessage) {
            holder.setText(R.id.item_chat_voice_time_right, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
            final EMVoiceMessageBody emVoiceMessageBody = (EMVoiceMessageBody) emMessage.getBody();
            if (null != emVoiceMessageBody) {
                // 如果语音文件本地路径不存在，则下载
                if (TextUtils.isEmpty(emVoiceMessageBody.getLocalUrl())) {
                    HyphenateHelper.getInstance().downloadAttachment(emMessage);
                }
                // 语音时间
                holder.setText(R.id.item_chat_voice_right_duration, TextUtils.concat(String.valueOf(emVoiceMessageBody.getLength()), "'").toString());
                holder.setOnClickListener(R.id.item_chat_voice_right_ll, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 播放语音动画
                        List<Drawable> list = new ArrayList<Drawable>();
                        list.add(mContext.getDrawable(R.drawable.sound_wave_right1));
                        list.add(mContext.getDrawable(R.drawable.sound_wave_right2));
                        list.add(mContext.getDrawable(R.drawable.sound_wave_right3));
                        final AnimationDrawable animationDrawable = Utils.getFrameAnim(list, true, 200);
                        holder.setImageDrawable(R.id.item_chat_voice_right, animationDrawable);
                        animationDrawable.start();
                        if (RecordUtil.getInstance().isPlaying()) {
                            RecordUtil.getInstance().stop();
                        }
                        RecordUtil.getInstance().play(emVoiceMessageBody.getLocalUrl(), new RecordUtil.OnPlayerListener() {
                            @Override
                            public void onCompleted() {
                                animationDrawable.stop();
                                holder.setImageResource(R.id.item_chat_voice_right, R.drawable.sound_wave_right3);
                            }

                            @Override
                            public void onPaused() {
                                animationDrawable.stop();
                                holder.setImageResource(R.id.item_chat_voice_right, R.drawable.sound_wave_right3);
                            }
                        });
                    }
                });
            }
        }
    }

}
