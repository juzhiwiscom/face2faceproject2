package com.wiscom.vchat.ui.detail.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.wiscom.vchat.common.VideoHelper;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.UserDetail;
import com.wiscom.vchat.data.model.UserDetailforOther;
import com.wiscom.vchat.data.model.UserPhoto;
import com.wiscom.vchat.parcelable.ChatParcelable;
import com.wiscom.vchat.parcelable.VideoInviteParcelable;
import com.wiscom.vchat.ui.chat.ChatActivity;
import com.wiscom.vchat.ui.detail.contract.UserDetailContract;
import com.wiscom.library.util.LaunchHelper;

import java.util.List;

/**
 * Created by Administrator on 2017/6/7.
 */

public class UserDetailPresenter implements UserDetailContract.IPresenter {
    private UserDetailContract.IView mUserDetailView;
    private Context mContext;
    private UserBase userBase;

    public UserDetailPresenter(UserDetailContract.IView mUserDetailView) {
        this.mUserDetailView = mUserDetailView;
        this.mContext = mUserDetailView.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void finish() {
        mUserDetailView = null;
        mContext = null;
    }

    @Override
    public void getUserInfoData() {
        mUserDetailView.showLoading();
        ApiManager.getUserInfo(mUserDetailView.getUserId(), new IGetDataListener<UserDetailforOther>() {

            @Override
            public void onResult(UserDetailforOther userDetailforOther, boolean isEmpty) {
                mUserDetailView.dismissLoading();
                if (userDetailforOther != null) {
                    mUserDetailView.getData(userDetailforOther);
                    UserDetail userDetail = userDetailforOther.getUserDetail();
                    if (userDetail != null) {
                        mUserDetailView.getFollowOrUnFollow(userDetail);
                        UserBase userBase1 = userDetail.getUserBase();
                        if (userBase1 != null) {
                            userBase = userBase1;
                            mUserDetailView.setInforData(userBase1);
                            if (!TextUtils.isEmpty(userBase1.getStatus())) {
                                int status = Integer.parseInt(userBase1.getStatus());
                                mUserDetailView.getStatus(status);
                            }
                            int userType = userBase1.getUserType();
                            if(userType==1){
                                mUserDetailView.isAnchor();
                            }
                        }
                        List<UserPhoto> userPhotos = userDetail.getUserPhotos();
                        mUserDetailView.setViewPagerData(userPhotos);
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mUserDetailView.dismissLoading();
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
                    mUserDetailView.showTip(msg);
                }
            }
        });
    }

    //关注
    @Override
    public void follow(String remoteUid) {
        ApiManager.follow(remoteUid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                mUserDetailView.followSucceed();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
                    mUserDetailView.showTip(msg);
                }
            }
        });
    }

    //取消关注
    @Override
    public void unFollow(String remoteUid) {
        ApiManager.unFollow(remoteUid, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                mUserDetailView.followSucceed();
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                if (isNetworkError) {
                    mUserDetailView.showNetworkError();
                } else {
                    mUserDetailView.showTip(msg);
                }
            }
        });
    }


    @Override
    public void goVideoCallPage() {
        if (userBase != null) {
            if (!TextUtils.isEmpty(userBase.getStatus())) {
                //跳转视频聊天的界面
                VideoHelper.startVideoInvite(new VideoInviteParcelable(false, userBase.getGuid(), userBase.getAccount()
                        , userBase.getNickName(), userBase.getIconUrlMininum()), mUserDetailView.obtainFragmentManager());
            }
        }
    }

    @Override
    public void goWriteMessagePage() {
        //跳转写信的界面
        if (userBase != null) {
            Log.e("AAAAAAA", "goWriteMessagePage: "+userBase.getGuid()+"--"+userBase.getAccount()+"--"+userBase.getNickName()+"--"+userBase.getIconUrlMininum());
            LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(userBase.getGuid(),
                    userBase.getAccount(), userBase.getNickName(), userBase.getIconUrlMininum()));
        }
    }

}
