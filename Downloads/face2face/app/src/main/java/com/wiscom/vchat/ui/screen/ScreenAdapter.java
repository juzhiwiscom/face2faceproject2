package com.wiscom.vchat.ui.screen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.model.UserBase;
import com.wiscom.vchat.data.model.VideoSquare;

import java.util.List;

/**
 * Created by tianzhentao on 2018/1/3.
 */
    class ScreenAdapter extends RecyclerView.Adapter<ScreenAdapter.ViewHolder> {

    private final Context mContext;
    private LayoutInflater mInflater;
    private boolean isnewVideo;
    private List<VideoSquare> mDatas;
    public interface OnItemClickLitener {
        void onItemClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    public ScreenAdapter(Context context, List<VideoSquare> datats,boolean isNewVideo) {
        mInflater = LayoutInflater.from(context);
        mDatas = datats;
        mContext=context;
        isnewVideo=isNewVideo;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ViewHolder(View arg0)
        {
            super(arg0);
        }

        ImageView mImg;
        TextView mTxt;
        TextView mPraise;
    }

    @Override
    public int getItemCount()
    {
        return mDatas.size();
    }

    /**
     * 创建ViewHolder
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View view = mInflater.inflate(R.layout.item_screen,
                viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.mImg = (ImageView) view
                .findViewById(R.id.item_screen_avatar);
        viewHolder.mTxt = (TextView) view.findViewById(R.id.item_screen_nickname);
        viewHolder.mPraise = (TextView) view.findViewById(R.id.item_screen_praise);

        return viewHolder;
    }

    /**
     * 设置值
     */
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        if(mDatas!=null&&mDatas.get(i)!=null){
            VideoSquare videoSquare = mDatas.get(i);
            if(isnewVideo){
                viewHolder.mPraise.setVisibility(View.GONE);
            }else {
                viewHolder.mPraise.setVisibility(View.VISIBLE);
                viewHolder.mPraise.setText(videoSquare.getUserVideoShow().getPraiseCount()+"");
            }

            UserBase userBase = videoSquare.getUserBase();
            viewHolder.mTxt.setText(userBase.getNickName()+","+userBase.getAge());
//            LinearLayout.LayoutParams params= (LinearLayout.LayoutParams) viewHolder.mImg.getLayoutParams();
////获取当前控件的布局对象
//            params.height=mHeight/4;//设置当前控件布局的高度
//            params.weight=mWidth/3;
//            viewHolder.mImg.setLayoutParams(params);//将设置好的布局参数应用到控件中
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userBase.getIconUrl())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(viewHolder.mImg).build());
            //如果设置了回调，则设置点击事件
            if (mOnItemClickLitener != null) {
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        mOnItemClickLitener.onItemClick(viewHolder.itemView, i);
                    }
                });

            }

        }
    }

}


//class ScreenAdapter extends CommonRecyclerViewAdapter<VideoSquare> {
//
//    public ScreenAdapter(Context context, int layoutResId) {
//        super(context, layoutResId);
//    }
//
//    public ScreenAdapter(Context context, int layoutResId, List<VideoSquare> dataList) {
//        super(context, layoutResId, dataList);
//    }
//
//    @Override
//    public void convert(VideoSquare userVideo, int position, RecyclerViewHolder holder) {
//        if(userVideo!=null){
//            UserBase userBase = userVideo.getUserBase();
//            UserVideoShow userVideoShow = userVideo.getUserVideoShow();
////            holder.setText(R.id.tv_pay_way,withdrawRecord.getWithdrawWay());
//            holder.setText(R.id.item_screen_nickname,userBase.getNickName()+"，"+userBase.getAge());
////            holder.setText(R.id.tv_withdraw_time, DateTimeUtil.convertTimeMillis2String(withdrawRecord.getRecordTime()));
//
////            Date nowTime = new Date(screenModel.getTime());
////            SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyy-MM-dd");
////            String retStrFormatNowDate = sdFormatter.format(nowTime);
////            holder.setText(R.id.item_screen_time,retStrFormatNowDate);
//
//            ImageView ivAvatar = (ImageView) holder.getView(R.id.item_screen_avatar);
//            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(userBase.getIconUrlMiddle())
//                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(ivAvatar).build());
//        }
//
//    }
//}
