package com.wiscom.vchat.ui.homepage.presenter;

import android.content.Context;
import android.view.View;

import com.google.gson.Gson;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.data.api.ApiConstant;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.SearchCriteria;
import com.wiscom.vchat.data.model.SearchUser;
import com.wiscom.vchat.data.model.SearchUserList;
import com.wiscom.vchat.data.preference.SearchPreference;
import com.wiscom.vchat.parcelable.UserDetailParcelable;
import com.wiscom.vchat.ui.detail.UserDetailActivity;
import com.wiscom.vchat.ui.homepage.ListAdapter;
import com.wiscom.vchat.ui.homepage.contract.ListContract;
import com.wiscom.library.adapter.MultiTypeRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.adapter.wrapper.OnLoadMoreListener;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.LogUtil;
import com.wiscom.library.util.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangdroid on 2017/6/2.
 */
public class ListPresenter implements ListContract.IPresenter {
    private ListContract.IView mListView;
    private Context mContext;
    private int pageNum = 1;
    private static final String pageSize = "20";
    private String url;
    private ListAdapter mListAdapter;

    public ListPresenter(ListContract.IView view) {
        this.mListView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void finish() {
        mListView = null;
        mContext = null;
        url = null;
        mListAdapter = null;
    }

    @Override
    public void loadRecommendUserList(int type) {
        switch (type) {
            case C.homepage.TYPE_GODDESS:
                url = ApiConstant.URL_HOMEPAGE_INCOME;
                break;
            case C.homepage.TYPE_ACTIVE_ANCHOR:
                url = ApiConstant.URL_HOMEPAGE_ACTIVE;
                break;
            case C.homepage.TYPE_NEW:
                url = ApiConstant.URL_HOMEPAGE_NEW;
                break;
            case C.homepage.TYPE_ACTIVE_FRIEND:
                url = ApiConstant.URL_HOMEPAGE_ACTIVE;
                break;
            case C.homepage.TYPE_NEW_FRIEND:
                url = ApiConstant.URL_HOMEPAGE_NEW;
                break;
        }
        mListAdapter = new ListAdapter(mContext, R.layout.item_homepage_list);
        mListAdapter.setFragmentManager(mListView.obtainFragmentManager());
        mListAdapter.setOnItemClickListener(new MultiTypeRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, RecyclerViewHolder viewHolder) {
                SearchUser searchUser = mListAdapter.getItemByPosition(position);
                if (null != searchUser) {
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(String.valueOf(searchUser.getUesrId())));
                }
            }
        });
        // 设置加载更多
        mListView.setAdapter(mListAdapter, R.layout.common_load_more);
        mListView.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadMore();
            }
        });
        load();
    }

    @Override
    public void refresh() {
        LogUtil.i("Homepage", "refresh()#url = " + url + " pageNum= " + pageNum);
        pageNum = 1;
        load();
    }

    /**
     * 上拉加载更多
     */
    private void loadMore() {
        LogUtil.i("Homepage", "loadMore()#url = " + url + " pageNum= " + pageNum);
        mListView.showLoadMore();
        pageNum++;
        load();
    }

    private void load() {
        LogUtil.i("Homepage", "load()#url = " + url + " pageNum= " + pageNum + " pageSize=" + pageSize);
        // 设置搜索条件
        Map<String, SearchCriteria> criteria = new HashMap<>();
        SearchCriteria searchCriteria = SearchPreference.getSearchCriteria();
        if (null != searchCriteria) {
            criteria.put("criteria", searchCriteria);
        }
        ApiManager.getHomepageRecommend(url, pageNum, pageSize, new Gson().toJson(criteria), new IGetDataListener<SearchUserList>() {
            @Override
            public void onResult(SearchUserList searchUserList, boolean isEmpty) {
                LogUtil.i("Homepage", "onResult()#pageNum= " + searchUserList.getPageNum() + " pageSize=" + searchUserList.getPageSize()
                        + " SearchUserList.size()= " + (searchUserList.getSearchUserList() == null ? 0 : searchUserList.getSearchUserList().size()));
                if (isEmpty) {
                    if (pageNum == 1) {
                        mListView.toggleShowEmpty(true, null);
                    } else if (pageNum > 1) {
                        mListView.showNoMore();
                    }
                } else {
                    if (null != searchUserList) {
                        List<SearchUser> list = searchUserList.getSearchUserList();
                        if (!Utils.isListEmpty(list)) {
                            if (pageNum == 1) {
                                mListAdapter.bind(list);
                            } else if (pageNum > 1) {
                                mListAdapter.appendToList(list);
                            }
                            mListView.hideLoadMore();
                        }
                    }
                }
                mListView.hideRefresh(1);
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mListView.hideRefresh(1);
                if (!isNetworkError) {
                    mListView.toggleShowError(true, msg);
                }
            }

        });
    }

}
