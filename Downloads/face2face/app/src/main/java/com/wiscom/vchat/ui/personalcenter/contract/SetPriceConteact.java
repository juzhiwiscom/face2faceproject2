package com.wiscom.vchat.ui.personalcenter.contract;

import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;

/**
 * Created by Administrator on 2017/7/5.
 */

public interface SetPriceConteact {

    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        String getTestring();

        void setTextString(String string);

        void finishActivity();
    }
    interface IPresenter extends BasePresenter {
//        void setTextString(String string);
        void savePrice();
}
    }
