package com.wiscom.vchat.ui.video.presenter;

import android.Manifest;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;

import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.AgoraHelper;
import com.wiscom.vchat.event.AgoraEvent;
import com.wiscom.vchat.parcelable.VideoParcelable;
import com.wiscom.vchat.ui.video.VideoActivity;
import com.wiscom.vchat.ui.video.contract.VideoInviteContract;
import com.wiscom.library.permission.PermissionCallback;
import com.wiscom.library.permission.PermissionManager;
import com.wiscom.library.permission.PermissonItem;
import com.wiscom.library.util.LaunchHelper;

import java.io.IOException;

/**
 * Created by zhangdroid on 2017/6/7.
 */
public class VideoInvitePresenter implements VideoInviteContract.IPresenter {
    private VideoInviteContract.IView mVideoInviteView;
    private Context mContext;
    private long mGuid;
    private String mAccount;
    private String mNickname;
    private String mHostPrice;
    private Camera mCamera;
    private String mCountry;
    private String mGender;
    private String imgageUrl;
    private String fromView;


    public VideoInvitePresenter(VideoInviteContract.IView view, long guid, String account, String nickname, String hostPrice,String mCountry,String mGender,String imgageUrl,String fromView) {
        this.mVideoInviteView = view;
        this.mContext = view.obtainContext();
        this.mGuid = guid;
        this.mAccount = account;
        this.mNickname = nickname;
        this.mHostPrice = hostPrice;
        this.mCountry=mCountry;
        this.mGender=mGender;
        this.imgageUrl=imgageUrl;
        this.fromView=fromView;
    }

    @Override
    public void start() {
    }

    @Override
    public void finish() {
        mVideoInviteView = null;
        mContext = null;
    }

    @Override
    public void startPreView() {
        // 检查录音和摄像头权限
        PermissionManager.getInstance(mContext)
                .addPermission(new PermissonItem(C.permission.PERMISSION_CAMERA, mContext.getString(R.string.permission_camera), R.drawable.permission_camera))
                .addPermission(new PermissonItem(C.permission.PERMISSION_RECORD_AUDIO, mContext.getString(R.string.permission_record), R.drawable.permission_record))
                .checkMutiPermission(new PermissionCallback() {
                    @Override
                    public void onGuaranteed(String permisson, int position) {
                        if (Manifest.permission.CAMERA.equals(permisson)) {
                            openCamera();
                            setPreview();
                        }
                    }

                    @Override
                    public void onDenied(String permisson, int position) {
                        if (Manifest.permission.CAMERA.equals(permisson)) {
                            mVideoInviteView.hideSurfaceView();
                            mVideoInviteView.showTip(mContext.getString(R.string.video_permission_camera));
                        } else if (Manifest.permission.RECORD_AUDIO.equals(permisson)) {
                            mVideoInviteView.showTip(mContext.getString(R.string.video_permission_record));
                        }
                        mVideoInviteView.finishActivity(2);
                    }

                    @Override
                    public void onFinished() {
                        mVideoInviteView.hideSurfaceView();
                    }

                    @Override
                    public void onClosed() {
                        mVideoInviteView.hideSurfaceView();
                    }
                });
    }

    @Override
    public void stopPreview() {
        releasePreview();
    }

    @Override
    public void startInvite(String guid, String account, String userInfo) {
        AgoraHelper.getInstance().startInvite(guid, account, userInfo);
    }

    @Override
    public void cancelInVite(String guid, String account) {
        AgoraHelper.getInstance().stopInvite(guid, account);
    }

    @Override
    public void acceptInvite(final String channelId, final String account) {
        LaunchHelper.getInstance().launchFinish(mContext, VideoActivity.class,
                new VideoParcelable(mGuid, mAccount, mNickname, channelId, "1",mCountry,mGender,imgageUrl,C.Video.ACCEPT_INVITE));
        AgoraHelper.getInstance().acceptInvite(channelId, account);


//        ApiManager.videoCall(String.valueOf(mGuid), new IGetDataListener<VideoCall>() {
//            @Override
//            public void onResult(VideoCall videoCall, boolean isEmpty) {
//                if (!"-1".equals(videoCall.getBeanStatus())) {// 余额充足
//                    LaunchHelper.getInstance().launchFinish(mContext, VideoActivity.class,
//                            new VideoParcelable(mGuid, mAccount, mNickname, channelId, videoCall.getHostPrice(),mCountry,mGender,imgageUrl,C.Video.ACCEPT_INVITE));
//                    AgoraHelper.getInstance().acceptInvite(channelId, account);
//                } else {
//                    // 余额不足，提示用户
//                    VideoHelper.showBalanceNotEnoughTip(mVideoInviteView.obtainFragmentManager());
//                }
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//                mVideoInviteView.showTip(mContext.getString(R.string.video_accept_failed));
//            }
//        });
    }

    @Override
    public void refuseInvite(String channelId, String account) {
        AgoraHelper.getInstance().refuseInvite(channelId, account);
        AgoraHelper.getInstance().leaveChannel(channelId);
        mVideoInviteView.finishActivity(1);
    }

    @Override
    public void handleAgoraEvent(AgoraEvent agoraEvent) {
        if (null != agoraEvent) {
            switch (agoraEvent.eventCode) {
                case AgoraHelper.EVENT_CODE_INVITE_FAILED:// 发起视频呼叫失败
                    mVideoInviteView.changeInviteState(mContext.getString(R.string.video_invite_failed));
                    AgoraHelper.getInstance().leaveChannel(agoraEvent.channelId);
                    mVideoInviteView.finishActivity(2);
                    break;
                case AgoraHelper.EVENT_CODE_INVITE_SUCCESS:// 发起视频呼叫成功
                    mVideoInviteView.changeInviteState(mContext.getString(R.string.video_inviting));
                    break;
                case AgoraHelper.EVENT_CODE_INVITE_CANCEL:// 取消已经发起的视频呼叫
                    AgoraHelper.getInstance().leaveChannel(agoraEvent.channelId);
                    mVideoInviteView.finishActivity(1);
                    break;
                case AgoraHelper.EVENT_CODE_INVITE_ACCEPT:// 对方接受了视频呼叫
                    LaunchHelper.getInstance().launchFinish(mContext, VideoActivity.class,
                            new VideoParcelable(mGuid, mAccount, mNickname, agoraEvent.channelId, "1",mCountry,mGender,imgageUrl,C.Video.GO_CALL));
                    break;
                case AgoraHelper.EVENT_CODE_INVITE_REFUSE:// 对方拒绝了视频呼叫
                    AgoraHelper.getInstance().leaveChannel(agoraEvent.channelId);
                    mVideoInviteView.changeInviteState(mContext.getString(R.string.video_invite_refused));
                    mVideoInviteView.finishActivity(2);
                    break;

                case AgoraHelper.EVENT_CODE_INVITE_END_PEER:// 发起方取消了视频呼叫
                    mVideoInviteView.changeInviteState(mContext.getString(R.string.video_invite_canceled));
                    mVideoInviteView.finishActivity(2);
                    break;
            }
        }
    }

    private void openCamera() {
        // 打开相机并设置默认为前置摄像头
        int numberOfCameras = Camera.getNumberOfCameras();
        CameraInfo cameraInfo = new CameraInfo();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
                mCamera = Camera.open(i);
                break;
            }
        }
    }

    private void setPreview() {
        if (null != mCamera) {
            // 设置SurfaceView并开始预览
            try {
                mCamera.setPreviewDisplay(mVideoInviteView.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
            // 竖屏需要旋转90度预览
            mCamera.setDisplayOrientation(90);
            mCamera.startPreview();
        }
    }

    private void releasePreview() {
        if (null != mCamera) {
            // 关闭预览并释放资源
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

}
