package com.wiscom.vchat.ui.screen;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wiscom.library.dialog.AlertDialog;
import com.wiscom.library.dialog.OnDialogClickListener;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.Utils;
import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragmentActivity;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.BaseModel;
import com.wiscom.vchat.data.model.RecommendNewJoinList;
import com.wiscom.vchat.data.model.UserVideoShow;
import com.wiscom.vchat.data.model.VideoSquare;
import com.wiscom.vchat.data.preference.BeanPreference;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.event.UpdateRecommendVideoEvent;
import com.wiscom.vchat.parcelable.PayParcelable;
import com.wiscom.vchat.parcelable.PlayViewParcelable;
import com.wiscom.vchat.ui.pay.PayActivity;
import com.wiscom.vchat.ui.personalcenter.PlayActivity;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by tianzhentao on 2018/4/9.
 */

public class Screen1Activity extends BaseFragmentActivity {
    @BindView(R.id.screen_exit)
    ImageView screenExit;
    @BindView(R.id.screen_all_people)
    LinearLayout allPeople;
    @BindView(R.id.screen_woman_people)
    LinearLayout womanPeople;
    @BindView(R.id.screen_man_people)
    LinearLayout manPeople;
    @BindView(R.id.screen_rv_new_video)
    RecyclerView screenRvNewVideo;
    @BindView(R.id.screen_rv_hot_video)
    RecyclerView screenRvHotVideo;
    private ScreenAdapter mNewVideoAdapter;
    private ScreenAdapter mHotVideoAdapter;
    private int width;
    private int height;
    private List<VideoSquare> mNewVideoList;
    private List<VideoSquare> mHotvideoList;
    private boolean isNewVideo=true;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_screen_1;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {

        DisplayMetrics dm = getResources().getDisplayMetrics();
        width = dm.widthPixels;
        height = dm.heightPixels;

        int beanCount = BeanPreference.getScreenGender();
        if(beanCount==0){
            manPeople.setBackgroundResource(R.mipmap.screen_select);
            womanPeople.setBackgroundResource(R.mipmap.screen_no_select);
            allPeople.setBackgroundResource(R.mipmap.screen_no_select);
        }else if(beanCount==1){
            womanPeople.setBackgroundResource(R.mipmap.screen_select);
            allPeople.setBackgroundResource(R.mipmap.screen_no_select);
            manPeople.setBackgroundResource(R.mipmap.screen_no_select);
        }else{
            allPeople.setBackgroundResource(R.mipmap.screen_select);
            womanPeople.setBackgroundResource(R.mipmap.screen_no_select);
            manPeople.setBackgroundResource(R.mipmap.screen_no_select);
        }


    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {
//        最新视频
        ApiManager.recommendNewJoin(1, "30", new IGetDataListener<RecommendNewJoinList>() {
            @Override
            public void onResult(RecommendNewJoinList recommendNewJoinList, boolean isEmpty) {if (null != recommendNewJoinList) {
                mNewVideoList = recommendNewJoinList.getVideoSquareList();
                if (!Utils.isListEmpty(mNewVideoList)) {
                    //        设置适配器布局管理器
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Screen1Activity.this);
                    linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    screenRvNewVideo.setLayoutManager(linearLayoutManager);
                    isNewVideo=true;
                    //设置适配器
                    mNewVideoAdapter = new ScreenAdapter(Screen1Activity.this, mNewVideoList,isNewVideo);
                    screenRvNewVideo.setAdapter(mNewVideoAdapter);
                    mNewVideoAdapter.setOnItemClickLitener(new ScreenAdapter.OnItemClickLitener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            if(mNewVideoList!=null&&mNewVideoList.get(position)!=null&&mNewVideoList.get(position).getUserVideoShow()!=null){
                                UserVideoShow userVideoShow = mNewVideoList.get(position).getUserVideoShow();
                                String videoUrl = userVideoShow.getVideoUrl();//视频Url
                                String thumbnailUrl = userVideoShow.getThumbnailUrl();//预加载图片
                                String nickName = mNewVideoList.get(position).getUserBase().getNickName();//昵称
                                String guid = mNewVideoList.get(position).getUserBase().getGuid()+"";//id
                                String icon = mNewVideoList.get(position).getUserBase().getIcon();
                                String account = mNewVideoList.get(position).getUserBase().getAccount();
                                int praiseCount = userVideoShow.getPraiseCount();//点赞数
                                String videoId = userVideoShow.getId()+"";
                                String isPraise = mNewVideoList.get(position).getIsPraise()+"";
                                LaunchHelper.getInstance().launch(mContext, PlayActivity.class,new PlayViewParcelable(videoUrl,thumbnailUrl,nickName,guid,icon,account,praiseCount+"",videoId,isPraise));
                            }
                        }
                    });
                }
            }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
//        最热视频
        ApiManager.recommendHotJoin(1, "30", new IGetDataListener<RecommendNewJoinList>() {

            @Override
            public void onResult(RecommendNewJoinList recommendNewJoinList, boolean isEmpty) {if (null != recommendNewJoinList) {
                 mHotvideoList = recommendNewJoinList.getVideoSquareList();
                if (!Utils.isListEmpty(mHotvideoList)) {
                    //        设置适配器布局管理器
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Screen1Activity.this);
                    linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    screenRvHotVideo.setLayoutManager(linearLayoutManager);
                    isNewVideo=false;
                    //设置适配器
                    mHotVideoAdapter = new ScreenAdapter(Screen1Activity.this, mHotvideoList,isNewVideo);
                    screenRvHotVideo.setAdapter(mHotVideoAdapter);
                    mHotVideoAdapter.setOnItemClickLitener(new ScreenAdapter.OnItemClickLitener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            if(mHotvideoList!=null&&mHotvideoList.get(position)!=null&&mHotvideoList.get(position).getUserVideoShow()!=null){
                                UserVideoShow userVideoShow = mHotvideoList.get(position).getUserVideoShow();
                                String videoUrl = userVideoShow.getVideoUrl();//视频Url
                                String thumbnailUrl = userVideoShow.getThumbnailUrl();//预加载图片
                                String nickName = mHotvideoList.get(position).getUserBase().getNickName();//昵称
                                String guid = mHotvideoList.get(position).getUserBase().getGuid()+"";//id
                                String icon = mHotvideoList.get(position).getUserBase().getIcon();
                                String account = mHotvideoList.get(position).getUserBase().getAccount();
                                int praiseCount = userVideoShow.getPraiseCount();//点赞数
                                String videoId = userVideoShow.getId()+"";
                                String isPraise = mHotvideoList.get(position).getIsPraise()+"";
                                LaunchHelper.getInstance().launch(mContext, PlayActivity.class,new PlayViewParcelable(videoUrl,thumbnailUrl,nickName,guid,icon,account,praiseCount+"",videoId,isPraise));
                            }
                        }
                    });
                }
            }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.screen_exit, R.id.screen_all_people, R.id.screen_woman_people, R.id.screen_man_people})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.screen_exit:
                finish();
                break;
            case R.id.screen_all_people:
                allPeople.setBackgroundResource(R.mipmap.screen_select);
                womanPeople.setBackgroundResource(R.mipmap.screen_no_select);
                manPeople.setBackgroundResource(R.mipmap.screen_no_select);
                BeanPreference.setScreenGender(-1);
                break;
            case R.id.screen_woman_people:
                AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.pay_diamon), mContext.getString(R.string.vedio_interupt),
                        mContext.getString(R.string.sure), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                            @Override
                            public void onNegativeClick(View view) {
                            }

                            @Override
                            public void onPositiveClick(View view) {
//                                判断钻石是否足够，不够的话跳转支付界面
                                int beanCount = BeanPreference.getBeanCount();
                                if (beanCount != 0 && beanCount > 50) {
//                                    钻石足够
                                    int currentBean = BeanPreference.getBeanCount() - 50;
                                    BeanPreference.setBeanCount(currentBean);
                                    womanPeople.setBackgroundResource(R.mipmap.screen_select);
                                    allPeople.setBackgroundResource(R.mipmap.screen_no_select);
                                    manPeople.setBackgroundResource(R.mipmap.screen_no_select);
                                    BeanPreference.setScreenGender(1);
                                } else {
//                                    充值
                                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
                                    intoPayActivityNum("1");
                                }

                            }
                        });

                break;
            case R.id.screen_man_people:
                AlertDialog.show(getSupportFragmentManager(), mContext.getString(R.string.pay_diamon), mContext.getString(R.string.vedio_interupt),
                        mContext.getString(R.string.sure), mContext.getString(R.string.cancel), new OnDialogClickListener() {
                            @Override
                            public void onNegativeClick(View view) {
                            }

                            @Override
                            public void onPositiveClick(View view) {
//                                判断钻石是否足够，不够的话跳转支付界面
                                int beanCount = BeanPreference.getBeanCount();
                                if (beanCount != 0 && beanCount > 2) {
//                                    钻石足够
                                    int currentBean = BeanPreference.getBeanCount() - 2;
                                    BeanPreference.setBeanCount(currentBean);
                                    manPeople.setBackgroundResource(R.mipmap.screen_select);
                                    womanPeople.setBackgroundResource(R.mipmap.screen_no_select);
                                    allPeople.setBackgroundResource(R.mipmap.screen_no_select);
                                    BeanPreference.setScreenGender(0);
                                } else {
//                                    充值
                                    LaunchHelper.getInstance().launch(mContext, PayActivity.class, new PayParcelable(C.pay.BUY_DIAMON));
                                    intoPayActivityNum("2");
                                }

                            }
                        });
                break;
        }
    }
    private void intoPayActivityNum(String type) {
        ApiManager.intoPayActivityNum("", UserPreference.getId(),"10001",type,new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }
    @Subscribe
    public void onEvent(UpdateRecommendVideoEvent event) {
        loadData();
    }

}
