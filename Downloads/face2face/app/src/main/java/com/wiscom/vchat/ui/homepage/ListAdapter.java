package com.wiscom.vchat.ui.homepage;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.wiscom.vchat.C;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.common.VideoHelper;
import com.wiscom.vchat.data.model.SearchUser;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.vchat.parcelable.ChatParcelable;
import com.wiscom.vchat.parcelable.VideoInviteParcelable;
import com.wiscom.vchat.ui.chat.ChatActivity;
import com.wiscom.library.adapter.CommonRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.util.LaunchHelper;

import java.util.List;

/**
 * 首页推荐用户适配器
 * Created by zhangdroid on 2017/6/3.
 */
public class ListAdapter extends CommonRecyclerViewAdapter<SearchUser> {
    private FragmentManager mFragmentManager;

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    public ListAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public ListAdapter(Context context, int layoutResId, List<SearchUser> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(final SearchUser searchUser, int position, RecyclerViewHolder holder) {
        if (null != searchUser) {
            ImageView imageView = (ImageView) holder.getView(R.id.item_homepage_avatar);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(searchUser.getIconUrlMiddle())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build());
            holder.setText(R.id.item_homepage_nickname, searchUser.getNickName());
            holder.setVisibility(R.id.item_video_chat, true);
            if (!UserPreference.isAnchor()) {
                holder.setText(R.id.item_homepage_price, mContext.getString(R.string.person_price, String.valueOf(searchUser.getHostprice())));
            } else {
                // 播主之间不能相互视频和聊天
                if (searchUser.getUserType() == 1) {
                    holder.setVisibility(R.id.item_video_chat, false);
                }
            }
            // 视频
            ImageButton iBVideo = (ImageButton) holder.getView(R.id.item_homepage_video);
            final int status = searchUser.getStatus();
            switch (status) {
                case C.homepage.STATE_FREE:// 空闲
                    iBVideo.setImageResource(R.drawable.ic_video_free);
                    break;
                case C.homepage.STATE_BUSY:// 忙线中
                    iBVideo.setImageResource(R.drawable.ic_video_busy);
                    break;
                case C.homepage.STATE_NO_DISTRUB:// 勿扰
                    iBVideo.setImageResource(R.drawable.ic_video_offline);
                    break;
            }
            iBVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, searchUser.getUesrId(), searchUser.getAccount()
                            , searchUser.getNickName(), searchUser.getIconUrlMininum()), mFragmentManager);
                }
            });
            // 聊天
            holder.setOnClickListener(R.id.item_homepage_chat, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(searchUser.getUesrId(),
                            searchUser.getAccount(), searchUser.getNickName(), searchUser.getIconUrlMininum()));
                }
            });
        }
    }

}
