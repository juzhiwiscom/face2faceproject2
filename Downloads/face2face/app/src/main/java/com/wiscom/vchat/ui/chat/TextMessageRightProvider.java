package com.wiscom.vchat.ui.chat;

import android.content.Context;
import android.widget.ImageView;

import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.preference.UserPreference;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.adapter.provider.ItemViewProvider;
import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.util.DateTimeUtil;

/**
 * 聊天文字消息（发送方）
 * Created by zhangdroid on 2017/6/29.
 */
public class TextMessageRightProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;

    public TextMessageRightProvider(Context context) {
        this.mContext = context;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_text_right;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.TXT && item.direct() == EMMessage.Direct.SEND);
    }

    @Override
    public void convert(EMMessage emMessage, int position, RecyclerViewHolder holder) {
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                .error(Util.getDefaultImageCircle()).url(UserPreference.getSmallImage()).imageView((ImageView) holder.getView(R.id.item_chat_text_avatar_right)).build());
        if (null != emMessage) {
            holder.setText(R.id.item_chat_text_time_right, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
            EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
            if (null != emTextMessageBody) {
                holder.setText(R.id.item_chat_text_right, emTextMessageBody.getMessage());
            }
        }
    }

}
