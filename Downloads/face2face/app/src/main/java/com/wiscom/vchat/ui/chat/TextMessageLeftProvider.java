package com.wiscom.vchat.ui.chat;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.wiscom.vchat.R;
import com.wiscom.vchat.common.Util;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.adapter.provider.ItemViewProvider;
import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.util.DateTimeUtil;
import com.wiscom.library.util.SharedPreferenceUtil;

/**
 * 聊天文字消息（接收方）
 * Created by zhangdroid on 2017/6/29.
 */
public class TextMessageLeftProvider implements ItemViewProvider<EMMessage> {
    private Context mContext;
    private String mAvatarUrl;

    public TextMessageLeftProvider(Context context, String url) {
        this.mContext = context;
        this.mAvatarUrl = url;
    }

    @Override
    public int getItemViewLayoutResId() {
        return R.layout.item_chat_text_left;
    }

    @Override
    public boolean isViewType(EMMessage item, int position) {
        return (item.getType() == EMMessage.Type.TXT && item.direct() == EMMessage.Direct.RECEIVE);
    }

    @Override
    public void convert(EMMessage emMessage, int position, RecyclerViewHolder holder) {
        // 头像
        ImageView ivAvatar = (ImageView) holder.getView(R.id.item_chat_text_avatar_left);
        String msgId = emMessage.getMsgId();
        if(mAvatarUrl!=null){
            if(mAvatarUrl.equals("KeFuIconUrl")){
                ivAvatar.setImageResource(R.mipmap.friend_secretary);

            }else{
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                        .error(Util.getDefaultImageCircle()).url(mAvatarUrl).imageView(ivAvatar).build());
                ivAvatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 发送事件，查看用户详情
//                    EventBus.getDefault().post(new SeeDetailEvent());
                    }
                });
            }

        }else{
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().transform(new CropCircleTransformation(mContext)).placeHolder(Util.getDefaultImageCircle())
                    .error(Util.getDefaultImageCircle()).url(mAvatarUrl).imageView(ivAvatar).build());
            ivAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 发送事件，查看用户详情
//                    EventBus.getDefault().post(new SeeDetailEvent());
                }
            });
        }

        if (null != emMessage) {
            holder.setText(R.id.item_chat_text_time_left, DateTimeUtil.convertTimeMillis2String(emMessage.getMsgTime()));
            TextView tvTime = (TextView) holder.getView(R.id.item_chat_text_time_left);
            SharedPreferenceUtil.setStringValue(mContext,"MESSAGE_ID",emMessage.getMsgId(), emMessage.getMsgTime()+"");
            EMTextMessageBody emTextMessageBody = (EMTextMessageBody) emMessage.getBody();
            if (null != emTextMessageBody) {
                holder.setText(R.id.item_chat_text_left, emTextMessageBody.getMessage());
            }
        }
    }

}
