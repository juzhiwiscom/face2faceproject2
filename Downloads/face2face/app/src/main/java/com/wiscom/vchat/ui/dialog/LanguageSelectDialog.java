package com.wiscom.vchat.ui.dialog;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.wiscom.vchat.R;
import com.wiscom.vchat.data.constant.DataDictManager;
import com.wiscom.vchat.ui.dialog.adapter.LanguageAdapter;
import com.wiscom.library.dialog.BaseDialogFragment;
import com.wiscom.library.util.ToastUtil;
import com.wiscom.library.util.Utils;

import java.util.List;

/**
 * 语言选择对话框（多选）
 * Created by zhangdroid on 2017/6/9.
 */
public class LanguageSelectDialog extends BaseDialogFragment {
    private List<String> mSelectedLanguages;
    private OnLanguageSelectListener mOnLanguageSelectListener;

    public static LanguageSelectDialog newInstance(List<String> selectedLanguages, OnLanguageSelectListener listener) {
        LanguageSelectDialog languageSelectDialog = new LanguageSelectDialog();
        languageSelectDialog.mSelectedLanguages = selectedLanguages;
        languageSelectDialog.mOnLanguageSelectListener = listener;
        return languageSelectDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_select_language;
    }

    @Override
    protected void setDialogContentView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.dialog_language_list);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);
        final LanguageAdapter languageAdapter = new LanguageAdapter(getContext(), R.layout.item_dialog_select_language,
                DataDictManager.getLanguageList());
        languageAdapter.initSelectState(mSelectedLanguages);
        recyclerView.setAdapter(languageAdapter);
        Button btnCancel = (Button) view.findViewById(R.id.dialog_language_cancel);
        Button btnSave = (Button) view.findViewById(R.id.dialog_language_save);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> selectedItems = languageAdapter.getSelectedItems();
                if (Utils.isListEmpty(selectedItems)) {
                    ToastUtil.showShortToast(getContext(), getString(R.string.tip_cannot_empty));
                } else {
                    dismiss();
                    if (null != mOnLanguageSelectListener) {
                        mOnLanguageSelectListener.onSelected(languageAdapter.getSelectedItems());
                    }
                }
            }
        });
    }

    public static void show(FragmentManager fragmentManager, List<String> selectedLanguages, OnLanguageSelectListener onLanguageSelectListener) {
        newInstance(selectedLanguages, onLanguageSelectListener).show(fragmentManager, "language_select");
    }

    public interface OnLanguageSelectListener {
        void onSelected(List<String> languages);
    }

}
