package com.wiscom.vchat.data.preference;

import android.content.Context;

import com.wiscom.vchat.base.BaseApplication;
import com.wiscom.vchat.data.model.UserBean;
import com.wiscom.library.util.SharedPreferenceUtil;

/**
 * 保存普通用户账户信息
 * Created by zhangdroid on 2017/6/24.
 */
public class BeanPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private BeanPreference() {
    }

    public static final String NAME = "vchat_pre_bean";
    public static final String BEAN_COUNT = "vchat_bean_count";
    public static final String SCREEN_GENDER = "vchat_screen_gender";


    public static void saveUserBean(UserBean userBean) {
        if (null != userBean) {
            setBeanCount(userBean.getCounts());
        }
    }

    /**
     * 保存用户账户当前金币数
     *
     * @param count
     */
    public static void setBeanCount(int count) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, BEAN_COUNT, count);
    }

    /**
     * @return 用户账户当前金币数
     */
    public static int getBeanCount() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, BEAN_COUNT, 0);
    }
    /**
     * 随机视频时筛选性别的信息
     *
     * @param gender 0是男性，1是女性，-1是没有
     */
    public static void setScreenGender(int gender) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, SCREEN_GENDER, gender);
    }

    /**
     * @return 随机视频时筛选性别的信息
     */
    public static int getScreenGender() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, SCREEN_GENDER, -1);
    }

}
