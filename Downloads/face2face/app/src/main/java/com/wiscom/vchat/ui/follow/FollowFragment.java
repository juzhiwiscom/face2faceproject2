package com.wiscom.vchat.ui.follow;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragment;
import com.wiscom.vchat.ui.follow.contract.FollowContract;
import com.wiscom.vchat.ui.follow.presenter.FollowPresenter;
import com.wiscom.library.adapter.wrapper.OnLoadMoreListener;
import com.wiscom.library.widget.AutoSwipeRefreshLayout;
import com.wiscom.library.widget.IndexView;
import com.wiscom.library.widget.XRecyclerView;

import butterknife.BindView;

/**
 * 关注
 * Created by zhangdroid on 2017/5/23.
 */
public class FollowFragment extends BaseFragment implements FollowContract.IView, IndexView.OnIndexChangeListener {
    @BindView(R.id.fragment_follow_xrecyerview)
    XRecyclerView mRecyclerView;
    @BindView(R.id.fragment_follow_swiperefresh)
    AutoSwipeRefreshLayout mSwipeRefresh;
    @BindView(R.id.fragment_follow_iv_word)
    IndexView iv_word;//右边的索引
    @BindView(R.id.fragment_follow_tv_word)
    TextView tv_word;//布局中间的字母
    private FollowPresenter mFollowPresenter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_follow;
    }

//    @Override
//    protected String getDefaultTitle() {
//        return getString(R.string.title_follow);
//    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
//        setRightIcon(R.drawable.icon_search, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mFollowPresenter.goToSearchPage();
//            }
//        });
        mSwipeRefresh.setColorSchemeResources(R.color.main_color);
        mSwipeRefresh.setProgressBackgroundColorSchemeColor(Color.WHITE);
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mFollowPresenter = new FollowPresenter(this);
    }

    @Override
    protected void setListeners() {
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mFollowPresenter.refresh();
            }
        });
        iv_word.setOnIndexChangeListener(this);//给右边的索引设置监听
    }

    @Override
    protected void loadData() {
        mFollowPresenter.loadFollowUserList();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }


    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefresh.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSwipeRefresh.isRefreshing()) {
                    mSwipeRefresh.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFollowPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFollowPresenter.refresh();
            }
        });
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getFragmentManager();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId) {
        mRecyclerView.setAdapter(adapter, loadMoreViewId);
    }

    @Override
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        mRecyclerView.setOnLoadingMoreListener(onLoadMoreListener);
    }

    @Override
    public void showLoadMore() {
        mRecyclerView.setVisibility(R.id.load_more_progress, true);
        mRecyclerView.setVisibility(R.id.load_more_msg, true);
        mRecyclerView.setVisibility(R.id.load_more_empty, false);
        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        mRecyclerView.setVisibility(R.id.load_more_empty, true);
        mRecyclerView.setVisibility(R.id.load_more_progress, false);
        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }

    @Override
    public void recyclerViewMoveTo(int i) {
        linearLayoutManager.scrollToPosition(i);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mFollowPresenter.finish();
    }

    @Override
    public void onIndexChange(String word) {
        //右边字母改变的监听
        mFollowPresenter.getSlidingWord(word);
        //显示
        tv_word.setVisibility(View.VISIBLE);
        tv_word.setText(word);
    }

    @Override
    public void tvGone() {
        //布局中间字母是否隐藏的监听  手抬起时隐藏
        tv_word.setVisibility(View.GONE);
    }


}
