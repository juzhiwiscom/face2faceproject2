package com.wiscom.vchat.event;

import com.wiscom.vchat.parcelable.VideoParcelable;

/**
 * Created by tianzhentao on 2017/12/27.
 */

public class InviteSuccessEvent {
    private VideoParcelable videoParcelable;
    public InviteSuccessEvent(VideoParcelable videoParcelable) {
        this.videoParcelable=videoParcelable;
    }

    public VideoParcelable getVideoParcelable() {
        return videoParcelable;
    }

    public void setVideoParcelable(VideoParcelable videoParcelable) {
        this.videoParcelable = videoParcelable;
    }
}
