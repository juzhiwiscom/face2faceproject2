package com.wiscom.vchat.data.model;

/**
 * Created by Administrator on 2017/7/14.
 * 提现记录
 */

public class WithdrawRecord {


    private long guid;// 自增ID
    private long addTime;// 记录时间
    private String withdrawType; //提现方式
    private String withdrawAmount;  //提现金额


    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public long getRecordTime() {
        return addTime;
    }

    public void setRecordTime(long addTime) {
        this.addTime = addTime;
    }

    public String getWithdrawWay(){
        return withdrawType;
    }
    public void setWithdrawWay(String withdrawType){
        this.withdrawType=withdrawType;
    }
    public String getWithdrawMoney(){
        return withdrawAmount;
    }
    public void setWithdrawMoney(String withdrawAmount){
        this.withdrawAmount=withdrawAmount;
    }

}
