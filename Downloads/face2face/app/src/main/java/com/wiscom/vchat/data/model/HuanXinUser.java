package com.wiscom.vchat.data.model;

/**
 * Created by Administrator on 2017/7/14.
 */

public class HuanXinUser {
    private String hxId;
    private String hxName;
    private String hxIcon;
    private String account;
    private String isRead;
    private String lastMsg;
    private String lastMsgTime;

    public HuanXinUser(String hxId, String hxName, String hxIcon, String account,String isRead,String lastMsg,String lastMsgTime) {
        this.hxId = hxId;
        this.hxName = hxName;
        this.hxIcon = hxIcon;
        this.account=account;
        this.isRead = isRead;
        this.lastMsg=lastMsg;
        this.lastMsgTime=lastMsgTime;

    }
    public HuanXinUser(String hxId, String hxName, String hxIcon, String account,String isRead) {
        this.hxId = hxId;
        this.hxName = hxName;
        this.hxIcon = hxIcon;
        this.account=account;
        this.isRead = isRead;


    }


    public HuanXinUser() {
    }

    public String getLastMsgTime() {
        return lastMsgTime;
    }

    public void setLastMsgTime(String lastMsgTime) {
        this.lastMsgTime = lastMsgTime;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getHxId() {
        return hxId;
    }

    public void setHxId(String hxId) {
        this.hxId = hxId;
    }

    public String getHxName() {
        return hxName;
    }

    public void setHxName(String hxName) {
        this.hxName = hxName;
    }

    public String getHxIcon() {
        return hxIcon;
    }

    public void setHxIcon(String hxIcon) {
        this.hxIcon = hxIcon;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    @Override
    public String toString() {
        return "HuanXinUser{" +
                "hxId='" + hxId + '\'' +
                ", hxName='" + hxName + '\'' +
                ", hxIcon='" + hxIcon + '\'' +
                ", account='" + account + '\'' +
                ", isRead='" + isRead + '\'' +
                '}';
    }
}
