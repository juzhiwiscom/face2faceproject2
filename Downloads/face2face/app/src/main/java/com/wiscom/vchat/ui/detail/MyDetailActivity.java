package com.wiscom.vchat.ui.detail;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseAppCompatActivity;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.model.UserPhoto;
import com.wiscom.vchat.event.UserInfoChangedEvent;
import com.wiscom.vchat.parcelable.BigPhotoParcelable;
import com.wiscom.vchat.ui.detail.adapter.CommonPhotoAdapter;
import com.wiscom.vchat.ui.detail.contract.MyDetailContract;
import com.wiscom.vchat.ui.detail.presenter.MyDetailPresenter;
import com.wiscom.vchat.ui.photo.BigPhotoActivity;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;
import com.wiscom.library.net.NetUtil;
import com.wiscom.library.util.LaunchHelper;
import com.wiscom.library.util.SnackBarUtil;
import com.wiscom.library.util.Utils;
import com.wiscom.library.widget.CollapsingTextView;
import com.tmall.ultraviewpager.UltraViewPager;
import com.tmall.ultraviewpager.transformer.UltraDepthScaleTransformer;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 个人信息页面
 * Created by zhangdroid on 2017/5/27.
 */
public class MyDetailActivity extends BaseAppCompatActivity implements View.OnClickListener, MyDetailContract.IView {
    @BindView(R.id.myInfo_coordinator)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.myDetail_toolbar)
    Toolbar mToolbar;
    // 照片墙占位图
    @BindView(R.id.myDetail_placeholder)
    ImageView mIvPlaceHolder;
    @BindView(R.id.myDetail_Photos)
    UltraViewPager mViewPager;
    // 用户资料项
    @BindView(R.id.myDetail_nickname)
    TextView mTvNickname;
    @BindView(R.id.myDetail_edit)
    Button mBtnEdit;
    @BindView(R.id.myDetail_gender)
    ImageView mIvGender;
    @BindView(R.id.myDetail_base_info)
    TextView mTvBaseInfo;
    @BindView(R.id.myDetail_id)
    TextView mTvId;
    @BindView(R.id.myDetail_introducation)
    CollapsingTextView mTvIntroducation;

    private MyDetailPresenter mMyDetailPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_my_detail;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
    }

    @Override
    protected View getNoticeView() {
        return mCoordinatorLayout;
    }

    @Override
    protected void initViews() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setTitle("");
        }
        mMyDetailPresenter = new MyDetailPresenter(this);
    }

    @Override
    protected void setListeners() {
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mBtnEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myDetail_edit:
                LaunchHelper.getInstance().launch(mContext, EditInfoActivity.class);
                break;
        }
    }

    @Override
    protected void loadData() {
        mMyDetailPresenter.getMyInfo();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMyDetailPresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mCoordinatorLayout, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                toggleShowLoading(false, null);
            }
        }, 1_000);
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void setUserPhotos(final List<UserPhoto> list) {
        if (!Utils.isListEmpty(list)) {
            // 添加图片ImageView
            List<ImageView> imageViews = new ArrayList<>();
            for (final UserPhoto item : list) {
                if (null != item) {
                    ImageView imageView = new ImageView(mContext);
                    imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(item.getFileUrlMiddle())
                            .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build());
                    // 点击图片查看大图
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LaunchHelper.getInstance().launch(mContext, BigPhotoActivity.class, new BigPhotoParcelable(list.indexOf(item),
                                    Util.convertPhotoUrl(list)));
                        }
                    });
                    imageViews.add(imageView);
                }
            }
            // 设置滑动方向
            mViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
            // 设置切换动画
            mViewPager.setPageTransformer(false, new UltraDepthScaleTransformer());
            mViewPager.setAdapter(new CommonPhotoAdapter(imageViews));
            if (list.size() > 1) {
                mViewPager.initIndicator();
                mViewPager.getIndicator()
                        .setOrientation(UltraViewPager.Orientation.HORIZONTAL)
                        .setGravity(Gravity.CENTER | Gravity.BOTTOM)
                        .setNormalColor(Color.GRAY)
                        .setFocusColor(getResources().getColor(R.color.main_color))
                        .build();
                // 设置自动循环
                mViewPager.setInfiniteLoop(true);
                // 设置自动循环时间间隔
                mViewPager.setAutoScroll(2_000);
            }
            mIvPlaceHolder.setVisibility(View.GONE);
            mViewPager.setVisibility(View.VISIBLE);
        } else {
            mIvPlaceHolder.setImageResource(Util.getDefaultImage());
            mIvPlaceHolder.setVisibility(View.VISIBLE);
            mViewPager.setVisibility(View.GONE);
        }
    }

    @Override
    public void setNickname(String nickname) {
        setTextIfNotNull(mTvNickname, nickname);
    }

    @Override
    public void setGender(boolean isMale) {
        mIvGender.setImageResource(isMale ? R.drawable.ic_male : R.drawable.ic_female);
    }

    @Override
    public void setUserInfo(String age, String country, String city) {
        setTextIfNotNull(mTvBaseInfo, TextUtils.concat(age, " ", country, " ", city).toString());
    }

    @Override
    public void setUserId(String id) {
        setTextIfNotNull(mTvId, getString(R.string.person_id, id));
    }

    @Override
    public void setIntroducation(String introducation) {
        if (!TextUtils.isEmpty(introducation)) {
            mTvIntroducation.setText(introducation);
        }
    }

    @Subscribe
    public void onEvent(UserInfoChangedEvent event) {
        mMyDetailPresenter.updateMyInfo();
    }

    private void setTextIfNotNull(TextView textView, String text) {
        if (!TextUtils.isEmpty(text)) {
            textView.setText(text);
        }
    }

}
