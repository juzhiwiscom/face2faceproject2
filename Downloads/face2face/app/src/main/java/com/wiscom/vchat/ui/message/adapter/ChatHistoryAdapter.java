package com.wiscom.vchat.ui.message.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.wiscom.vchat.R;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.model.HuanXinUser;
import com.wiscom.library.adapter.CommonRecyclerViewAdapter;
import com.wiscom.library.adapter.RecyclerViewHolder;
import com.wiscom.library.image.CropCircleTransformation;
import com.wiscom.library.image.ImageLoader;
import com.wiscom.library.image.ImageLoaderUtil;

/**
 * 聊天记录适配器
 * Created by zhangdroid on 2017/7/6.
 */
public class ChatHistoryAdapter extends CommonRecyclerViewAdapter<HuanXinUser>{

    public ChatHistoryAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }
    @Override
    public void convert(HuanXinUser user, int position, RecyclerViewHolder holder) {
        ImageView ivAvatar = (ImageView) holder.getView(R.id.item_chat_history_avatar);
        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon()).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(ivAvatar).build());
        holder.setText(R.id.item_chat_history_nickname,user.getHxName());
//         holder.getView(R.id.item_chat_history_message)
        String isRead = user.getIsRead();
        if(isRead.equals("1")){//尚未读取
            holder.setVisibility(R.id.item_chat_is_unread,true);
        }else{//已经读取
            holder.setVisibility(R.id.item_chat_is_unread,false);

        }
    }


}
