package com.wiscom.vchat.ui.videoshow;

import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.wiscom.library.util.Utils;
import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragment;
import com.wiscom.vchat.common.RefreshRecyclerView;
import com.wiscom.vchat.common.Util;
import com.wiscom.vchat.data.api.ApiManager;
import com.wiscom.vchat.data.api.IGetDataListener;
import com.wiscom.vchat.data.model.RecommendNewJoinList;
import com.wiscom.vchat.data.model.VideoSquare;

import java.util.List;

import butterknife.BindView;

/**
 * Created by tianzhentao on 2018/5/4.
 */

 public class NewestVideoFragemnt extends BaseFragment {
   @BindView(R.id.message_recyclerview)
   RefreshRecyclerView mRefreshRecyclerView;
   private VideoNewestAdapter mVideoHistoryAdapter;
    private int pageNum=1;
    private String pageSize="30";
    private List<VideoSquare> mNewVideoList;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_newest;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {
       // 设置下拉刷新样式
       mRefreshRecyclerView.setColorSchemeResources(R.color.main_color);
       mRefreshRecyclerView.setProgressBackgroundColorSchemeColor(Color.WHITE);
       mRefreshRecyclerView.setLayoutManager(new GridLayoutManager(mContext,2));
       // 设置加载动画
       mRefreshRecyclerView.setLoadingView(Util.getLoadingView(getActivity()));
       mVideoHistoryAdapter = new VideoNewestAdapter(mContext, R.layout.item_video_newest);
       mRefreshRecyclerView.setAdapter(mVideoHistoryAdapter);
    }

    @Override
    protected void setListeners() {
        // 下拉刷新
        mRefreshRecyclerView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        // 上拉加载更多
        mRefreshRecyclerView.setOnLoadingMoreListener(new RefreshRecyclerView.OnLoadingMoreListener() {
            @Override
            public void onLoadMore() {
                loadMore();
            }
        });

    }

    private void loadMore() {
        pageNum++;
        loadData();

    }

    private void refresh() {
        pageNum = 1;
        loadData();

    }

    @Override
    protected void loadData() {
        ApiManager.recommendNewJoin(pageNum, pageSize, new IGetDataListener<RecommendNewJoinList>() {
            @Override
            public void onResult(RecommendNewJoinList recommendNewJoinList, boolean isEmpty) {if (null != recommendNewJoinList) {
                mNewVideoList = recommendNewJoinList.getVideoSquareList();
                if (!Utils.isListEmpty(mNewVideoList)) {
                    if(pageNum==1){
                        mVideoHistoryAdapter.replaceAll(mNewVideoList);
                    }else if(pageNum>1){
                        mVideoHistoryAdapter.appendToList(mNewVideoList);
                    }
                }
            }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
}
