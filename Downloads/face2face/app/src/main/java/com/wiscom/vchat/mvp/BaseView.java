package com.wiscom.vchat.mvp;

import android.content.Context;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public interface BaseView {

    Context obtainContext();

    /**
     * Show tip message(Toast or SnackBar)
     *
     * @param msg
     * @see com.wiscom.library.util.ToastUtil
     * @see com.wiscom.library.util.SnackBarUtil
     */
    void showTip(String msg);

}
