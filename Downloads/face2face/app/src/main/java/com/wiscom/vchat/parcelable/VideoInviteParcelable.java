package com.wiscom.vchat.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 跳转视频邀请页面需要传递的参数
 * Created by zhangdroid on 2017/6/7.
 */
public class VideoInviteParcelable implements Parcelable {
    /**
     * 是否被邀请，false表示主动发起视频邀请
     */
    public boolean isInvited;
    /**
     * 被邀请人/邀请人id
     */
    public long uId;
    /**
     * 被邀请人/邀请人account
     */
    public String account;
    /**
     * 被邀请人/邀请人昵称
     */
    public String nickname;
    /**
     * 被邀请人/邀请人头像
     */
    public String imgageUrl;
    /**
     * 被邀请人/邀请人国家
     */
    public String country;
    /**
     * 被邀请人/邀请人性别
     */
    public String genter;
    /**
     * 被邀请加入的频道id
     */
    public String channelId;
    /**
     * 播主价格
     */
    public String hostPrice;

    public VideoInviteParcelable(boolean isInvited, long uId, String account, String nickname, String imgageUrl) {
        this.isInvited = isInvited;
        this.uId = uId;
        this.account = account;
        this.nickname = nickname;
        this.imgageUrl = imgageUrl;
    }

    public VideoInviteParcelable(boolean isInvited, long uId, String account, String nickname, String imgageUrl,String country,String genter) {
        this.isInvited = isInvited;
        this.uId = uId;
        this.account = account;
        this.nickname = nickname;
        this.imgageUrl = imgageUrl;
        this.country=country;
        this.genter=genter;
    }
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public void setHostPrice(String hostPrice) {
        this.hostPrice = hostPrice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isInvited ? (byte) 1 : (byte) 0);
        dest.writeLong(this.uId);
        dest.writeString(this.account);
        dest.writeString(this.nickname);
        dest.writeString(this.imgageUrl);
        dest.writeString(this.channelId);
        dest.writeString(this.country);
        dest.writeString(this.genter);
    }

    protected VideoInviteParcelable(Parcel in) {
        this.isInvited = in.readByte() != 0;
        this.uId = in.readLong();
        this.account = in.readString();
        this.nickname = in.readString();
        this.imgageUrl = in.readString();
        this.channelId = in.readString();
        this.country = in.readString();
        this.genter = in.readString();
    }

    public static final Parcelable.Creator<VideoInviteParcelable> CREATOR = new Parcelable.Creator<VideoInviteParcelable>() {
        @Override
        public VideoInviteParcelable createFromParcel(Parcel source) {
            return new VideoInviteParcelable(source);
        }

        @Override
        public VideoInviteParcelable[] newArray(int size) {
            return new VideoInviteParcelable[size];
        }
    };
}
