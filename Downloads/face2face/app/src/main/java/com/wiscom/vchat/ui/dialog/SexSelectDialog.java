package com.wiscom.vchat.ui.dialog;

import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.wiscom.vchat.R;
import com.wiscom.library.dialog.BaseDialogFragment;

/**
 * 性别选择框
 * Created by zhangdroid on 2017/5/31.
 */
public class SexSelectDialog extends BaseDialogFragment {
    private boolean mIsMale;
    private OnSexSelectedListener mOnSexSelectedListener;

    private static SexSelectDialog newInstance(boolean isMale, OnSexSelectedListener listener) {
        SexSelectDialog sexSelectDialog = new SexSelectDialog();
        sexSelectDialog.mIsMale = isMale;
        sexSelectDialog.mOnSexSelectedListener = listener;
        return sexSelectDialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_select_sex;
    }

    @Override
    protected void setDialogContentView(View view) {
        final RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.dialog_sex_radio);
        radioGroup.check(mIsMale ? R.id.dialog_sex_male : R.id.dialog_sex_female);
        Button btnSure = (Button) view.findViewById(R.id.dialog_sex_sure);
        btnSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mOnSexSelectedListener) {
                    mOnSexSelectedListener.onSelected(radioGroup.getCheckedRadioButtonId() == R.id.dialog_sex_male);
                }
                dismiss();
            }
        });
    }

    public static void show(FragmentManager fragmentManager, boolean isMale, OnSexSelectedListener onSexSelectedListener) {
        newInstance(isMale, onSexSelectedListener).show(fragmentManager, "sex_select");
    }

    public interface OnSexSelectedListener {
        void onSelected(boolean isMale);
    }

}
