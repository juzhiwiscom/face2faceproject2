package com.wiscom.vchat.ui.homepage;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseFragment;
import com.wiscom.vchat.event.SearchEvent;
import com.wiscom.vchat.parcelable.ListParcelable;
import com.wiscom.vchat.ui.homepage.contract.ListContract;
import com.wiscom.vchat.ui.homepage.presenter.ListPresenter;
import com.wiscom.library.adapter.wrapper.OnLoadMoreListener;
import com.wiscom.library.util.ArgumentUtil;
import com.wiscom.library.widget.AutoSwipeRefreshLayout;
import com.wiscom.library.widget.XRecyclerView;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 首页列表
 * Created by zhangdroid on 2017/5/31.
 */
public class ListFragment3 extends BaseFragment implements ListContract.IView {
    @BindView(R.id.list_swiperefresh)
    AutoSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.list_recyclerview)
    XRecyclerView mRecyclerView;

    private ListPresenter mListPresenter;
    private ListParcelable mListParcelable;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;

    public static ListFragment3 newInstance(ListParcelable parcelable) {
        ListFragment3 listFragment = new ListFragment3();
        listFragment.setArguments(ArgumentUtil.setArgumentBundle(parcelable));
        return listFragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_list;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return mSwipeRefreshLayout;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {
        mListParcelable = (ListParcelable) parcelable;
    }

    @Override
    protected void initViews() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.main_color);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.WHITE);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mListPresenter = new ListPresenter(this);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mIsFirstLoad) {
                    mIsFirstLoad = false;
                } else {
                    mListPresenter.refresh();
                }
            }
        });
    }

    @Override
    protected void loadData() {
        mListPresenter.loadRecommendUserList(mListParcelable.type);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mListPresenter.finish();
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }

    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mListPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                mListPresenter.refresh();
            }
        });
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getFragmentManager();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId) {
        mRecyclerView.setAdapter(adapter, loadMoreViewId);
    }

    @Override
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        mRecyclerView.setOnLoadingMoreListener(onLoadMoreListener);
    }

    @Override
    public void showLoadMore() {
        mRecyclerView.setVisibility(R.id.load_more_progress, true);
        mRecyclerView.setVisibility(R.id.load_more_msg, true);
        mRecyclerView.setVisibility(R.id.load_more_empty, false);
        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        mRecyclerView.setVisibility(R.id.load_more_empty, true);
        mRecyclerView.setVisibility(R.id.load_more_progress, false);
        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }

    @Subscribe
    public void onEvent(SearchEvent searchEvent) {
        if (!mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(true);
        }
        mListPresenter.refresh();
    }

}
