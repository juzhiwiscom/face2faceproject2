package com.wiscom.vchat.ui.homepage.contract;

import android.support.v4.app.FragmentManager;

import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/6/7.
 */
public interface SearchContract {

    interface IView extends BaseView {

        /**
         * 设置国家
         *
         * @param country
         */
        void setCountry(String country);

        /**
         * 设置语言
         *
         * @param language
         */
        void setLanguage(String language);

        String getCountry();

        String getLanguage();

        FragmentManager obtainFragmentManager();
    }

    interface IPresenter extends BasePresenter {

        void selectCountry();

        void selectLanguage();

        void startSearch();
    }

}
