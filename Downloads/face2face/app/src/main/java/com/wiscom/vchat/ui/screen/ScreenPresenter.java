//package com.wiscom.vchat.ui.screen;
//
//import android.content.Context;
//
//import com.wiscom.library.util.Utils;
//import com.wiscom.vchat.data.api.ApiManager;
//import com.wiscom.vchat.data.api.IGetDataListener;
//import com.wiscom.vchat.data.model.RecommendNewJoinList;
//import com.wiscom.vchat.data.model.VideoSquare;
//
//import java.util.List;
//
///**
// * Created by Administrator on 2017/7/14.
// */
//
//public class ScreenPresenter implements ScreenContract.IPresenter{
//   private ScreenContract.IView mScreenView;
//    private Context mContext;
//    private int pageNum = 1;
//    private static final String pageSize = "20";
//    public ScreenPresenter(ScreenContract.IView view){
//        this.mScreenView=view;
//        this.mContext=view.obtainContext();
//    }
//
//    @Override
//    public void start() {
//
//    }
//
//    @Override
//    public void finish() {
//        mContext=null;
//        mScreenView=null;
//
//    }
//    @Override
//    public void loadHistoryList() {
//        load();
//    }
//
//    @Override
//    public void refresh() {
//        pageNum = 1;
//        load();
//    }
//
//    @Override
//    public void loadMore() {
////        mWithdrawRecordView.showLoadMore();
//        pageNum++;
//        load();
//    }
//
//    private void load() {
//        ApiManager.recommendNewJoin(pageNum, pageSize, new IGetDataListener<RecommendNewJoinList>() {
//
//            @Override
//            public void onResult(RecommendNewJoinList recommendNewJoinList, boolean isEmpty) {
//                if (isEmpty) {
//                    if (pageNum == 1) {
//                        mScreenView.toggleShowEmpty(true, null);
//                    } else if (pageNum > 1) {
//                        mScreenView.showNoMore();
//                    }
//                } else {
//                    if (null != recommendNewJoinList) {
//                        List<VideoSquare> list = recommendNewJoinList.getVideoSquareList();
//                        if (!Utils.isListEmpty(list)) {
//                            if (pageNum == 1) {
//                                mScreenView.getVideoHistoryAdapter().bind(list);
//                            } else if (pageNum > 1) {
//                                mScreenView.getVideoHistoryAdapter().appendToList(list);
//                            }
//                            mScreenView.hideLoadMore();
//                        }
//                    }
//                }
//                mScreenView.hideRefresh(1);
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//                mScreenView.hideRefresh(1);
//                if (!isNetworkError) {
//                    mScreenView.toggleShowError(true, msg);
//                }
//            }
//        });
//    }
//
//}
