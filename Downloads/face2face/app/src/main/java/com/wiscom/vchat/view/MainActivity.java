//package com.wiscom.vchat.view;
//
//import android.content.Context;
//import android.opengl.GLSurfaceView;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.SurfaceView;
//import android.view.WindowManager;
//import android.widget.FrameLayout;
//
//import com.wiscom.vchat.R;
//import com.wiscom.vchat.view.beauty.core.MediaType;
//import com.wiscom.vchat.view.beauty.filter.GPUImageFilter;
//import com.wiscom.vchat.view.beauty.filter.MagicCameraDisplay;
//import com.wiscom.vchat.view.beauty.utils.MagicFilterFactory;
//import com.wiscom.vchat.view.beauty.utils.MagicFilterType;
//import com.wiscom.vchat.view.camera.CameraInterface;
//import com.wiscom.vchat.view.utils.DisplayUtil;
//
//import io.agora.rtc.Constants;
//import io.agora.rtc.IRtcEngineEventHandler;
//import io.agora.rtc.RtcEngine;
//import io.agora.rtc.video.VideoCanvas;
//
//public class MainActivity extends AppCompatActivity {
//    private GLSurfaceView glSurfaceView;
//    private FrameLayout fl_remote;
//
//    private MagicCameraDisplay mMagicCameraDisplay;
//
//    private boolean mIsRecording = false;
//    private static RtcEngine rtcEngine;
//    public final static String RECORD_MOVIE_PATH = "movie_path";
//    private Context mContext;
//    private int romoteId = 1;
//    private int myUid = 2;
//
//    public static RtcEngine getRtcEngine() {
//        return rtcEngine;
//    }
//
//    private Handler mHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            //创建视频渲染视图, 设置远端视频视图
//            SurfaceView surfaceView = RtcEngine.CreateRendererView(mContext);
//            fl_remote.addView(surfaceView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
//                    FrameLayout.LayoutParams.MATCH_PARENT));
//            rtcEngine.setupRemoteVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_ADAPTIVE, romoteId));
//            surfaceView.setTag(romoteId);
//        }
//    };
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
//        initView();
//        initSurface();
//    }
//
//
//    private void initView() {
//        mContext = MainActivity.this;
//        fl_remote = (FrameLayout) findViewById(R.id.fl_remote);
//
//        //创建 RtcEngine 对象
//        try {
//            rtcEngine = RtcEngine.create(mContext, "34ad81873fc141208f4581aa271510fa", myRtcEventHandler);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        //打开视频模式
////        rtcEngine.enableVideo();
//
//        //设置视频分辨率
//        rtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_720P, false);
//
//        //加入频道
//        rtcEngine.joinChannel(null, "hehe", null, myUid);
//    }
//
//    /**
//     * 初始化相机预览
//     */
//    private void initSurface() {
//        glSurfaceView = (GLSurfaceView) findViewById(R.id.glsurfaceview);
//        mMagicCameraDisplay = new MagicCameraDisplay(this, glSurfaceView, MediaType.MEDIA_PUSH);
//
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                openBeauty();
//            }
//        }, 1000);
//
//        float previewRate = DisplayUtil.getScreenRate(this); // 默认全屏的比例预览
//        CameraInterface.getInstance().setPreviewRate(previewRate);
//        mMagicCameraDisplay.startRecording();
//    }
//
//    protected void openBeauty() {
//        GPUImageFilter mFilter = MagicFilterFactory.getFilters(
//                MagicFilterType.BEAUTY, this);
//        mMagicCameraDisplay.setFilter(mFilter);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (mIsRecording) {
//            mMagicCameraDisplay.startRecording();
//        }
//        if (mMagicCameraDisplay != null) {
//            mMagicCameraDisplay.onResume();
//        }
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        CameraInterface.getInstance().onPause();
//        if (mIsRecording) {
//            mMagicCameraDisplay.stopRecording();
//        }
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        CameraInterface.getInstance().onPause();
//        if (mIsRecording) {
//            mMagicCameraDisplay.stopRecording();
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        onCloseCamera();
//    }
//
//    /**
//     * 关闭摄像头，同时释放美颜
//     */
//    private void onCloseCamera() {
//        CameraInterface.getInstance().onDestroy();
//        if (mMagicCameraDisplay != null)
//            mMagicCameraDisplay.onDestroy();
//    }
//
//    IRtcEngineEventHandler myRtcEventHandler = new IRtcEngineEventHandler() {
//        @Override
//        public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
//            Log.e("joinchannel", "Success");
//            super.onJoinChannelSuccess(channel, uid, elapsed);
//        }
//
//        @Override
//        public void onError(int err) {
//            Log.e("joinchannel", "error:" + err);
//            super.onError(err);
//        }
//
//        @Override
//        public void onFirstRemoteVideoDecoded(int uid, int width, int height, int elapsed) {
//            mHandler.sendEmptyMessageDelayed(0, 100);
//        }
//    };
//
//}
