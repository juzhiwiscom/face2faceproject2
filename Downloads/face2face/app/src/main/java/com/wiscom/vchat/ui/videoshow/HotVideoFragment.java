package com.wiscom.vchat.ui.videoshow;

import android.os.Parcelable;
import android.view.View;

import com.wiscom.vchat.base.BaseFragment;

/**
 * Created by tianzhentao on 2018/5/4.
 */

public class HotVideoFragment extends BaseFragment {
    @Override
    protected int getLayoutResId() {
        return 0;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }
}
