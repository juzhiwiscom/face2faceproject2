package com.wiscom.vchat.ui.pay;

import android.content.Intent;

import com.wiscom.vchat.mvp.BasePresenter;
import com.wiscom.vchat.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public interface PayContract {

    interface IView extends BaseView {

        /**
         * 显示加载
         */
        void showLoading();

        /**
         * 隐藏加载
         */
        void dismissLoading();

        void showNetworkError();

        /**
         * 设置商品信息适配器
         */
        void setAdapter(PayAdapter payAdapter);

    }

    interface IPresenter extends BasePresenter {

        /**
         * @return 从后台获取支付渠道信息
         */
        void getPayWay(String fromTag);

        /**
         * 根据SKU调用支付
         *
         * @param sku
         */
        void purchase(String sku);

        /**
         * 处理支付结果回调
         *
         * @param requestCode
         * @param resultCode
         * @param data
         * @return
         */
        boolean handleActivityResult(int requestCode, int resultCode, Intent data);

    }

}
