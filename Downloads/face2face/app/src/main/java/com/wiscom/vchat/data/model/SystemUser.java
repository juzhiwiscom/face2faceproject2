package com.wiscom.vchat.data.model;

/**
 * Created by tianzhentao on 2018/3/15.
 */

public class SystemUser extends BaseModel{
    private UserBase remoteMatchUser;//匹配到的用户信息
    private String matchVideo;//视频地址

    public String getMatchVideo() {
        return matchVideo;
    }

    public void setMatchVideo(String matchVideo) {
        this.matchVideo = matchVideo;
    }

    public UserBase getRemoteMatchUser() {
        return remoteMatchUser;
    }

    public void setRemoteMatchUser(UserBase remoteMatchUser) {
        this.remoteMatchUser = remoteMatchUser;
    }
}
