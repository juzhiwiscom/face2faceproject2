package com.wiscom.vchat.ui.homepage;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.wiscom.vchat.R;
import com.wiscom.vchat.base.BaseTopBarFragment;
import com.wiscom.vchat.ui.homepage.contract.HomepageContract;
import com.wiscom.vchat.ui.homepage.presenter.HomepagePresenter;
import com.wiscom.library.util.LaunchHelper;

import butterknife.BindView;

/**
 * 首页
 * Created by zhangdroid on 2017/5/23.
 */
public class HomepageFragment extends BaseTopBarFragment implements HomepageContract.IView {
    @BindView(R.id.homepage_tab)
    TabLayout mTabLayout;
    @BindView(R.id.homepage_viewpager)
    ViewPager mViewPager;

    private HomepagePresenter mHomepagePresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_homepage;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.title_homepage);
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void initViews() {
        setLeftIcon(R.drawable.ic_search, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LaunchHelper.getInstance().launch(mContext, SearchActivity.class);
            }
        });
        mViewPager.setOffscreenPageLimit(2);
        mHomepagePresenter = new HomepagePresenter(this);
        mHomepagePresenter.addTabs();
    }

    @Override
    protected void setListeners() {
    }

    @Override
    protected void loadData() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHomepagePresenter.finish();
    }

    @Override
    public FragmentManager getManager() {
        return getChildFragmentManager();
    }

    @Override
    public void setAdapter(PagerAdapter pagerAdapter) {
        mViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void showTip(String msg) {
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }
}
