package com.wiscom.library.image;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.support.v4.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

/**
 * Image loader util, single instance pattern
 * Created by zhangdroid on 2016/5/31.
 */
public class ImageLoaderUtil {
    private static volatile ImageLoaderUtil sDefault;

    private ImageLoaderUtil() {
    }

    public static ImageLoaderUtil getInstance() {
        if (sDefault == null) {
            synchronized (ImageLoaderUtil.class) {
                if (sDefault == null) {
                    sDefault = new ImageLoaderUtil();
                }
            }
        }
        return sDefault;
    }

    /**
     * 加载图片(网络/本地)
     *
     * @param context
     * @param imageLoader
     */
    public void loadImage(Context context, ImageLoader imageLoader) {
        load(Glide.with(context), imageLoader);
    }

    public void loadImage(Activity activity, ImageLoader imageLoader) {
        load(Glide.with(activity), imageLoader);
    }

    public void loadImage(FragmentActivity fragmentActivity, ImageLoader imageLoader) {
        load(Glide.with(fragmentActivity), imageLoader);
    }

    public void loadImage(Fragment fragment, ImageLoader imageLoader) {
        load(Glide.with(fragment), imageLoader);
    }

    public void loadImage(android.support.v4.app.Fragment fragment, ImageLoader imageLoader) {
        load(Glide.with(fragment), imageLoader);
    }

    private void load(RequestManager requestManager, ImageLoader imageLoader) {
        requestManager.load(imageLoader.getUrl())
                .asBitmap()
                .crossFade()
                .placeholder(imageLoader.getPlaceHolder())
                .error(imageLoader.getError())
                .transform(imageLoader.getTransformation())
                .into(imageLoader.getImageView());
    }

    /**
     * 清除内存缓存（UI线程）
     */
    public void clearMemoryCache(Context context) {
        Glide.get(context).clearMemory();
    }

    /**
     * 清除SD卡缓存（非UI线程）
     */
    public void clearDiskCache(Context context) {
        Glide.get(context).clearDiskCache();
    }

    /**
     * 图片加载监听器
     */
    public interface OnImageLoadListener {
        /**
         * 加载完成
         */
        void onCompleted();

        /**
         * 加载失败
         */
        void onFailed();
    }
    /**
     * 带有回调的加载网络图片
     *
     * @param imageLoader
     * @param listener
     */
    public void loadImage(android.support.v4.app.Fragment fragment, ImageLoader imageLoader,final OnImageLoadListener listener) {
        load(Glide.with(fragment), imageLoader,listener);
    }
    private void load(RequestManager requestManager, ImageLoader imageLoader,final OnImageLoadListener listener) {
        requestManager.load(imageLoader.getUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        if (listener != null) {
                            listener.onCompleted();
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        if (listener != null) {
                            listener.onFailed();
                        }
                        return false;
                    }
                })
                .crossFade()
                .placeholder(imageLoader.getPlaceHolder())
                .error(imageLoader.getError())
                .transform(imageLoader.getTransformation())
                .into(imageLoader.getImageView());
    }
}
