package com.wiscom.library.dialog;

import android.support.v4.app.FragmentManager;

/**
 * Created by zhangdroid on 2016/6/24.
 */
public class DialogUtil {
    private static final String TAG_DOUBLE_BUTTON = "doubleBtnDialog";
    private static final String TAG_SINGLE_BUTTON = "singleBtnDialog";
    private static final String TAG_DATE_SELECT = "dateSelectDialog";
    private static final String TAG_AGE_SELECT = "ageSelectDialog";
    private static final String TAG_TWO_AGE_SELECT = "twoAgeSelectDialog";

    /**
     * Show a double button dialog
     *
     * @param fragmentManager FragmentManager to show the dialog
     * @param title           title of dialog , it may be null
     * @param message         message of dialog ,it may be null
     * @param positive        positive button text ,if it is null, use the default
     * @param negative        negative button text ,if it is null, use the default
     * @param isCancelable    the dialog is cancelable, default is true
     * @param listener        the {@link OnDoubleDialogClickListener} listener for the double button dialog
     */
    public static void showDoubleBtnDialog(FragmentManager fragmentManager, String title, String message, String positive, String negative,
                                           boolean isCancelable, OnDoubleDialogClickListener listener) {
        DoubleButtonDialog.newInstance(title, message, positive, negative, isCancelable, listener).show(fragmentManager, TAG_DOUBLE_BUTTON);
    }

//    /**
//     * Show a double button dialog
//     *
//     * @param fragmentManager FragmentManager to show the dialog
//     * @param title           title of dialog , it may be null
//     * @param message         message of dialog ,it may be null
//     * @param positive        positive button text ,if it is null, use the default
//     * @param negative        negative button text ,if it is null, use the default
//     * @param isCancelable    the dialog is cancelable, default is true
//     * @param listener        the {@link OnDoubleDialogClickListener} listener for the double button dialog
//     */
//    public static void showEditDoubleBtnDialog(FragmentManager fragmentManager, String title, String message, String positive, String negative,
//                                               boolean isCancelable, OnEditDoubleDialogClickListener listener) {
//        EditDoubleButtonDialog.newInstance(title, message, positive, negative, isCancelable, listener).show(fragmentManager, TAG_DOUBLE_BUTTON);
//    }
//
//    public static void showListDoubleBtnDialog(FragmentManager fragmentManager, String title, String positive, String negative,
//                                               boolean isCancelable, List<String> list, OnListDoubleDialogClickListener listener) {
//        ListDoubleButtonDialog.newInstance(title, positive, negative, isCancelable, list, listener).show(fragmentManager, TAG_DOUBLE_BUTTON);
//    }
//
//    public static void showCheckBoxDoubleBtnDialog(FragmentManager fragmentManager, String title, String positive, String negative,
//                                                   boolean isCancelable, List<String> list, OnCheckBoxDoubleDialogClickListener listener) {
//        CheckBoxDoubleButtonDialog.newInstance(title, positive, negative, isCancelable, list, listener).show(fragmentManager, TAG_DOUBLE_BUTTON);
//    }
//
//    /**
//     * Show a single button dialog
//     *
//     * @param fragmentManager FragmentManager to show the dialog
//     * @param title           title of dialog , it may be null
//     * @param message         message of dialog ,it may be null
//     * @param positive        positive button text ,if it is null, use the default
//     * @param isCancelable    the dialog is cancelable, default is true
//     * @param listener        the {@link OnSingleDialogClickListener} listener for the single button dialog
//     */
//    public static void showSingleBtnDialog(FragmentManager fragmentManager, String title, String message, String positive,
//                                           boolean isCancelable, OnSingleDialogClickListener listener) {
//        SingleButtonDialog.newInstance(title, message, positive, isCancelable, listener).show(fragmentManager, TAG_SINGLE_BUTTON);
//    }
//
//    public static void showXiYiSingleBtnDialog(FragmentManager fragmentManager, String title, String message, String positive,
//                                               boolean isCancelable, OnSingleDialogClickListener listener) {
//        XiYiSingleButtonDialog.newInstance(title, message, positive, isCancelable, listener).show(fragmentManager, TAG_SINGLE_BUTTON);
//    }
//
//    /**
//     * Show date select wheelview dialog
//     *
//     * @param fragmentManager FragmentManager to show the dialog
//     * @param title           title of dialog , it may be null
//     * @param positive        positive button text ,if it is null, use the default
//     * @param isCancelable    the dialog is cancelable, default is true
//     * @param listener        the {@link ThreeWheelDialog.OnThreeWheelDialogClickListener} listener for the wheelview dialog
//     */
//    public static void showDateSelectDialog(FragmentManager fragmentManager, String title, String positive, String negative,
//                                            boolean isCancelable, ThreeWheelDialog.OnThreeWheelDialogClickListener listener) {
//        DateSelectDialog.newInstance(title, positive, negative, isCancelable, listener).show(fragmentManager, TAG_DATE_SELECT);
//    }
//
//    /**
//     * Show age select wheelview dialog
//     *
//     * @param fragmentManager FragmentManager to show the dialog
//     * @param title           title of dialog , it may be null
//     * @param positive        positive button text ,if it is null, use the default
//     * @param isCancelable    the dialog is cancelable, default is true
//     * @param listener        the {@link OneWheelDialog.OnOneWheelDialogClickListener} listener for the wheelview dialog
//     */
//    public static void showAgeSelectDialog(FragmentManager fragmentManager, String title, String positive, String negative,
//                                           boolean isCancelable, OneWheelDialog.OnOneWheelDialogClickListener listener) {
//        AgeSelectDialog.newInstance(title, positive, negative, isCancelable, listener).show(fragmentManager, TAG_AGE_SELECT);
//    }
//
//    /**
//     * Show two columns age select wheelview dialog
//     *
//     * @param fragmentManager FragmentManager to show the dialog
//     * @param title           title of dialog , it may be null
//     * @param positive        positive button text ,if it is null, use the default
//     * @param isCancelable    the dialog is cancelable, default is true
//     * @param listener        the {@link TwoWheelDialog.OnTwoWheelDialogClickListener} listener for the wheelview dialog
//     */
//    public static void showTwoAgeSelectDialog(FragmentManager fragmentManager, String title, String positive, String negative,
//                                              boolean isCancelable, TwoWheelDialog.OnTwoWheelDialogClickListener listener) {
//        TwoAgeSelectDialog.newInstance(title, positive, negative, isCancelable, listener).show(fragmentManager, TAG_TWO_AGE_SELECT);
//    }
//    public static void showListDialog(FragmentManager fragmentManager, String title, String positive, String negative,
//                                      boolean isCancelable, List<String> list, OnListDoubleDialogClickListener listener) {
//        ListDialog.newInstance(title, positive, negative, isCancelable, list, listener).show(fragmentManager, TAG_DOUBLE_BUTTON);
//    }
//    /**
//     * 显示加载动画
//     *
//     * @param context
//     * @return
//     */
//    public static Dialog showLoadingDialog(Context context) {
//        return showLoadingDialog(context, false);
//    }
//
//    public static Dialog showLoadingDialog(Context context, boolean isCancelable) {
//        View view = LayoutInflater.from(context).inflate(R.layout.layout_loading, null);
//        ImageView ivLoading = (ImageView) view.findViewById(R.id.loading_imageview);
//        // 设置旋转动画
//        RotateAnimation rotateAnimation = new RotateAnimation(0, 360, RotateAnimation.RELATIVE_TO_SELF,
//                0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
//        rotateAnimation.setDuration(1500);
//        rotateAnimation.setInterpolator(new DecelerateInterpolator());
//        rotateAnimation.setRepeatCount(Animation.INFINITE);
//        ivLoading.startAnimation(rotateAnimation);
//
//        Dialog dialog = new Dialog(context, android.R.style.Theme_Panel);
//        dialog.setContentView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//        dialog.setCancelable(isCancelable);
//        dialog.setCanceledOnTouchOutside(isCancelable);
//        dialog.show();
//        return dialog;
//    }
////    public static void showSingleWheelDialog(FragmentManager fragmentManager, List<String> stringList, String title, String positive, String negative,
//                                             boolean isCancelable, OneWheelDialog.OnOneWheelDialogClickListener listener) {
//        SingleWheelDialog.newInstance(stringList,title, positive, negative, isCancelable, listener).show(fragmentManager, TAG_AGE_SELECT);
//    }
}
