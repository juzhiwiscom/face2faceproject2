package com.wiscom.okhttp.builder;


import com.wiscom.okhttp.OkHttpHelper;
import com.wiscom.okhttp.request.CommonRequest;
import com.wiscom.okhttp.request.OtherRequest;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public class HeadBuilder extends GetBuilder {

    @Override
    public CommonRequest build() {
        return new OtherRequest(null, null, OkHttpHelper.METHOD.HEAD, url, tag, params, headers, id).build();
    }

}
