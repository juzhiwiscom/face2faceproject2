package com.wiscom.okhttp.builder;

import com.wiscom.okhttp.request.PostStringRequest;
import com.wiscom.okhttp.request.CommonRequest;

import okhttp3.MediaType;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public class PostStringBuilder extends OkHttpRequestBuilder<PostStringBuilder> {
    private String content;
    private MediaType mediaType;

    public PostStringBuilder content(String content) {
        this.content = content;
        return this;
    }

    public PostStringBuilder mediaType(MediaType mediaType) {
        this.mediaType = mediaType;
        return this;
    }

    @Override
    public CommonRequest build() {
        return new PostStringRequest(url, tag, params, headers, content, mediaType, id).build();
    }

}
