package com.wiscomwis.jimo.ui.video.presenter;

import android.content.Context;
import android.os.CountDownTimer;

import com.wiscomwis.jimo.data.preference.AnchorPreference;
import com.wiscomwis.jimo.data.preference.DataPreference;
import com.wiscomwis.jimo.ui.video.contract.MassVideoOrVoiceContract;

/**
 * Created by WangYong on 2017/11/28.
 */

public class MassVideoOrVoicePresenter implements MassVideoOrVoiceContract.IPresenter {
    private MassVideoOrVoiceContract.IView mMassVideoOrVoice;
    private Context context;
    private CountDownTimer countDownTimer;
    private CountDownTimer countDownTimer1;

    public MassVideoOrVoicePresenter(MassVideoOrVoiceContract.IView mMassVideoOrVoice) {
        this.mMassVideoOrVoice = mMassVideoOrVoice;
        this.context = mMassVideoOrVoice.obtainContext();
    }

    @Override
    public void start() {

    }

    @Override
    public void initData(final String type) {
        if (type.equals("1")) {
            mMassVideoOrVoice.getAnchorPrice(AnchorPreference.getPrice());
        } else {
            mMassVideoOrVoice.getAnchorPrice(String.valueOf(AnchorPreference.getAudioPrice()));
        }
    }

    @Override
    public void startCountTime() {
        long time = DataPreference.getTime();
        if (time == 0) {
            DataPreference.saveTime(System.currentTimeMillis());
            countDownTimer = new CountDownTimer(120000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mMassVideoOrVoice.getSendTime(millisUntilFinished);
                    if (millisUntilFinished <= 60000) {
                        mMassVideoOrVoice.clickStop();
                    }
                    if (DataPreference.getTime() == 0) {
                        countDownTimer.onFinish();
                        countDownTimer.cancel();
                    }
                }

                @Override
                public void onFinish() {
                    DataPreference.saveTime(0);
                    mMassVideoOrVoice.timeOnFinish();
                    mMassVideoOrVoice.finishActivity();
                }
            };
            countDownTimer.start();
        } else {
            long time1 = System.currentTimeMillis() - time;
            if (time1 > 1000 && time1 < 120000) {
                countDownTimer1 = new CountDownTimer(120000 - time1, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        mMassVideoOrVoice.getSendTime(millisUntilFinished);
                        if (millisUntilFinished <= 60000) {
                            mMassVideoOrVoice.clickStop();
                        }
                        if (DataPreference.getTime() == 0) {
                            countDownTimer1.onFinish();
                            countDownTimer1.cancel();
                        }
                    }

                    @Override
                    public void onFinish() {
                        DataPreference.saveTime(0);
                        mMassVideoOrVoice.timeOnFinish();
                        mMassVideoOrVoice.finishActivity();
                    }
                };
                countDownTimer1.start();
            }
        }
    }
}
