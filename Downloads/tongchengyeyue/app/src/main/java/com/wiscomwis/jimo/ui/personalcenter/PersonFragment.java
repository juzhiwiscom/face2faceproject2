package com.wiscomwis.jimo.ui.personalcenter;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.jimo.C;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseTopBarFragment;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.event.PaySuccessEvent;
import com.wiscomwis.jimo.event.RefreshEvent;
import com.wiscomwis.jimo.event.UserInfoChangedEvent;
import com.wiscomwis.jimo.parcelable.PayParcelable;
import com.wiscomwis.jimo.parcelable.TvPriceParcelable;
import com.wiscomwis.jimo.ui.auto.AutoReplyActivity;
import com.wiscomwis.jimo.ui.detail.EditInfoActivity;
import com.wiscomwis.jimo.ui.detail.adapter.AlbumPhotoAdapter;
import com.wiscomwis.jimo.ui.follow.FollowActivity;
import com.wiscomwis.jimo.ui.pay.activity.MyDiamondActivity;
import com.wiscomwis.jimo.ui.pay.activity.MyWalletActivity;
import com.wiscomwis.jimo.ui.pay.activity.RechargeActivity;
import com.wiscomwis.jimo.ui.personalcenter.contract.PersonContract;
import com.wiscomwis.jimo.ui.personalcenter.presenter.PersonPresenter;
import com.wiscomwis.jimo.ui.setting.SettingActivity;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.LaunchHelper;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 个人中心
 * Created by zhangdroid on 2017/5/23.
 */
public class PersonFragment extends BaseTopBarFragment implements View.OnClickListener, PersonContract.IView {
    // 用户信息
    @BindView(R.id.person_user_layout)
    RelativeLayout mRlUser;
    @BindView(R.id.person_user_avatar)
    ImageView mIvUserAvatar;
    @BindView(R.id.user_avater_status)
    ImageView user_avater_status;
    @BindView(R.id.person_user_name)
    TextView mTvUserName;
    @BindView(R.id.person_user_id)
    TextView mTvUserId;
    @BindView(R.id.person_user_percent)
    TextView mTvUserPercent;// 接听率/投诉率
    // 账号信息
    @BindView(R.id.person_account_balance)
    TextView mTvBalance;
    @BindView(R.id.person_recharge)
    TextView mTvRecharge;// 充值
    @BindView(R.id.person_withdraw)
    TextView mTvWithdraw;// 提现
    // 价格设置（主播）
    @BindView(R.id.person_price_layout)
    RelativeLayout mRlPrice;
    // 设置
    @BindView(R.id.person_setting_layout)
    RelativeLayout mRlSetting;
    //再此以上是以前所写，一下是现在所写
    @BindView(R.id.person_fragment_ll_person_user)
    LinearLayout ll_person_user;
    @BindView(R.id.person_fragment_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.person_fragment_rl_mywallet)
    RelativeLayout rl_mywallet;
    @BindView(R.id.person_fragment_rl_mytoken)
    RelativeLayout rl_mytoken;
    @BindView(R.id.person_fragment_rl_mydiamond)
    RelativeLayout rl_mydiamond;
    @BindView(R.id.person_fragment_rl_setting)
    RelativeLayout rl_setting;
    @BindView(R.id.person_fragment_center_rl_author)
    RelativeLayout rl_author;
    @BindView(R.id.person_fragment_view1)
    View view1;
    @BindView(R.id.person_fragment_view2)
    View view2;
    @BindView(R.id.person_fragment_view3)
    View view3;
    @BindView(R.id.person_fragment_iv_right2)
    ImageView iv_right2;
    @BindView(R.id.person_fragment_ll_authentication2)
    LinearLayout ll_authentication2;
    @BindView(R.id.person_fragment_tv_renzheng2)
    TextView tv_renzheng2;
    @BindView(R.id.person_fragment_center_rl_author1)
    RelativeLayout rl_author1;
    @BindView(R.id.person_fragment_rl_myreceive)
    RelativeLayout rl_autoreceive;
    @BindView(R.id.person_fragment_view4)
    View view4;
    @BindView(R.id.person_fragment_btn_renzheng)
    Button btn_renzhegn;
    @BindView(R.id.person_fragment_center_rl_renzheng_famale)
    RelativeLayout rl_renzheng_famale;
    @BindView(R.id.person_fragment_center_tv_renzheng_male)
    TextView tv_renzheng_male;
    @BindView(R.id.person_fragment_rl_follow)
    RelativeLayout rl_follow;
    private PersonPresenter mPersonPresenter;
    private String pirce;
    private int audioPrice;
    private boolean isCheck = false;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_person;
    }

    @Override
    protected String getDefaultTitle() {
        return getString(R.string.title_person);
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void initViews() {
        mPersonPresenter = new PersonPresenter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        mPersonPresenter.start();
        mPersonPresenter.loadPersonInfor();
        if (!UserPreference.isMale()) {
            rl_renzheng_famale.setVisibility(View.VISIBLE);
            tv_renzheng_male.setVisibility(View.GONE);
        }else{
            tv_renzheng_male.setVisibility(View.VISIBLE);
            rl_renzheng_famale.setVisibility(View.GONE);
        }
    }

    @Override
    protected void setListeners() {
        mRlUser.setOnClickListener(this);
        mTvRecharge.setOnClickListener(this);
        mTvWithdraw.setOnClickListener(this);
        mRlPrice.setOnClickListener(this);
        mRlSetting.setOnClickListener(this);
        //再此以上是以前所写，一下是现在所写
        rl_setting.setOnClickListener(this);
        ll_person_user.setOnClickListener(this);
        rl_mydiamond.setOnClickListener(this);
        rl_mytoken.setOnClickListener(this);
        rl_mywallet.setOnClickListener(this);
        rl_author.setOnClickListener(this);
        ll_authentication2.setOnClickListener(this);
        rl_autoreceive.setOnClickListener(this);
        btn_renzhegn.setOnClickListener(this);
        rl_follow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.person_fragment_ll_person_user:// 查看个人页
                LaunchHelper.getInstance().launch(mContext, EditInfoActivity.class);
                break;
            case R.id.person_withdraw:// 提现
//                LaunchHelper.getInstance().launch(mContext, WithdrawActivity.class);
//                LaunchHelper.getInstance().launch(mContext, WithdrawActivity.class);
                break;

            case R.id.person_fragment_btn_renzheng:// 主播认证
                if (isCheck) {
                    Toast.makeText(mContext, "审核中......", Toast.LENGTH_SHORT).show();
                } else {
                    LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                }
                break;
            case R.id.person_fragment_ll_authentication2:// 主播认证
                if (isCheck) {
                    Toast.makeText(mContext, "审核中......", Toast.LENGTH_SHORT).show();
                } else {
                    LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                }
                break;

            case R.id.person_price_layout:// 主播价格设置
                LaunchHelper.getInstance().launch(mContext, SetPriceActivity.class,
                        new TvPriceParcelable(pirce, audioPrice));
                break;
            case R.id.person_fragment_rl_setting:// 设置
                LaunchHelper.getInstance().launch(mContext, SettingActivity.class);
                break;
            case R.id.person_fragment_rl_mydiamond:
                LaunchHelper.getInstance().launch(mContext, MyDiamondActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 0));
                break;
            case R.id.person_fragment_rl_mytoken:
                LaunchHelper.getInstance().launch(mContext, RechargeActivity.class, new PayParcelable(C.pay.FROM_TAG_PERSON, 1));
                break;
            case R.id.person_fragment_rl_mywallet:
                LaunchHelper.getInstance().launch(mContext, MyWalletActivity.class);
                break;
            case R.id.person_fragment_center_rl_author:
                break;
            case R.id.person_fragment_rl_myreceive:
                LaunchHelper.getInstance().launch(mContext, AutoReplyActivity.class);
                break;
            case R.id.person_user_layout:
                LaunchHelper.getInstance().launch(mContext, EditInfoActivity.class);
                break;
            case R.id.person_fragment_rl_follow:
                LaunchHelper.getInstance().launch(mContext, FollowActivity.class);
                break;
        }
    }

    @Override
    protected void loadData() {
//        mPersonPresenter.getUserInfo();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setUserAvatar(String url, String status) {
        if (!TextUtils.isEmpty(status)) {
            if (status.equals("1")) {
                user_avater_status.setVisibility(View.GONE);
            } else {
                user_avater_status.setVisibility(View.VISIBLE);
            }
        }
        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(url).transform(new CropCircleTransformation(mContext))
                .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(mIvUserAvatar).build());
    }

    @Override
    public void setUserName(String name) {
        if (!TextUtils.isEmpty(name)) {
            mTvUserName.setText(name);
        }
    }

    @Override
    public void setUserId(String id) {
        if (!TextUtils.isEmpty(id)) {
            mTvUserId.setText(getString(R.string.person_id, id));
        }
    }

    @Override
    public void setUserPrecent(String receivePrecent, String complainPrecent) {
        if (!TextUtils.isEmpty(receivePrecent) && !TextUtils.isEmpty(complainPrecent)) {
            mTvUserPercent.setText(TextUtils.concat(getString(R.string.person_receive_precent, receivePrecent), "% \n",
                    getString(R.string.person_complain_precent, complainPrecent), "%"));
        } else if (!TextUtils.isEmpty(complainPrecent)) {
            mTvUserPercent.setText(getString(R.string.person_complain_precent, complainPrecent));
        }
    }

    @Override
    public void setBalance(float balance) {
        mTvBalance.setText("" + balance);
    }

    @Override
    public void setIsAnchor(boolean isAnchor) {
        mTvWithdraw.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        mTvRecharge.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
        mRlPrice.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        rl_author.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
//        rl_mytoken.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
        rl_mydiamond.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
//        rl_author1.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
        view4.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        rl_autoreceive.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        rl_mywallet.setVisibility(isAnchor ? View.VISIBLE : View.GONE);
        if (isAnchor) {
            ll_authentication2.setVisibility(View.GONE);
        }
//        view1.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
//        view2.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
//        view3.setVisibility(isAnchor ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setAccumulatedIncome(String income) {

    }

    @Override
    public void setAnchorCurrentIncome(String currentIncome) {
    }

    @Override
    public void setPrice(String price, int audioPrice1) {
        if (!TextUtils.isEmpty(price)) {
            pirce = price;
            audioPrice = audioPrice1;
        }
    }

    @Override
    public void vipDays(int days) {
    }

    @Override
    public void setAdapter(AlbumPhotoAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void picGone() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void switchAllPay() {
        rl_author.setVisibility(View.GONE);
        mRlPrice.setVisibility(View.GONE);
        rl_mywallet.setVisibility(View.GONE);
        rl_mytoken.setVisibility(View.GONE);
        rl_mydiamond.setVisibility(View.GONE);
        iv_right2.setVisibility(View.VISIBLE);
        if (UserPreference.isAnchor()) {
            ll_authentication2.setVisibility(View.GONE);
            iv_right2.setVisibility(View.GONE);
            rl_author1.setVisibility(View.GONE);
        } else {
            ll_authentication2.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void switchWallet() {
        rl_mywallet.setVisibility(View.GONE);
    }

    @Override
    public void switchDionmads() {
        rl_mydiamond.setVisibility(View.GONE);
    }

    @Override
    public void switchKey() {
        rl_mytoken.setVisibility(View.GONE);
    }

    @Override
    public void switchVip() {

    }

    @Override
    public void getCheckStatus(int pos) {
        if (pos == 0) {//审核中
            tv_renzheng2.setText(mContext.getString(R.string.under_review));
            isCheck = true;
        }

    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
    }

    @Subscribe
    public void onEvent(UserInfoChangedEvent event) {
        mPersonPresenter.getUserInfo();
    }

    @Subscribe
    public void onEvent(PaySuccessEvent event) {

    }

    @Override
    public void onResume() {
        super.onResume();
        mPersonPresenter.getUserInfo();
        mPersonPresenter.getUploadInfo();
        if (!UserPreference.isAnchor()) {
            mPersonPresenter.getCheckStatus();
        }
    }

    @Subscribe
    public void onEvent(RefreshEvent event) {
        mPersonPresenter.getUserInfo();
        mPersonPresenter.getUploadInfo();
    }
}
