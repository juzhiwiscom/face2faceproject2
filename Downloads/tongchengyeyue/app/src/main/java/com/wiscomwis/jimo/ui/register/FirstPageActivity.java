package com.wiscomwis.jimo.ui.register;

import android.os.Parcelable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseFragmentActivity;
import com.wiscomwis.jimo.base.CommonWebActivity;
import com.wiscomwis.jimo.data.api.ApiConstant;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.NickName;
import com.wiscomwis.jimo.data.model.Register;
import com.wiscomwis.jimo.data.model.UserBase;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.db.DbModle;
import com.wiscomwis.jimo.event.FinishEvent;
import com.wiscomwis.jimo.event.RegisterAndLoginFinish;
import com.wiscomwis.jimo.parcelable.WebParcelable;
import com.wiscomwis.jimo.ui.login.LoginActivity;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Administrator on 2017/8/26.
 */

public class FirstPageActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.firstpage_activity_iv_bg)
    ImageView iv_bg;
    @BindView(R.id.register_login)
    TextView tv_login;
    @BindView(R.id.register)
    Button btn_register;
    @BindView(R.id.iv_male)
    ImageView iv_male;
    @BindView(R.id.rl_male)
    RelativeLayout rl_male;
    @BindView(R.id.iv_female)
    ImageView iv_female;
    @BindView(R.id.rl_female)
    RelativeLayout rl_female;
    @BindView(R.id.register_xieyi)
    TextView tv_xieyi;
    private boolean isMale = true;
    private String mNickName;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_firstpage;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        setAnimation();
        String text = tv_xieyi.getText().toString();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                LaunchHelper.getInstance().launch(FirstPageActivity.this, CommonWebActivity.class,
                        new WebParcelable(getString(R.string.user_agreement), ApiConstant.URL_AGREEMENT_USER, false));
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.main_color));
            }

        }, text.indexOf("《"), text.indexOf("》") + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                LaunchHelper.getInstance().launch(FirstPageActivity.this, CommonWebActivity.class,
                        new WebParcelable(getString(R.string.privacy_agreement), ApiConstant.URL_AGREEMENT_PRIVACY, false));
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.main_color));
            }
        }, text.lastIndexOf("《"), text.lastIndexOf("》") + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        tv_xieyi.setMovementMethod(new LinkMovementMethod());
        tv_xieyi.setText(spannableStringBuilder);
    }

    @Override
    protected void setListeners() {
        btn_register.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        rl_male.setOnClickListener(this);
        rl_female.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        btn_register.getBackground().setAlpha(200);
        ApiManager.nickName(new IGetDataListener<NickName>() {

            @Override
            public void onResult(NickName nickName, boolean isEmpty) {
                if (nickName != null) {
                    mNickName = nickName.getNickName();
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_male://男
                if (!isMale) {
                    rl_male.setBackgroundResource(R.drawable.checked_first_page);
                    rl_female.setBackgroundResource(R.drawable.uncheck_first_page);
                    isMale = true;
                }
                break;
            case R.id.rl_female://女
                if (isMale) {
                    rl_female.setBackgroundResource(R.drawable.checked_first_page);
                    rl_male.setBackgroundResource(R.drawable.uncheck_first_page);
                    isMale = false;
                }
                break;
            case R.id.register://开启泡泡之旅

                LoadingDialog.show(getSupportFragmentManager());
                if (!TextUtils.isEmpty(mNickName)){
                    register();
                }else{
                    ApiManager.nickName(new IGetDataListener<NickName>() {

                        @Override
                        public void onResult(NickName nickName, boolean isEmpty) {
                            if (nickName != null) {
                                mNickName = nickName.getNickName();
                                register();
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                            LoadingDialog.hide();
                            ToastUtil.showShortToast(mContext,TextUtils.isEmpty(msg)?getString(R.string.tip_error):msg);
                        }
                    });
                }

                break;
            case R.id.register_login://已有账号
                LaunchHelper.getInstance().launch(mContext, LoginActivity.class);
                break;
        }
    }

    private void register() {
        ApiManager.register(mNickName, isMale, "0",
                new IGetDataListener<Register>() {
                    @Override
                    public void onResult(Register register, boolean isEmpty) {
                        LoadingDialog.hide();
                        // 保存用户信息到本地
                        UserBase userBase = register.getUserBase();
                        if (null != userBase) {
                            UserPreference.saveUserInfo(userBase);
                        }
                        DbModle.getInstance().getUserAccountDao().deleteAllData();//删除数据库中的所有数据
                        // 关闭之前打开的页面
                        EventBus.getDefault().post(new FinishEvent());
                        // 跳转主页面
                        LaunchHelper.getInstance().launchFinish(mContext, CompleteInfoActivity.class);
                        UserPreference.registered();//注册成功的标记
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        LoadingDialog.hide();
                        if (isNetworkError) {
                            ToastUtil.showShortToast(FirstPageActivity.this, getString(R.string.tip_network_error));
                        } else {
                            ToastUtil.showShortToast(FirstPageActivity.this, TextUtils.isEmpty(msg) ? getString(R.string.tip_empty) : msg);
                        }
                    }
                });
    }

    @Subscribe
    public void onEvent(RegisterAndLoginFinish event) {
        finish();
    }

    private void setAnimation() {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1, 1.2f, 1, 1.2f,
                Animation.RELATIVE_TO_SELF, 0.1f, Animation.RELATIVE_TO_SELF, 0.1f);
//        //3秒完成动画
        scaleAnimation.setDuration(7000);
//        //将AlphaAnimation这个已经设置好的动画添加到 AnimationSet中
        scaleAnimation.setRepeatMode(ScaleAnimation.REVERSE);
        //设置动画播放次数
        scaleAnimation.setRepeatCount(ScaleAnimation.INFINITE);
        animationSet.addAnimation(scaleAnimation);
        //从当前位置，向下和向右各平移300px
        TranslateAnimation animation = new TranslateAnimation(0.0f, -100f, 0.0f, 0.0f);
        animation.setDuration(7000);
        animation.setRepeatMode(TranslateAnimation.REVERSE);
        //设置动画播放次数
        animation.setRepeatCount(TranslateAnimation.INFINITE);
        animationSet.addAnimation(animation);
        //启动动画
        iv_bg.startAnimation(animationSet);
    }
}
