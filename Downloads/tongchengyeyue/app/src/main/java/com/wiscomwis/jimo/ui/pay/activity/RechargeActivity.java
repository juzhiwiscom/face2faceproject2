package com.wiscomwis.jimo.ui.pay.activity;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseAppCompatActivity;
import com.wiscomwis.jimo.common.TimeUtils;
import com.wiscomwis.jimo.data.model.NettyMessage;
import com.wiscomwis.jimo.event.SingleLoginFinishEvent;
import com.wiscomwis.jimo.parcelable.PayParcelable;
import com.wiscomwis.jimo.ui.pay.adapter.PayAdapter;
import com.wiscomwis.jimo.ui.pay.contract.RechargeContract;
import com.wiscomwis.jimo.ui.pay.presenter.RechargePresenter;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.SnackBarUtil;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * 支付页面
 * Created by zhangdroid on 2017/5/25.
 */
public class RechargeActivity extends BaseAppCompatActivity implements RechargeContract.IView, View.OnClickListener {
    @BindView(R.id.recharge_root)
    LinearLayout mLlRoot;
    @BindView(R.id.recharge_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.recharge_activity_viewpager)
    ViewPager mViewPager;
    private RechargePresenter mRechargePresenter;
    private PayParcelable mPayParcelable;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_recharge;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mPayParcelable = (PayParcelable) parcelable;

    }

    @Override
    protected View getNoticeView() {
        return mLlRoot;
    }

    @Override
    protected void initViews() {
        mViewPager.setOffscreenPageLimit(1);
        mRechargePresenter = new RechargePresenter(this);
        mRechargePresenter.addTabs();
        if(TimeUtils.timeIsPast()){//判断页脚是否过期
            msgHandler.sendEmptyMessage(1);
        }else if(!TimeUtils.timeIsPast()){
            msgHandler.sendEmptyMessage(2);
        }
    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void loadData() {
        mRechargePresenter.getPayWay(mPayParcelable.fromTag);
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
        mRechargePresenter.getPayWay(mPayParcelable.fromTag);
    }

    @Override
    protected void networkDisconnected() {
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mLlRoot, msg);
    }

    @Override
    public void showLoading() {
        toggleShowLoading(true, null);
    }

    @Override
    public void dismissLoading() {
        toggleShowLoading(false, null);
    }

    @Override
    public void showNetworkError() {
        super.showNetworkError();
    }

    @Override
    public void setAdapter(PagerAdapter pagerAdapter) {
        mViewPager.setAdapter(pagerAdapter);
        if (mPayParcelable.type == 1) {
            mViewPager.setCurrentItem(2);
        }
    }

    @Override
    public FragmentManager getManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void setAdapter(PayAdapter payAdapter) {

    }

    @Override
    public void setTextDetail(String detial) {

    }

    @Override
    public void getKeyNum(String num) {

    }

    @Override
    public void getNowMoney(String money) {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recharge_activity_rl_back:
                finish();
                break;
        }
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }
    @Subscribe
    public void onEvent(NettyMessage event) {
        msgHandler.sendEmptyMessage(1);
    }
    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    isCanShowPop(true);
                    break;
                case 2:
                    closeShowPop();
                    break;
            }
        }
    };

    @Override
    protected void closeShowPop() {
        super.closeShowPop();
    }

    @Override
    protected void isCanShowPop(boolean flag) {
        super.isCanShowPop(flag);
    }
}
