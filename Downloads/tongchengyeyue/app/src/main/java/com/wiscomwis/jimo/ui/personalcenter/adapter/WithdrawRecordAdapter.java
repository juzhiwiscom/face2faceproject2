package com.wiscomwis.jimo.ui.personalcenter.adapter;

import android.content.Context;
import android.widget.TextView;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.data.model.WithdrawRecord;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.util.DateTimeUtil;

import java.util.List;

/**
 * Created by Administrator on 2017/7/14.
 */

public class WithdrawRecordAdapter extends CommonRecyclerViewAdapter<WithdrawRecord> {
    public WithdrawRecordAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    public WithdrawRecordAdapter(Context context, int layoutResId, List<WithdrawRecord> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(WithdrawRecord withdrawRecord, int position, RecyclerViewHolder holder) {
        if(withdrawRecord!=null){
            holder.setText(R.id.withdraw_tv_money,String.valueOf(withdrawRecord.getWithdrawBeanAmount()) );
             holder.setText(R.id.withdraw_tv_time, DateTimeUtil.convertTimeMillis2String(withdrawRecord.getRecordTime()));
            TextView tv_state = (TextView) holder.getView(R.id.withdraw_tv_state);
           switch (withdrawRecord.getAuditStatus()){
               case 1:
                   tv_state.setText(mContext.getString(R.string.under_review));
                   break;
               case 2:
                   tv_state.setText(mContext.getString(R.string.withdraw_fail));
                   break;
               case 3:
                   tv_state.setText(mContext.getString(R.string.withdraw_success));
                   break;
           }
        }

    }
}
