package com.wiscomwis.jimo.ui.detail.banner.loader;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.wiscomwis.jimo.common.Util;

/**
 * Created by xuzhaole on 2018/3/1.
 */

public class GlideImageLoader extends ImageLoader {
    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        Glide.with(context.getApplicationContext())
                .load(path)
                .placeholder(Util.getDefaultImage())
                .into(imageView);
    }
}
