package com.wiscomwis.jimo.ui.detail.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.model.UserPhoto;
import com.wiscomwis.library.adapter.base.BaseItemDraggableAdapter;
import com.wiscomwis.library.adapter.base.BaseViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;

import java.util.List;

/**
 * Created by xuzhaole on 2018/3/2.
 */

public class NewAlbumAdapter extends BaseItemDraggableAdapter<UserPhoto, BaseViewHolder> {

    public NewAlbumAdapter(List<UserPhoto> data) {
        super(R.layout.album_photo_item, data);
    }


    @Override
    protected void convert(BaseViewHolder holder, UserPhoto userPhoto) {
        if (userPhoto == null || TextUtils.isEmpty(userPhoto.getFileUrlMinimum())) {
            Glide.with(mContext).load(R.drawable.chick_pic_video).into((ImageView) holder.getView(R.id.item_photo_iv));
        } else {
            if (userPhoto != null && !TextUtils.isEmpty(userPhoto.getStatus())) {
                if (userPhoto.getStatus().equals("1")) {//已通过
                    holder.getView(R.id.layout_layer).setVisibility(View.GONE);
                } else {
                    holder.getView(R.id.layout_layer).setVisibility(View.VISIBLE);
                }
            }
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder()
                    .url(userPhoto.getFileUrlMinimum()).imageView((ImageView) holder.getView(R.id.item_photo_iv))
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).build());
        }
    }
}
