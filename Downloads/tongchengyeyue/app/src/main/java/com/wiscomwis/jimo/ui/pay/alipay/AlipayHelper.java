package com.wiscomwis.jimo.ui.pay.alipay;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.alipay.sdk.app.PayTask;
import com.wiscomwis.jimo.common.AgoraHelper;
import com.wiscomwis.jimo.common.CustomDialogAboutPay;
import com.wiscomwis.jimo.data.model.PayResult;
import com.wiscomwis.jimo.event.PaySuccessEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

/**
 * Created by WangYong on 2017/9/7.
 */
public class AlipayHelper {
    private static AlipayHelper sInstance;
    private static final int SDK_PAY_FLAG=1;
    private  static Context context1;
    private static int type=1;//1钻石 2 vip 3 钥匙
    private static String serviceName="";
    private static String price="";
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case SDK_PAY_FLAG:
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    if(payResult.getResultStatus().equals("9000")){
                        if(context1!=null){
                            EventBus.getDefault().post(new PaySuccessEvent());
                            //支付成功之后直接重新访问个人的数据
                            CustomDialogAboutPay.paySucceedShow(context1,serviceName,price,type);
                        }
                    }else{
                        if(context1!=null){
                            CustomDialogAboutPay.payFaildShow(context1);
                        }
                    }
                    break;
            }
        }
    };

    public static AlipayHelper getInstance() {
        if (null == sInstance) {
            synchronized (AgoraHelper.class) {
                if (null == sInstance) {
                    sInstance = new AlipayHelper();
                }
            }
        }
        return sInstance;
    }
    //调用支付宝支付的api
    public void callAlipayApi(Context context, final String orderInfo,String serviceName1,String price1,int type1){
        context1=context;
        type=type1;
        serviceName=serviceName1;
        price=price1;
        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                PayTask alipay = new PayTask((Activity) context1);
                Map<String, String> result = alipay.payV2(orderInfo,true);
                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                handler.sendMessage(msg);
            }
        };
        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }
}
