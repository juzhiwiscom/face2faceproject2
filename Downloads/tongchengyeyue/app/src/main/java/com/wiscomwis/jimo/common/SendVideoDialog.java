package com.wiscomwis.jimo.common;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.BaseModel;
import com.wiscomwis.jimo.db.DbModle;
import com.wiscomwis.jimo.event.SendPrivateMessageEvent;
import com.wiscomwis.jimo.ui.photo.GetVideoActivity;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.SharedPreferenceUtil;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

/**
 * Created by xuzhaole on 2017/11/29.
 * 所要礼物-视频对话框
 */

public class SendVideoDialog {
    private Button btn_sure;
    private ImageView iv_add_photo;

    private Context context;
    private String giftId;
    private String[] giftIds;
    private File mFile;
    private File mBitmapPath;
    private String mVideoDuration;


    public void setContext(Context context) {
        this.context = context;
        String gifts = SharedPreferenceUtil.getStringValue(context, "askGiftVideo", "askGiftVideo", null);
        if (gifts != null) {
            giftIds = gifts.split(",");
            giftId = giftIds[0];
        }
    }

    //发送私密视频
    public void sendVideo() {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        final View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_send_video_gifts, null);
        bottomDialog.setContentView(contentView);
        final LinearLayout ll_rose = (LinearLayout) contentView.findViewById(R.id.send_video_ll_rose);
        final LinearLayout ll_kiss = (LinearLayout) contentView.findViewById(R.id.send_video_ll_kiss);
        final LinearLayout ll_bixin = (LinearLayout) contentView.findViewById(R.id.send_video_ll_bixin);
        ImageView iv_rose = (ImageView) contentView.findViewById(R.id.send_video_iv_rose);
        ImageView iv_kiss = (ImageView) contentView.findViewById(R.id.send_video_iv_kiss);
        ImageView iv_bixin = (ImageView) contentView.findViewById(R.id.send_video_iv_bixin);
        iv_add_photo = (ImageView) contentView.findViewById(R.id.send_video_iv_add_photo);
        btn_sure = (Button) contentView.findViewById(R.id.send_video_btn_sure);
        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[0]).getPicIcon())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_rose).build());
        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[1]).getPicIcon())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_kiss).build());
        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[2]).getPicIcon())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_bixin).build());

        TextView tv_name_1 = (TextView) contentView.findViewById(R.id.tv_name_1);
        TextView tv_name_2 = (TextView) contentView.findViewById(R.id.tv_name_2);
        TextView tv_name_3 = (TextView) contentView.findViewById(R.id.tv_name_3);

        TextView tv_price_1 = (TextView) contentView.findViewById(R.id.tv_price_1);
        TextView tv_price_2 = (TextView) contentView.findViewById(R.id.tv_price_2);
        TextView tv_price_3 = (TextView) contentView.findViewById(R.id.tv_price_3);

        tv_price_1.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[0]).getPicPrice() + "钻石");
        tv_price_2.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[1]).getPicPrice() + "钻石");
        tv_price_3.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[2]).getPicPrice() + "钻石");

        tv_name_1.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[0]).getPicName());
        tv_name_2.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[1]).getPicName());
        tv_name_3.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[2]).getPicName());


        ll_rose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                giftId = "301000111";
                ll_rose.setBackgroundResource(R.drawable.gifts_unselected);
                ll_kiss.setBackgroundResource(R.drawable.gifts_selected);
                ll_bixin.setBackgroundResource(R.drawable.gifts_selected);
            }
        });
        ll_kiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                giftId = "301000105";
                ll_rose.setBackgroundResource(R.drawable.gifts_selected);
                ll_kiss.setBackgroundResource(R.drawable.gifts_unselected);
                ll_bixin.setBackgroundResource(R.drawable.gifts_selected);
            }
        });
        ll_bixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                giftId = "301000103";
                ll_rose.setBackgroundResource(R.drawable.gifts_selected);
                ll_kiss.setBackgroundResource(R.drawable.gifts_selected);
                ll_bixin.setBackgroundResource(R.drawable.gifts_unselected);
            }
        });
        iv_add_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetVideoActivity.toGetVideoActivity(context, new GetVideoActivity.OnGetVideoListener() {
                    @Override
                    public void getSelectedVideo(String filePath, String duration, String bitmapFilePath) {
                        if (null != filePath) {
                            Glide.with(context).load(new File(bitmapFilePath)).error(Util.getDefaultImage())
                                    .placeholder(Util.getDefaultImage()).into(iv_add_photo);
                            mFile = new File(filePath);
                            mVideoDuration = duration;
                            mBitmapPath = new File(bitmapFilePath);
                        }
                    }
                });
            }
        });

        //发送
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFile != null && mBitmapPath != null) {
                    final ProgressDialog progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage(context.getString(R.string.upload_photo_wait));
                    progressDialog.setCancelable(false);
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();
                    ApiManager.sendPrivateVideo(mVideoDuration, giftId, mFile, mBitmapPath, new IGetDataListener<BaseModel>() {
                        @Override
                        public void onResult(BaseModel baseModel, boolean isEmpty) {
                            progressDialog.dismiss();
                            bottomDialog.dismiss();
                            EventBus.getDefault().post(new SendPrivateMessageEvent());
                            ToastUtil.showShortToast(context, context.getString(R.string.dialog_success_ask_gift));
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                            progressDialog.dismiss();
                            bottomDialog.dismiss();
                            ToastUtil.showShortToast(context, context.getString(R.string.dialog_success_ask_gift));
                        }
                    });
                } else {
                    ToastUtil.showShortToast(context, context.getString(R.string.upload_you_own_video));
                }
            }
        });

        dialogShow(context, bottomDialog, contentView, Gravity.BOTTOM);
    }

    //对话框初始化显示的方法
    private static void dialogShow(Context context, Dialog bottomDialog, View contentView, int location) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(location);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                context.getResources().getDisplayMetrics());
    }
}
