package com.wiscomwis.jimo.ui.personalcenter;

import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseTopBarActivity;
import com.wiscomwis.jimo.common.AskForGiftsDialog;
import com.wiscomwis.jimo.common.SendPrivatePhotoDialog;
import com.wiscomwis.jimo.common.SendVideoDialog;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.HostInfo;
import com.wiscomwis.jimo.data.model.MyInfo;
import com.wiscomwis.jimo.data.model.UserBase;
import com.wiscomwis.jimo.data.model.UserDetail;
import com.wiscomwis.jimo.data.preference.AnchorPreference;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.event.SendPrivateMessageEvent;
import com.wiscomwis.library.net.NetUtil;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/11/7.
 */

public class AskGiftsActivity extends BaseTopBarActivity implements View.OnClickListener {
    @BindView(R.id.activity_ask_gifts_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.activity_ask_for_gifts_btn_private_msg)
    Button btn_private_msg;
    @BindView(R.id.activity_ask_for_gifts_btn_private_photo)
    Button btn_private_photo;
    @BindView(R.id.activity_ask_for_gifts_btn_private_video)
    Button btn_private_video;
    @BindView(R.id.total_send_text)
    TextView totalSendText;
    @BindView(R.id.total_send_photo)
    TextView totalSendPhoto;
    @BindView(R.id.total_send_video)
    TextView totalSendVideo;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_ask_for_gifts;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected String getDefaultTitle() {
        return null;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void setListeners() {
        rl_back.setOnClickListener(this);
        btn_private_msg.setOnClickListener(this);
        btn_private_photo.setOnClickListener(this);
        btn_private_video.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
        totalSendText.setText(AnchorPreference.getTotalSendText() + "");
        totalSendPhoto.setText(AnchorPreference.getTotalSendPhoto() + "");
        totalSendVideo.setText(AnchorPreference.getTotalSendVideo() + "");
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_ask_gifts_rl_back:
                finish();
                break;
            case R.id.activity_ask_for_gifts_btn_private_msg:
//                CustomDialogAboutOther.askForGiftsShow(AskGiftsActivity.this);
                AskForGiftsDialog askForGiftsDialog = new AskForGiftsDialog();
                askForGiftsDialog.setContext(mContext);
                askForGiftsDialog.askForGiftsShow();
                break;
            case R.id.activity_ask_for_gifts_btn_private_photo:
//                CustomDialogAboutOther.sendPrivatePhotoShow(AskGiftsActivity.this);
                SendPrivatePhotoDialog dialog = new SendPrivatePhotoDialog();
                dialog.setContext(mContext);
                dialog.sendPrivatePhoto();
                break;
            case R.id.activity_ask_for_gifts_btn_private_video:
//                CustomDialogAboutOther.sendVideoShow(AskGiftsActivity.this);
                SendVideoDialog videoDialog = new SendVideoDialog();
                videoDialog.setContext(mContext);
                videoDialog.sendVideo();
                break;
        }
    }
    @Subscribe
    public void onEvent(SendPrivateMessageEvent event) {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if(myInfo!=null){
                    UserDetail userDetail = myInfo.getUserDetail();
                    if(userDetail!=null){
                        HostInfo hostInfo = userDetail.getHostInfo();
                        if(hostInfo!=null){
                            AnchorPreference.saveHostInfo(hostInfo);
                            loadData();
                        }
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase!=null) {
                            UserPreference.saveUserInfo(userBase);
                        }
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

}
