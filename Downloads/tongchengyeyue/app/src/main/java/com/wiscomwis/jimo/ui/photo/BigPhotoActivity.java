package com.wiscomwis.jimo.ui.photo;

import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.tmall.ultraviewpager.transformer.UltraDepthScaleTransformer;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseFragmentActivity;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.parcelable.BigPhotoParcelable;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * 显示大图片
 * Created by zhangdroid on 2017/6/3.
 */
public class BigPhotoActivity extends BaseFragmentActivity {
    @BindView(R.id.big_photo_viewpager)
    PhotoViewPager mViewPager;

    private BigPhotoParcelable mBigPhotoParcelable;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_big_photo;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mBigPhotoParcelable = (BigPhotoParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        if (null != mBigPhotoParcelable) {
            List<String> list = mBigPhotoParcelable.imgUrlList;
            if (!Utils.isListEmpty(list)) {
                // 添加图片ImageView
                List<PhotoView> imageViews = new ArrayList<>();
                for (String url : list) {
                    if (!TextUtils.isEmpty(url)) {
                        PhotoView photoView = new PhotoView(mContext);
                        photoView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT));
                        // 允许缩放
                        photoView.setZoomable(true);
                        ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder().url(url)
                                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(photoView).build());
                        photoView.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
                            @Override
                            public void onViewTap(View view, float x, float y) {
                                // 轻击退出
                                finish();
                            }
                        });
                        imageViews.add(photoView);
                    }
                }
                mViewPager.setAdapter(new BigPhotoAdapter(imageViews));
                mViewPager.setPageTransformer(false, new UltraDepthScaleTransformer());
                mViewPager.setCurrentItem(mBigPhotoParcelable.index, false);
            }
        }
    }

    @Override
    protected void setListeners() {
    }

    @Override
    protected void loadData() {
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {
    }

    @Override
    protected void networkDisconnected() {
    }

}
