package com.wiscomwis.jimo.ui.pay.presenter;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.HostInfo;
import com.wiscomwis.jimo.data.model.MyInfo;
import com.wiscomwis.jimo.data.model.UserBase;
import com.wiscomwis.jimo.data.model.UserDetail;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.ui.homepage.HomepageAdapter;
import com.wiscomwis.jimo.ui.pay.contract.RechargeContract;
import com.wiscomwis.jimo.ui.pay.fragment.DredgeVipFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public class RechargePresenter implements RechargeContract.IPresenter {
    private static final String TAG = RechargePresenter.class.getSimpleName();
    private RechargeContract.IView mPayView;
    private Context mContext;

    public RechargePresenter(RechargeContract.IView view) {
        this.mPayView = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }


    @Override
    public void getPayWay(String fromTag) {

    }

    @Override
    public void addTabs() {
        HomepageAdapter homepageAdapter = new HomepageAdapter(mPayView.getManager());
        List<Fragment> fragmentList = new ArrayList<>();
        List<String>  tabList = Arrays.asList(mContext.getString(R.string.open_vip),mContext.getString(R.string.buy_keys));
            fragmentList.add(new DredgeVipFragment());
        homepageAdapter.setData(fragmentList, tabList);
        mPayView.setAdapter(homepageAdapter);
    }

    @Override
    public void getMyInfo() {
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if(myInfo!=null){
                    UserDetail userDetail = myInfo.getUserDetail();
                  if(userDetail!=null){
                      HostInfo hostInfo = userDetail.getHostInfo();
                      if(hostInfo!=null){
                          mPayView.getNowMoney(String.valueOf(hostInfo.getBalance()));
                      }
                      UserBase userBase = userDetail.getUserBase();
                      if (userBase!=null) {
                          UserPreference.saveUserInfo(userBase);
                      }
                  }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

}
