package com.wiscomwis.jimo.ui.pay.contract;

import android.support.v7.widget.RecyclerView;

import com.wiscomwis.jimo.mvp.BasePresenter;
import com.wiscomwis.jimo.mvp.BaseView;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public interface IncomeContract {


    interface IView extends BaseView {

        /**
         * 隐藏下拉刷新
         */
        void hideRefresh(int delaySeconds);

        void toggleShowError(boolean toggle, String msg);

        void toggleShowEmpty(boolean toggle, String msg);

        void setAdapter(RecyclerView.Adapter pagerAdapter);
    }

    interface IPresenter extends BasePresenter {

        void loadData(boolean isRefresh);

        void refresh();

        void start(RecyclerView recyclerView);
    }
}
