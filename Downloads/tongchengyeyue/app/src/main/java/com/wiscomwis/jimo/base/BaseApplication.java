package com.wiscomwis.jimo.base;

import android.app.Application;
import android.content.Context;

import com.danikula.videocache.HttpProxyCacheServer;
import com.wiscomwis.jimo.common.HyphenateHelper;
import com.wiscomwis.jimo.db.DbModle;
import com.wiscomwis.jimo.service.InitializeService;

/**
 * Created by zhangdroid on 2017/5/11.
 */
public class BaseApplication extends Application {
    private static Context sApplicationContext;

    private HttpProxyCacheServer proxy;

    public static HttpProxyCacheServer getProxy(Context context) {
        BaseApplication app = (BaseApplication) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(this).maxCacheSize(1024 * 1024 * 1024)       // 1 Gb for cache
                .build();
    }

    public static Context getGlobalContext() {
        return sApplicationContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplicationContext = getApplicationContext();
        // 初始化环信
        HyphenateHelper.getInstance().init(this);
        InitializeService.start(this);
        DbModle.getInstance().init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

}
