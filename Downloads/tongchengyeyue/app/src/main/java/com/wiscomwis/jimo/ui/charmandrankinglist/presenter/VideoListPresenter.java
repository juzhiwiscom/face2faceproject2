package com.wiscomwis.jimo.ui.charmandrankinglist.presenter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.CustomDialogAboutPay;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.VideoSquare;
import com.wiscomwis.jimo.data.model.VideoSquareList;
import com.wiscomwis.jimo.ui.charmandrankinglist.adapter.VideoListAdapter;
import com.wiscomwis.jimo.ui.charmandrankinglist.contract.VideoListContract;
import com.wiscomwis.library.util.ToastUtil;

import java.util.List;

/**
 * Created by xuzhaole on 2018/3/20.
 */

public class VideoListPresenter implements VideoListContract.IPresenter {
    private VideoListAdapter adapter;
    private VideoListContract.IView mVideoListView;
    private Context mContext;


    public VideoListPresenter(VideoListContract.IView videoListView) {
        this.mVideoListView = videoListView;
        mContext = videoListView.obtainContext();
    }

    @Override
    public void start() {
        adapter = new VideoListAdapter(((FragmentActivity) mContext).getSupportFragmentManager());
        mVideoListView.setAdapter(adapter);
    }

    @Override
    public void loadVideoShowList(final int num) {
        ApiManager.VideoShowList(num + "", "5", new IGetDataListener<VideoSquareList>() {
            @Override
            public void onResult(VideoSquareList videoSquareList, boolean isEmpty) {
                if (videoSquareList.getVideoSquareList() == null || videoSquareList.getVideoSquareList().size() == 0) {
                    if (num == 1) {
                        mVideoListView.setEmptyView(true, null);
                    } else {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.tip_no_more));
                    }
                } else {
                    mVideoListView.setPostion();
                    if (num == 1) {
                        adapter.setData(videoSquareList.getVideoSquareList());
                    } else {
                        adapter.addData(videoSquareList.getVideoSquareList());
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mVideoListView.setErrorView(true, msg);
            }
        });
    }

    @Override
    public void reportShow(int currentPosition) {
        List<VideoSquare> data = adapter.getData();
        if (data != null && currentPosition < data.size()) {
            VideoSquare videoSquare = data.get(currentPosition);
            long guid = videoSquare.getUserBase().getGuid();
            CustomDialogAboutPay.reportShow(mContext, guid + "");
        }
    }
}
