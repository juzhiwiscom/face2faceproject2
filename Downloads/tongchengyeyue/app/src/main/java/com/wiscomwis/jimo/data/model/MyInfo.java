package com.wiscomwis.jimo.data.model;

/**
 * 用户信息对象
 * Created by zhangdroid on 2017/6/2.
 */
public class MyInfo extends BaseModel {
    private UserDetail userDetail;

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    @Override
    public String toString() {
        return "MyInfo{" +
                "userDetail=" + userDetail +
                '}';
    }
}
