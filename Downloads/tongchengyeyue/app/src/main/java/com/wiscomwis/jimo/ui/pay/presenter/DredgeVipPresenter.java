package com.wiscomwis.jimo.ui.pay.presenter;

import android.content.Context;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.PayDict;
import com.wiscomwis.jimo.data.model.PayWay;
import com.wiscomwis.jimo.data.model.UserVip;
import com.wiscomwis.jimo.ui.pay.adapter.DredgeVipAdapter;
import com.wiscomwis.jimo.ui.pay.contract.DredgeVipContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangdroid on 2017/5/25.
 */
public class DredgeVipPresenter implements DredgeVipContract.IPresenter {
    private DredgeVipContract.IView mDredgeVipview;
    private Context mContext;
    private DredgeVipAdapter adapter;
    public DredgeVipPresenter(DredgeVipContract.IView view) {
        this.mDredgeVipview = view;
        this.mContext = view.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void getPayWay(String fromTag) {
        ApiManager.getPayWay("1", "2", new IGetDataListener<PayWay>() {
            @Override
            public void onResult(PayWay payWay, boolean isEmpty) {
                if(payWay!=null){
                    List<UserVip> vipList = payWay.getVipList();
                    if(vipList!=null&&vipList.size()>0){
                        List<String> content=new ArrayList<String>();
                        List<String> name=new ArrayList<String>();
                        for (UserVip userVip : vipList) {
                            name.add(userVip.getNickName());
                            content.add(userVip.getStartTime()+mContext.getString(R.string.start_time)+userVip.getServiceName()+mContext.getString(R.string.day)+"VIP");
                        }
                        mDredgeVipview.getGunDongText(name,content);
                    }
                    PayDict payDict1 = payWay.getDictPayList().get(0);
                    if(payDict1!=null){
                        mDredgeVipview.setInfoString1(payDict1.getServiceName(),payDict1.getPrice(),payDict1.getServiceId(),payDict1.getServiceDesc());
                    }
                    PayDict payDict2 = payWay.getDictPayList().get(1);
                    if(payDict1!=null){
                        mDredgeVipview.setInfoString2(payDict2.getServiceName(),payDict2.getPrice(),payDict2.getServiceId(),payDict2.getServiceDesc());
                    }
                    PayDict payDict3 = payWay.getDictPayList().get(2);
                    PayDict payDict4 = payWay.getDictPayList().get(3);
                    if(payDict1!=null){
                        int vipSaleCutdown = payWay.getVipSaleCutdown();
                        if(vipSaleCutdown>0){
                            mDredgeVipview.setInfoString3(payDict4.getServiceName(),payDict4.getPrice(),payDict4.getServiceId(),payDict4.getServiceDesc());
                        }else{
                            mDredgeVipview.setInfoString3(payDict3.getServiceName(),payDict3.getPrice(),payDict3.getServiceId(),"￥"+payDict3.getPrice()+"|"+payDict3.getServiceDesc());
                            mDredgeVipview.isPast24Hours();
                        }
                    }
                    if(payWay.getDescList()!=null&&payWay.getDescList().size()>0){
                        adapter=new DredgeVipAdapter(mContext,payWay.getDescList());
                        mDredgeVipview.setAdapter(adapter);
                    }

                }
            }
            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });

    }
}
