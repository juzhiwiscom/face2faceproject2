package com.wiscomwis.jimo.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2017/7/11.
 * 用来从个人中心页面向设置价格界面传递价格的参数
 */

public class TvPriceParcelable implements Parcelable {
    public String price;
    public int audioPrice;

    public TvPriceParcelable(String price,int audioPrice) {
        this.price=price;
        this.audioPrice=audioPrice;
    }
    public TvPriceParcelable(Parcel in) {
        price=in.readString();
        audioPrice=in.readInt();
    }

    public static final Creator<TvPriceParcelable> CREATOR = new Creator<TvPriceParcelable>() {
        @Override
        public TvPriceParcelable createFromParcel(Parcel in) {
            return new TvPriceParcelable(in);
        }

        @Override
        public TvPriceParcelable[] newArray(int size) {
            return new TvPriceParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(price);
        parcel.writeInt(audioPrice);
    }
}
