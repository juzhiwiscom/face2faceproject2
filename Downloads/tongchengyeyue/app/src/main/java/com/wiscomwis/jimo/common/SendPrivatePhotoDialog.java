package com.wiscomwis.jimo.common;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.BaseModel;
import com.wiscomwis.jimo.db.DbModle;
import com.wiscomwis.jimo.event.SendPrivateMessageEvent;
import com.wiscomwis.jimo.ui.photo.GetPhotoActivity;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.SharedPreferenceUtil;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;

/**
 * Created by xuzhaole on 2017/11/29.
 * 所要礼物-私密图片对话框
 */

public class SendPrivatePhotoDialog {
    private Button btn_sure;
    private EditText et_msg;
    private ImageView iv_add_photo;
    private TextView tv_number;
    private LinearLayout ll_dialog;

    private Context context;
    private String giftId;
    private String[] giftIds;
    private File mFile;


    public void setContext(Context context) {
        this.context = context;
        String gifts = SharedPreferenceUtil.getStringValue(context, "askGiftPhoto", "askGiftPhoto", null);
        if (gifts != null) {
            giftIds = gifts.split(",");
            giftId = giftIds[0];
        }
    }

    //发送私密图片
    public void sendPrivatePhoto() {
        final Dialog bottomDialog = new Dialog(context, R.style.BottomDialog);
        View contentView = LayoutInflater.from(context).inflate(R.layout.dialog_send_private_photo_gifts, null);
        bottomDialog.setContentView(contentView);
        final LinearLayout ll_rose = (LinearLayout) contentView.findViewById(R.id.send_private_photo_ll_rose);
        final LinearLayout ll_kiss = (LinearLayout) contentView.findViewById(R.id.send_private_photo_ll_kiss);
        final LinearLayout ll_bixin = (LinearLayout) contentView.findViewById(R.id.send_private_photo_ll_bixin);
        ImageView iv_rose = (ImageView) contentView.findViewById(R.id.send_private_photo_iv_rose);
        ImageView iv_kiss = (ImageView) contentView.findViewById(R.id.send_private_photo_iv_kiss);
        ImageView iv_bixin = (ImageView) contentView.findViewById(R.id.send_private_photo_iv_bixin);
        TextView tv_name_1 = (TextView) contentView.findViewById(R.id.tv_name_1);
        TextView tv_name_2 = (TextView) contentView.findViewById(R.id.tv_name_2);
        TextView tv_name_3 = (TextView) contentView.findViewById(R.id.tv_name_3);

        TextView tv_price_1 = (TextView) contentView.findViewById(R.id.tv_price_1);
        TextView tv_price_2 = (TextView) contentView.findViewById(R.id.tv_price_2);
        TextView tv_price_3 = (TextView) contentView.findViewById(R.id.tv_price_3);

        tv_price_1.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[0]).getPicPrice() + "钻石");
        tv_price_2.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[1]).getPicPrice() + "钻石");
        tv_price_3.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[2]).getPicPrice() + "钻石");

        tv_name_1.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[0]).getPicName());
        tv_name_2.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[1]).getPicName());
        tv_name_3.setText(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[2]).getPicName());


        iv_add_photo = (ImageView) contentView.findViewById(R.id.send_private_photo_iv_add_photo);
        et_msg = (EditText) contentView.findViewById(R.id.send_private_photo_text_input);
        btn_sure = (Button) contentView.findViewById(R.id.send_private_photo_btn_sure);
        tv_number = (TextView) contentView.findViewById(R.id.tv_number);
        ll_dialog = (LinearLayout) contentView.findViewById(R.id.ll_dialog);


        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[0]).getPicIcon())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_rose).build());
        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[1]).getPicIcon())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_kiss).build());
        ImageLoaderUtil.getInstance().loadImage(context, new ImageLoader.Builder().url(DbModle.getInstance().getUserAccountDao().getPicInfo(giftIds[2]).getPicIcon())
                .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(iv_bixin).build());

        ll_rose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                giftId = giftIds[0];
                ll_rose.setBackgroundResource(R.drawable.gifts_unselected);
                ll_kiss.setBackgroundResource(R.drawable.gifts_selected);
                ll_bixin.setBackgroundResource(R.drawable.gifts_selected);
            }
        });
        ll_kiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                giftId = giftIds[1];
                ll_rose.setBackgroundResource(R.drawable.gifts_selected);
                ll_kiss.setBackgroundResource(R.drawable.gifts_unselected);
                ll_bixin.setBackgroundResource(R.drawable.gifts_selected);
            }
        });
        ll_bixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                giftId = giftIds[2];
                ll_rose.setBackgroundResource(R.drawable.gifts_selected);
                ll_kiss.setBackgroundResource(R.drawable.gifts_selected);
                ll_bixin.setBackgroundResource(R.drawable.gifts_unselected);
            }
        });
        iv_add_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetPhotoActivity.toGetPhotoActivity(context, new GetPhotoActivity.OnGetPhotoListener() {
                    @Override
                    public void getSelectedPhoto(File file) {
                        if (null != file) {
                            try {
                                String canonicalPath = file.getCanonicalPath();
                                Bitmap bitmap = BitmapFactory.decodeFile(canonicalPath, null);
                                iv_add_photo.setImageBitmap(bitmap);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            mFile = file;
                        }
                    }
                });
            }
        });
        ll_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)
                        context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });

        et_msg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                tv_number.setText(charSequence.length() + "/40");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        //发送
        btn_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(et_msg.getText().toString().trim())) {
                    if (mFile != null) {
                        String content = Util.encodeHeadInfo(et_msg.getText().toString().trim());
                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage(context.getString(R.string.upload_photo_wait));
                        progressDialog.setCancelable(false);
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.show();
                        ApiManager.sendPrivatePhoto(content, giftId, mFile, new IGetDataListener<BaseModel>() {
                            @Override
                            public void onResult(BaseModel baseModel, boolean isEmpty) {
                                progressDialog.dismiss();
                                EventBus.getDefault().post(new SendPrivateMessageEvent());
                                ToastUtil.showShortToast(context, context.getString(R.string.dialog_success_ask_gift));
                                bottomDialog.dismiss();
                            }

                            @Override
                            public void onError(String msg, boolean isNetworkError) {
                                progressDialog.dismiss();
                                bottomDialog.dismiss();
                                ToastUtil.showShortToast(context, context.getString(R.string.dialog_success_ask_gift));
                            }
                        });
                    } else {
                        ToastUtil.showShortToast(context, context.getString(R.string.upload_you_own_photo));
                    }
                } else {
                    ToastUtil.showShortToast(context, context.getString(R.string.input_what_you_say));
                }
            }
        });


        dialogShow(context, bottomDialog, contentView, Gravity.BOTTOM);
    }

    //对话框初始化显示的方法
    private static void dialogShow(Context context, Dialog bottomDialog, View contentView, int location) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
        params.width = context.getResources().getDisplayMetrics().widthPixels - dp2px(context, 16f);
        params.bottomMargin = dp2px(context, 8f);
        contentView.setLayoutParams(params);
        bottomDialog.setCanceledOnTouchOutside(true);
        bottomDialog.getWindow().setGravity(location);
        bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        bottomDialog.show();
    }

    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal,
                context.getResources().getDisplayMetrics());
    }
}
