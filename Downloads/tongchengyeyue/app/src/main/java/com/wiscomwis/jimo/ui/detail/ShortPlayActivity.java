package com.wiscomwis.jimo.ui.detail;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Parcelable;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import com.danikula.videocache.HttpProxyCacheServer;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseApplication;
import com.wiscomwis.jimo.base.BaseFragmentActivity;
import com.wiscomwis.jimo.parcelable.ShortPlayParcelable;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.widget.PlayView;

import butterknife.BindView;

/**
 * 短视频播放
 */

public class ShortPlayActivity extends BaseFragmentActivity {
    @BindView(R.id.playview)
    PlayView playView;
    @BindView(R.id.loading_progress)
    ProgressBar loadingProgress;
    private ShortPlayParcelable mShortPlayParcelable;
    private String stringExtra;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_short_play;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {
        mShortPlayParcelable = (ShortPlayParcelable) parcelable;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        stringExtra = mShortPlayParcelable.urlData;
    }


    private void initPlayView() {
        if (!TextUtils.isEmpty(stringExtra)) {
            try {
                HttpProxyCacheServer proxy = BaseApplication.getProxy(mContext);
                String proxyUrl = proxy.getProxyUrl(stringExtra);
                playView.setVideoURI(Uri.parse(proxyUrl));
                playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        playView.start();
                    }
                });
                playView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        loadingProgress.setVisibility(View.GONE);
                        //获取视频资源的宽度
                        int videoWidth = mp.getVideoWidth();
                        //获取视频资源的高度
                        int videoHeight = mp.getVideoHeight();
                        playView.setSizeH(videoHeight);
                        playView.setSizeW(videoWidth);
                        playView.requestLayout();
                    }
                });
                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                boolean isScreenOn = pm.isScreenOn();//如果为true，则表示屏幕“亮”了，否则屏幕“暗”了。
                if (!isScreenOn) {
                    pauseVideo();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    protected void setListeners() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        playView.seekTo(1);
        initPlayView();
        play();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playView.stopPlayback();
    }

    @Override
    protected void onPause() {
        super.onPause();
        playView.pause();
        pauseVideo();

    }

    private void pauseVideo() {
        playView.pause();
    }

    private void startVideo() {
        playView.start();

    }

    /**
     * 播放
     */
    private void play() {
        if (playView.isPlaying()) {
            pauseVideo();
        } else {
            if (playView.getCurrentPosition() == playView.getDuration()) {
                playView.seekTo(0);
            }
            startVideo();
        }
    }
}
