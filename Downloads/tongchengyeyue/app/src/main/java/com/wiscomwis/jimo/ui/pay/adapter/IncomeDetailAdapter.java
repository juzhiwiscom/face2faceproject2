package com.wiscomwis.jimo.ui.pay.adapter;

import android.support.annotation.LayoutRes;
import android.text.TextUtils;
import android.util.Log;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.data.model.IncomeRecord;
import com.wiscomwis.jimo.data.model.PicInfo;
import com.wiscomwis.jimo.db.DbModle;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.adapter.base.BaseViewHolder;
import com.wiscomwis.library.util.DateTimeUtil;

/**
 * Created by xuzhaole on 2018/1/24.
 */

public class IncomeDetailAdapter extends BaseQuickAdapter<IncomeRecord, BaseViewHolder> {


    public IncomeDetailAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder holder, IncomeRecord incomeRecord) {
        if (incomeRecord != null) {
            Log.e("incomerecord",incomeRecord.toString());
            holder.setText(R.id.income_record_tv_time, DateTimeUtil.convertTimeMillis2String(incomeRecord.getAddTime()));
            holder.setText(R.id.income_record_tv_money, String.valueOf(incomeRecord.getBeanAmount()));
            switch (incomeRecord.getType()) {
                case 1:
                    holder.setText(R.id.income_record_tv_tujing, mContext.getString(R.string.tab_message));
                    break;
                case 2:
                    holder.setText(R.id.income_record_tv_tujing, mContext.getString(R.string.video_video));
                    break;
                case 3:
                    if (!TextUtils.isEmpty(incomeRecord.getGiftId())
                            && DbModle.getInstance().getUserAccountDao().getPicInfo(incomeRecord.getGiftId()) != null) {
                        PicInfo picInfo = DbModle.getInstance().getUserAccountDao().getPicInfo(incomeRecord.getGiftId());
                        holder.setText(R.id.income_record_tv_tujing, picInfo.getPicName() + "*" + incomeRecord.getQuantity());
                    } else {
                        holder.setText(R.id.income_record_tv_tujing, mContext.getString(R.string.gift));
                    }
                    break;
                case 4:
                    holder.setText(R.id.income_record_tv_tujing, mContext.getString(R.string.user_detail_voice));
                    break;
            }
        }
    }
}
