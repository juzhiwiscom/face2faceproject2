package com.wiscomwis.jimo.ui.detail.contract;

import android.support.v4.app.FragmentManager;

import com.wiscomwis.jimo.data.model.VideoOrImage;
import com.wiscomwis.jimo.mvp.BasePresenter;
import com.wiscomwis.jimo.mvp.BaseView;

import java.util.List;

/**
 * Created by Administrator on 2017/6/9.
 */
public interface EditInfoContract {

    interface IView extends BaseView {
        void showLoading();

        void dismissLoading();

        void showNetworkError();

        void setIntroducation(String introducation);

        FragmentManager obtainFragmentManager();

        void finishActivity();

        String stringNickName();

        String stringHeight();

        String stringRelation();

        String stringWeight();

        String stringSign();

        String stringAge();

        String stringOwnWords();


        void isMale(boolean male);

        void getNickNmae(String name);

        void getUserId(String id);

        void getAge(String age);

        void getHeight(String height);

        void getMarriage(String marriage);

        void getWeight(String weight);

        void getSign(String sign);

        void getOwnWords(String ownWords);

        void setStatus(String stauts);

        void startBanner(List<VideoOrImage> list);

        void upBanner(List<VideoOrImage> list);

    }

    interface IPresenter extends BasePresenter {

        /**
         * 设置用户信息
         */
        void setLocalInfo();

        /**
         * 上传用户信息
         */
        void upLoadMyInfo();

        /**
         * 获取用户的个人信息
         */
        void getUserInfo();

    }

}
