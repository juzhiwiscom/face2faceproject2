package com.gooday123.shouyintai.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gooday123.shouyintai.R;
import com.gooday123.shouyintai.mode.AppsItemInfo;
import com.gooday123.shouyintai.mode.StateStorage;
import com.gooday123.shouyintai.util.Tool;

import java.util.List;

public class AppIconAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<AppsItemInfo> list;
	private Activity mContext;

	public AppIconAdapter(List<AppsItemInfo> iconInfoList, Activity context) {
		this.mContext = context;
		this.list = iconInfoList;
		this.inflater = LayoutInflater.from(context);
	}

	public void setList(List<AppsItemInfo> iconInfoList) {

		this.list = iconInfoList;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public AppsItemInfo getItem(int position) {

		return list == null ? null : list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder = null;
		final AppsItemInfo appsItemInfo = getItem(position);
		if (convertView == null) {
			mHolder = new ViewHolder();
			convertView = inflater.inflate(R.layout.apps_item, null);

			mHolder.icon = (ImageView) convertView
					.findViewById(R.id.apps_image);
			mHolder.label = (TextView) convertView
					.findViewById(R.id.apps_textview);
			mHolder.apps_backgrundLLT = (LinearLayout) convertView
					.findViewById(R.id.apps_backgrundLLT);
			convertView.setTag(mHolder);

			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}
		mHolder.icon.setImageDrawable(appsItemInfo.getIcon());
		mHolder.label.setText(appsItemInfo.getLabel().toString());
		mHolder.apps_backgrundLLT.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (Tool.isFastDouleClick()) {
					return;
				}
                Intent intent = new Intent();
                intent = mContext.getPackageManager()
                        .getLaunchIntentForPackage(
                                appsItemInfo.getPackageName());
                mContext.startActivity(intent);
			}
		});
		mHolder.apps_backgrundLLT
				.setOnLongClickListener(new OnLongClickListener() {

					@Override
					public boolean onLongClick(View arg0) {
						DeleteDialog(appsItemInfo, mContext);
						return false;
					}
				});
		return convertView;
	}

	/**
	 * 退出系统确认框
	 */
	private void DeleteDialog(final AppsItemInfo appsItemInfo,
			final Context context) {
		AlertDialog.Builder builder = new Builder(mContext);

		builder.setMessage("确定要卸载应用吗？");
		builder.setTitle("提示");

		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				removeApp(appsItemInfo, context);

			}
		});

		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		builder.create().show();
	}

	private void removeApp(AppsItemInfo appsItemInfo, Context context) {

		Uri packageURI = Uri.parse("package:" + appsItemInfo.getPackageName());

		Intent intent = new Intent(Intent.ACTION_DELETE, packageURI);
		// 执行卸载程序
		context.startActivity(intent);
		StateStorage.setStateStorage(context, appsItemInfo.getPackageName(), false);

	};

	private class ViewHolder {
		private ImageView icon;
		private TextView label;
		private LinearLayout apps_backgrundLLT;
	}


}
