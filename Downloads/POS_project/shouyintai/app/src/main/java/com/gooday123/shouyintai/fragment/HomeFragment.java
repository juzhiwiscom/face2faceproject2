package com.gooday123.shouyintai.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gooday123.shouyintai.R;
import com.gooday123.shouyintai.activity.SettingActivity;
import com.gooday123.shouyintai.util.Tool;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by tianzhentao on 2018/5/12.
 */

public class HomeFragment extends Fragment {
    @BindView(R.id.tv_yinlian_shoukuan)
    TextView tvYinlianShoukuan;
    @BindView(R.id.tv_fuka_shoukuan)
    LinearLayout tvFukaShoukuan;
    @BindView(R.id.tv_saoyisao)
    TextView tvSaoyisao;
    @BindView(R.id.tv_atv_sale)
    TextView tvAtvSale;
    @BindView(R.id.tv_atv_sale_odds)
    TextView tvAtvSaleOdds;
    @BindView(R.id.tv_deal_detail)
    TextView tvDealDetail;
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.tv_sales_return)
    TextView tvSalesReturn;
    @BindView(R.id.tv_purchase_cancellation)
    TextView tvPurchaseCancellation;
    @BindView(R.id.tv_card_accredit)
    TextView tvCardAccredit;
    @BindView(R.id.tv_print)
    TextView tvPrint;
    @BindView(R.id.tv_group_account)
    TextView tvGroupAccount;
    @BindView(R.id.tv_setting)
    TextView tvSetting;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tv_yinlian_shoukuan, R.id.tv_fuka_shoukuan, R.id.tv_saoyisao, R.id.tv_deal_detail, R.id.tv_search, R.id.tv_sales_return, R.id.tv_purchase_cancellation, R.id.tv_card_accredit, R.id.tv_print, R.id.tv_group_account,R.id.tv_setting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_yinlian_shoukuan:
                break;
            case R.id.tv_fuka_shoukuan:
                break;
            case R.id.tv_saoyisao:
                break;
            case R.id.tv_deal_detail:
                break;
            case R.id.tv_search:
                break;
            case R.id.tv_sales_return:
                break;
            case R.id.tv_purchase_cancellation:
                break;
            case R.id.tv_card_accredit:
                break;
            case R.id.tv_print:
                break;
            case R.id.tv_group_account:
                break;
            case R.id.tv_setting:
                Tool.startActivity(getActivity(),SettingActivity.class,true);

                break;
        }
    }
}
