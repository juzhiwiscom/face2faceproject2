package com.gooday123.shouyintai.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tianzhentao on 2018/5/12.
 */

public class Tool {
    // 防止按钮点击两次的方法
    private static long lastCilickTime;

    public synchronized static boolean isFastDouleClick() {
        long time = System.currentTimeMillis();
        if (time - lastCilickTime < 500) {
            lastCilickTime = time;
            return true;
        }
        lastCilickTime = time;
        return false;
    }

    public static void loge(String tag, String msg) {
        Log.i(tag, msg);

    }
    public static List<PackageInfo> getAllApps(Context context) {

        List<PackageInfo> apps = new ArrayList<PackageInfo>();
        PackageManager pManager = context.getPackageManager();
        // 获取手机内所有应用
        List<PackageInfo> packlist = pManager.getInstalledPackages(0);
        for (int i = 0; i < packlist.size(); i++) {
            PackageInfo pak = (PackageInfo) packlist.get(i);

            // 判断是否为非系统预装的应用程序
            // 这里还可以添加系统自带的，这里就先不添加了，如果有需要可以自己添加
            // if()里的值如果<=0则为自己装的程序，否则为系统工程自带
            if ((pak.applicationInfo.flags & pak.applicationInfo.FLAG_SYSTEM) <= 0) {
                // 添加自己已经安装的应用程序
                apps.add(pak);
            }

        }
        return apps;
    }

    public static void startActivity(Activity activity, Class<? extends Activity> clz , boolean isFinish) {
        Intent intent = new Intent(activity,clz);
        if(isFinish)
            activity.finish();
        activity.startActivity(intent);

    }

    public static ArrayList getWhiteListApps() {
        ArrayList<String> whiteListApps = new ArrayList<>();
        whiteListApps.add("com.gooday123.unionpay");
        whiteListApps.add("com.gooday123.yufu");
        whiteListApps.add("com.gooday123.shouyintai");
        return whiteListApps;

    }


}
