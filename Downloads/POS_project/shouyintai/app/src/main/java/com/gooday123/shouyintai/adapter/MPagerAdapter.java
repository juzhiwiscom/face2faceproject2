package com.gooday123.shouyintai.adapter;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.ArrayList;

public class MPagerAdapter extends PagerAdapter {

	private ArrayList<View> listView;

	public MPagerAdapter(ArrayList<View> listView) {

		this.listView = listView;

	}

	public void setList(ArrayList<View> listView) {

		this.listView = listView;
	}

	@Override
	public int getCount() {
		return listView.size();
	}

	@Override
	public void destroyItem(View container, int position, Object object) {
		((ViewPager) container).removeView(listView.get(position));
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return null;

	}

	@Override
	public Object instantiateItem(View container, int position) {
		((ViewPager) container).addView(listView.get(position));
		return listView.get(position);
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		// TODO Auto-generated method stub
		 return arg0 == arg1;
	}

}
