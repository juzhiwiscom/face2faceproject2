package com.gooday123.shouyintai;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gooday123.shouyintai.fragment.AppsFragment;
import com.gooday123.shouyintai.fragment.HomeFragment;
import com.gooday123.shouyintai.util.Tool;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final int HOME_FRAGMENT = 0;
    private static final int FIRST_FRAGMENT = 1;
    private static final int SECOND_FRAGMENT = 2;
    private static final String FROM_MODEL = "from_model";
    public static final String INDEPENDENT_MODEL = "independent_model";
    public static final String CASHIER_MODEL = "cashier_model";
    @BindView(R.id.mainTitleSettingImageView)
    ImageView mainTitleSettingImageView;
    @BindView(R.id.topMainRelativeLayout)
    RelativeLayout topMainRelativeLayout;
    @BindView(R.id.page0)
    ImageView page0;
    @BindView(R.id.page1)
    ImageView page1;
    @BindView(R.id.page2)
    ImageView page2;
    @BindView(R.id.page3)
    ImageView page3;
    @BindView(R.id.page4)
    ImageView page4;
    @BindView(R.id.page5)
    ImageView page5;
    @BindView(R.id.page6)
    ImageView page6;
    @BindView(R.id.page7)
    ImageView page7;
    @BindView(R.id.bottomMainLinearLayout)
    LinearLayout bottomMainLinearLayout;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;
    private MainAdapter mainAdapter;
    private MainActivity mContext;
    private PackageManager pManager;
    private int pageNum;
    private ArrayList<ImageView> dotView=new ArrayList<>();
    private String fromModel;

    public static void toMainActivity(Context context,String fromModel){

        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(FROM_MODEL,fromModel);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setStatusBar(Color.TRANSPARENT);
        mContext = MainActivity.this;
        Intent intent = getIntent();
        if(intent!=null){
             fromModel = intent.getStringExtra(FROM_MODEL);
        }
        init();
        listener();

    }


    private void init() {
        dotView.add(page0);
        dotView.add(page1);
        dotView.add(page2);
        dotView.add(page3);
        dotView.add(page4);
        dotView.add(page5);
        dotView.add(page6);
        dotView.add(page7);
        pManager = mContext.getPackageManager();
        List<PackageInfo> appList = Tool.getAllApps(mContext);
        if(!TextUtils.isEmpty(fromModel)&&fromModel.equals(INDEPENDENT_MODEL)){
            pageNum = (int)Math.ceil(((double)appList.size())/8);
            mainAdapter = new MainAdapter(getSupportFragmentManager(),pageNum,fromModel);
        }else{
            pageNum = (int)Math.ceil(((double)appList.size())/8)+1;
            mainAdapter = new MainAdapter(getSupportFragmentManager(),pageNum);
        }
        for (int i = 7; i > pageNum-1; i--) {
            dotView.get(i).setVisibility(View.GONE);
        }

        mViewPager.setAdapter(mainAdapter);
        mViewPager.setOffscreenPageLimit(pageNum);
        mViewPager.setCurrentItem(HOME_FRAGMENT);
    }

    private void listener() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < pageNum; i++) {
                    if(i==position){
                        dotView.get(i).setBackgroundResource(R.drawable.dot_red);
                    }else{
                        dotView.get(i).setBackgroundResource(R.drawable.dot_white);
                    }

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
    private static class MainAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments = new ArrayList<>();
        public MainAdapter(FragmentManager supportFragmentManager, int pageNum,String fromModel) {
            super(supportFragmentManager);
            for (int i = 0; i < pageNum; i++) {
                fragments.add(new AppsFragment(i,fromModel));
            }
        }
        public MainAdapter(FragmentManager supportFragmentManager, int pageNum) {
                super(supportFragmentManager);
                    fragments.add(new HomeFragment());
                    for (int i = 0; i < pageNum-1; i++) {
                        fragments.add(new AppsFragment(i));
                    }
                }


                @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
    /**
     * 设置状态栏背景色
     *
     * @param statusBarColorRes 5.0及以上系统可以设置状态栏背景色
     */
    protected void setStatusBar(int statusBarColorRes) {

        // 透明状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 5.0及以上
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            // 设置状态栏背景
            getWindow().setStatusBarColor(statusBarColorRes);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {// 4.4
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }
}
