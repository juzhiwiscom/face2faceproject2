package com.gooday123.shouyintai.mode;

import java.util.HashMap;

public class ConstantValue {
	// 超时时间
	public static final int TIME_OUT = 10000;
	/* mobilePlatform */
	/* 收款 */
    /**
     * 智能POS相关
     */
	
    /**手机银行主界面*/
    public static final String SPOS_BANK_CODE ="10";
    /**扫一扫*/
    public static final String SPOS_SAO_YI_SAO_CODE ="11";
    /**刷卡支付*/
    public static final String SPOS_SHU_KA_CODE ="12";
    /**交易明细*/
    public static final String SPOS_MING_XI_CODE ="13";
    /**账单*/
    public static final String SPOS_ZHANGDAN_CODE ="14";
    /**POS撤销*/
    public static final String SPOS_CHEXIAO_CODE ="15";
    /**POS退货*/
    public static final String SPOS_TUIHUO_CODE ="16";
    
    /**P结算*/
    public static final String SPOS_PJIESUAN_CODE ="17";
    
    /**更多*/
    public static final String SPOS_MORE_CODE ="18";
    
    
    
	/* 商户信息 */
	public static final int SMPOS_SHXX = 4;
	/* 店员管理 */
	public static final int SMPOS_DYGL = 5;
	/* 终端管理 */
	public static final int SMPOS_ZDGL = 6;
	/* 支付宝 */
	public static final int SMPOS_ZFBSK = 7;
	/* 微信 */
	public static final int SMPOS_WXSK = 8;
	/* 银联收款 */
	public static final int SMPOS_YLSK = 9;
	/* 用户信息 */
	public static final int SMPOS_USER = 10;
	/* 乐收银MAIN */
	public static final int SMPOS_LSY_MAIN = 11;
	/* 乐收银HELP */
	public static final int SMPOS_LSY_HELP = 12;

	/* mobilePlatform */
	public static final String APP_HOME_UATM = "https://111.205.207.103/mobilePlatform";

	/**
	 * uat: http://wxtest.cmbc.com.cn:8080/mobilePlatform/
	 * 
	 * 版本机 http://wxtest.cmbc.com.cn:8080/appverweb/ 开发环境
	 */

	public static final String APP_HOME_SIT = "https://111.205.207.103/appsitweb";

	/**
	 * UAT环境
	 */

	public static final String APP_HOME_UAT = "http://wxtest.cmbc.com.cn:8080/mobilePlatform";
	/**
	 * VER环境
	 */
	public static final String APP_HOME_VER = "https://111.205.207.103/appweb";

	/**
	 * 生产环境 https://epay.cmbc.com.cn/appweb/index.html
	 */
	//
	public static final String APP_HOME_PRO = "https://epay.cmbc.com.cn/appweb";// 主机地址

	/**
	 * APP免责列表
	 * 
	 */


	public static HashMap<String, String> getAPPDisclaimerList() {
		HashMap<String, String> APPDisclaimerList = new HashMap<String, String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{

				put("com.ruimin.xingtaimcht", "祥牛云商户");
				put("com.xtbank.pos.device", "智能POS");
//				put("com.landicorp.zacloud.appmarket", "应用市场");
//				put("com.landicorp.zacloud.settings", "设置");

			}

		};
		return APPDisclaimerList;
	};
	/*免责声明*/
	public static final String DISCLAIMER_STRING="该应用程序由第三方提供并由使用者自行安装及使用，邢台银行不对该应用提供任何形式的服务支持和承诺。";
	
	
}
