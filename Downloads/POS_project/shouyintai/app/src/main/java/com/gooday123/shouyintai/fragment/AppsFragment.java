package com.gooday123.shouyintai.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.gooday123.shouyintai.MainActivity;
import com.gooday123.shouyintai.R;
import com.gooday123.shouyintai.adapter.AppIconAdapter;
import com.gooday123.shouyintai.mode.AppsItemInfo;
import com.gooday123.shouyintai.util.Tool;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by tianzhentao on 2018/5/12.
 */

public class AppsFragment extends Fragment {
    @BindView(R.id.allAppGridviewPage)
    GridView allAppGridviewPage;
    Unbinder unbinder;
    private int page;
    private String fromModel;
    private List<AppsItemInfo> iconInfoList = new ArrayList<AppsItemInfo>();
    private int n;
    private boolean isWhiteApp=false;

    public AppsFragment(int page) {
        this.page=page;

    }

    public AppsFragment(int page, String fromModel) {
        this.page=page;
        this.fromModel=fromModel;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_apps, container, false);
        unbinder = ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        PackageManager pManager = getActivity().getPackageManager();
        List<PackageInfo> appList = Tool.getAllApps(getActivity());
        iconInfoList.clear();
        int startValue = page * 8;
        int endValue = startValue + 8;
        if(endValue>appList.size()){
            endValue=appList.size();
        }
        for (int i = 0; i < appList.size(); i++) {
            PackageInfo pinfo = appList.get(i);
//            判断是否是独立模式，除去白名单应用
            if(!TextUtils.isEmpty(fromModel)&&fromModel.equals(MainActivity.INDEPENDENT_MODEL)) {
                for (int j = 0; j < Tool.getWhiteListApps().size(); j++) {
                    String packageName = pinfo.applicationInfo.packageName;
                    String whiteListApp = Tool.getWhiteListApps().get(j).toString();
                    if (packageName.equals(whiteListApp)) {
                        isWhiteApp=true;
                        continue;
                    }
                }
                if(isWhiteApp){
                    isWhiteApp=false;
                    continue;
                }
            }
            AppsItemInfo shareItem = new AppsItemInfo();
            // 设置图片
            shareItem.setIcon(pManager
                    .getApplicationIcon(pinfo.applicationInfo));
            // 设置应用程序名字
            shareItem.setLabel(pManager.getApplicationLabel(
                    pinfo.applicationInfo).toString());
            // 设置应用程序的包名
            shareItem.setPackageName(pinfo.applicationInfo.packageName);
            Intent intent =  getActivity().getPackageManager()
                    .getLaunchIntentForPackage(
                            pinfo.applicationInfo.packageName);
            if (intent == null) {
                continue;
            }else {
                n++;
            }
            if (null != shareItem) {
                if(n>startValue&&n<endValue+1){
                    iconInfoList.add(shareItem);
                }
            }
        }
        allAppGridviewPage.setAdapter(new AppIconAdapter(iconInfoList, getActivity()));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        allAppGridviewPage.setAdapter(new AppIconAdapter(iconInfoList, getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
