package com.gooday123.shouyintai.mode;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

public class StateStorage {

	public static String getAppJSON(Context context) {
		SharedPreferences loginState = context.getSharedPreferences(
				"com.xtbank.pos.launcher", Context.MODE_PRIVATE);
		JSONObject jsonObj = new JSONObject();
		String AppJSONString = loginState.getString("AppMapString",
				jsonObj.toString());

		return AppJSONString;
	}

	public static void setAppJSON(Context context, String AppMapString) {
		SharedPreferences loginState = context.getSharedPreferences(
				"com.xtbank.pos.launcher", Context.MODE_PRIVATE);
		android.content.SharedPreferences.Editor editor = loginState.edit();

		editor.putString("AppMapString", AppMapString);
		editor.commit();
	}

	public static boolean isStateStorage(Context context, String AppPackage) {
		boolean flag = false;
		String AppJSONString = getAppJSON(context);
		try {
			JSONObject jasonObject = new JSONObject(AppJSONString);
			if (jasonObject.has(AppPackage)) {
				flag = jasonObject.getBoolean(AppPackage);
			} else {
				flag = false;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	
	public static void setStateStorage(Context context, String AppPackage, boolean isStateStorage) {
	
		String AppJSONString = getAppJSON(context);
		try {
			JSONObject jasonObject = new JSONObject(AppJSONString);
			if (jasonObject.has(AppPackage)) {
				jasonObject.remove(AppPackage);
				jasonObject.put(AppPackage, isStateStorage);
			} else {
				jasonObject.put(AppPackage, isStateStorage);
			}
			setAppJSON(context, jasonObject.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
