package com.gooday123.shouyintai.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gooday123.shouyintai.MainActivity;
import com.gooday123.shouyintai.R;
import com.gooday123.shouyintai.util.Tool;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by tianzhentao on 2018/5/13.
 */

public class SettingActivity extends AppCompatActivity {
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.ll_switch_modle)
    RelativeLayout llSwitchModle;
    @BindView(R.id.tv_version)
    TextView tvVersion;
    @BindView(R.id.tv_sn_code)
    TextView tvSnCode;
    @BindView(R.id.tv_commodity_code)
    TextView tvCommodityCode;
    @BindView(R.id.tv_terminal_number)
    TextView tvTerminalNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.iv_back, R.id.ll_switch_modle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                Tool.startActivity(SettingActivity.this,MainActivity.class,true);
                break;
            case R.id.ll_switch_modle:
                showModleDialog();
                break;
        }
    }
    private void showModleDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_select_modle,
                null);
        final Dialog dialog = new Dialog(this, R.style.transparentFrameWindowStyle);
        dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        Window window = dialog.getWindow();

        TextView independentModle = (TextView) view.findViewById(R.id.independent_modle);
        TextView cashierModle = (TextView) view.findViewById(R.id.cashier_modle);

        independentModle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                MainActivity.toMainActivity(SettingActivity.this,MainActivity.INDEPENDENT_MODEL);
                finish();
            }
        });
        cashierModle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Tool.startActivity(SettingActivity.this,MainActivity.class,true);
            }
        });


        // 设置显示动画
        window.setWindowAnimations(R.style.modle_menu_animstyle);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.x = 0;
        wl.y = getWindowManager().getDefaultDisplay().getHeight();
        // 以下这两句是为了保证按钮可以水平满屏
        wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
        wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        wl.dimAmount=0f;
        // 设置显示位置
        dialog.onWindowAttributesChanged(wl);
        // 设置点击外围解散
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }
}
