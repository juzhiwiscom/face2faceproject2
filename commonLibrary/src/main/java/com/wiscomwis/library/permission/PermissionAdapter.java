package com.wiscomwis.library.permission;

import android.content.Context;

import com.wiscomwis.library.R;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;

import java.util.List;

/**
 * 权限列表适配器
 * Created by zhangdroid on 2017/5/19.
 */
public class PermissionAdapter extends CommonRecyclerViewAdapter<PermissonItem> {

    public PermissionAdapter(Context context, int layoutResId, List<PermissonItem> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(PermissonItem permissonItem, int position, RecyclerViewHolder holder) {
        if (null != permissonItem) {
             holder.setText(R.id.permission_name, permissonItem.permissionName);
           // holder.setImageResource(R.id.permission_icon, permissonItem.permissionIconRes);
        }
    }

}
